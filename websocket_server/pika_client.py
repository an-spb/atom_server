import logging
import pika
import time
from pika.adapters.tornado_connection import TornadoConnection
pika.log = logging.getLogger(__name__)


class PikaClient(object):
    def __init__(self):
        self.connected = False
        self.connecting = False
        self.connection = None
        self.in_channel = None
        self.on_message_callback = lambda *args, **kwargs: None

    def connect(self, on_message_callback):
        if self.connecting:
            return
        self.connecting = True

        self.on_message_callback = on_message_callback

        cred = pika.PlainCredentials('guest', 'guest')
        param = pika.ConnectionParameters(
            host='127.0.0.1',
            port=5672,
            virtual_host='/',
            credentials=cred
        )

        self.connection = TornadoConnection(param,
                                            on_open_callback=self.on_connected)

    def on_connected(self, connection):
        print('RABBITMQ: connected')
        self.connected = True
        self.connection = connection
        self.in_channel = self.connection.channel(on_open_callback=self.on_conn_open)

    def on_conn_open(self, channel):
        print('RABBITMQ: channel opened')
        self.in_channel.exchange_declare(exchange='tornado_input')
        channel.queue_declare(callback=self.on_input_queue_declare, queue='in_queue')

    def on_input_queue_declare(self, queue):
        self.in_channel.queue_bind(callback=None,
                                   exchange='tornado_input',
                                   queue='in_queue',
                                   routing_key="#")
        self.in_channel.basic_consume(queue='in_queue', on_message_callback=self.on_message)

    def on_message(self, channel, method, header, body):
        self.on_message_callback(body)
        channel.basic_ack(delivery_tag=method.delivery_tag)
