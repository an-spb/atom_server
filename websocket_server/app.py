import json

import tornado
from tornado import websocket, web, ioloop
import pika
import pprint

from pika_client import PikaClient


class EchoWebSocket(websocket.WebSocketHandler):
    clients_by_session = {}
    clients_by_instance = {}

    # Websocket opened
    def open(self):
        print('opened')
    # Websocket closed
    def on_close(self):
        if hasattr(self, 'session'): 
            EchoWebSocket.clients_by_session[self.session].remove(self)
        if hasattr(self, 'instance_id'): 
            EchoWebSocket.clients_by_instance[self.instance_id].remove(self)
        print('closed')
    # On websocket message
    def on_message(self, message):
        try:
            message = json.loads(message)
        except ValueError:
            self.write_message({'invalid_json': True})
            print('INVALID JSON')
            return
        if 'instance_id' in message:
            self.instance_id = message['instance_id']
            clients = EchoWebSocket.clients_by_instance
            if self.instance_id not in clients:
                clients[self.instance_id] = set()
            clients[self.instance_id].add(self)
            self.write_message('{"instance_id_received": true}')
            print('instance id', self.instance_id)  
        elif 'websocket_session' in message:
            self.session = message['websocket_session']
            clients = EchoWebSocket.clients_by_session
            if self.session not in clients:
                clients[self.session] = set()
            clients[self.session].add(self)
            self.write_message('{"session_received": true}')
            print('session key', self.session)  
        else:        
            self.write_message(message)
            print(message)
    
    # On Redis message
    @classmethod
    @tornado.gen.engine
    def on_rabbitmq_message(cls, message):
        message = json.loads(message.decode())
        pprint.pprint(message)
        clients = []
        for_session = message.get('session', None)        
        for_instance_id = message.get('instance_id', None)        
        if for_session:
            clients = EchoWebSocket.clients_by_session.get(for_session, [])
        if for_instance_id:
            clients = EchoWebSocket.clients_by_instance.get(for_instance_id, [])
        if len(clients) > 0:
            client_message = json.dumps(message['data'])
            for client in clients:
                client.write_message(client_message)

    def check_origin(self, origin):
        return True


if __name__ == '__main__':
    PikaClient().connect(EchoWebSocket.on_rabbitmq_message)
    web.Application([(r'/wsws/', EchoWebSocket)]).listen(8181)
    ioloop.IOLoop.instance().start()
    
