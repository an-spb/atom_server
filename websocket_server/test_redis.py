import time

import pika


connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

channel.basic_publish(exchange='tornado_input',
                      routing_key='#',
                      body='{"session": "qqqq", "data": [1, 2, 3]}')
print(" [x] Sent 'Hello World!'")


