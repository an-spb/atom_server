module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    browserify: {
      dist: {
        files: {
          // destination for transpiled js : source js
          'static/web/admin.js': 'src/web/static/web/admin.js'
        },
        options: {
          transform: [['babelify', { presets: ["es2015"] }]],
          browserifyOptions: {
            debug: true
          }
        }
      }
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-browserify');

  grunt.registerTask('default', [
    // 'jshint',
    //'concat', <-- Probably don't need to concat the files, assuming index.es6 imports the necessary modules.
    'browserify:dist',
    // 'uglify'
  ]);
};