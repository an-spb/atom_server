--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.14
-- Dumped by pg_dump version 9.5.14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO impulse;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO impulse;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO impulse;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO impulse;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO impulse;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO impulse;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO impulse;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO impulse;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO impulse;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO impulse;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO impulse;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO impulse;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: chat_message; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.chat_message (
    id integer NOT NULL,
    text text NOT NULL,
    created_at timestamp with time zone,
    is_alive boolean NOT NULL,
    updated_at timestamp with time zone,
    is_read boolean NOT NULL,
    is_received boolean NOT NULL,
    is_human_made boolean NOT NULL,
    participant_id integer NOT NULL,
    tmp_id character varying(32)
);


ALTER TABLE public.chat_message OWNER TO impulse;

--
-- Name: chat_message_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.chat_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chat_message_id_seq OWNER TO impulse;

--
-- Name: chat_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.chat_message_id_seq OWNED BY public.chat_message.id;


--
-- Name: chat_room; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.chat_room (
    id integer NOT NULL,
    created_at timestamp with time zone,
    is_alive boolean NOT NULL,
    updated_at timestamp with time zone
);


ALTER TABLE public.chat_room OWNER TO impulse;

--
-- Name: chat_room_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.chat_room_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chat_room_id_seq OWNER TO impulse;

--
-- Name: chat_room_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.chat_room_id_seq OWNED BY public.chat_room.id;


--
-- Name: chat_roomparticipant; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.chat_roomparticipant (
    id integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    is_alive boolean NOT NULL,
    room_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.chat_roomparticipant OWNER TO impulse;

--
-- Name: chat_roomparticipant_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.chat_roomparticipant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chat_roomparticipant_id_seq OWNER TO impulse;

--
-- Name: chat_roomparticipant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.chat_roomparticipant_id_seq OWNED BY public.chat_roomparticipant.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO impulse;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO impulse;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO impulse;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO impulse;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO impulse;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO impulse;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO impulse;

--
-- Name: faq_faqgroup; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.faq_faqgroup (
    id integer NOT NULL,
    header character varying(255) NOT NULL,
    not_display_name boolean NOT NULL,
    instance_id integer NOT NULL,
    created_at timestamp with time zone,
    is_alive boolean NOT NULL,
    "order" integer NOT NULL,
    updated_at timestamp with time zone
);


ALTER TABLE public.faq_faqgroup OWNER TO impulse;

--
-- Name: faq_faqgroup_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.faq_faqgroup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.faq_faqgroup_id_seq OWNER TO impulse;

--
-- Name: faq_faqgroup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.faq_faqgroup_id_seq OWNED BY public.faq_faqgroup.id;


--
-- Name: faq_question; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.faq_question (
    id integer NOT NULL,
    question character varying(255) NOT NULL,
    answer text NOT NULL,
    is_opened boolean NOT NULL,
    group_id integer NOT NULL,
    "order" integer NOT NULL
);


ALTER TABLE public.faq_question OWNER TO impulse;

--
-- Name: faq_question_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.faq_question_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.faq_question_id_seq OWNER TO impulse;

--
-- Name: faq_question_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.faq_question_id_seq OWNED BY public.faq_question.id;


--
-- Name: feed_item; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.feed_item (
    id integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    is_alive boolean NOT NULL,
    title character varying(255) NOT NULL,
    text text NOT NULL,
    published_at timestamp with time zone NOT NULL,
    video_file character varying(100),
    have_order_button boolean NOT NULL,
    starts_from_space boolean NOT NULL,
    page_id integer NOT NULL
);


ALTER TABLE public.feed_item OWNER TO impulse;

--
-- Name: feed_item_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.feed_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.feed_item_id_seq OWNER TO impulse;

--
-- Name: feed_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.feed_item_id_seq OWNED BY public.feed_item.id;


--
-- Name: feed_itemimage; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.feed_itemimage (
    id integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    is_alive boolean NOT NULL,
    image_file character varying(100) NOT NULL,
    item_id integer NOT NULL
);


ALTER TABLE public.feed_itemimage OWNER TO impulse;

--
-- Name: feed_itemimage_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.feed_itemimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.feed_itemimage_id_seq OWNER TO impulse;

--
-- Name: feed_itemimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.feed_itemimage_id_seq OWNED BY public.feed_itemimage.id;


--
-- Name: goods_additionalgoods; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.goods_additionalgoods (
    id integer NOT NULL,
    "order" integer NOT NULL,
    goods_add_id integer NOT NULL,
    goods_to_id integer NOT NULL
);


ALTER TABLE public.goods_additionalgoods OWNER TO impulse;

--
-- Name: goods_additionalgoods_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.goods_additionalgoods_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.goods_additionalgoods_id_seq OWNER TO impulse;

--
-- Name: goods_additionalgoods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.goods_additionalgoods_id_seq OWNED BY public.goods_additionalgoods.id;


--
-- Name: goods_address; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.goods_address (
    id integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    is_alive boolean NOT NULL,
    city character varying(255) NOT NULL,
    street character varying(255) NOT NULL,
    house character varying(255) NOT NULL,
    housing character varying(255) NOT NULL,
    flat character varying(255) NOT NULL,
    intercome_code character varying(255) NOT NULL,
    user_id integer NOT NULL,
    floor character varying(255) NOT NULL
);


ALTER TABLE public.goods_address OWNER TO impulse;

--
-- Name: goods_address_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.goods_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.goods_address_id_seq OWNER TO impulse;

--
-- Name: goods_address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.goods_address_id_seq OWNED BY public.goods_address.id;


--
-- Name: goods_descriptionline; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.goods_descriptionline (
    id integer NOT NULL,
    line_type integer NOT NULL,
    text text NOT NULL,
    goods_id integer NOT NULL,
    "order" integer NOT NULL
);


ALTER TABLE public.goods_descriptionline OWNER TO impulse;

--
-- Name: goods_descriptionline_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.goods_descriptionline_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.goods_descriptionline_id_seq OWNER TO impulse;

--
-- Name: goods_descriptionline_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.goods_descriptionline_id_seq OWNED BY public.goods_descriptionline.id;


--
-- Name: goods_displacegoods; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.goods_displacegoods (
    id integer NOT NULL,
    displace_by_id integer NOT NULL,
    goods_id integer NOT NULL
);


ALTER TABLE public.goods_displacegoods OWNER TO impulse;

--
-- Name: goods_displacegoods_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.goods_displacegoods_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.goods_displacegoods_id_seq OWNER TO impulse;

--
-- Name: goods_displacegoods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.goods_displacegoods_id_seq OWNED BY public.goods_displacegoods.id;


--
-- Name: goods_goods; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.goods_goods (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    image character varying(100),
    description_line_1 character varying(255),
    "order" integer NOT NULL,
    instance_id integer NOT NULL,
    force_to_basket boolean NOT NULL,
    parent_id integer,
    details_button_text character varying(255),
    not_display_in_catalog boolean NOT NULL,
    created_at timestamp with time zone,
    is_alive boolean NOT NULL,
    updated_at timestamp with time zone,
    level integer NOT NULL,
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    item_type integer NOT NULL,
    price_formula character varying(255),
    image_content text,
    description_line_2 character varying(255),
    CONSTRAINT goods_goods_level_check CHECK ((level >= 0)),
    CONSTRAINT goods_goods_lft_check CHECK ((lft >= 0)),
    CONSTRAINT goods_goods_rght_check CHECK ((rght >= 0)),
    CONSTRAINT goods_goods_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.goods_goods OWNER TO impulse;

--
-- Name: goods_goods_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.goods_goods_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.goods_goods_id_seq OWNER TO impulse;

--
-- Name: goods_goods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.goods_goods_id_seq OWNED BY public.goods_goods.id;


--
-- Name: goods_goodsimage; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.goods_goodsimage (
    id integer NOT NULL,
    image character varying(100),
    "order" integer NOT NULL,
    goods_id integer NOT NULL
);


ALTER TABLE public.goods_goodsimage OWNER TO impulse;

--
-- Name: goods_goodsimage_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.goods_goodsimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.goods_goodsimage_id_seq OWNER TO impulse;

--
-- Name: goods_goodsimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.goods_goodsimage_id_seq OWNED BY public.goods_goodsimage.id;


--
-- Name: goods_goodsparam; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.goods_goodsparam (
    id integer NOT NULL,
    name character varying(255),
    unit character varying(255),
    min integer,
    max integer,
    step integer,
    "default" integer,
    goods_id integer NOT NULL,
    formula_letter character varying(1),
    "order" integer NOT NULL
);


ALTER TABLE public.goods_goodsparam OWNER TO impulse;

--
-- Name: goods_goodsparam_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.goods_goodsparam_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.goods_goodsparam_id_seq OWNER TO impulse;

--
-- Name: goods_goodsparam_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.goods_goodsparam_id_seq OWNED BY public.goods_goodsparam.id;


--
-- Name: goods_order; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.goods_order (
    id integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    is_alive boolean NOT NULL,
    instance_id integer NOT NULL,
    address_id integer,
    user_id integer NOT NULL,
    tmp_id character varying(32),
    datetime timestamp with time zone,
    confirmation_status integer NOT NULL,
    ind integer NOT NULL
);


ALTER TABLE public.goods_order OWNER TO impulse;

--
-- Name: goods_order_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.goods_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.goods_order_id_seq OWNER TO impulse;

--
-- Name: goods_order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.goods_order_id_seq OWNED BY public.goods_order.id;


--
-- Name: goods_orderitem; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.goods_orderitem (
    id integer NOT NULL,
    order_id integer NOT NULL,
    goods_id integer NOT NULL,
    order_field integer NOT NULL,
    goods_dump jsonb NOT NULL,
    is_added_by_user boolean NOT NULL
);


ALTER TABLE public.goods_orderitem OWNER TO impulse;

--
-- Name: goods_orderitem_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.goods_orderitem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.goods_orderitem_id_seq OWNER TO impulse;

--
-- Name: goods_orderitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.goods_orderitem_id_seq OWNED BY public.goods_orderitem.id;


--
-- Name: goods_orderitemparamvalue; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.goods_orderitemparamvalue (
    id integer NOT NULL,
    float_value double precision NOT NULL,
    order_item_id integer NOT NULL,
    param_id integer NOT NULL
);


ALTER TABLE public.goods_orderitemparamvalue OWNER TO impulse;

--
-- Name: goods_orderitemparamvalue_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.goods_orderitemparamvalue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.goods_orderitemparamvalue_id_seq OWNER TO impulse;

--
-- Name: goods_orderitemparamvalue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.goods_orderitemparamvalue_id_seq OWNED BY public.goods_orderitemparamvalue.id;


--
-- Name: instances_colorscheme; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.instances_colorscheme (
    id integer NOT NULL,
    system_name character varying(255) NOT NULL,
    verbose_name character varying(255),
    created_at timestamp with time zone,
    is_alive boolean NOT NULL,
    updated_at timestamp with time zone,
    base_accent character varying(6),
    base_additional character varying(6),
    button_bg character varying(6),
    button_color character varying(6),
    base_color character varying(6),
    base_bg character varying(6),
    menu_bg character varying(6),
    menu_accent character varying(6),
    menu_color character varying(6),
    base_border character varying(6),
    menu_border character varying(6),
    button_border character varying(6),
    button_disabled_bg character varying(6),
    button_disabled_border character varying(6),
    button_disabled_color character varying(6),
    button_error_bg character varying(6),
    button_error_border character varying(6),
    button_error_color character varying(6),
    button_error_border_color character varying(6)
);


ALTER TABLE public.instances_colorscheme OWNER TO impulse;

--
-- Name: instances_colorscheme_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.instances_colorscheme_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.instances_colorscheme_id_seq OWNER TO impulse;

--
-- Name: instances_colorscheme_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.instances_colorscheme_id_seq OWNED BY public.instances_colorscheme.id;


--
-- Name: instances_instance; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.instances_instance (
    id integer NOT NULL,
    name character varying(255),
    created_at timestamp with time zone,
    is_alive boolean NOT NULL,
    updated_at timestamp with time zone,
    admin_id integer
);


ALTER TABLE public.instances_instance OWNER TO impulse;

--
-- Name: instances_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.instances_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.instances_instance_id_seq OWNER TO impulse;

--
-- Name: instances_instance_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.instances_instance_id_seq OWNED BY public.instances_instance.id;


--
-- Name: instances_instancepage; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.instances_instancepage (
    id integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    is_alive boolean NOT NULL,
    "order" integer NOT NULL,
    instance_id integer NOT NULL,
    page_class_id integer NOT NULL
);


ALTER TABLE public.instances_instancepage OWNER TO impulse;

--
-- Name: instances_instancesettings; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.instances_instancesettings (
    id integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    is_alive boolean NOT NULL,
    logo character varying(100),
    color_scheme_id integer,
    instance_id integer,
    company_name character varying(255),
    open_logo_id integer
);


ALTER TABLE public.instances_instancesettings OWNER TO impulse;

--
-- Name: instances_instancesettings_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.instances_instancesettings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.instances_instancesettings_id_seq OWNER TO impulse;

--
-- Name: instances_instancesettings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.instances_instancesettings_id_seq OWNED BY public.instances_instancesettings.id;


--
-- Name: instances_openlogo; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.instances_openlogo (
    id integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    is_alive boolean NOT NULL,
    name character varying(255),
    logo character varying(100)
);


ALTER TABLE public.instances_openlogo OWNER TO impulse;

--
-- Name: instances_openlogo_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.instances_openlogo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.instances_openlogo_id_seq OWNER TO impulse;

--
-- Name: instances_openlogo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.instances_openlogo_id_seq OWNED BY public.instances_openlogo.id;


--
-- Name: instances_page_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.instances_page_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.instances_page_id_seq OWNER TO impulse;

--
-- Name: instances_page_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.instances_page_id_seq OWNED BY public.instances_instancepage.id;


--
-- Name: instances_pageclass; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.instances_pageclass (
    id integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    is_alive boolean NOT NULL,
    system_name character varying(255) NOT NULL,
    verbose_name character varying(255) NOT NULL,
    "order" integer NOT NULL
);


ALTER TABLE public.instances_pageclass OWNER TO impulse;

--
-- Name: instances_pageclass_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.instances_pageclass_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.instances_pageclass_id_seq OWNER TO impulse;

--
-- Name: instances_pageclass_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.instances_pageclass_id_seq OWNED BY public.instances_pageclass.id;


--
-- Name: reset_resetrequest; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.reset_resetrequest (
    id integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    is_alive boolean NOT NULL,
    instance_id integer
);


ALTER TABLE public.reset_resetrequest OWNER TO impulse;

--
-- Name: reset_reset_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.reset_reset_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reset_reset_id_seq OWNER TO impulse;

--
-- Name: reset_reset_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.reset_reset_id_seq OWNED BY public.reset_resetrequest.id;


--
-- Name: sms_sms; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.sms_sms (
    id integer NOT NULL,
    text text NOT NULL,
    is_delivered boolean NOT NULL,
    is_error boolean NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp with time zone,
    is_alive boolean NOT NULL,
    status_code integer,
    updated_at timestamp with time zone,
    message_type character varying(30) NOT NULL,
    provider_sms_id character varying(20),
    attempt_number integer NOT NULL,
    status_attempt_number integer NOT NULL
);


ALTER TABLE public.sms_sms OWNER TO impulse;

--
-- Name: sms_sms_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.sms_sms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sms_sms_id_seq OWNER TO impulse;

--
-- Name: sms_sms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.sms_sms_id_seq OWNED BY public.sms_sms.id;


--
-- Name: users_profile; Type: TABLE; Schema: public; Owner: impulse
--

CREATE TABLE public.users_profile (
    id integer NOT NULL,
    phone_number character varying(20) NOT NULL,
    user_id integer NOT NULL,
    websocket_session uuid NOT NULL,
    instance_id integer,
    is_phone_verified boolean NOT NULL
);


ALTER TABLE public.users_profile OWNER TO impulse;

--
-- Name: users_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: impulse
--

CREATE SEQUENCE public.users_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_profile_id_seq OWNER TO impulse;

--
-- Name: users_profile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: impulse
--

ALTER SEQUENCE public.users_profile_id_seq OWNED BY public.users_profile.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.chat_message ALTER COLUMN id SET DEFAULT nextval('public.chat_message_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.chat_room ALTER COLUMN id SET DEFAULT nextval('public.chat_room_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.chat_roomparticipant ALTER COLUMN id SET DEFAULT nextval('public.chat_roomparticipant_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.faq_faqgroup ALTER COLUMN id SET DEFAULT nextval('public.faq_faqgroup_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.faq_question ALTER COLUMN id SET DEFAULT nextval('public.faq_question_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.feed_item ALTER COLUMN id SET DEFAULT nextval('public.feed_item_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.feed_itemimage ALTER COLUMN id SET DEFAULT nextval('public.feed_itemimage_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_additionalgoods ALTER COLUMN id SET DEFAULT nextval('public.goods_additionalgoods_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_address ALTER COLUMN id SET DEFAULT nextval('public.goods_address_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_descriptionline ALTER COLUMN id SET DEFAULT nextval('public.goods_descriptionline_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_displacegoods ALTER COLUMN id SET DEFAULT nextval('public.goods_displacegoods_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_goods ALTER COLUMN id SET DEFAULT nextval('public.goods_goods_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_goodsimage ALTER COLUMN id SET DEFAULT nextval('public.goods_goodsimage_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_goodsparam ALTER COLUMN id SET DEFAULT nextval('public.goods_goodsparam_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_order ALTER COLUMN id SET DEFAULT nextval('public.goods_order_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_orderitem ALTER COLUMN id SET DEFAULT nextval('public.goods_orderitem_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_orderitemparamvalue ALTER COLUMN id SET DEFAULT nextval('public.goods_orderitemparamvalue_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.instances_colorscheme ALTER COLUMN id SET DEFAULT nextval('public.instances_colorscheme_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.instances_instance ALTER COLUMN id SET DEFAULT nextval('public.instances_instance_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.instances_instancepage ALTER COLUMN id SET DEFAULT nextval('public.instances_page_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.instances_instancesettings ALTER COLUMN id SET DEFAULT nextval('public.instances_instancesettings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.instances_openlogo ALTER COLUMN id SET DEFAULT nextval('public.instances_openlogo_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.instances_pageclass ALTER COLUMN id SET DEFAULT nextval('public.instances_pageclass_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.reset_resetrequest ALTER COLUMN id SET DEFAULT nextval('public.reset_reset_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.sms_sms ALTER COLUMN id SET DEFAULT nextval('public.sms_sms_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.users_profile ALTER COLUMN id SET DEFAULT nextval('public.users_profile_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.auth_group (id, name) FROM stdin;
1	instance admin
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, true);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
49	1	25
50	1	26
51	1	27
52	1	64
53	1	65
54	1	66
55	1	67
56	1	68
57	1	69
58	1	70
59	1	71
60	1	72
61	1	73
62	1	74
63	1	75
64	1	79
65	1	80
66	1	81
67	1	82
68	1	83
69	1	84
70	1	85
71	1	86
72	1	87
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 72, true);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add user	2	add_user
5	Can change user	2	change_user
6	Can delete user	2	delete_user
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add permission	4	add_permission
11	Can change permission	4	change_permission
12	Can delete permission	4	delete_permission
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add instance settings	7	add_instancesettings
20	Can change instance settings	7	change_instancesettings
21	Can delete instance settings	7	delete_instancesettings
22	Can add page class	8	add_pageclass
23	Can change page class	8	change_pageclass
24	Can delete page class	8	delete_pageclass
25	Can add instance page	9	add_instancepage
26	Can change instance page	9	change_instancepage
27	Can delete instance page	9	delete_instancepage
28	Can add instance	10	add_instance
29	Can change instance	10	change_instance
30	Can delete instance	10	delete_instance
31	Can add open logo	11	add_openlogo
32	Can change open logo	11	change_openlogo
33	Can delete open logo	11	delete_openlogo
34	Can add color scheme	12	add_colorscheme
35	Can change color scheme	12	change_colorscheme
36	Can delete color scheme	12	delete_colorscheme
37	Can add profile	13	add_profile
38	Can change profile	13	change_profile
39	Can delete profile	13	delete_profile
40	Can add message	14	add_message
41	Can change message	14	change_message
42	Can delete message	14	delete_message
43	Can add room	15	add_room
44	Can change room	15	change_room
45	Can delete room	15	delete_room
46	Can add room participant	16	add_roomparticipant
47	Can change room participant	16	change_roomparticipant
48	Can delete room participant	16	delete_roomparticipant
49	Can add item	17	add_item
50	Can change item	17	change_item
51	Can delete item	17	delete_item
52	Can add item image	18	add_itemimage
53	Can change item image	18	change_itemimage
54	Can delete item image	18	delete_itemimage
55	Can add feed item	17	add_feeditem
56	Can change feed item	17	change_feeditem
57	Can delete feed item	17	delete_feeditem
58	Can add discount item	17	add_discountitem
59	Can change discount item	17	change_discountitem
60	Can delete discount item	17	delete_discountitem
61	Can add order item	21	add_orderitem
62	Can change order item	21	change_orderitem
63	Can delete order item	21	delete_orderitem
64	Can add order item param value	22	add_orderitemparamvalue
65	Can change order item param value	22	change_orderitemparamvalue
66	Can delete order item param value	22	delete_orderitemparamvalue
67	Can add goods	23	add_goods
68	Can change goods	23	change_goods
69	Can delete goods	23	delete_goods
70	Can add description line	24	add_descriptionline
71	Can change description line	24	change_descriptionline
72	Can delete description line	24	delete_descriptionline
73	Can add goods param	25	add_goodsparam
74	Can change goods param	25	change_goodsparam
75	Can delete goods param	25	delete_goodsparam
76	Can add order	26	add_order
77	Can change order	26	change_order
78	Can delete order	26	delete_order
79	Can add displace goods	27	add_displacegoods
80	Can change displace goods	27	change_displacegoods
81	Can delete displace goods	27	delete_displacegoods
82	Can add additional goods	28	add_additionalgoods
83	Can change additional goods	28	change_additionalgoods
84	Can delete additional goods	28	delete_additionalgoods
85	Can add address	29	add_address
86	Can change address	29	change_address
87	Can delete address	29	delete_address
88	Can add reset request	30	add_resetrequest
89	Can change reset request	30	change_resetrequest
90	Can delete reset request	30	delete_resetrequest
91	Can add sms	31	add_sms
92	Can change sms	31	change_sms
93	Can delete sms	31	delete_sms
94	Can add goods image	32	add_goodsimage
95	Can change goods image	32	change_goodsimage
96	Can delete goods image	32	delete_goodsimage
97	Can add question	33	add_question
98	Can change question	33	change_question
99	Can delete question	33	delete_question
100	Can add faq group	34	add_faqgroup
101	Can change faq group	34	change_faqgroup
102	Can delete faq group	34	delete_faqgroup
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 102, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
20		2019-03-03 14:11:43.545969+03	f	app2__79219765859				f	t	2019-02-13 19:48:23+03
2	pbkdf2_sha256$100000$KlNH9EcWJRov$/xAbhpiRmmJt6HkK3Fx4fFPpp6MOa/ocCNz6/hvronQ=	2018-11-09 23:11:15+03	f	andrey	Андрей			t	t	2018-10-22 15:22:16+03
1	pbkdf2_sha256$100000$iiXYQIL1xDM6$E0v1i5pYukxPvzPz0a+gk/eKvoF1dwTy+HJawskl7Ps=	2019-02-10 16:06:25.977878+03	t	epq			no@email.now	t	t	2018-10-22 15:20:00+03
22		\N	f	app2__79219765853				f	t	2019-02-14 21:02:15.255858+03
23		\N	f	app2__79219765851				f	t	2019-02-14 21:03:06.235833+03
24		\N	f	app2__79219765863				f	t	2019-02-14 21:13:16.140042+03
25		2019-02-14 21:13:33.716899+03	f	app2__79219765864				f	t	2019-02-14 21:13:19.881999+03
26		\N	f	app2__79219765811				f	t	2019-02-14 21:13:51.905655+03
19		2019-02-14 21:28:53.280965+03	f	app2__79999999999				f	t	2018-12-09 20:56:20.088567+03
21		2019-02-14 23:55:06.800107+03	f	app2__79219765856				f	t	2019-02-14 21:02:09.953904+03
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
4	2	1
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 4, true);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 26, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: chat_message; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.chat_message (id, text, created_at, is_alive, updated_at, is_read, is_received, is_human_made, participant_id, tmp_id) FROM stdin;
\.


--
-- Name: chat_message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.chat_message_id_seq', 1, false);


--
-- Data for Name: chat_room; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.chat_room (id, created_at, is_alive, updated_at) FROM stdin;
\.


--
-- Name: chat_room_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.chat_room_id_seq', 1, false);


--
-- Data for Name: chat_roomparticipant; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.chat_roomparticipant (id, created_at, updated_at, is_alive, room_id, user_id) FROM stdin;
\.


--
-- Name: chat_roomparticipant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.chat_roomparticipant_id_seq', 1, false);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2018-12-08 04:46:18.152466+03	2	andrey	1	[{"added": {}}]	2	1
2	2018-12-08 04:46:23.422253+03	2	andrey	2	[]	2	1
3	2018-12-08 06:44:44.416356+03	7	79999999999 (Пропрр)	2	[{"changed": {"fields": ["instance"], "object": "Profile object (7)", "name": "profile"}}]	2	1
4	2018-12-09 20:25:59.222773+03	10	79119611648 (CleanMan )	3		2	1
5	2018-12-09 20:25:59.302284+03	6	79215555555	3		2	1
6	2018-12-09 20:25:59.351899+03	4	79219765859 (Gg)	3		2	1
7	2018-12-09 20:25:59.360158+03	3	+79219765859 (Ттт)	3		2	1
8	2018-12-09 20:25:59.368368+03	5	79995555555 (Роо)	3		2	1
9	2018-12-09 20:25:59.376632+03	8	79999995363 (Ргди)	3		2	1
10	2018-12-09 20:25:59.385129+03	9	79999999993 (Порпп)	3		2	1
11	2018-12-09 20:25:59.39339+03	7	79999999999 (Пропрр)	3		2	1
12	2018-12-09 20:25:59.401701+03	13	app2_79999999998	3		2	1
13	2018-12-09 20:25:59.409974+03	11	app2_79999999999	3		2	1
14	2018-12-09 20:25:59.418306+03	12	app2__79999999999	3		2	1
15	2018-12-09 20:27:21.144582+03	14	app2_79999999999	3		2	1
16	2018-12-09 20:50:21.578524+03	1	epq	2	[{"changed": {"object": "Profile object (2)", "name": "profile", "fields": ["phone_number", "instance"]}}]	2	1
17	2018-12-09 20:50:31.076099+03	2	andrey (Андрей)	2	[{"changed": {"object": "Profile object (3)", "name": "profile", "fields": ["phone_number", "instance"]}}]	2	1
18	2018-12-09 20:50:48.574059+03	15	app2_79999999999 (Роо)	3		2	1
19	2018-12-09 20:51:32.745367+03	16	sdasdasd	1	[{"added": {}}]	2	1
20	2018-12-09 20:51:39.569862+03	16	sdasdasd	2	[]	2	1
21	2018-12-09 20:54:20.541556+03	17	andasd	1	[{"added": {}}]	2	1
22	2018-12-09 20:54:25.575274+03	17	andasd	2	[]	2	1
23	2018-12-09 20:55:53.706639+03	17	andasd	3		2	1
24	2018-12-09 20:55:53.730935+03	16	sdasdasd	3		2	1
25	2018-12-09 20:56:04.963104+03	18	app2_79999999999	3		2	1
26	2018-12-09 23:00:56.003782+03	1	ResetRequest object (1)	1	[{"added": {}}]	30	1
27	2018-12-09 23:01:22.617438+03	2	ResetRequest object (2)	1	[{"added": {}}]	30	1
28	2018-12-28 19:04:09.777422+03	3	ResetRequest object (3)	1	[{"added": {}}]	30	1
29	2018-12-28 19:06:09.634991+03	4	ResetRequest object (4)	1	[{"added": {}}]	30	1
30	2018-12-28 19:06:33.621076+03	5	ResetRequest object (5)	1	[{"added": {}}]	30	1
31	2018-12-30 19:55:07.21467+03	6	ResetRequest object (6)	1	[{"added": {}}]	30	1
32	2018-12-30 19:59:32.817438+03	7	ResetRequest object (7)	1	[{"added": {}}]	30	1
33	2019-01-03 22:04:56.096059+03	8	ResetRequest object (8)	1	[{"added": {}}]	30	1
34	2019-01-03 22:07:05.820694+03	9	ResetRequest object (9)	1	[{"added": {}}]	30	1
35	2019-01-05 10:13:44.276465+03	69	ResetRequest object (69)	1	[{"added": {}}]	30	1
36	2019-01-07 12:41:04.52721+03	80	ResetRequest object (80)	1	[{"added": {}}]	30	1
37	2019-01-07 13:12:59.823715+03	81	ResetRequest object (81)	1	[{"added": {}}]	30	1
38	2019-01-07 13:13:16.567681+03	82	ResetRequest object (82)	1	[{"added": {}}]	30	1
39	2019-01-07 13:13:32.873323+03	83	ResetRequest object (83)	1	[{"added": {}}]	30	1
40	2019-01-07 13:24:02.417934+03	1308	Order object (1308)	2	[{"changed": {"fields": ["is_alive"]}}]	26	1
41	2019-01-07 13:44:01.156714+03	1308	Order object (1308)	2	[]	26	1
42	2019-01-07 13:44:14.249991+03	1308	Order object (1308)	2	[]	26	1
43	2019-01-07 15:01:17.610648+03	1309	Order object (1309)	2	[]	26	1
44	2019-01-07 15:03:54.396556+03	84	ResetRequest object (84)	1	[{"added": {}}]	30	1
45	2019-01-22 01:32:41.284645+03	2002	Order object (2002)	1	[{"added": {}}]	26	1
46	2019-01-22 01:43:20.472115+03	2037	Order object (2037)	1	[{"added": {}}]	26	1
47	2019-01-22 01:44:04.104332+03	2039	Order object (2039)	1	[{"added": {}}]	26	1
48	2019-01-22 01:45:44.086265+03	2039	Order object (2039)	2	[{"changed": {"fields": ["address"]}}]	26	1
49	2019-01-22 01:46:49.038819+03	2040	Order object (2040)	1	[{"added": {}}]	26	1
50	2019-01-22 01:46:58.583336+03	2039	Order object (2039)	2	[]	26	1
51	2019-01-22 01:48:01.813177+03	2039	1004	2	[]	26	1
52	2019-01-22 01:48:14.70297+03	2040	1005	2	[]	26	1
53	2019-01-22 02:01:08.317402+03	2042	1023	2	[]	26	1
54	2019-01-22 02:10:51.835072+03	96	ResetRequest object (96)	1	[{"added": {}}]	30	1
55	2019-01-22 15:59:29.911245+03	21	  поддерживающая уборка	2	[{"changed": {"fields": ["image"]}}]	23	1
56	2019-01-22 16:02:31.883077+03	21	  поддерживающая уборка	2	[{"changed": {"fields": ["image"]}}]	23	1
57	2019-01-22 16:02:56.760227+03	23	  генеральная уборка	2	[{"changed": {"fields": ["image"]}}]	23	1
58	2019-01-22 16:03:33.13987+03	24	  уборка после ремонта	2	[{"changed": {"fields": ["image"]}}]	23	1
59	2019-01-22 16:05:39.645875+03	25	  мытье окон	2	[{"changed": {"fields": ["image", "price_formula"]}}]	23	1
60	2019-01-22 16:05:53.702686+03	26	|--  пластиковые окна	2	[{"changed": {"fields": ["image", "price_formula"]}}]	23	1
61	2019-01-22 16:06:43.264156+03	27	|--  деревянные окна	2	[{"changed": {"fields": ["image"]}}]	23	1
62	2019-01-22 16:06:53.218505+03	28	  химчистка мебели	2	[{"changed": {"fields": ["image"]}}]	23	1
63	2019-01-22 16:07:07.0965+03	42	|--  текстиль	2	[{"changed": {"fields": ["image"]}}]	23	1
64	2019-01-22 16:07:20.214828+03	43	|--  кожа	2	[{"changed": {"fields": ["image"]}}]	23	1
65	2019-01-22 16:15:52.241799+03	21	  поддерживающая уборка1	2	[{"changed": {"fields": ["name"]}}]	23	1
66	2019-01-22 16:16:03.565362+03	21	  поддерживающая уборка	2	[{"changed": {"fields": ["name"]}}]	23	1
67	2019-01-23 17:37:02.096767+03	21	  поддерживающая уборка	2	[]	23	1
68	2019-01-23 17:39:55.079625+03	29	  дополнительно	2	[]	23	1
69	2019-01-23 19:55:04.441878+03	97	ResetRequest object (97)	1	[{"added": {}}]	30	1
70	2019-01-23 21:53:31.356165+03	98	ResetRequest object (98)	1	[{"added": {}}]	30	1
71	2019-01-23 21:53:55.522452+03	99	ResetRequest object (99)	1	[{"added": {}}]	30	1
72	2019-01-23 21:54:14.502471+03	100	ResetRequest object (100)	1	[{"added": {}}]	30	1
73	2019-01-23 23:26:20.469216+03	25	  мытье окон	2	[{"changed": {"fields": ["description"]}}]	23	1
74	2019-01-23 23:27:48.698907+03	28	  химчистка мебели	2	[{"changed": {"fields": ["description"]}}]	23	1
75	2019-01-23 23:28:05.744683+03	24	  уборка после ремонта	2	[]	23	1
76	2019-01-24 00:04:18.280024+03	21	  поддерживающая уборка1	2	[{"changed": {"fields": ["name"]}}]	23	1
77	2019-01-24 00:06:24.452997+03	21	  поддерживающая уборка	2	[{"changed": {"fields": ["name"]}}]	23	1
78	2019-01-24 00:09:03.252725+03	21	  поддерживающая уборка1	2	[{"changed": {"fields": ["name"]}}]	23	1
79	2019-01-24 00:12:39.55231+03	21	  поддерживающая уборка	2	[{"changed": {"fields": ["name"]}}]	23	1
80	2019-01-24 00:13:55.11283+03	21	  поддерживающая уборка2	2	[{"changed": {"fields": ["name"]}}]	23	1
81	2019-01-24 00:15:08.698476+03	21	  поддерживающая уборка23	2	[{"changed": {"fields": ["name"]}}]	23	1
82	2019-01-24 00:16:20.587396+03	21	  поддерживающая уборка234	2	[{"changed": {"fields": ["name"]}}]	23	1
83	2019-01-24 00:16:26.83485+03	21	  поддерживающая уборка	2	[{"changed": {"fields": ["name"]}}]	23	1
84	2019-01-24 00:28:26.974254+03	101	ResetRequest object (101)	1	[{"added": {}}]	30	1
85	2019-01-24 00:29:53.002864+03	102	ResetRequest object (102)	1	[{"added": {}}]	30	1
86	2019-01-24 00:30:03.913133+03	103	ResetRequest object (103)	1	[{"added": {}}]	30	1
87	2019-01-24 00:34:51.9818+03	21	  поддерживающая уборка	2	[{"changed": {"fields": ["description_line_2"]}}]	23	1
88	2019-01-24 00:35:15.204738+03	21	  поддерживающая уборка	2	[]	23	1
89	2019-01-24 00:35:22.931309+03	23	  генеральная уборка	2	[{"changed": {"fields": ["description_line_2"]}}]	23	1
90	2019-01-24 00:35:27.922548+03	24	  уборка после ремонта	2	[{"changed": {"fields": ["description_line_2"]}}]	23	1
91	2019-01-24 00:40:24.361733+03	27	|--  деревянные окна	2	[{"changed": {"fields": ["image"]}}]	23	1
92	2019-01-24 00:41:10.173759+03	27	|--  деревянные окна	2	[{"changed": {"fields": ["image"]}}]	23	1
93	2019-01-24 00:41:18.698187+03	27	|--  деревянные окна	2	[{"changed": {"fields": ["image"]}}]	23	1
94	2019-01-24 00:44:47.087469+03	27	|--  деревянные окна	2	[{"changed": {"fields": ["description_line_2"]}}]	23	1
95	2019-01-24 00:44:57.919692+03	26	|--  пластиковые окна	2	[{"changed": {"fields": ["description_line_2"]}}]	23	1
96	2019-01-24 00:45:02.942037+03	25	  мытье окон	2	[{"changed": {"fields": ["description_line_2"]}}]	23	1
97	2019-01-24 00:46:24.645697+03	26	|--  пластиковые окна	2	[{"changed": {"fields": ["description_line_1"]}}]	23	1
98	2019-01-24 00:49:22.59406+03	26	|--  пластиковые окна	2	[{"changed": {"fields": ["description_line_1"]}}]	23	1
99	2019-01-24 00:59:23.634408+03	27	|--  деревянные окна	2	[{"changed": {"fields": ["description_line_1"]}}]	23	1
100	2019-01-24 02:05:45.579541+03	28	  химчистка	2	[{"changed": {"fields": ["name"]}}]	23	1
101	2019-01-24 02:07:09.850497+03	42	|--  мебель / текстиль	2	[{"changed": {"fields": ["name"]}}]	23	1
102	2019-01-24 02:07:18.721224+03	43	|--  мебель / кожа	2	[{"changed": {"fields": ["name"]}}]	23	1
103	2019-01-24 02:10:49.22074+03	66	|--  ковер	1	[{"added": {}}, {"added": {"name": "goods param", "object": "GoodsParam object (41)"}}]	23	1
104	2019-01-24 02:11:17.855034+03	66	|--  ковер	2	[{"changed": {"fields": ["price_formula"]}}]	23	1
105	2019-01-24 02:12:42.687594+03	28	  химчистка	2	[{"changed": {"fields": ["description_line_1"]}}]	23	1
106	2019-01-24 03:02:28.302472+03	21	  поддерживающая уборка	2	[{"changed": {"name": "goods param", "fields": ["unit"], "object": "GoodsParam object (1)"}}]	23	1
107	2019-01-24 03:02:40.540669+03	23	  генеральная уборка	2	[{"changed": {"name": "goods param", "fields": ["unit"], "object": "GoodsParam object (8)"}}]	23	1
108	2019-01-24 03:02:46.849572+03	24	  уборка после ремонта	2	[{"changed": {"name": "goods param", "fields": ["unit"], "object": "GoodsParam object (13)"}}]	23	1
109	2019-01-24 03:02:55.271482+03	66	|--  ковер	2	[{"changed": {"name": "goods param", "fields": ["unit"], "object": "GoodsParam object (41)"}}]	23	1
110	2019-01-24 14:50:38.69023+03	104	ResetRequest object (104)	1	[{"added": {}}]	30	1
111	2019-01-24 18:29:33.600543+03	21	  поддерживающая уборка	2	[{"added": {"object": "DescriptionLine object (1)", "name": "description line"}}, {"added": {"object": "DescriptionLine object (2)", "name": "description line"}}, {"added": {"object": "DescriptionLine object (3)", "name": "description line"}}]	23	1
112	2019-01-24 18:33:52.09925+03	21	  поддерживающая уборка	2	[{"added": {"object": "DescriptionLine object (4)", "name": "description line"}}, {"added": {"object": "DescriptionLine object (5)", "name": "description line"}}, {"added": {"object": "DescriptionLine object (6)", "name": "description line"}}, {"changed": {"object": "DescriptionLine object (1)", "name": "description line", "fields": ["order"]}}, {"changed": {"object": "DescriptionLine object (2)", "name": "description line", "fields": ["order"]}}, {"changed": {"object": "DescriptionLine object (3)", "name": "description line", "fields": ["order"]}}]	23	1
113	2019-01-24 18:34:47.413493+03	21	  поддерживающая уборка	2	[{"added": {"object": "DescriptionLine object (7)", "name": "description line"}}]	23	1
114	2019-01-25 20:41:02.045971+03	21	  поддерживающая уборка	2	[{"added": {"object": "GoodsImage object (1)", "name": "goods image"}}, {"added": {"object": "GoodsImage object (2)", "name": "goods image"}}, {"added": {"object": "GoodsImage object (3)", "name": "goods image"}}]	23	1
115	2019-01-25 20:41:40.541641+03	21	  поддерживающая уборка	2	[{"added": {"object": "GoodsImage object (4)", "name": "goods image"}}]	23	1
116	2019-01-25 21:18:28.561825+03	105	ResetRequest object (105)	1	[{"added": {}}]	30	1
117	2019-01-25 21:20:26.036941+03	106	ResetRequest object (106)	1	[{"added": {}}]	30	1
118	2019-02-10 01:07:35.647873+03	2	Основное	1	[{"added": {}}, {"added": {"name": "question", "object": "\\u041a\\u0442\\u043e \\u043c\\u044b \\u0442\\u0430\\u043a\\u0438\\u0435?"}}, {"added": {"name": "question", "object": "\\u0427\\u0442\\u043e \\u043c\\u044b \\u0434\\u0435\\u043b\\u0430\\u0435\\u043c?"}}, {"added": {"name": "question", "object": "\\u042d\\u0442\\u043e \\u0432\\u043e\\u043f\\u0440\\u043e\\u0441\\u044b \\u0438 \\u041e\\u0442\\u0432\\u0435\\u0442\\u044b"}}]	34	1
119	2019-02-10 01:08:24.93654+03	3	Наши клинеры	1	[{"added": {}}, {"added": {"name": "question", "object": "\\u041a\\u0442\\u043e \\u043e\\u043d\\u0438?"}}, {"added": {"name": "question", "object": "\\u0415\\u0449\\u0435 \\u0432\\u043e\\u043f\\u0440\\u043e\\u0441?"}}]	34	1
120	2019-02-10 01:08:54.465316+03	4	Чистящие средства	1	[{"added": {}}, {"added": {"name": "question", "object": "\\u0412\\u043e\\u043f\\u0440\\u043e\\u0441"}}, {"added": {"name": "question", "object": "\\u0412\\u043e\\u043f\\u0440\\u043e\\u0441"}}]	34	1
121	2019-02-10 01:09:01.930428+03	3	Наши клинеры	2	[{"changed": {"name": "question", "object": "\\u0415\\u0449\\u0435 \\u0432\\u043e\\u043f\\u0440\\u043e\\u0441?", "fields": ["order"]}}, {"changed": {"name": "question", "object": "\\u041a\\u0442\\u043e \\u043e\\u043d\\u0438?", "fields": ["order"]}}]	34	1
122	2019-02-10 01:09:06.562932+03	2	Основное	2	[]	34	1
123	2019-02-10 16:53:57.699509+03	3	Наши клинеры	2	[{"changed": {"fields": ["instance"]}}]	34	1
124	2019-02-10 19:33:45.280703+03	2	Основное	2	[{"changed": {"name": "question", "object": "\\u041a\\u0442\\u043e \\u043c\\u044b \\u0442\\u0430\\u043a\\u0438\\u0435?", "fields": ["is_opened"]}}, {"changed": {"name": "question", "object": "\\u0427\\u0442\\u043e \\u043c\\u044b \\u0434\\u0435\\u043b\\u0430\\u0435\\u043c?", "fields": ["is_opened"]}}, {"deleted": {"name": "question", "object": "\\u042d\\u0442\\u043e \\u0432\\u043e\\u043f\\u0440\\u043e\\u0441\\u044b \\u0438 \\u041e\\u0442\\u0432\\u0435\\u0442\\u044b"}}]	34	1
125	2019-02-10 19:38:07.981429+03	2	Основное	2	[{"changed": {"name": "question", "object": "\\u0427\\u0442\\u043e \\u043c\\u044b \\u0434\\u0435\\u043b\\u0430\\u0435\\u043c?", "fields": ["is_opened"]}}]	34	1
126	2019-02-10 19:38:12.827814+03	2	Основное	2	[{"changed": {"name": "question", "object": "\\u0427\\u0442\\u043e \\u043c\\u044b \\u0434\\u0435\\u043b\\u0430\\u0435\\u043c?", "fields": ["is_opened"]}}]	34	1
127	2019-02-14 00:44:57.947989+03	20	app2__79219765859	2	[{"changed": {"fields": ["phone_number", "websocket_session"], "name": "profile", "object": "Profile object (80)"}}]	2	1
128	2019-02-14 00:59:21.073712+03	20	app2__79219765859	2	[{"changed": {"fields": ["phone_number"], "name": "profile", "object": "Profile object (80)"}}]	2	1
129	2019-02-14 02:06:24.50729+03	20	app2__79219765859	2	[{"changed": {"fields": ["phone_number"], "name": "profile", "object": "Profile object (80)"}}]	2	1
130	2019-02-14 02:11:29.019329+03	20	app2__79219765859	2	[{"changed": {"fields": ["phone_number"], "name": "profile", "object": "Profile object (80)"}}]	2	1
131	2019-02-14 02:44:33.863238+03	20	app2__79219765859	2	[{"changed": {"object": "Profile object (80)", "fields": ["phone_number"], "name": "profile"}}]	2	1
132	2019-02-14 02:47:39.590524+03	20	app2__79219765859	2	[{"changed": {"object": "Profile object (80)", "fields": ["phone_number"], "name": "profile"}}]	2	1
133	2019-02-14 03:12:03.450393+03	20	app2__79219765859	2	[{"changed": {"object": "Profile object (80)", "fields": ["phone_number"], "name": "profile"}}]	2	1
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 133, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	user
3	auth	group
4	auth	permission
5	contenttypes	contenttype
6	sessions	session
7	instances	instancesettings
8	instances	pageclass
9	instances	instancepage
10	instances	instance
11	instances	openlogo
12	instances	colorscheme
13	users	profile
14	chat	message
15	chat	room
16	chat	roomparticipant
17	feed	item
18	feed	itemimage
19	feed	feeditem
20	feed	discountitem
21	goods	orderitem
22	goods	orderitemparamvalue
23	goods	goods
24	goods	descriptionline
25	goods	goodsparam
26	goods	order
27	goods	displacegoods
28	goods	additionalgoods
29	goods	address
30	reset	resetrequest
31	sms	sms
32	goods	goodsimage
33	faq	question
34	faq	faqgroup
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 34, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2018-12-08 04:42:29.04723+03
2	auth	0001_initial	2018-12-08 04:42:30.211339+03
3	admin	0001_initial	2018-12-08 04:42:30.461471+03
4	admin	0002_logentry_remove_auto_add	2018-12-08 04:42:30.498504+03
5	contenttypes	0002_remove_content_type_name	2018-12-08 04:42:30.577061+03
6	auth	0002_alter_permission_name_max_length	2018-12-08 04:42:30.61798+03
7	auth	0003_alter_user_email_max_length	2018-12-08 04:42:30.659542+03
8	auth	0004_alter_user_username_opts	2018-12-08 04:42:30.686534+03
9	auth	0005_alter_user_last_login_null	2018-12-08 04:42:30.734395+03
10	auth	0006_require_contenttypes_0002	2018-12-08 04:42:30.742606+03
11	auth	0007_alter_validators_add_error_messages	2018-12-08 04:42:30.768997+03
12	auth	0008_alter_user_username_max_length	2018-12-08 04:42:30.884743+03
13	auth	0009_alter_user_last_name_max_length	2018-12-08 04:42:30.933476+03
14	chat	0001_initial	2018-12-08 04:42:31.516315+03
15	chat	0002_auto_20180810_1427	2018-12-08 04:42:32.559476+03
16	chat	0003_auto_20180810_1618	2018-12-08 04:42:32.781214+03
17	chat	0004_auto_20180817_1436	2018-12-08 04:42:32.989661+03
18	chat	0005_auto_20180817_1442	2018-12-08 04:42:33.055282+03
19	chat	0006_auto_20180817_1508	2018-12-08 04:42:33.414035+03
20	chat	0007_message_is_auto	2018-12-08 04:42:33.604133+03
21	chat	0008_auto_20180817_1511	2018-12-08 04:42:33.795132+03
22	chat	0009_auto_20180817_1536	2018-12-08 04:42:33.986704+03
23	chat	0010_auto_20180817_1536	2018-12-08 04:42:34.169407+03
24	chat	0011_auto_20180817_1541	2018-12-08 04:42:34.327123+03
25	chat	0012_auto_20180817_1544	2018-12-08 04:42:34.366963+03
26	chat	0013_roomparticipant_not_read_count	2018-12-08 04:42:34.676134+03
27	chat	0014_remove_roomparticipant_not_read_count	2018-12-08 04:42:34.71765+03
28	chat	0015_auto_20180912_2333	2018-12-08 04:42:34.850532+03
29	chat	0016_auto_20180917_1020	2018-12-08 04:42:34.891505+03
30	chat	0017_message__hash	2018-12-08 04:42:34.933045+03
31	chat	0018_auto_20180917_1743	2018-12-08 04:42:34.974512+03
32	instances	0001_initial	2018-12-08 04:42:35.066538+03
33	instances	0002_auto_20180912_2333	2018-12-08 04:42:35.198928+03
34	instances	0003_instance_admin	2018-12-08 04:42:35.307338+03
35	instances	0004_auto_20180913_1115	2018-12-08 04:42:35.515184+03
36	instances	0005_colorscheme_instancesettings	2018-12-08 04:42:35.839365+03
37	instances	0006_auto_20180913_1146	2018-12-08 04:42:35.897638+03
38	instances	0007_auto_20180913_1151	2018-12-08 04:42:35.955063+03
39	instances	0008_auto_20180913_1156	2018-12-08 04:42:36.013649+03
40	instances	0009_auto_20180913_1221	2018-12-08 04:42:36.038021+03
41	instances	0010_auto_20180913_1223	2018-12-08 04:42:36.180922+03
42	instances	0011_instancesettings_company_name	2018-12-08 04:42:36.221001+03
43	instances	0012_auto_20180914_0813	2018-12-08 04:42:36.44593+03
44	instances	0013_auto_20181020_1233	2018-12-08 04:42:36.787432+03
45	goods	0001_initial	2018-12-08 04:42:37.053492+03
46	goods	0002_auto_20181007_1223	2018-12-08 04:42:37.419281+03
47	goods	0003_auto_20181007_1233	2018-12-08 04:42:37.535656+03
48	goods	0004_auto_20181007_1305	2018-12-08 04:42:37.935691+03
49	goods	0005_goods_min_price	2018-12-08 04:42:38.184843+03
50	goods	0006_auto_20181007_1327	2018-12-08 04:42:38.332949+03
51	goods	0007_goods_param_default	2018-12-08 04:42:38.38297+03
52	goods	0008_goods_unique_set	2018-12-08 04:42:38.432887+03
53	goods	0009_goods_param_unit	2018-12-08 04:42:38.482291+03
54	goods	0010_descriptionline	2018-12-08 04:42:38.774541+03
55	goods	0011_auto_20181015_1556	2018-12-08 04:42:39.032546+03
56	goods	0012_auto_20181015_1604	2018-12-08 04:42:39.306435+03
57	goods	0013_auto_20181015_1835	2018-12-08 04:42:39.564746+03
58	goods	0014_auto_20181016_2013	2018-12-08 04:42:39.896404+03
59	feed	0001_initial	2018-12-08 04:42:40.095217+03
60	feed	0002_auto_20180911_1914	2018-12-08 04:42:40.128295+03
61	feed	0003_itemimage_item	2018-12-08 04:42:40.221049+03
62	feed	0004_auto_20180911_1919	2018-12-08 04:42:40.26129+03
63	feed	0005_auto_20180911_1935	2018-12-08 04:42:40.311485+03
64	feed	0006_auto_20180912_2324	2018-12-08 04:42:40.338245+03
65	feed	0007_auto_20180912_2333	2018-12-08 04:42:40.410846+03
66	feed	0008_discountitem_feeditem	2018-12-08 04:42:40.427742+03
67	feed	0009_item_instance	2018-12-08 04:42:40.652286+03
68	feed	0010_auto_20180914_0941	2018-12-08 04:42:40.719007+03
69	feed	0011_item_have_order_button	2018-12-08 04:42:40.928244+03
70	feed	0012_item_start_from_space	2018-12-08 04:42:41.161203+03
71	feed	0013_auto_20180914_1959	2018-12-08 04:42:41.226518+03
72	instances	0014_auto_20181022_1212	2018-12-08 04:42:41.358761+03
73	feed	0014_auto_20181022_1212	2018-12-08 04:42:41.517489+03
74	feed	0015_auto_20181022_1239	2018-12-08 04:42:41.616121+03
75	instances	0015_auto_20181022_1254	2018-12-08 04:42:41.64902+03
76	instances	0016_remove_instancesettings_feed_button_text	2018-12-08 04:42:41.699057+03
77	instances	0017_remove_instancesettings_order_page_text	2018-12-08 04:42:41.749504+03
78	instances	0018_auto_20181022_1305	2018-12-08 04:42:42.057468+03
79	instances	0019_auto_20181022_1315	2018-12-08 04:42:42.123368+03
80	instances	0020_auto_20181022_1316	2018-12-08 04:42:42.185392+03
81	instances	0021_auto_20181022_2315	2018-12-08 04:42:42.364628+03
82	goods	0015_auto_20181022_1212	2018-12-08 04:42:42.806883+03
83	goods	0016_auto_20181022_1239	2018-12-08 04:42:43.02088+03
84	goods	0017_auto_20181022_1330	2018-12-08 04:42:43.187968+03
85	goods	0018_auto_20181022_2315	2018-12-08 04:42:43.712021+03
86	goods	0019_auto_20181022_2317	2018-12-08 04:42:43.91767+03
87	goods	0020_goods_force_to_basket	2018-12-08 04:42:44.293146+03
88	goods	0021_auto_20181026_1101	2018-12-08 04:42:44.775503+03
89	goods	0022_auto_20181026_1204	2018-12-08 04:42:45.523075+03
90	goods	0023_auto_20181026_1404	2018-12-08 04:42:46.014306+03
91	goods	0024_auto_20181026_1405	2018-12-08 04:42:46.19581+03
92	goods	0025_auto_20181026_1453	2018-12-08 04:42:46.536026+03
93	goods	0026_remove_descriptionline_category	2018-12-08 04:42:46.598145+03
94	goods	0027_auto_20181026_1455	2018-12-08 04:42:46.66071+03
95	goods	0028_goods_details_button_text	2018-12-08 04:42:46.718708+03
96	goods	0029_goods_not_display_in_catalog	2018-12-08 04:42:47.035966+03
97	goods	0030_auto_20181109_1714	2018-12-08 04:42:48.559204+03
98	goods	0031_auto_20181109_1723	2018-12-08 04:42:49.082427+03
99	goods	0032_auto_20181109_1838	2018-12-08 04:42:49.405634+03
100	goods	0033_auto_20181109_1840	2018-12-08 04:42:50.654987+03
101	goods	0034_goods_item_type	2018-12-08 04:42:51.135907+03
102	goods	0035_auto_20181201_2118	2018-12-08 04:42:51.800289+03
103	goods	0036_goods_price_formula	2018-12-08 04:42:52.032086+03
104	goods	0037_orderitem_price_formula	2018-12-08 04:42:52.098062+03
105	goods	0038_auto_20181202_1153	2018-12-08 04:42:52.193572+03
106	goods	0039_goodsparam_formula_letter	2018-12-08 04:42:52.415199+03
107	goods	0040_auto_20181202_1238	2018-12-08 04:42:52.454673+03
108	goods	0041_auto_20181202_1332	2018-12-08 04:42:52.517574+03
109	goods	0042_auto_20181202_1346	2018-12-08 04:42:52.779314+03
110	goods	0043_auto_20181202_1401	2018-12-08 04:42:52.995645+03
111	goods	0044_auto_20181202_1526	2018-12-08 04:42:53.128591+03
112	goods	0045_auto_20181202_1528	2018-12-08 04:42:53.354393+03
113	goods	0046_substitutionalgoods	2018-12-08 04:42:53.577401+03
114	goods	0047_auto_20181203_2137	2018-12-08 04:42:53.944081+03
115	goods	0048_address_user	2018-12-08 04:42:54.076479+03
116	goods	0049_auto_20181208_0059	2018-12-08 04:42:54.225407+03
117	goods	0050_auto_20181208_0109	2018-12-08 04:42:54.642463+03
118	goods	0051_auto_20181208_0109	2018-12-08 04:42:54.690483+03
119	instances	0022_auto_20181022_2325	2018-12-08 04:42:54.816334+03
120	instances	0023_colorscheme_superlight	2018-12-08 04:42:54.85711+03
121	instances	0024_colorscheme_bg_add_border	2018-12-08 04:42:54.889966+03
122	instances	0025_auto_20181108_1220	2018-12-08 04:42:54.923141+03
123	instances	0026_auto_20181108_1220	2018-12-08 04:42:54.964162+03
124	instances	0027_auto_20181108_1220	2018-12-08 04:42:54.997785+03
125	instances	0028_auto_20181108_1221	2018-12-08 04:42:55.03133+03
126	instances	0029_colorscheme_base_border	2018-12-08 04:42:55.072742+03
127	instances	0030_auto_20181108_1223	2018-12-08 04:42:55.105546+03
128	instances	0031_auto_20181108_1223	2018-12-08 04:42:55.139854+03
129	instances	0032_auto_20181108_1223	2018-12-08 04:42:55.180895+03
130	instances	0033_auto_20181108_1223	2018-12-08 04:42:55.21344+03
131	instances	0034_auto_20181108_1224	2018-12-08 04:42:55.239082+03
132	instances	0035_auto_20181108_1224	2018-12-08 04:42:55.271609+03
133	instances	0036_remove_colorscheme_base_border	2018-12-08 04:42:55.305181+03
134	instances	0037_auto_20181108_1224	2018-12-08 04:42:55.329694+03
135	instances	0038_auto_20181108_1227	2018-12-08 04:42:55.454775+03
136	instances	0039_auto_20181108_1958	2018-12-08 04:42:55.754965+03
137	reset	0001_initial	2018-12-08 04:42:56.036941+03
138	reset	0002_auto_20181008_1913	2018-12-08 04:42:56.277349+03
139	reset	0003_auto_20181015_1552	2018-12-08 04:42:56.352854+03
140	sessions	0001_initial	2018-12-08 04:42:56.576872+03
141	sms	0001_initial	2018-12-08 04:42:56.743394+03
142	sms	0002_auto_20181115_1809	2018-12-08 04:42:57.652735+03
143	sms	0003_sms_sms_id	2018-12-08 04:42:57.885873+03
144	users	0001_initial	2018-12-08 04:42:58.158211+03
145	users	0002_auto_20180628_1421	2018-12-08 04:42:58.406817+03
146	users	0003_auto_20180808_1133	2018-12-08 04:42:58.806285+03
147	users	0004_profile_instance	2018-12-08 04:42:58.923319+03
148	goods	0052_auto_20181208_0150	2018-12-08 04:50:39.174639+03
149	goods	0053_auto_20181208_0345	2018-12-08 06:46:01.660851+03
150	goods	0054_order_tmp_id	2018-12-08 08:26:55.319131+03
151	users	0005_profile_is_phone_verified	2018-12-09 20:11:17.094222+03
152	goods	0055_orderitem_tmp_id	2018-12-10 00:39:52.181157+03
153	users	0006_auto_20181209_2139	2018-12-10 00:39:52.223162+03
154	goods	0056_auto_20181230_1706	2018-12-30 20:06:25.091448+03
155	goods	0057_order_confrimation_state	2019-01-07 14:57:17.981369+03
156	goods	0058_auto_20190107_1209	2019-01-07 15:09:36.519632+03
157	goods	0059_auto_20190107_1210	2019-01-07 15:10:28.931606+03
158	goods	0060_auto_20190121_2228	2019-01-22 01:30:24.797168+03
159	goods	0061_address_floor	2019-01-22 02:00:29.062994+03
160	goods	0062_auto_20190122_1258	2019-01-22 15:58:33.650911+03
161	goods	0063_auto_20190122_1306	2019-01-22 16:06:29.013462+03
162	goods	0064_auto_20190123_1436	2019-01-23 17:36:21.286321+03
163	goods	0065_auto_20190123_1443	2019-01-23 17:46:30.454422+03
164	goods	0066_auto_20190123_2127	2019-01-24 00:27:47.49771+03
165	goods	0067_auto_20190123_2127	2019-01-24 00:27:48.457216+03
166	goods	0068_auto_20190123_2357	2019-01-24 02:57:51.271293+03
167	goods	0069_auto_20190124_0001	2019-01-24 03:01:17.785838+03
168	faq	0001_initial	2019-02-09 23:19:06.497929+03
169	faq	0002_auto_20190209_2157	2019-02-10 00:57:51.366195+03
170	faq	0003_auto_20190209_2205	2019-02-10 01:05:05.991913+03
171	goods	0070_auto_20190213_1547	2019-02-13 18:48:11.713979+03
172	sms	0004_auto_20190213_1548	2019-02-13 18:48:12.498043+03
173	sms	0005_auto_20190213_1556	2019-02-13 18:56:13.939168+03
174	sms	0006_auto_20190213_1713	2019-02-13 20:13:22.114174+03
175	sms	0007_auto_20190213_1854	2019-02-13 21:54:34.202012+03
176	sms	0008_remove_sms_is_read	2019-02-13 22:13:27.177526+03
177	sms	0009_sms_attempt_number	2019-02-13 23:17:48.622708+03
178	sms	0010_auto_20190213_2131	2019-02-14 00:32:00.949269+03
179	sms	0011_auto_20190213_2131	2019-02-14 00:32:01.003987+03
180	sms	0012_sms_status_attempt_number	2019-02-14 02:29:25.774647+03
181	users	0007_auto_20190214_1811	2019-02-14 21:12:48.154365+03
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 181, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
xe5lks9ipjuh1wmiyvwv7rwyvrstp2jd	ZjU4MTQ1YWI5NGQ3NGViNmJjOTRmMGIwYWMzMDU4YTgwNTQ2ZjFjMzp7InBob25lX3ZlcmlmaWNhdGlvbl91c2VyX2lkIjoxOSwiX2F1dGhfdXNlcl9oYXNoIjoiZDc4M2U1YmE0MmNjZTA0ZDczOWY1NzYxNGM4MTg1YTlmZWRiYjJkMCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE5In0=	2018-12-25 15:19:05.322031+03
0zv7slwyoivn3v75ctuj06pk65opcuy7	NjllMDQ3N2MyNGUzOTdlMjUwMjUyYmI1ZDZmYTRiNWZlNDAwOTgwYzp7InBob25lX3ZlcmlmaWNhdGlvbl91c2VyX2lkIjoxOSwiX2F1dGhfdXNlcl9pZCI6IjE5IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkNzgzZTViYTQyY2NlMDRkNzM5ZjU3NjE0YzgxODVhOWZlZGJiMmQwIn0=	2019-01-02 17:39:46.483037+03
a5w9eeb0aue2rvxc1p8xluntos3xs5sb	ODRkZGYxM2RmNWIxNjE0ZTM4MTY1NjU1MmIwM2YyNGE1YmQyYWIwMjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE5IiwicGhvbmVfdmVyaWZpY2F0aW9uX3VzZXJfaWQiOjE5LCJfYXV0aF91c2VyX2hhc2giOiJkNzgzZTViYTQyY2NlMDRkNzM5ZjU3NjE0YzgxODVhOWZlZGJiMmQwIn0=	2019-01-11 23:01:12.425784+03
42hpt5q596wpmvnoci92u6rz2yoykx73	ZjU4MTQ1YWI5NGQ3NGViNmJjOTRmMGIwYWMzMDU4YTgwNTQ2ZjFjMzp7InBob25lX3ZlcmlmaWNhdGlvbl91c2VyX2lkIjoxOSwiX2F1dGhfdXNlcl9oYXNoIjoiZDc4M2U1YmE0MmNjZTA0ZDczOWY1NzYxNGM4MTg1YTlmZWRiYjJkMCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE5In0=	2019-01-17 21:07:54.5146+03
oegnr4uf4bw9z7jeac5egzt5rjopki5o	OGMxOTg4OGI0YzdiMGQwYzA3M2Q0Y2E1MDhkYjdjNWIzNjliNjc2MDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2VjMTU1NTZkZjk0OTkxM2M4MGQ4MzU5M2MzZDA1YjhjNWM0YmUxOCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-12-22 04:48:37.146249+03
l0b67djcysan4sbz96fifd7ud85jv89v	ODA4NWU2ZTZiZmRhMjQ5OWQyZDNlNTAwN2Y1MjQwM2IxYjMxOTkxZDp7InBob25lX3RvX3ZlcmlmeSI6Ijc5OTk5OTk5OTk5IiwicGhvbmVfdmVyaWZpY2F0aW9uX2NvZGUiOiI1NTU1NSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDc4M2U1YmE0MmNjZTA0ZDczOWY1NzYxNGM4MTg1YTlmZWRiYjJkMCIsIl9hdXRoX3VzZXJfaWQiOiI3In0=	2018-12-22 22:58:38.243622+03
nf845n55nzioajxc9d58v39doi4dc9bg	ZGNkMmRhODU2YTJhOWRjNTAxOGQyYjI3ODI1NjEzOTg1Y2E2ZTU0ZDp7InBob25lX3ZlcmlmaWNhdGlvbl9jb2RlIjoiNTU1NTUiLCJwaG9uZV92ZXJpZmljYXRpb25fdXNlcl9pZCI6MTUsInVzZXJfaWRfdG9fdmVyaWZ5IjoxNX0=	2018-12-23 20:30:47.440767+03
0lnx8nrcfnkck1dq7bclrpaks8ngv9ms	MDg5MmQ3ZDk2MmE5YmY2NTliMGY5YjQ4MzI2NGRhNWQ3MTFkM2JhZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDc4M2U1YmE0MmNjZTA0ZDczOWY1NzYxNGM4MTg1YTlmZWRiYjJkMCIsInBob25lX3ZlcmlmaWNhdGlvbl91c2VyX2lkIjoxOSwiX2F1dGhfdXNlcl9pZCI6IjE5In0=	2019-01-19 19:05:59.834452+03
xvq3z4cx2zq58g8l5da40dg2j5j47ul5	OGMxOTg4OGI0YzdiMGQwYzA3M2Q0Y2E1MDhkYjdjNWIzNjliNjc2MDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2VjMTU1NTZkZjk0OTkxM2M4MGQ4MzU5M2MzZDA1YjhjNWM0YmUxOCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2019-01-19 19:28:25.699428+03
wyan4i7vor5xx4ww2wp1fcx8i0rm2s5s	MjZiZDg2NDcwODNmNzRhZGU3NDFhNTIyY2ZjZDg2MjEyNDg2ODEyMzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI3ZWMxNTU1NmRmOTQ5OTEzYzgwZDgzNTkzYzNkMDViOGM1YzRiZTE4In0=	2019-02-04 17:16:51.854216+03
xlsqlx8ti1w5wz1rmtl7y6ut2tgq7584	ZWVhZGEwMGU1MmJmZjIwMjQ3ZjdlMDAxNWFiNDJjMDdmOGQzOWM0NTp7InBob25lX3ZlcmlmaWNhdGlvbl91c2VyX2lkIjoxOSwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkNzgzZTViYTQyY2NlMDRkNzM5ZjU3NjE0YzgxODVhOWZlZGJiMmQwIiwiX2F1dGhfdXNlcl9pZCI6IjE5In0=	2019-02-07 01:09:38.881906+03
9x4yan8ti4wnkinzf4g8vd3dmc9y8zcx	MWFlOWE0YmRhOTNmOWE2NDAzZGNkYTc0ZjAzOGNmNjJlZDhmZmE3Mzp7Il9hdXRoX3VzZXJfaWQiOiIxOSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDc4M2U1YmE0MmNjZTA0ZDczOWY1NzYxNGM4MTg1YTlmZWRiYjJkMCIsInBob25lX3ZlcmlmaWNhdGlvbl91c2VyX2lkIjoxOX0=	2019-02-09 19:44:46.235708+03
gmgpjoyk5zzdenciax18ktfei5u1zyaj	MWFlOWE0YmRhOTNmOWE2NDAzZGNkYTc0ZjAzOGNmNjJlZDhmZmE3Mzp7Il9hdXRoX3VzZXJfaWQiOiIxOSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDc4M2U1YmE0MmNjZTA0ZDczOWY1NzYxNGM4MTg1YTlmZWRiYjJkMCIsInBob25lX3ZlcmlmaWNhdGlvbl91c2VyX2lkIjoxOX0=	2019-02-09 19:45:39.319608+03
3hphncxslfcrt9f27qvk7lzqzrgra82v	MWFlOWE0YmRhOTNmOWE2NDAzZGNkYTc0ZjAzOGNmNjJlZDhmZmE3Mzp7Il9hdXRoX3VzZXJfaWQiOiIxOSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDc4M2U1YmE0MmNjZTA0ZDczOWY1NzYxNGM4MTg1YTlmZWRiYjJkMCIsInBob25lX3ZlcmlmaWNhdGlvbl91c2VyX2lkIjoxOX0=	2019-02-09 19:45:52.445119+03
wgfv3ui0q3huhsg9guzvpo1w2tthofr2	MDg5MmQ3ZDk2MmE5YmY2NTliMGY5YjQ4MzI2NGRhNWQ3MTFkM2JhZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDc4M2U1YmE0MmNjZTA0ZDczOWY1NzYxNGM4MTg1YTlmZWRiYjJkMCIsInBob25lX3ZlcmlmaWNhdGlvbl91c2VyX2lkIjoxOSwiX2F1dGhfdXNlcl9pZCI6IjE5In0=	2019-02-12 12:39:09.010748+03
fy8x7jdimepj5ifhj7o0srj3zq3u2skp	MDg5MmQ3ZDk2MmE5YmY2NTliMGY5YjQ4MzI2NGRhNWQ3MTFkM2JhZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDc4M2U1YmE0MmNjZTA0ZDczOWY1NzYxNGM4MTg1YTlmZWRiYjJkMCIsInBob25lX3ZlcmlmaWNhdGlvbl91c2VyX2lkIjoxOSwiX2F1dGhfdXNlcl9pZCI6IjE5In0=	2019-02-12 12:41:02.959119+03
c0jd8w3ab2hvk7v9iypab4ctbjcdm303	MDg5MmQ3ZDk2MmE5YmY2NTliMGY5YjQ4MzI2NGRhNWQ3MTFkM2JhZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDc4M2U1YmE0MmNjZTA0ZDczOWY1NzYxNGM4MTg1YTlmZWRiYjJkMCIsInBob25lX3ZlcmlmaWNhdGlvbl91c2VyX2lkIjoxOSwiX2F1dGhfdXNlcl9pZCI6IjE5In0=	2019-02-12 12:48:25.599267+03
x2rc8uo799oq0rxm2xhraf7umpmul3nl	ODRkZGYxM2RmNWIxNjE0ZTM4MTY1NjU1MmIwM2YyNGE1YmQyYWIwMjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE5IiwicGhvbmVfdmVyaWZpY2F0aW9uX3VzZXJfaWQiOjE5LCJfYXV0aF91c2VyX2hhc2giOiJkNzgzZTViYTQyY2NlMDRkNzM5ZjU3NjE0YzgxODVhOWZlZGJiMmQwIn0=	2019-02-12 23:05:59.423067+03
0oyqasjuiuhiu7cij8w3532xvs57sgxf	ODRkZGYxM2RmNWIxNjE0ZTM4MTY1NjU1MmIwM2YyNGE1YmQyYWIwMjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE5IiwicGhvbmVfdmVyaWZpY2F0aW9uX3VzZXJfaWQiOjE5LCJfYXV0aF91c2VyX2hhc2giOiJkNzgzZTViYTQyY2NlMDRkNzM5ZjU3NjE0YzgxODVhOWZlZGJiMmQwIn0=	2019-02-12 23:16:14.846219+03
rja57a1yebwj8lujwqedtk8agacz3nx4	ODRkZGYxM2RmNWIxNjE0ZTM4MTY1NjU1MmIwM2YyNGE1YmQyYWIwMjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE5IiwicGhvbmVfdmVyaWZpY2F0aW9uX3VzZXJfaWQiOjE5LCJfYXV0aF91c2VyX2hhc2giOiJkNzgzZTViYTQyY2NlMDRkNzM5ZjU3NjE0YzgxODVhOWZlZGJiMmQwIn0=	2019-02-12 23:58:06.874686+03
iuffxt08u9ctugq9e3iguhx7le1h9zvi	ODRkZGYxM2RmNWIxNjE0ZTM4MTY1NjU1MmIwM2YyNGE1YmQyYWIwMjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE5IiwicGhvbmVfdmVyaWZpY2F0aW9uX3VzZXJfaWQiOjE5LCJfYXV0aF91c2VyX2hhc2giOiJkNzgzZTViYTQyY2NlMDRkNzM5ZjU3NjE0YzgxODVhOWZlZGJiMmQwIn0=	2019-02-13 12:24:39.37331+03
64kcetegxx2l3h0np2o0ocrtiwvec8os	ODRkZGYxM2RmNWIxNjE0ZTM4MTY1NjU1MmIwM2YyNGE1YmQyYWIwMjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE5IiwicGhvbmVfdmVyaWZpY2F0aW9uX3VzZXJfaWQiOjE5LCJfYXV0aF91c2VyX2hhc2giOiJkNzgzZTViYTQyY2NlMDRkNzM5ZjU3NjE0YzgxODVhOWZlZGJiMmQwIn0=	2019-02-13 20:25:11.543019+03
agtgf8fmy4wdet0pbs3w8xru73yni006	MjZiZDg2NDcwODNmNzRhZGU3NDFhNTIyY2ZjZDg2MjEyNDg2ODEyMzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI3ZWMxNTU1NmRmOTQ5OTEzYzgwZDgzNTkzYzNkMDViOGM1YzRiZTE4In0=	2019-02-24 00:58:01.222938+03
kfmz3r4ftrr5mptsee4gyvw5na2sphjy	OGIyN2Q0ZjMxYjYwNGQ3ODkyZmZmNDJiNzI4OTBiYjZmZjIxZWU5NTp7Il9hdXRoX3VzZXJfaGFzaCI6IjdlYzE1NTU2ZGY5NDk5MTNjODBkODM1OTNjM2QwNWI4YzVjNGJlMTgiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2019-02-24 16:06:26.028256+03
zaovfjcmfi6qrgfgre43lhzghu63x019	YjJiOGJiOTJjOTVhZDBhNzYwZmY2OWRmMzViNTBhZmQzMzA0NjU0Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjIwIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDc4M2U1YmE0MmNjZTA0ZDczOWY1NzYxNGM4MTg1YTlmZWRiYjJkMCJ9	2019-03-12 19:11:22.399279+03
ajxfuigiptpseczzvjuvmi8xs47dn2ak	YjJiOGJiOTJjOTVhZDBhNzYwZmY2OWRmMzViNTBhZmQzMzA0NjU0Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjIwIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDc4M2U1YmE0MmNjZTA0ZDczOWY1NzYxNGM4MTg1YTlmZWRiYjJkMCJ9	2019-03-12 19:16:20.15992+03
o4x6ii5v7fkt5hahd951ko9cb27zurp8	YjJiOGJiOTJjOTVhZDBhNzYwZmY2OWRmMzViNTBhZmQzMzA0NjU0Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjIwIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDc4M2U1YmE0MmNjZTA0ZDczOWY1NzYxNGM4MTg1YTlmZWRiYjJkMCJ9	2019-03-17 14:11:43.645886+03
8leee9hdri4lp6yvv0xbjiz5hdh08l2b	ZGI4MjBjNTg3YjdmZjMyYjc1MzMwZTBiZDM0ZGI4NTgyYzg2Mzg1MTp7InBob25lX3ZlcmlmaWNhdGlvbl91c2VyX2lkIjoxOSwicGhvbmVfdmVyaWZpY2F0aW9uX2NvZGUiOiI1NTU1NSJ9	2019-02-26 13:49:21.156324+03
6isun3si7z7ohe5yxxqjvq5zurqeo7y1	NzBmZThmYzRmOGI0NWE4ZDQ1NzZlNWZmOWI4MzlkMDJhYjhjMjE1NTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQ3ODNlNWJhNDJjY2UwNGQ3MzlmNTc2MTRjODE4NWE5ZmVkYmIyZDAiLCJfYXV0aF91c2VyX2lkIjoiMjAiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2019-03-06 16:02:14.89056+03
qbz5i8ljsuvy78p2eyn4g7a0fjejee7w	MTYwN2E3MTUwZmRhMjhjMTkwNzRjMTljYTUzNGU1ZDk0YjkzMzMxODp7Il9hdXRoX3VzZXJfaGFzaCI6ImQ3ODNlNWJhNDJjY2UwNGQ3MzlmNTc2MTRjODE4NWE5ZmVkYmIyZDAiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyMCJ9	2019-03-08 20:31:08.231643+03
\.


--
-- Data for Name: faq_faqgroup; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.faq_faqgroup (id, header, not_display_name, instance_id, created_at, is_alive, "order", updated_at) FROM stdin;
4	Чистящие средства	f	2	2019-02-10 01:08:54.440992+03	t	30	2019-02-10 01:08:54.440992+03
3	Наши клинеры	f	2	2019-02-10 01:08:24.910156+03	t	20	2019-02-10 16:53:57.655137+03
2	Основное	t	2	2019-02-10 01:07:35.617893+03	t	10	2019-02-10 19:38:12.805375+03
\.


--
-- Name: faq_faqgroup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.faq_faqgroup_id_seq', 4, true);


--
-- Data for Name: faq_question; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.faq_question (id, question, answer, is_opened, group_id, "order") FROM stdin;
9	Вопрос	Ответ	f	4	10
10	Вопрос	Ответ	f	4	20
8	Еще вопрос?	Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ	f	3	10
7	Кто они?	Лучшие клинеры Лучшие клинеры Лучшие клинеры Лучшие клинеры Лучшие клинеры Лучшие клинеры Лучшие клинеры Лучшие клинеры Лучшие клинеры Лучшие клинеры Лучшие клинеры Лучшие клинеры Лучшие клинеры Лучшие клинеры Лучшие клинеры Лучшие клинеры Лучшие клинеры Лучшие клинеры Лучшие клинеры Лучшие клинеры Лучшие клинеры Лучшие клинеры	f	3	20
4	Кто мы такие?	Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ Ответ	t	2	10
5	Что мы делаем?	Так и так Так и так Так и так Так и так Так и так Так и так Так и так Так и так Так и так Так и так Так и так Так и так Так и так Так и так Так и так Так и так Так и так Так и так Так и так Так и так Так и так Так и так	t	2	20
\.


--
-- Name: faq_question_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.faq_question_id_seq', 10, true);


--
-- Data for Name: feed_item; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.feed_item (id, created_at, updated_at, is_alive, title, text, published_at, video_file, have_order_button, starts_from_space, page_id) FROM stdin;
\.


--
-- Name: feed_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.feed_item_id_seq', 1, false);


--
-- Data for Name: feed_itemimage; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.feed_itemimage (id, created_at, updated_at, is_alive, image_file, item_id) FROM stdin;
\.


--
-- Name: feed_itemimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.feed_itemimage_id_seq', 1, false);


--
-- Data for Name: goods_additionalgoods; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.goods_additionalgoods (id, "order", goods_add_id, goods_to_id) FROM stdin;
3	0	29	21
4	0	29	23
5	0	29	24
\.


--
-- Name: goods_additionalgoods_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.goods_additionalgoods_id_seq', 5, true);


--
-- Data for Name: goods_address; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.goods_address (id, created_at, updated_at, is_alive, city, street, house, housing, flat, intercome_code, user_id, floor) FROM stdin;
1	2019-01-05 11:25:35.625751+03	2019-01-05 11:25:35.625751+03	t	Санкт-Петербург	Большая Пушкарская	24		3		19	
2	2019-01-05 11:25:35.839581+03	2019-01-05 11:25:35.839581+03	t	Санкт-Петербург	Аэродромная	5		121		19	
3	2019-01-05 11:25:35.997642+03	2019-01-05 11:25:35.997642+03	t	Санкт-Петербург	Добролюбова	5		30		19	
4	2019-01-05 11:25:36.130537+03	2019-01-05 11:25:36.130537+03	t	Санкт-Петербург	Аэродромная	24		3		19	
5	2019-01-05 11:25:36.296723+03	2019-01-05 11:25:36.296723+03	t	Санкт-Петербург	Большая Пушкарская	12		57		19	
6	2019-01-05 11:25:36.520801+03	2019-01-05 11:25:36.520801+03	t	Санкт-Петербург	Добролюбова	24		3		19	
7	2019-01-05 11:25:36.645578+03	2019-01-05 11:25:36.645578+03	t	Санкт-Петербург	Большая Пушкарская	12		121		19	
8	2019-01-05 11:25:36.786589+03	2019-01-05 11:25:36.786589+03	t	Санкт-Петербург	Аэродромная	12		3		19	
9	2019-01-05 11:25:36.98636+03	2019-01-05 11:25:36.98636+03	t	Санкт-Петербург	Большая Пушкарская	24		30		19	
10	2019-01-05 11:25:37.268863+03	2019-01-05 11:25:37.268863+03	t	Санкт-Петербург	Ленская	5		30		19	
11	2019-01-05 11:25:37.451606+03	2019-01-05 11:25:37.451606+03	t	Санкт-Петербург	Ленская	5		30		19	
12	2019-01-05 11:25:37.60946+03	2019-01-05 11:25:37.60946+03	t	Санкт-Петербург	Большая Пушкарская	33		30		19	
13	2019-01-05 11:25:37.791942+03	2019-01-05 11:25:37.791942+03	t	Санкт-Петербург	Аэродромная	33		57		19	
14	2019-01-05 11:25:37.908265+03	2019-01-05 11:25:37.908265+03	t	Санкт-Петербург	Большая Пушкарская	5		30		19	
15	2019-01-05 11:25:38.133199+03	2019-01-05 11:25:38.133199+03	t	Санкт-Петербург	Большая Пушкарская	5		30		19	
16	2019-01-05 11:51:01.688715+03	2019-01-05 11:51:01.688715+03	t	Санкт-Петербург	Большая Пушкарская	39		81		19	
17	2019-01-05 11:51:02.085785+03	2019-01-05 11:51:02.085785+03	t	Санкт-Петербург	Ленская	39		95		19	
18	2019-01-05 11:51:02.251765+03	2019-01-05 11:51:02.251765+03	t	Санкт-Петербург	Ленская	33		15		19	
19	2019-01-05 11:51:02.368074+03	2019-01-05 11:51:02.368074+03	t	Санкт-Петербург	Большая Пушкарская	37		21		19	
20	2019-01-05 11:51:02.525869+03	2019-01-05 11:51:02.525869+03	t	Санкт-Петербург	Большая Пушкарская	12		30		19	
21	2019-01-05 11:51:02.642288+03	2019-01-05 11:51:02.642288+03	t	Санкт-Петербург	Аэродромная	24		95		19	
22	2019-01-05 11:51:02.87507+03	2019-01-05 11:51:02.87507+03	t	Санкт-Петербург	Добролюбова	33		121		19	
23	2019-01-05 11:51:03.049478+03	2019-01-05 11:51:03.049478+03	t	Санкт-Петербург	Ленская	39		81		19	
24	2019-01-05 11:51:03.207448+03	2019-01-05 11:51:03.207448+03	t	Санкт-Петербург	Добролюбова	39		99		19	
25	2019-01-05 11:51:03.364902+03	2019-01-05 11:51:03.364902+03	t	Санкт-Петербург	Большая Пушкарская	11		99		19	
49	2019-01-07 18:50:43.368062+03	2019-01-07 18:50:43.368062+03	t	Санкт-Петербург	Ленская	5		95		19	
50	2019-01-07 18:50:43.542672+03	2019-01-07 18:50:43.542672+03	t	Санкт-Петербург	Большая Пушкарская	39		95		19	
44	2019-01-07 15:41:28.311196+03	2019-01-07 15:41:28.311196+03	t	Санкт-Петербург	Аэродромная	8		110		19	
42	2019-01-07 15:41:27.936942+03	2019-01-07 15:41:27.936942+03	t	Санкт-Петербург	Ленская	12		21		19	
51	2019-01-07 18:50:43.667371+03	2019-01-07 18:50:43.667371+03	t	Санкт-Петербург	Большая Пушкарская	5		3		19	
27	2019-01-05 11:51:03.630969+03	2019-01-05 11:51:03.630969+03	t	Санкт-Петербург	Добролюбова	37		121		19	
26	2019-01-05 11:51:03.481239+03	2019-01-05 11:51:03.481239+03	t	Санкт-Петербург	Ленская	33		81		19	
31	2019-01-07 15:41:25.72033+03	2019-01-07 15:41:25.72033+03	t	Санкт-Петербург	Добролюбова	12		57		19	
32	2019-01-07 15:41:25.86802+03	2019-01-07 15:41:25.86802+03	t	Санкт-Петербург	Добролюбова	12		57		19	
33	2019-01-07 15:41:26.034121+03	2019-01-07 15:41:26.034121+03	t	Санкт-Петербург	Ленская	5		17		19	
34	2019-01-07 15:41:26.283821+03	2019-01-07 15:41:26.283821+03	t	Санкт-Петербург	Большая Пушкарская	27		110		19	
35	2019-01-07 15:41:26.499886+03	2019-01-07 15:41:26.499886+03	t	Санкт-Петербург	Ленская	33		57		19	
36	2019-01-07 15:41:26.699016+03	2019-01-07 15:41:26.699016+03	t	Санкт-Петербург	Аэродромная	24		110		19	
40	2019-01-07 15:41:27.488591+03	2019-01-07 15:41:27.488591+03	t	Санкт-Петербург	Ленская	28		30		19	
41	2019-01-07 15:41:27.737629+03	2019-01-07 15:41:27.737629+03	t	Санкт-Петербург	Аэродромная	12		67		19	
52	2019-01-07 18:50:43.824891+03	2019-01-07 18:50:43.824891+03	t	Санкт-Петербург	Добролюбова	21		1		19	
53	2019-01-07 18:50:44.049139+03	2019-01-07 18:50:44.049139+03	t	Санкт-Петербург	Большая Пушкарская	39		17		19	
54	2019-01-07 18:50:44.165471+03	2019-01-07 18:50:44.165471+03	t	Санкт-Петербург	Аэродромная	11		15		19	
39	2019-01-07 15:41:27.314114+03	2019-01-07 15:41:27.314114+03	t	Санкт-Петербург	Ленская	11		57		19	
30	2019-01-05 11:51:04.154435+03	2019-01-05 11:51:04.154435+03	t	Санкт-Петербург	Добролюбова	27		67		19	
29	2019-01-05 11:51:03.980146+03	2019-01-05 11:51:03.980146+03	t	Санкт-Петербург	Аэродромная	3		21		19	
28	2019-01-05 11:51:03.788839+03	2019-01-05 11:51:03.788839+03	t	Санкт-Петербург	Ленская	5		67		19	
55	2019-01-07 18:50:44.274061+03	2019-01-07 18:50:44.274061+03	t	Санкт-Петербург	Большая Пушкарская	39		15		19	
56	2019-01-07 18:50:44.398587+03	2019-01-07 18:50:44.398587+03	t	Санкт-Петербург	Аэродромная	39		57		19	
57	2019-01-07 18:50:44.564736+03	2019-01-07 18:50:44.564736+03	t	Санкт-Петербург	Добролюбова	8		121		19	
43	2019-01-07 15:41:28.078125+03	2019-01-07 15:41:28.078125+03	t	Санкт-Петербург	Добролюбова	7		17		19	
58	2019-01-07 18:50:44.813736+03	2019-01-07 18:50:44.813736+03	t	Санкт-Петербург	Аэродромная	28		21		19	
45	2019-01-07 15:41:28.468979+03	2019-01-07 15:41:28.468979+03	t	Санкт-Петербург	Аэродромная	7		57		19	
38	2019-01-07 15:41:27.139836+03	2019-01-07 15:41:27.139836+03	t	Санкт-Петербург	Большая Пушкарская	8		67		19	
46	2019-01-07 18:50:42.864849+03	2019-01-07 18:50:42.864849+03	t	Санкт-Петербург	Аэродромная	37		15		19	
47	2019-01-07 18:50:43.085567+03	2019-01-07 18:50:43.085567+03	t	Санкт-Петербург	Ленская	33		30		19	
37	2019-01-07 15:41:26.881752+03	2019-01-07 15:41:26.881752+03	t	Санкт-Петербург	Ленская	7		110		19	
48	2019-01-07 18:50:43.243542+03	2019-01-07 18:50:43.243542+03	t	Санкт-Петербург	Ленская	24		17		19	
59	2019-01-07 18:50:44.905259+03	2019-01-07 18:50:44.905259+03	t	Санкт-Петербург	Большая Пушкарская	24		81		19	
60	2019-01-07 18:50:45.170984+03	2019-01-07 18:50:45.170984+03	t	Санкт-Петербург	Большая Пушкарская	37		21		19	
61	2019-01-07 18:51:44.669995+03	2019-01-07 18:51:44.669995+03	t	Санкт-Петербург	Ленская	8		99		19	
62	2019-01-07 18:51:44.852138+03	2019-01-07 18:51:44.852138+03	t	Санкт-Петербург	Аэродромная	21		81		19	
63	2019-01-07 18:51:45.018577+03	2019-01-07 18:51:45.018577+03	t	Санкт-Петербург	Большая Пушкарская	39		3		19	
64	2019-01-07 18:51:45.134911+03	2019-01-07 18:51:45.134911+03	t	Санкт-Петербург	Добролюбова	12		30		19	
65	2019-01-07 18:51:45.334187+03	2019-01-07 18:51:45.334187+03	t	Санкт-Петербург	Добролюбова	3		121		19	
66	2019-01-07 18:51:45.450492+03	2019-01-07 18:51:45.450492+03	t	Санкт-Петербург	Ленская	27		15		19	
67	2019-01-07 18:51:45.616739+03	2019-01-07 18:51:45.616739+03	t	Санкт-Петербург	Добролюбова	21		95		19	
68	2019-01-07 18:51:45.857586+03	2019-01-07 18:51:45.857586+03	t	Санкт-Петербург	Большая Пушкарская	3		110		19	
69	2019-01-07 18:51:45.957418+03	2019-01-07 18:51:45.957418+03	t	Санкт-Петербург	Большая Пушкарская	21		30		19	
70	2019-01-07 18:51:46.090348+03	2019-01-07 18:51:46.090348+03	t	Санкт-Петербург	Добролюбова	37		17		19	
71	2019-01-07 18:51:46.372894+03	2019-01-07 18:51:46.372894+03	t	Санкт-Петербург	Ленская	21		15		19	
72	2019-01-07 18:51:46.580683+03	2019-01-07 18:51:46.580683+03	t	Санкт-Петербург	Ленская	33		30		19	
73	2019-01-07 18:51:46.896124+03	2019-01-07 18:51:46.896124+03	t	Санкт-Петербург	Аэродромная	27		17		19	
74	2019-01-07 18:51:47.087264+03	2019-01-07 18:51:47.087264+03	t	Санкт-Петербург	Ленская	24		17		19	
75	2019-01-07 18:51:47.278695+03	2019-01-07 18:51:47.278695+03	t	Санкт-Петербург	Ленская	39		30		19	
76	2019-01-07 18:52:01.908891+03	2019-01-07 18:52:01.908891+03	t	Санкт-Петербург	Большая Пушкарская	33		1		19	
77	2019-01-07 18:52:02.057678+03	2019-01-07 18:52:02.057678+03	t	Санкт-Петербург	Аэродромная	24		15		19	
78	2019-01-07 18:52:02.290335+03	2019-01-07 18:52:02.290335+03	t	Санкт-Петербург	Ленская	3		30		19	
79	2019-01-07 18:52:02.464998+03	2019-01-07 18:52:02.464998+03	t	Санкт-Петербург	Большая Пушкарская	28		3		19	
80	2019-01-07 18:52:02.813653+03	2019-01-07 18:52:02.813653+03	t	Санкт-Петербург	Ленская	28		81		19	
81	2019-01-07 18:52:03.00507+03	2019-01-07 18:52:03.00507+03	t	Санкт-Петербург	Добролюбова	24		30		19	
82	2019-01-07 18:52:03.129737+03	2019-01-07 18:52:03.129737+03	t	Санкт-Петербург	Аэродромная	3		17		19	
83	2019-01-07 18:52:03.245817+03	2019-01-07 18:52:03.245817+03	t	Санкт-Петербург	Большая Пушкарская	37		99		19	
84	2019-01-07 18:52:03.403839+03	2019-01-07 18:52:03.403839+03	t	Санкт-Петербург	Ленская	11		17		19	
85	2019-01-07 18:52:03.553563+03	2019-01-07 18:52:03.553563+03	t	Санкт-Петербург	Большая Пушкарская	11		1		19	
86	2019-01-07 18:52:03.711127+03	2019-01-07 18:52:03.711127+03	t	Санкт-Петербург	Ленская	11		110		19	
87	2019-01-07 18:52:03.852288+03	2019-01-07 18:52:03.852288+03	t	Санкт-Петербург	Ленская	11		110		19	
88	2019-01-07 18:52:03.952142+03	2019-01-07 18:52:03.952142+03	t	Санкт-Петербург	Ленская	11		121		19	
89	2019-01-07 18:52:04.193199+03	2019-01-07 18:52:04.193199+03	t	Санкт-Петербург	Добролюбова	12		17		19	
90	2019-01-07 18:52:04.359426+03	2019-01-07 18:52:04.359426+03	t	Санкт-Петербург	Аэродромная	28		99		19	
91	2019-01-07 18:52:18.873037+03	2019-01-07 18:52:18.873037+03	t	Санкт-Петербург	Добролюбова	28		57		19	
93	2019-01-07 18:52:19.1631+03	2019-01-07 18:52:19.1631+03	t	Санкт-Петербург	Большая Пушкарская	11		30		19	
94	2019-01-07 18:52:19.287937+03	2019-01-07 18:52:19.287937+03	t	Санкт-Петербург	Аэродромная	7		121		19	
95	2019-01-07 18:52:19.429324+03	2019-01-07 18:52:19.429324+03	t	Санкт-Петербург	Ленская	28		81		19	
96	2019-01-07 18:52:19.528974+03	2019-01-07 18:52:19.528974+03	t	Санкт-Петербург	Добролюбова	24		95		19	
97	2019-01-07 18:52:19.786371+03	2019-01-07 18:52:19.786371+03	t	Санкт-Петербург	Аэродромная	5		110		19	
98	2019-01-07 18:52:20.035656+03	2019-01-07 18:52:20.035656+03	t	Санкт-Петербург	Большая Пушкарская	39		99		19	
100	2019-01-07 18:52:20.343081+03	2019-01-07 18:52:20.343081+03	t	Санкт-Петербург	Аэродромная	28		95		19	
103	2019-01-07 18:52:20.999346+03	2019-01-07 18:52:20.999346+03	t	Санкт-Петербург	Аэродромная	37		30		19	
102	2019-01-07 18:52:20.74183+03	2019-01-07 18:52:20.74183+03	t	Санкт-Петербург	Добролюбова	27		81		19	
92	2019-01-07 18:52:19.030085+03	2019-01-07 18:52:19.030085+03	t	Санкт-Петербург	Добролюбова	12		30		19	
101	2019-01-07 18:52:20.575973+03	2019-01-07 18:52:20.575973+03	t	Санкт-Петербург	Большая Пушкарская	12		67		19	
585	2019-01-18 18:35:10.510972+03	2019-01-18 18:35:10.510972+03	t	Санкт-Петербург	Аэродромная	39		110		19	
99	2019-01-07 18:52:20.226781+03	2019-01-07 18:52:20.226781+03	t	Санкт-Петербург	Большая Пушкарская	3		1		19	
104	2019-01-07 18:52:21.265572+03	2019-01-07 18:52:21.265572+03	t	Санкт-Петербург	Ленская	7		110		19	
105	2019-01-07 18:52:21.473266+03	2019-01-07 18:52:21.473266+03	t	Санкт-Петербург	Добролюбова	11		121		19	
578	2019-01-18 18:35:09.205269+03	2019-01-18 18:35:09.205269+03	t	Санкт-Петербург	Большая Пушкарская	24		3		19	
579	2019-01-18 18:35:09.47342+03	2019-01-18 18:35:09.47342+03	t	Санкт-Петербург	Ленская	28		21		19	
580	2019-01-18 18:35:09.589795+03	2019-01-18 18:35:09.589795+03	t	Санкт-Петербург	Большая Пушкарская	11		1		19	
581	2019-01-18 18:35:09.764046+03	2019-01-18 18:35:09.764046+03	t	Санкт-Петербург	Большая Пушкарская	27		57		19	
582	2019-01-18 18:35:09.921907+03	2019-01-18 18:35:09.921907+03	t	Санкт-Петербург	Добролюбова	12		81		19	
583	2019-01-18 18:35:10.113371+03	2019-01-18 18:35:10.113371+03	t	Санкт-Петербург	Аэродромная	39		1		19	
584	2019-01-18 18:35:10.38736+03	2019-01-18 18:35:10.38736+03	t	Санкт-Петербург	Добролюбова	39		15		19	
586	2019-01-18 18:35:10.669952+03	2019-01-18 18:35:10.669952+03	t	Санкт-Петербург	Аэродромная	5		30		19	
587	2019-01-18 18:35:10.885799+03	2019-01-18 18:35:10.885799+03	t	Санкт-Петербург	Аэродромная	3		99		19	
588	2019-01-18 18:35:11.027007+03	2019-01-18 18:35:11.027007+03	t	Санкт-Петербург	Добролюбова	21		17		19	
589	2019-01-18 18:35:11.184892+03	2019-01-18 18:35:11.184892+03	t	Санкт-Петербург	Добролюбова	24		99		19	
590	2019-01-18 18:35:11.326432+03	2019-01-18 18:35:11.326432+03	t	Санкт-Петербург	Ленская	33		1		19	
591	2019-01-18 18:35:11.426211+03	2019-01-18 18:35:11.426211+03	t	Санкт-Петербург	Ленская	39		67		19	
592	2019-01-18 18:35:11.592484+03	2019-01-18 18:35:11.592484+03	t	Санкт-Петербург	Большая Пушкарская	24		17		19	
593	2019-01-18 18:35:32.55762+03	2019-01-18 18:35:32.55762+03	t	Санкт-Петербург	Добролюбова	27		95		19	
594	2019-01-18 18:35:32.759982+03	2019-01-18 18:35:32.759982+03	t	Санкт-Петербург	Ленская	8		3		19	
595	2019-01-18 18:35:32.893116+03	2019-01-18 18:35:32.893116+03	t	Санкт-Петербург	Большая Пушкарская	27		81		19	
596	2019-01-18 18:35:33.050894+03	2019-01-18 18:35:33.050894+03	t	Санкт-Петербург	Аэродромная	3		95		19	
597	2019-01-18 18:35:33.283456+03	2019-01-18 18:35:33.283456+03	t	Санкт-Петербург	Большая Пушкарская	21		21		19	
598	2019-01-18 18:35:33.399881+03	2019-01-18 18:35:33.399881+03	t	Санкт-Петербург	Добролюбова	8		110		19	
599	2019-01-18 18:35:33.558026+03	2019-01-18 18:35:33.558026+03	t	Санкт-Петербург	Ленская	21		3		19	
600	2019-01-18 18:35:33.657409+03	2019-01-18 18:35:33.657409+03	t	Санкт-Петербург	Ленская	33		21		19	
601	2019-01-18 18:35:33.806919+03	2019-01-18 18:35:33.806919+03	t	Санкт-Петербург	Добролюбова	24		17		19	
602	2019-01-18 18:35:33.939902+03	2019-01-18 18:35:33.939902+03	t	Санкт-Петербург	Добролюбова	7		1		19	
603	2019-01-18 18:35:34.097663+03	2019-01-18 18:35:34.097663+03	t	Санкт-Петербург	Ленская	28		30		19	
604	2019-01-18 18:35:34.206036+03	2019-01-18 18:35:34.206036+03	t	Санкт-Петербург	Добролюбова	5		21		19	
605	2019-01-18 18:35:34.322127+03	2019-01-18 18:35:34.322127+03	t	Санкт-Петербург	Добролюбова	21		1		19	
606	2019-01-18 18:35:34.504981+03	2019-01-18 18:35:34.504981+03	t	Санкт-Петербург	Добролюбова	3		121		19	
607	2019-01-18 18:35:34.712601+03	2019-01-18 18:35:34.712601+03	t	Санкт-Петербург	Аэродромная	7		17		19	
624	2019-01-22 02:08:44.154949+03	2019-01-22 02:08:44.154949+03	t	Санкт-Петербург	R	G		V		19	
664	2019-01-22 02:11:37.237676+03	2019-01-22 02:11:37.237676+03	t							19	
665	2019-01-22 02:11:57.75695+03	2019-01-22 02:11:57.75695+03	t							19	
631	2019-01-22 02:09:09.266929+03	2019-01-22 02:09:09.266929+03	t	Санкт-Петербург	R	G		V		19	
666	2019-01-22 02:12:17.574844+03	2019-01-22 02:12:17.574844+03	t	Санкт-Петербург	По	Про		Ти		19	
634	2019-01-22 02:09:19.207242+03	2019-01-22 02:09:19.207242+03	t	Санкт-Петербург	R	G		V		19	
667	2019-01-22 02:13:42.105174+03	2019-01-22 02:13:42.105174+03	t	Санкт-Петербург	По	То		Лл		19	
668	2019-01-22 02:14:34.112116+03	2019-01-22 02:14:34.112116+03	t	Санкт-Петербург	По	То		Лл		19	
669	2019-01-22 02:14:36.373434+03	2019-01-22 02:14:36.373434+03	t	Санкт-Петербург	По	То		Лл		19	
670	2019-01-22 02:14:38.70073+03	2019-01-22 02:14:38.70073+03	t	Санкт-Петербург	По	То		Лл		19	
675	2019-01-23 21:15:44.322495+03	2019-01-23 21:15:44.322495+03	t	Санкт-Петербург	Про	Ног		Прорр		19	
676	2019-01-23 21:36:39.266789+03	2019-01-23 21:36:39.266789+03	t	Санкт-Петербург	П	Р		Ррррр		19	
677	2019-01-23 21:44:09.314536+03	2019-01-23 21:44:09.314536+03	t	Санкт-Петербург	Про	Ролл		Рооо		19	
678	2019-01-23 21:45:31.134725+03	2019-01-23 21:45:31.134725+03	t	Санкт-Петербург	Ими	Им		По		19	
679	2019-01-23 21:51:31.134326+03	2019-01-23 21:51:31.134326+03	t	Санкт-Петербург	Р	Р		Ии		19	
671	2019-01-22 02:14:55.083373+03	2019-01-22 02:14:55.083373+03	t	Санкт-Петербург	По	То		111		19	
680	2019-01-23 21:53:02.884185+03	2019-01-23 21:53:02.884185+03	t	Санкт-Петербург	Пара	Нара		Иас		19	
672	2019-01-22 02:37:24.69203+03	2019-01-22 02:37:24.69203+03	t	Санкт-Петербург	Пор	Ролл		Оллл		19	
681	2019-01-24 02:33:35.158739+03	2019-01-24 02:33:35.158739+03	t	Санкт-Петербург	Про	Оооо		4		19	
673	2019-01-22 16:36:48.965754+03	2019-01-22 16:36:48.965754+03	t	Санкт-Петербург	Им	Ролл		Ро		19	
686	2019-02-09 17:01:05.057511+03	2019-02-09 17:01:05.057511+03	t	Санкт-Петербург	Ggg	Bb		Ggg		19	
674	2019-01-22 16:37:55.141932+03	2019-01-22 16:37:55.141932+03	t	Санкт-Петербург	К	К		К		19	
683	2019-01-24 15:05:38.767689+03	2019-01-24 15:05:38.767689+03	t	Санкт-Петербург	Им	Гол		Гол		19	
682	2019-01-24 03:38:02.584347+03	2019-01-24 03:38:02.584347+03	t	Санкт-Петербург	Что	А		Пап 		19	
684	2019-01-25 22:42:58.786826+03	2019-01-25 22:42:58.786826+03	t	Санкт-Петербург	И	И		И		19	
685	2019-02-06 21:15:26.448745+03	2019-02-06 21:15:26.448745+03	t	Санкт-Петербург	Ggg	Ggg		Ggg		19	
\.


--
-- Name: goods_address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.goods_address_id_seq', 5018, true);


--
-- Data for Name: goods_descriptionline; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.goods_descriptionline (id, line_type, text, goods_id, "order") FROM stdin;
1	1	Как мы делаем уборку?	21	100
2	0	вот так	21	110
3	0	и так	21	120
4	1	Что входит в уборку?	21	0
5	0	раз	21	0
6	0	два	21	0
7	0	три	21	0
\.


--
-- Name: goods_descriptionline_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.goods_descriptionline_id_seq', 7, true);


--
-- Data for Name: goods_displacegoods; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.goods_displacegoods (id, displace_by_id, goods_id) FROM stdin;
1	21	23
2	21	24
3	24	21
4	24	23
5	23	21
6	23	24
7	30	36
8	31	37
9	32	38
10	33	39
11	34	40
12	35	41
13	36	30
14	37	31
15	38	32
16	39	33
17	40	34
18	41	35
\.


--
-- Name: goods_displacegoods_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.goods_displacegoods_id_seq', 18, true);


--
-- Data for Name: goods_goods; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.goods_goods (id, name, image, description_line_1, "order", instance_id, force_to_basket, parent_id, details_button_text, not_display_in_catalog, created_at, is_alive, updated_at, level, lft, rght, tree_id, item_type, price_formula, image_content, description_line_2) FROM stdin;
30	одна створка			10	2	f	26	Как мы моем окна?	f	2018-11-10 22:32:20+03	t	2019-01-23 17:46:30.210709+03	2	3	4	4	1	220.00 * a	\N	\N
31	две створки			20	2	f	26	Как мы моем окна?	f	2018-11-10 22:32:40+03	t	2019-01-23 17:46:30.215772+03	2	5	6	4	1	400.00 * a	\N	\N
32	три створки			30	2	f	26	\N	f	2018-11-10 22:33:11+03	t	2019-01-23 17:46:30.220738+03	2	7	8	4	1	500.00 * a	\N	\N
33	панорамное окно			40	2	f	26	\N	f	2018-11-10 22:33:25+03	t	2019-01-23 17:46:30.225789+03	2	9	10	4	1	350.00 * a	\N	\N
34	балконная дверь			50	2	f	26	\N	f	2018-11-10 22:33:45+03	t	2019-01-23 17:46:30.230933+03	2	11	12	4	1	250.00 * a	\N	\N
35	балконная дверь с окном			60	2	f	26	\N	f	2018-11-10 22:34:02+03	t	2019-01-23 17:46:30.236006+03	2	13	14	4	1	550.00 * a	\N	\N
36	одна створка			10	2	f	27	\N	f	2018-11-10 23:44:04+03	t	2019-01-23 17:46:30.246875+03	2	17	18	4	1	300.00 * a	\N	\N
37	две створки			10	2	f	27	\N	f	2018-11-10 23:44:31+03	t	2019-01-23 17:46:30.251752+03	2	19	20	4	1	480.00 * a	\N	\N
38	три створки			30	2	f	27	\N	f	2018-11-10 23:45:35+03	t	2019-01-23 17:46:30.256827+03	2	21	22	4	1	600.00 * a	\N	\N
39	панорамное окно			40	2	f	27	\N	f	2018-11-10 23:46:13+03	t	2019-01-23 17:46:30.261807+03	2	23	24	4	1	450.00 * a	\N	\N
40	балконная дверь			50	2	f	27	\N	f	2018-11-10 23:46:45+03	t	2019-01-23 17:46:30.266852+03	2	25	26	4	1	400.00 * a	\N	\N
26	пластиковые окна	7.svg	Быстро и качественно, не оставим разводов и царапин	10	2	t	25	Как мы моем окна?	f	2018-11-10 22:29:22+03	t	2019-01-24 00:49:22.559314+03	1	2	15	4	2	1	<?xml version="1.0" encoding="utf-8"?>\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\n<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n\t viewBox="0 0 70.9 70.9" style="enable-background:new 0 0 70.9 70.9;" xml:space="preserve">\n<style type="text/css">\n\t.st0{fill:#66BCEE;}\n</style>\n<g>\n\t<path class="st0" d="M35.5,0.6C16.3,0.6,0.7,16.2,0.7,35.4c0,19.2,15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8\n\t\tC70.4,16.2,54.8,0.6,35.5,0.6z M35.5,67.3C18,67.3,3.7,53,3.7,35.4C3.7,17.9,18,3.6,35.5,3.6c17.6,0,31.8,14.3,31.8,31.8\n\t\tC67.4,53,53.1,67.3,35.5,67.3z"/>\n\t<path class="st0" d="M53.5,45.6V28.7c0-9.8-8-17.8-17.8-17.8c-9.8,0-17.8,8-17.8,17.8v17.6c0,0.3,0.1,0.6,0.2,0.8l-3.5,3.5\n\t\tc-0.7,0.7-0.9,1.6-0.6,2.3c0.3,0.8,1.1,1.2,2.1,1.2h39.3c1,0,1.7-0.4,2.1-1.1c0.4-0.7,0.3-1.6-0.3-2.4L53.5,45.6z M33.6,43.6h-9.9\n\t\tc-0.6,0-1.2,0.1-1.8,0.4V28.7c0-6.8,5.1-12.6,11.7-13.6V43.6z M49.4,43.6H37.7V15.2c6.6,1,11.7,6.8,11.7,13.6V43.6z M18.2,51.3\n\t\tl2.9-2.9h30.4c0.1,0,0.2,0,0.3,0l2.1,2.9L18.2,51.3z"/>\n</g>\n</svg>\n	от 220₽ / окно
41	балконная дверь с окном			60	2	f	27	\N	f	2018-11-10 23:47:22+03	t	2019-01-23 17:46:30.271841+03	2	27	28	4	1	700.00 * a	\N	\N
44	стул без спинки			10	2	f	42	\N	f	2018-11-11 00:08:15.91+03	t	2019-01-23 17:46:30.29414+03	2	3	4	5	1	150.00 * a	\N	\N
45	стул со спинкой			20	2	f	42	\N	f	2018-11-11 00:08:48.016+03	t	2019-01-23 17:46:30.301286+03	2	5	6	5	1	200.00 * a	\N	\N
46	кресло			30	2	f	42	\N	f	2018-11-11 00:09:14.173+03	t	2019-01-23 17:46:30.307961+03	2	7	8	5	1	450.00 * a	\N	\N
47	матрас			40	2	f	42	\N	f	2018-11-11 00:09:46.603+03	t	2019-01-23 17:46:30.313896+03	2	9	10	5	1	500.00 * a	\N	\N
51	кухонный уголок			45	2	f	42	\N	f	2018-11-11 00:12:36.686+03	t	2019-01-23 17:46:30.321219+03	2	11	12	5	1	900.00 * a	\N	\N
48	2-х местный диван			50	2	f	42	\N	f	2018-11-11 00:10:42.324+03	t	2019-01-23 17:46:30.329269+03	2	13	14	5	1	1200.00 * a	\N	\N
49	3-х местный диван			60	2	f	42	\N	f	2018-11-11 00:11:08.999+03	t	2019-01-23 17:46:30.335407+03	2	15	16	5	1	1500.00 * a	\N	\N
50	угловой диван			70	2	f	42	\N	f	2018-11-11 00:11:44.769+03	t	2019-01-23 17:46:30.342123+03	2	17	18	5	1	1800.00 * a	\N	\N
52	«П» образный диван			90	2	f	42	\N	f	2018-11-11 00:13:11.7+03	t	2019-01-23 17:46:30.347901+03	2	19	20	5	1	2500.00 * a	\N	\N
24	уборка после ремонта	3.svg	Уберем и очистим вашу квартиру от строительной пыли и мусора	30	2	t	\N	что входит в уборку?	f	2018-11-10 22:27:45+03	t	2019-01-24 03:02:46.814855+03	0	1	2	3	1	90.00 * a + 1000 * b	<?xml version="1.0" encoding="utf-8"?>\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\n<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n\t viewBox="0 0 70.9 70.9" style="enable-background:new 0 0 70.9 70.9;" xml:space="preserve">\n<style type="text/css">\n\t.st0{fill:#66BCEE;}\n</style>\n<g>\n\t<polygon class="st0" points="55.9,45.2 51.4,45.2 53.9,48.7 58.4,48.7 \t"/>\n\t<path class="st0" d="M35.4,0.6C16.2,0.6,0.6,16.2,0.6,35.4s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.7,0.6,35.4,0.6z M56.5,23.5\n\t\tc0.6,3.2,2,4.8,5.3,5.3c-3.2,0.6-4.8,2.3-5.3,4.9c-0.5-2.7-2-4.3-5.2-4.9C54.5,28.3,56,26.7,56.5,23.5z M39.5,13.6\n\t\tc1.9-1.9,2.6-5,3.8-7.3c0.6,6.2,4.1,10.1,11,10.9c-6.7,0.9-10.5,4.6-11.3,11.5c-0.8-6.7-4.3-10.6-10.7-11.1\n\t\tC34.7,16.3,37.7,15.5,39.5,13.6z M24.2,64.4c-0.5-2.7-2-4.3-5.2-4.9c3.1-0.6,4.6-2.2,5.2-5.4c0.6,3.2,2,4.8,5.3,5.3\n\t\tC26.3,60.1,24.7,61.7,24.2,64.4z M48.8,50.8L48.8,50.8l-4.2-1.6l-13.5-1.2c-1,0.2-1.9,0.3-2.7,0.3c-0.1,0-0.2,0-0.3,0\n\t\tc-0.4,0-0.9,0-1.3,0c-3.3,0-6.8-0.1-10.2-4.8c-1.8-2.5-2.4-4.2-2.9-5.4c-0.1-0.2-0.2-0.5-0.2-0.6c-0.2,0.1-0.3,0.1-0.5,0.1\n\t\tc-0.5,0-0.9-0.2-1.2-0.5l-5-5c-0.3-0.3-0.5-0.7-0.5-1.2c0-0.5,0.2-0.9,0.5-1.2l9.2-9.2c0.7-0.7,1.7-0.7,2.4,0l5,5\n\t\tc0.6,0.6,0.7,1.6,0.1,2.3c1.5,1.4,2.9,2.5,3.4,2.6c0.4,0.1,1.5,0.2,2.7,0.3c3.6,0.4,9.1,0.9,11.5,1.6c2.1,0.7,2.5,0.9,3.3,1.9\n\t\tc0.4,0.4,0.8,1,2,2c3.6,3.1,5.3,5.7,6,6.9l5.1,0.1l5.3,7.5L48.8,50.8z"/>\n\t<path class="st0" d="M44.3,37.8c-1.3-1.1-2.6-2.4-4.2-2.9c-2.1-0.7-7.6-1.2-10.9-1.5c-1.4-0.1-2.4-0.2-2.9-0.3\n\t\tc-1.4-0.2-3.4-1.9-5-3.4l-5.7,5.7c0.3,0.5,0.5,1,0.7,1.6c0.4,1.2,1,2.6,2.5,4.8c2.5,3.6,4.8,3.6,7.9,3.6c0.4,0,0.9,0,1.4,0\n\t\tc2.7,0.1,7.9-1.7,10.4-2.6c-0.1-0.1-0.3-0.2-0.5-0.3c-0.4-0.2-0.9-0.2-1.3-0.3c-1-0.1-1.7-0.3-2.6-0.4c-1,0-1.7,0.1-2.6,0.4\n\t\tc-1.9,0.6-2.7-2.4-0.8-3c1.2-0.3,2.4-0.6,3.6-0.5c1.2,0.1,2.4,0.3,3.7,0.5c1.2,0.2,2.1,0.8,3.1,1.5c0.5,0.4,1,0.7,1.4,1.2\n\t\tc3.4,4.3,7.7,2.9,7,2C48.8,42.9,47.2,40.4,44.3,37.8z"/>\n</g>\n</svg>\n	90₽ / м²
53	стул без спинки			10	2	f	43	\N	f	2018-11-11 00:30:34.272+03	t	2019-01-23 17:46:30.360381+03	2	23	24	5	1	250.00 * a	\N	\N
54	стул со спинкой			20	2	f	43	\N	f	2018-11-11 00:31:02.458+03	t	2019-01-23 17:46:30.366885+03	2	25	26	5	1	300.00 * a	\N	\N
55	кресло			30	2	f	43	\N	f	2018-11-11 00:31:41.488+03	t	2019-01-23 17:46:30.375079+03	2	27	28	5	1	900.00 * a	\N	\N
56	кухонный уголок			40	2	f	43	\N	f	2018-11-11 00:32:17.606+03	t	2019-01-23 17:46:30.380408+03	2	29	30	5	1	1400.00 * a	\N	\N
57	2-х местный диван			50	2	f	43	\N	f	2018-11-11 00:32:50.944+03	t	2019-01-23 17:46:30.386754+03	2	31	32	5	1	1700.00 * a	\N	\N
58	3-х местный диван			70	2	f	43	\N	f	2018-11-11 00:33:20.219+03	t	2019-01-23 17:46:30.392738+03	2	33	34	5	1	2000.00 * a	\N	\N
59	3-х местный диван			70	2	f	43	\N	f	2018-11-11 00:33:34.496+03	f	2019-01-23 17:46:30.399661+03	2	35	36	5	1	2000.00 * a	\N	\N
60	«П» образный диван			80	2	f	43	\N	f	2018-11-11 00:35:31.835+03	t	2019-01-23 17:46:30.404947+03	2	37	38	5	1	3200.00 * a	\N	\N
29	дополнительно			60	2	f	\N	\N	t	2018-11-10 22:31:17+03	t	2019-01-23 17:46:30.407563+03	0	1	12	6	2	\N	\N	\N
61	чистка духовки внутри			10	2	f	29	\N	f	2018-11-11 00:44:57.348+03	t	2019-01-23 17:46:30.412603+03	1	2	3	6	1	450.00 * a	\N	\N
62	чистка холодильника внутри			20	2	f	29	\N	f	2018-11-11 00:45:38+03	t	2019-01-23 17:46:30.41778+03	1	4	5	6	1	450.00 * a	\N	\N
63	чистка микроволновой печи изнутри			30	2	f	29	\N	f	2018-11-11 00:46:24.588+03	t	2019-01-23 17:46:30.422778+03	1	6	7	6	1	350.00 * a	\N	\N
64	чистка кухонного шкафа изнутри			40	2	f	29	\N	f	2018-11-11 00:47:01.452+03	t	2019-01-23 17:46:30.428678+03	1	8	9	6	1	550.00 * a	\N	\N
65	глажка белья			50	2	f	29	\N	f	2018-11-11 00:48:22.472+03	t	2019-01-23 17:46:30.434695+03	1	10	11	6	1	500.00 * a	\N	\N
42	мебель / текстиль	9.svg	\N	10	2	t	28	\N	f	2018-11-11 00:06:51+03	t	2019-01-24 02:07:09.817397+03	1	2	21	5	2	\N	<?xml version="1.0" encoding="utf-8"?>\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\n<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n\t viewBox="0 0 70.9 70.9" style="enable-background:new 0 0 70.9 70.9;" xml:space="preserve">\n<style type="text/css">\n\t.st0{fill:#6CBBE8;}\n</style>\n<path class="st0" d="M35.4,3.7c17.6,0,31.8,14.3,31.8,31.8S53,67.3,35.4,67.3S3.6,53.1,3.6,35.5S17.9,3.7,35.4,3.7 M35.4,0.7\n\tC16.2,0.7,0.6,16.3,0.6,35.5s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.7,0.7,35.4,0.7L35.4,0.7z M44.4,45.9\n\tc0.4-0.4,0.7-0.8,1.1-1.1c0.6-0.7,0.7-1.6,0.1-2.3c-0.2-0.3-0.4-0.5-0.6-0.8l-1.2-1.5l0.4-0.5c0.5-0.6,0.9-1.1,1.4-1.7\n\tc0.6-0.8,0.6-1.6,0-2.4l-1.8-2.2l0.5-0.6c0.4-0.5,0.9-1.1,1.3-1.6c0.7-0.8,0.6-1.8-0.2-2.5c-1.1-0.9-2.2-1.8-3.3-2.7\n\tc-0.7-0.6-1.6-0.6-2.3,0c-0.4,0.4-0.9,0.7-1.3,1.1l-0.8,0.7c0,0-0.1,0-0.1,0.1l-0.5-0.5c-0.4-0.4-0.9-0.8-1.3-1.2\n\tc-0.7-0.7-1.6-0.7-2.4-0.1c-0.4,0.3-0.8,0.7-1.2,1l-0.7,0.6c-0.1,0.1-0.2,0.2-0.4,0.3l-0.6-0.5c-0.6-0.5-1.1-0.9-1.6-1.4\n\tc-0.5-0.4-1-0.5-1.5-0.4c-0.4,0.1-0.7,0.3-0.9,0.5c-0.6,0.5-1.1,0.9-1.7,1.4l-0.5,0.4L24,27.4c-0.6-0.5-1.2-1-1.8-1.5\n\tc-0.8-0.7-1.6-0.7-2.4,0L19,26.6c-0.6,0.5-1.3,1.1-2,1.6c-0.5,0.4-0.9,0.8-1.1,1.4l0,0.6l0,0.1c0.2,0.7,0.7,1.1,1.1,1.6\n\tc0.2,0.2,0.4,0.5,0.6,0.7c0.2,0.2,0.3,0.4,0.5,0.6l0.1,0.1l-0.3,0.4c-0.4,0.5-0.8,0.9-1.1,1.4c-0.2,0.3-0.4,0.6-0.6,0.9\n\tc-0.1,0.1-0.2,0.3-0.2,0.4l0,0.1l0,0.4c0.1,0.6,0.5,1.1,0.9,1.5c0.1,0.2,0.3,0.3,0.4,0.5c0.2,0.3,0.5,0.6,0.8,0.9l0.2,0.3l-1.1,1.3\n\tc-0.2,0.3-0.5,0.6-0.7,0.9c-0.1,0.2-0.3,0.4-0.4,0.6c0,0.1-0.1,0.2-0.1,0.3l0,0.1v0.4l0,0.1c0.2,0.6,0.6,1,1,1.4l0.2,0.2\n\tc0.3,0.3,0.6,0.6,0.9,0.9l0.2,0.2L17.8,47c-0.4,0.5-0.8,0.9-1.1,1.4c-0.2,0.2-0.3,0.5-0.5,0.7c-0.1,0.1-0.1,0.2-0.2,0.3l0,0.1v0.7\n\tl0,0.1c0.1,0.1,0.1,0.2,0.2,0.3c0.2,0.3,0.3,0.5,0.6,0.7c0.4,0.4,0.9,0.7,1.3,1.1c0.5,0.4,0.9,0.7,1.3,1.1c1.2,1.1,2,1.1,3.3,0\n\tc0.4-0.4,0.8-0.7,1.3-1c0.1-0.1,0.3-0.2,0.4-0.3l0.7,0.6c0.4,0.3,0.7,0.7,1.1,1c0.8,0.7,1.6,0.7,2.4,0.1c0.2-0.2,0.4-0.3,0.6-0.5\n\tl1.7-1.4l2.1,1.7c0.9,0.8,1.7,0.8,2.6,0l2.1-1.7l0.9,0.8c0.4,0.4,0.9,0.7,1.3,1.1c0.3,0.3,0.7,0.4,1.1,0.4c0.4,0,0.8-0.1,1.1-0.4\n\tc1.1-0.9,2.2-1.8,3.4-2.8c0.5-0.4,0.7-1.1,0.6-1.7c-0.1-0.3-0.3-0.6-0.5-0.8c-0.4-0.5-0.9-1.1-1.3-1.6l-0.4-0.5\n\tc0-0.1,0.1-0.1,0.1-0.2L44.4,45.9z M39.8,39.9c-0.1,0.5,0,0.9,0.4,1.4c0.4,0.5,0.9,1,1.3,1.6l0.4,0.5c0,0,0.1,0.1,0.1,0.1l-1.6,1.7\n\tc-0.8,0.8-0.8,1.7-0.1,2.6l1.6,1.9l-0.1,0.1c-0.3,0.2-0.6,0.5-0.9,0.7c0,0-0.1,0-0.1-0.1c-0.5-0.4-1.1-0.9-1.6-1.3l-0.5-0.4\n\tc-0.8-0.7-1.6-0.7-2.5,0l-0.2,0.2c-0.6,0.5-1.3,1-1.9,1.6c-0.1,0-0.1,0.1-0.1,0.1c0,0,0,0-0.1-0.1c-0.6-0.5-1.1-0.9-1.7-1.4\n\tl-0.4-0.3c-0.4-0.3-0.8-0.5-1.2-0.5c-0.4,0-0.8,0.2-1.2,0.5c-0.5,0.4-1.1,0.9-1.6,1.3l-0.5,0.4l-1.7-1.7c-0.8-0.7-1.6-0.8-2.5-0.1\n\tL21,50.4l-1-0.8c0,0,0.1-0.1,0.1-0.1l1.5-1.8c0.7-0.9,0.7-1.7-0.1-2.6c-0.4-0.4-0.8-0.8-1.2-1.3l-0.4-0.4l1.7-2\n\tc0.8-0.9,0.8-1.7,0-2.6L21,38.2c-0.4-0.5-0.7-0.9-1.1-1.3c-0.1-0.1-0.1-0.1-0.1-0.1c0,0,0,0,0.1-0.1c0.5-0.6,1.1-1.3,1.6-1.9\n\tl0.1-0.1c0.7-0.8,0.7-1.6,0-2.4c-0.4-0.5-0.8-1-1.2-1.4L20,30.2l1-0.8l0.5,0.4c0.5,0.4,1.1,0.9,1.6,1.3c0.9,0.7,1.6,0.7,2.5,0\n\tl2.1-1.8l2.1,1.7c0.9,0.8,1.7,0.8,2.6,0l2-1.7l1.6,1.6c0.9,0.8,1.7,0.9,2.6,0.1c0.7-0.5,1.3-1.1,2-1.6l0.1-0.1l1,0.8l-1.6,1.9\n\tc-0.7,0.9-0.7,1.6,0,2.5l1.8,2.1l-0.4,0.5c-0.5,0.6-1,1.2-1.4,1.7C40,39.2,39.9,39.6,39.8,39.9z M50,16.6c-0.9,0.7-1.7,1.4-2.6,2.1\n\tc-0.2,0.1-0.3,0.3-0.5,0.4c-0.7-0.6-1.3-1.3-2-1.9c-0.2-0.2-0.5-0.4-0.8-0.6c-0.1,0-0.3,0-0.4,0c-0.8,0.7-1.7,1.3-2.5,2\n\tc-0.2,0.2-0.4,0.3-0.6,0.5c-1.1-0.8-2.1-1.7-3.2-2.5c-0.1,0-0.3,0-0.4,0c-0.4,0.3-0.9,0.6-1.3,0.9C35,18,34.5,18.5,33.9,19\n\tc-0.2,0.1-0.3,0.2-0.5,0c-0.6-0.5-1.2-1-1.8-1.5c-0.4-0.3-0.7-0.6-1.1-0.8c-0.2,0-0.3,0-0.5,0c-0.1,0.1-0.2,0.1-0.3,0.2\n\tc-1.2,1-2.3,1.9-3.5,2.9c-0.4,0.3-0.4,0.9-0.1,1.3c0.3,0.4,0.6,0.8,1,1.1c0.1,0.1,0.2,0.1,0.4,0.1c1.2-0.2,2.3,0.1,3.2,0.9\n\tc0.3,0.2,0.6,0.5,0.9,0.7c2.1-2.1,4.3-2.3,6.5-0.2c0.2-0.2,0.5-0.4,0.7-0.6c1.5-1.2,3.5-1.2,4.9,0c1.1,0.9,2.7,2.4,3.8,3.3\n\tc1.8,1.5,2,3.8,0.5,5.6c-0.2,0.3-0.4,0.5-0.7,0.8c0.2,0.3,0.4,0.5,0.6,0.7c1.4,1.7,1.4,3.7,0,5.4c-0.6,0.7-0.6,0.7,0,1.4\n\tc0.6,0.7,1,1.4,1,2.3c0,0.7,0.2,1.1,0.8,1.4c0,0,0.1,0.1,0.1,0.1c0.6,0.5,1,0.5,1.6,0c1-0.8,2-1.7,3-2.5c0.7-0.6,0.8-1,0.2-1.8\n\tc-0.7-0.8-1.4-1.7-2.1-2.5c0.1-0.1,0.2-0.2,0.3-0.3c0.6-0.6,1.2-1.2,1.8-1.9c0.5-0.6,0.6-1,0.1-1.6c-0.2-0.2-0.3-0.4-0.5-0.6\n\tc-0.5-0.7-1.1-1.3-1.6-2c0.7-0.9,1.4-1.7,2.1-2.6c0.5-0.7,0.5-1,0-1.7c-0.7-0.8-1.4-1.7-2.1-2.5c0-0.1,0-0.1,0.1-0.1\n\tc0.7-0.9,1.4-1.7,2.1-2.6c0.5-0.6,0.4-1.1-0.1-1.5c-0.4-0.4-0.9-0.7-1.3-1.1c-0.8-0.6-2-1.8-2.8-2.4C50.3,16.6,50.1,16.6,50,16.6z"\n\t/>\n</svg>\n	\N
25	мытье окон	4.svg	Быстро и без разводов отмоем окна в вашей квартире	40	2	f	\N	\N	f	2018-11-10 22:28:18+03	t	2019-01-24 00:45:02.91592+03	0	1	30	4	0	1	<?xml version="1.0" encoding="utf-8"?>\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\n<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n\t viewBox="0 0 70.9 70.9" style="enable-background:new 0 0 70.9 70.9;" xml:space="preserve">\n<style type="text/css">\n\t.st0{fill:#66BCEE;}\n</style>\n<g>\n\t<path class="st0" d="M35.7,46.5c0.9-2.8,1.9-5.5,2.8-8.2c0.3-0.7,0.5-1.4,0.8-2.1v-4.8c-0.2-0.1-0.4-0.2-0.7-0.3l-3.7-1.4l-0.6-0.2\n\t\tv18.3C35.3,47.6,35.5,47.3,35.7,46.5z"/>\n\t<path class="st0" d="M36.8,0.5C18.7,0.5,4,15.2,4,33.3v27.5c0.9-1.2,1.8-2.4,2.7-3.6C7.3,56.4,8.1,55.7,9,55v-1.8\n\t\tc-0.4-0.3-0.7-0.6-1-0.9c-2.1-2.1-3.2-5-3.1-8.1c0.1-3.5,1.7-6.5,4.2-8.5v-2.3C9,18.8,20.1,6.9,34.3,5.6v14l1.4,0.5l3.6,1.3V5.6\n\t\tc14.2,1.3,25.3,13.2,25.3,27.7v31.4H39.3V59c-0.5,0.3-1,0.5-1.5,0.6c-1.1,0.3-2.2,0.7-3.5,1v4.1h-6.9c0.1,1.7,0.1,3.3,0.2,5h39.5\n\t\tc1.4,0,2.5-1.1,2.5-2.5V33.3C69.6,15.2,54.9,0.5,36.8,0.5z"/>\n\t<path class="st0" d="M15.9,52.6C15.9,52.6,15.9,52.6,15.9,52.6c4.7,0,8.4-3.7,8.5-8.1c0-4.5-3.8-8.3-8.3-8.3c0,0,0,0,0,0\n\t\tc-4.5,0-8.1,3.5-8.2,8C7.7,48.9,11.2,52.6,15.9,52.6z"/>\n\t<path class="st0" d="M50.3,47.4l-2.6-4.8l-0.2,5.5c0,0.5-0.1,1.1-0.2,1.7c-0.2,1.5-0.4,3,0.1,4c0.5,1.1,1.8,1.6,2.8,1.6l0.2,0\n\t\tl0.1,0c0.9-0.1,2.1-0.8,2.4-2.6c0.2-1.1-0.5-2.4-1.8-4.3C50.7,48.2,50.5,47.7,50.3,47.4z"/>\n\t<path class="st0" d="M54.8,48.2c0.6-0.1,1.2-0.7,1.4-1.8c0.2-1.2-2.3-4.5-3.1-6c-0.1,2.3-0.9,5.5-0.3,6.8\n\t\tC53.2,47.9,54.3,48.2,54.8,48.2z"/>\n\t<path class="st0" d="M37.5,56.6c0.1,0,0.1-0.1,0.2-0.1c0.1-0.1,0.3-0.1,0.4-0.2c0.1,0,0.1-0.1,0.2-0.1c0.1-0.1,0.3-0.2,0.4-0.3\n\t\tc0,0,0.1-0.1,0.1-0.1c0.2-0.1,0.4-0.3,0.5-0.4c0,0,0,0,0,0l0,0c0.4-0.4,0.7-0.8,0.9-1.2c1-2.7,1.8-5.5,2.7-8.2l0,0\n\t\tc0,0,0.6-2.1,2.1-3.7c1.1-1.2,2.2-1.8,2.8-2.7c1.4-2.3,1.5-2.6,1.3-3.8c-0.2-1.2-2.1-1-2.1-1l-0.3,0.2c0,0-0.1,0-0.1,0\n\t\tc0.8-1.6,2.1-2.3,3.7-2.3c0.5,0,1,0.1,1.6,0.2c0.2,0.1,1.3,0.4,1.6,0.5c1.8,0.6,3.6,1.2,5.5,1.9c0.4-1.1,0.8-2,1.3-3.2\n\t\tc-8.5-3-16.9-6-25.4-9.1c-0.4,1.3-0.7,2.1-1.1,3.2c0.7,0.3,1.4,0.5,2.1,0.8c1.2,0.5,2.4,0.9,3.6,1.3l0,0c0.1,0,0.2,0.1,0.3,0.1\n\t\tc3.7,1.3,4.5,3.2,3.2,7c0,0,0,0,0,0.1c-0.9,0.4-1.6,0.9-1.5,1.5c0.1,1.6,0,2.6-0.2,3.2c-0.1,0.2-0.2,0.5-0.3,0.7c0,0,0,0,0,0l0,0\n\t\tc-0.8,2.2-1.6,4.3-2.2,6.5c-0.7,2.1-1.9,3-4,3.5c-6.7,1.4-13.3,2.7-19.9,4.5c-2.1,0.6-4.3,2-5.7,3.7c-0.9,1.1-1.7,2.2-2.6,3.3\n\t\tc0,0.1-0.1,0.1-0.1,0.2c-1.7,2.3-3.4,4.7-5.1,7.1h5.2h18.2c-0.1-1.7-0.1-3.3-0.2-5l0,0c0-1-0.1-2-0.1-3c-0.1-1.7,0.6-2.3,2.2-2.6\n\t\tc3.5-0.6,7-1.4,10.4-2.4C37.1,56.7,37.3,56.7,37.5,56.6z"/>\n</g>\n</svg>\n	от 220₽ / окно
23	генеральная уборка	2.svg	До блеска и неузнаваемости отмоем вашу квартиру	20	2	t	\N	что входит в уборку?	f	2018-11-09 21:49:58+03	t	2019-01-24 03:02:40.510788+03	0	1	2	2	1	90.00 * a + 1000 * b	<?xml version="1.0" encoding="utf-8"?>\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\n<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n\t viewBox="0 0 70.9 70.9" style="enable-background:new 0 0 70.9 70.9;" xml:space="preserve">\n<style type="text/css">\n\t.st0{fill:#65bcf1;}\n</style>\n<path class="st0" d="M59.7,57.2L59.7,57.2l-0.3,0.1c-1.3,0.5-3.5,0.8-5.3,0.3c-1.7-0.5-2.9-1.7-4.3-2.9c-0.5-0.5-1.1-1-1.5-1.4\n\tL43,49l8.4,2.2c0.5,0.1,1.3,0.3,2.1,0.5c3.6,0.8,5.9,1.3,6.8,2.3C61.6,55.5,60.9,56.6,59.7,57.2z M59.1,46.9c0.8-0.4,1-1.2,0-2.3\n\tc-1-1.1-8.3-2.3-11.1-3c2.4,1.9,4.9,5,7.2,5.7C56.5,47.7,58.5,47.2,59.1,46.9z M36.9,57.1c1.4,0.4,3.4,0,4.1-0.4\n\tc0.9-0.4,1-1.3,0-2.4c-1-1.1-8.7-2.4-11.6-3.2C31.9,53.1,34.5,56.3,36.9,57.1z M35.4,0.6c-6.1,0-11.8,1.5-16.7,4.3\n\tc0.9,2.1,1.7,4.1,2.6,6.2c1.5,3.5,2.8,7.2,4.3,10.7c1.1,2.4,1.7,5.2,3.4,5.8c2.2,0.8,5.7,0.8,8,0.7c3.7-0.3,4.1-0.1,8.9-0.2\n\tc1.6,0,2.8,1.1,3.4,2.5c1.6,3.6-1,4.3-2,4.7c-5.7,2.5-11.5,5-17.2,7.4c-5.7,2.7-11.3,5.3-17,7.9c-1,0.4-3.2,2-4.8-1.6\n\tc-0.6-1.4-0.7-3,0.4-4.2c3.3-3.5,3.3-3.9,6-6.5c1.7-1.6,4-4.2,4.8-6.4c0.6-1.7-1-4-2.1-6.4c-1.6-3.5-3.4-6.9-5.1-10.4\n\tc-0.6-1.3-1.2-2.6-1.8-3.9C4.3,17.4,0.6,26,0.6,35.4c0,19.2,15.6,34.8,34.8,34.8c5.9,0,11.5-1.5,16.4-4.1L12.6,52.9l37.1-17l19.9,6\n\tc0.4-2.1,0.6-4.3,0.6-6.5C70.3,16.2,54.7,0.6,35.4,0.6z M51.9,66.2C51.8,66.2,51.8,66.2,51.9,66.2L51.9,66.2\n\tC51.8,66.2,51.8,66.2,51.9,66.2z M17.8,17c0.2,0,0.5-0.1,0.7-0.2c0.2-0.1,0.4-0.3,0.6-0.4c1-0.7,1.1-1.5,0.5-2.9c-1-2.4-2-4.8-3-7.2\n\tc0,0,0-0.1,0-0.1c0.7-0.5,1.4-0.9,2.2-1.3c-2.3,1.3-4.4,2.8-6.4,4.5c1,2,1.9,4.1,2.9,6.1C15.9,16.8,16.6,17.2,17.8,17z M12.4,9.3\n\tC12.4,9.3,12.3,9.3,12.4,9.3C12.3,9.3,12.4,9.3,12.4,9.3C12.4,9.3,12.4,9.3,12.4,9.3z M10.4,11.2c0.6-0.6,1.2-1.2,1.9-1.8\n\tC11.7,9.9,11.1,10.5,10.4,11.2z M29.5,40.2L29.5,40.2c5.8-2.4,11.5-4.9,17.3-7.3c0.9-0.4,0.7-1.1,0.4-1.7c-0.5-1.1-2.6-0.1-4,0.5\n\tc-5,2.2-10.1,4.3-15.2,6.5c-5,2.3-9.9,4.7-14.9,7c-1.3,0.6-3.5,1.6-3,2.7c0.2,0.5,0.6,1.2,1.5,0.8c5.6-2.7,11.3-5.3,17-8l0,0\n\tc0.2-0.1,0.3-0.1,0.5-0.2C29.2,40.3,29.4,40.3,29.5,40.2z M43.2,29.7c-1,0-9,0.3-11.4,0.3c-0.6,0-1.2,0.1-1.9,0.1\n\tC27.8,30,26.5,29,25.7,27c-1-2.4-2-4.7-3.1-7c-0.6-1.2-1.4-1.6-2.8-1.2c-0.1,0-0.2,0.1-0.3,0.1c-0.1,0.1-0.2,0.1-0.3,0.2\n\tc-1.2,0.8-1.5,1.6-0.9,2.9c1,2.3,2,4.7,3.1,7c0.9,2,0.9,3.6-0.5,5.2c-0.4,0.5-0.9,0.9-1.3,1.3c-1.6,1.8-7.2,7.5-7.8,8.3\n\tc-0.7,0.8,1.3-0.5,4.3-1.8c2.7-1.2,7-3.3,11-5.2l0,0c0.2-0.1,0.3-0.1,0.5-0.2c0.2-0.1,0.3-0.1,0.5-0.2l0,0c4.1-1.7,8.5-3.5,11.2-4.8\n\tC42,30.4,44.2,29.7,43.2,29.7z M69.7,42"/>\n</svg>\n	90₽ / м²
27	деревянные окна	WIND_qOjhFu4.svg	Быстро и качественно, не оставим разводов и царапин	20	2	t	25	Как мы моем окна?	f	2018-11-10 22:29:56+03	t	2019-01-24 00:59:23.602001+03	1	16	29	4	2	\N	<?xml version="1.0" encoding="utf-8"?>\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\n<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n\t viewBox="0 0 70.9 70.9" style="enable-background:new 0 0 70.9 70.9;" xml:space="preserve">\n<style type="text/css">\n\t.st0{fill:#65BCF1;}\n</style>\n<path class="st0" d="M35.7,0.7C16.4,0.7,0.8,16.3,0.8,35.5s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.9,0.7,35.7,0.7z M35.7,67.3\n\tC18.1,67.3,3.9,53,3.9,35.5c0-17.6,14.3-31.8,31.8-31.8c17.6,0,31.8,14.3,31.8,31.8C67.5,53.1,53.2,67.3,35.7,67.3z M53.4,45.4\n\tc-0.3-0.4-0.7-0.8-1.2-1.1V14.6h-1h-3.3H25.3H22h-1v29.9c-0.3,0.2-0.5,0.4-0.7,0.6l-5.7,5.7c-0.7,0.7-0.9,1.6-0.6,2.3\n\tc0.3,0.8,1.1,1.2,2.1,1.2h39.3c1,0,1.7-0.4,2.1-1.1c0.4-0.7,0.3-1.6-0.3-2.4L53.4,45.4z M25.3,29h9.2v14.6h-9.2V29z M38.8,29H48\n\tv14.6h-9.2V29z M47.9,24.7h-9.2v-5.9h9.2V24.7z M34.5,18.9v5.9h-9.2v-5.9H34.5z M18.3,51.3l4.2-4.2c0.3-0.3,1-0.6,1.4-0.6H50\n\tc0.3,0,0.8,0.3,1,0.5l3,4.2L18.3,51.3z"/>\n</svg>\n	от 300₽ / окно
43	мебель / кожа	11.svg	\N	20	2	t	28	\N	f	2018-11-11 00:07:16+03	t	2019-01-24 02:07:18.68377+03	1	22	39	5	2	\N	<?xml version="1.0" encoding="utf-8"?>\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\n<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n\t viewBox="0 0 70.9 70.9" style="enable-background:new 0 0 70.9 70.9;" xml:space="preserve">\n<style type="text/css">\n\t.st0{fill:#66BCEE;}\n</style>\n<path class="st0" d="M35.6,3.6c17.6,0,31.8,14.3,31.8,31.8S53.2,67.3,35.6,67.3S3.7,53,3.7,35.4S18,3.6,35.6,3.6 M35.6,0.6\n\tC16.3,0.6,0.7,16.2,0.7,35.4s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.8,0.6,35.6,0.6L35.6,0.6z M40.5,15.6\n\tc1.2,4.5,4.3,6.4,8.7,5c1.2-0.4,2-0.1,2.8,0.7c1.5,1.5,2.5,3.2,3,5.3c0.1,0.5,0,1.1-0.3,1.5c-4.5,5.4-4.6,13.3,0,18.7\n\tc0.5,0.6,0.5,1,0.4,1.7c-0.6,2.1-1.7,3.9-3.3,5.5c-0.6,0.6-1.3,0.8-2.2,0.5c-2.5-0.7-5-0.8-7.5-0.1c-2.6,0.8-4.4,2.5-5.6,5\n\tc-0.2,0.3-0.6,0.8-0.8,0.8c-0.3-0.1-0.8-0.4-0.9-0.8c-2.3-4.7-6.5-5.8-11-5.3c-0.7,0.1-1.4,0.1-2,0.4c-1,0.4-1.6,0.1-2.3-0.6\n\tc-1.6-1.5-2.7-3.3-3.2-5.4c-0.2-0.7-0.1-1.2,0.4-1.7c4.5-5.2,4.5-13.4,0-18.7c-0.3-0.3-0.5-0.9-0.4-1.3c0.6-2.4,1.7-4.5,3.8-6.1\n\tc0.4-0.3,1-0.3,1.5-0.3c1.2,0.1,2.5,0.6,3.7,0.5c2.4,0,4.1-1.5,5-3.6c0.2-0.5,0.5-1,0.5-1.5c0-2,3.4-2.7,5-2.7\n\tC37.1,13.2,40.5,13.6,40.5,15.6z M49.4,51.2c1.1,0.1,1.4-0.9,1.9-1.6c0.4-0.6,0.7-1.1,0.1-1.9c-2.2-3-3.1-6.3-3.2-10\n\tc0-3.8,0.9-7.4,3.2-10.5c0.5-0.7,0.2-1.3-0.2-1.9c-0.9-1.7-1.1-1.6-2.9-1.4c-1.3,0.2-2.6,0.2-3.8,0c-3.5-0.7-5.5-3.1-6.8-6.3\n\tc-0.2-0.4-0.4-1-0.7-1.1c-0.9-0.2-2-0.3-2.9,0c-0.4,0.1-0.8,0.9-1,1.5c-1.9,4.7-5.9,6.9-10.8,5.9c-1.2-0.3-1.8,0.1-2.3,1\n\tc-0.5,0.9-0.9,1.6-0.2,2.7c4,5.8,4,14,0,19.8c-0.8,1.2-0.1,1.9,0.3,2.8c0.5,0.9,1.1,1.2,2.1,1c4.6-0.8,8.9-0.2,12.4,3.1\n\tc0.5,0.5,1,0.5,1.5,0c2.6-2.5,5.8-3.4,9.3-3.4C46.9,50.9,48.2,51.1,49.4,51.2z M35.6,52.8c-3.9-3.2-8.3-4-13.1-3.2\n\tc-0.3,0-0.8-0.1-1-0.4c-0.2-0.3-0.1-0.8,0-1.1c4.1-7.1,4.1-14.1,0-21.1c-0.2-0.3-0.2-0.9,0-1c0.3-0.2,0.8-0.4,1.2-0.3\n\tc2.3,0.4,4.6,0.2,6.7-0.9c2.6-1.3,4.2-3.4,5.3-6c0.2-0.4,0.3-0.8,0.9-0.8c0.6,0,0.8,0.3,1,0.8c1.6,3.7,4.1,6.3,8.2,6.9\n\tc1.2,0.2,2.5,0,3.7,0c0.4,0,1,0.1,1.1,0.3c0.2,0.2,0,0.8-0.2,1.2c-3.9,6.8-4,13.6-0.1,20.5c0.3,0.5,0.8,0.8,0.3,1.6\n\tc-0.5,0.7-1,0.3-1.5,0.2C43.6,48.9,39.3,49.6,35.6,52.8z"/>\n</svg>\n	\N
66	ковер	9_xFBdCYj.svg	\N	30	2	t	28	\N	f	2019-01-24 02:10:49+03	t	2019-01-24 03:02:55.239519+03	1	40	41	5	1	a * 300	<?xml version="1.0" encoding="utf-8"?>\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\n<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n\t viewBox="0 0 70.9 70.9" style="enable-background:new 0 0 70.9 70.9;" xml:space="preserve">\n<style type="text/css">\n\t.st0{fill:#6CBBE8;}\n</style>\n<path class="st0" d="M35.4,3.7c17.6,0,31.8,14.3,31.8,31.8S53,67.3,35.4,67.3S3.6,53.1,3.6,35.5S17.9,3.7,35.4,3.7 M35.4,0.7\n\tC16.2,0.7,0.6,16.3,0.6,35.5s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.7,0.7,35.4,0.7L35.4,0.7z M44.4,45.9\n\tc0.4-0.4,0.7-0.8,1.1-1.1c0.6-0.7,0.7-1.6,0.1-2.3c-0.2-0.3-0.4-0.5-0.6-0.8l-1.2-1.5l0.4-0.5c0.5-0.6,0.9-1.1,1.4-1.7\n\tc0.6-0.8,0.6-1.6,0-2.4l-1.8-2.2l0.5-0.6c0.4-0.5,0.9-1.1,1.3-1.6c0.7-0.8,0.6-1.8-0.2-2.5c-1.1-0.9-2.2-1.8-3.3-2.7\n\tc-0.7-0.6-1.6-0.6-2.3,0c-0.4,0.4-0.9,0.7-1.3,1.1l-0.8,0.7c0,0-0.1,0-0.1,0.1l-0.5-0.5c-0.4-0.4-0.9-0.8-1.3-1.2\n\tc-0.7-0.7-1.6-0.7-2.4-0.1c-0.4,0.3-0.8,0.7-1.2,1l-0.7,0.6c-0.1,0.1-0.2,0.2-0.4,0.3l-0.6-0.5c-0.6-0.5-1.1-0.9-1.6-1.4\n\tc-0.5-0.4-1-0.5-1.5-0.4c-0.4,0.1-0.7,0.3-0.9,0.5c-0.6,0.5-1.1,0.9-1.7,1.4l-0.5,0.4L24,27.4c-0.6-0.5-1.2-1-1.8-1.5\n\tc-0.8-0.7-1.6-0.7-2.4,0L19,26.6c-0.6,0.5-1.3,1.1-2,1.6c-0.5,0.4-0.9,0.8-1.1,1.4l0,0.6l0,0.1c0.2,0.7,0.7,1.1,1.1,1.6\n\tc0.2,0.2,0.4,0.5,0.6,0.7c0.2,0.2,0.3,0.4,0.5,0.6l0.1,0.1l-0.3,0.4c-0.4,0.5-0.8,0.9-1.1,1.4c-0.2,0.3-0.4,0.6-0.6,0.9\n\tc-0.1,0.1-0.2,0.3-0.2,0.4l0,0.1l0,0.4c0.1,0.6,0.5,1.1,0.9,1.5c0.1,0.2,0.3,0.3,0.4,0.5c0.2,0.3,0.5,0.6,0.8,0.9l0.2,0.3l-1.1,1.3\n\tc-0.2,0.3-0.5,0.6-0.7,0.9c-0.1,0.2-0.3,0.4-0.4,0.6c0,0.1-0.1,0.2-0.1,0.3l0,0.1v0.4l0,0.1c0.2,0.6,0.6,1,1,1.4l0.2,0.2\n\tc0.3,0.3,0.6,0.6,0.9,0.9l0.2,0.2L17.8,47c-0.4,0.5-0.8,0.9-1.1,1.4c-0.2,0.2-0.3,0.5-0.5,0.7c-0.1,0.1-0.1,0.2-0.2,0.3l0,0.1v0.7\n\tl0,0.1c0.1,0.1,0.1,0.2,0.2,0.3c0.2,0.3,0.3,0.5,0.6,0.7c0.4,0.4,0.9,0.7,1.3,1.1c0.5,0.4,0.9,0.7,1.3,1.1c1.2,1.1,2,1.1,3.3,0\n\tc0.4-0.4,0.8-0.7,1.3-1c0.1-0.1,0.3-0.2,0.4-0.3l0.7,0.6c0.4,0.3,0.7,0.7,1.1,1c0.8,0.7,1.6,0.7,2.4,0.1c0.2-0.2,0.4-0.3,0.6-0.5\n\tl1.7-1.4l2.1,1.7c0.9,0.8,1.7,0.8,2.6,0l2.1-1.7l0.9,0.8c0.4,0.4,0.9,0.7,1.3,1.1c0.3,0.3,0.7,0.4,1.1,0.4c0.4,0,0.8-0.1,1.1-0.4\n\tc1.1-0.9,2.2-1.8,3.4-2.8c0.5-0.4,0.7-1.1,0.6-1.7c-0.1-0.3-0.3-0.6-0.5-0.8c-0.4-0.5-0.9-1.1-1.3-1.6l-0.4-0.5\n\tc0-0.1,0.1-0.1,0.1-0.2L44.4,45.9z M39.8,39.9c-0.1,0.5,0,0.9,0.4,1.4c0.4,0.5,0.9,1,1.3,1.6l0.4,0.5c0,0,0.1,0.1,0.1,0.1l-1.6,1.7\n\tc-0.8,0.8-0.8,1.7-0.1,2.6l1.6,1.9l-0.1,0.1c-0.3,0.2-0.6,0.5-0.9,0.7c0,0-0.1,0-0.1-0.1c-0.5-0.4-1.1-0.9-1.6-1.3l-0.5-0.4\n\tc-0.8-0.7-1.6-0.7-2.5,0l-0.2,0.2c-0.6,0.5-1.3,1-1.9,1.6c-0.1,0-0.1,0.1-0.1,0.1c0,0,0,0-0.1-0.1c-0.6-0.5-1.1-0.9-1.7-1.4\n\tl-0.4-0.3c-0.4-0.3-0.8-0.5-1.2-0.5c-0.4,0-0.8,0.2-1.2,0.5c-0.5,0.4-1.1,0.9-1.6,1.3l-0.5,0.4l-1.7-1.7c-0.8-0.7-1.6-0.8-2.5-0.1\n\tL21,50.4l-1-0.8c0,0,0.1-0.1,0.1-0.1l1.5-1.8c0.7-0.9,0.7-1.7-0.1-2.6c-0.4-0.4-0.8-0.8-1.2-1.3l-0.4-0.4l1.7-2\n\tc0.8-0.9,0.8-1.7,0-2.6L21,38.2c-0.4-0.5-0.7-0.9-1.1-1.3c-0.1-0.1-0.1-0.1-0.1-0.1c0,0,0,0,0.1-0.1c0.5-0.6,1.1-1.3,1.6-1.9\n\tl0.1-0.1c0.7-0.8,0.7-1.6,0-2.4c-0.4-0.5-0.8-1-1.2-1.4L20,30.2l1-0.8l0.5,0.4c0.5,0.4,1.1,0.9,1.6,1.3c0.9,0.7,1.6,0.7,2.5,0\n\tl2.1-1.8l2.1,1.7c0.9,0.8,1.7,0.8,2.6,0l2-1.7l1.6,1.6c0.9,0.8,1.7,0.9,2.6,0.1c0.7-0.5,1.3-1.1,2-1.6l0.1-0.1l1,0.8l-1.6,1.9\n\tc-0.7,0.9-0.7,1.6,0,2.5l1.8,2.1l-0.4,0.5c-0.5,0.6-1,1.2-1.4,1.7C40,39.2,39.9,39.6,39.8,39.9z M50,16.6c-0.9,0.7-1.7,1.4-2.6,2.1\n\tc-0.2,0.1-0.3,0.3-0.5,0.4c-0.7-0.6-1.3-1.3-2-1.9c-0.2-0.2-0.5-0.4-0.8-0.6c-0.1,0-0.3,0-0.4,0c-0.8,0.7-1.7,1.3-2.5,2\n\tc-0.2,0.2-0.4,0.3-0.6,0.5c-1.1-0.8-2.1-1.7-3.2-2.5c-0.1,0-0.3,0-0.4,0c-0.4,0.3-0.9,0.6-1.3,0.9C35,18,34.5,18.5,33.9,19\n\tc-0.2,0.1-0.3,0.2-0.5,0c-0.6-0.5-1.2-1-1.8-1.5c-0.4-0.3-0.7-0.6-1.1-0.8c-0.2,0-0.3,0-0.5,0c-0.1,0.1-0.2,0.1-0.3,0.2\n\tc-1.2,1-2.3,1.9-3.5,2.9c-0.4,0.3-0.4,0.9-0.1,1.3c0.3,0.4,0.6,0.8,1,1.1c0.1,0.1,0.2,0.1,0.4,0.1c1.2-0.2,2.3,0.1,3.2,0.9\n\tc0.3,0.2,0.6,0.5,0.9,0.7c2.1-2.1,4.3-2.3,6.5-0.2c0.2-0.2,0.5-0.4,0.7-0.6c1.5-1.2,3.5-1.2,4.9,0c1.1,0.9,2.7,2.4,3.8,3.3\n\tc1.8,1.5,2,3.8,0.5,5.6c-0.2,0.3-0.4,0.5-0.7,0.8c0.2,0.3,0.4,0.5,0.6,0.7c1.4,1.7,1.4,3.7,0,5.4c-0.6,0.7-0.6,0.7,0,1.4\n\tc0.6,0.7,1,1.4,1,2.3c0,0.7,0.2,1.1,0.8,1.4c0,0,0.1,0.1,0.1,0.1c0.6,0.5,1,0.5,1.6,0c1-0.8,2-1.7,3-2.5c0.7-0.6,0.8-1,0.2-1.8\n\tc-0.7-0.8-1.4-1.7-2.1-2.5c0.1-0.1,0.2-0.2,0.3-0.3c0.6-0.6,1.2-1.2,1.8-1.9c0.5-0.6,0.6-1,0.1-1.6c-0.2-0.2-0.3-0.4-0.5-0.6\n\tc-0.5-0.7-1.1-1.3-1.6-2c0.7-0.9,1.4-1.7,2.1-2.6c0.5-0.7,0.5-1,0-1.7c-0.7-0.8-1.4-1.7-2.1-2.5c0-0.1,0-0.1,0.1-0.1\n\tc0.7-0.9,1.4-1.7,2.1-2.6c0.5-0.6,0.4-1.1-0.1-1.5c-0.4-0.4-0.9-0.7-1.3-1.1c-0.8-0.6-2-1.8-2.8-2.4C50.3,16.6,50.1,16.6,50,16.6z"\n\t/>\n</svg>\n	\N
28	химчистка	5.svg	Профессиональная химчистка мягкой мебели и ковров	50	2	f	\N	\N	f	2018-11-10 22:30:35+03	t	2019-01-24 02:12:42.656641+03	0	1	42	5	0	\N	<?xml version="1.0" encoding="utf-8"?>\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\n<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n\t viewBox="0 0 70.9 70.9" style="enable-background:new 0 0 70.9 70.9;" xml:space="preserve">\n<style type="text/css">\n\t.st0{fill:#66BCEE;}\n</style>\n<path class="st0" d="M35.1,59.5c-5.8,0-11.5,0-17.3,0l-2.5,0c-0.9,0-1.9-0.1-2.8-0.2c-3-0.5-3.6-1.2-3.9-4.3c-0.2-2-0.4-4-0.8-6.3\n\tc-0.2-1-0.9-2.4-2-3c-1.1-0.6-1.9-1.4-2.4-2.6v-1.8c0,0,0-0.1,0-0.1c0.7-2,1.9-2.9,3.8-3l0.1,0c1.2,0,2.7,0.3,3.7,2.8\n\tc0.3,0.7,0.6,1.5,1,2.2c0.4,0.9,0.8,1.8,1.1,2.6c0.7,1.7,1.8,2.5,3.3,2.5c0.4,0,0.9-0.1,1.4-0.2c4.7-1.3,9.7-2.1,14.8-2.3\n\tc0.8,0,1.7-0.1,2.5-0.1c5.7,0,11.4,0.8,17.1,2.4c0.5,0.1,0.9,0.2,1.2,0.2c0.8,0,2.3-0.3,3.1-2.3c0.8-2,1.7-3.9,2.6-5.7\n\tc0.7-1.4,1.8-2.2,3.3-2.2c0.3,0,0.6,0,0.9,0.1c1.5,0.3,2.4,1.1,3.2,2.8v2c-0.5,1.2-1.3,2-2.4,2.6c-1.2,0.6-1.9,2-2.1,3.1\n\tc-0.4,2.3-0.6,4.6-0.8,6.5c-0.2,2.2-0.8,2.9-1.7,3.3c-1.3,0.6-2.4,0.8-3.3,0.8C50,59.4,43.3,59.5,35.1,59.5z M15.5,64.4\n\tc-2-0.2-3.9-0.5-5.9-0.7c-0.3,0-0.9,0.4-0.9,0.7c-0.8,3.1,0.8,5.2,4,5.4c0.9-0.2,1.9-0.2,2.7-0.6c1.3-0.7,1.2-2.1,1.2-3.4\n\tC16.8,64.9,16.4,64.5,15.5,64.4z M53.6,64.6c0,1.9-0.5,3.7,1.5,4.8c0.9,0.5,4.6,0.3,5.2-0.3c1.6-1.6,1.7-3.4,1-5.5\n\tC58.7,63.9,56.2,64.2,53.6,64.6z M61.8,25.6c-0.8-2.2-2.2-3.4-4.2-3.5c-3.4-0.3-7-0.5-10.6-0.5c-0.6,0-1.2,0-1.7,0\n\tc-0.6,1.3-0.4,1.6,0.1,2.2c0,0,0,0.1,0.1,0.1l0.9-0.6l1.6,1l3.6,2.2l0.2,0.1l0.1,0.1c1,0.8,2.2,1.7,2.3,3.5c0.1,1.3-0.4,2.3-1.2,3.9\n\tc-0.3,0.6-0.7,1.5-1.2,2.5c-1,2.1-3.7,2.3-4.5,2.3c-0.7,0-1.4-0.1-1.9-0.4c-3.7-1.6-7.5-3.2-10.7-4.6l0,0l0,0\n\tc-3.3-1.5-6.9-3.2-10.5-4.9c-1.6-0.8-3.9-3.7-2.9-6.1c0.2-0.4,0.3-0.8,0.5-1.1c-2.5,0-5,0.1-7.3,0.1c-3.3,0-5.1,1.3-6,4\n\tc-0.6,2-0.6,4-0.5,6.2c3.5,0.2,6.2,1.7,8,4.5c0.6,0.9,1,1.8,1.4,2.7c0.3,0.6,0.5,1.2,0.8,1.7c0,0,0.1,0.1,0.2,0.2\n\tc1.3-0.1,2.6-0.3,3.8-0.5c2.4-0.3,4.8-0.7,7.2-0.7c1.3,0,2.6-0.1,3.9-0.1c3.9,0,7.8,0.2,11.4,0.3c2,0.1,3.9,0.4,5.7,0.7\n\tc0.5,0.1,1.1,0.2,1.6,0.2c0.3-0.5,0.6-1,0.8-1.4c0.9-1.5,1.8-3.1,2.9-4.5c1.5-1.9,3.8-2.9,6.8-3.1C62.5,29.8,62.5,27.6,61.8,25.6z\n\t M25.3,26.4c3.5,1.7,7,3.3,10.5,4.9c3.5,1.5,7.1,3.1,10.6,4.5c0.2,0.1,0.5,0.1,0.8,0.1c0.8,0,1.7-0.2,1.8-0.6c2.4-5.1,2.9-4.7,1-6.2\n\tc-1.3-0.8-2.6-1.6-3.6-2.2c-0.5,0.4-0.7,0.4-0.8,0.5c-0.7,2.7-1.1,3.8-2.3,3.8c-0.7,0-1.7-0.4-3.2-1c-1.1-0.5-2.2-1-3.3-1.5\n\tc-1.1-0.5-2.2-1-3.3-1.5c-4-1.8-4-2.1-1.6-5.9c0.1-0.1,0-0.3-0.1-0.9c-1.1-0.3-2.6-0.8-4.1-1.2c-0.5-0.1-0.9-0.1-1.2-0.1\n\tc-1.1,0-1.1,0.9-2.8,5C23.6,24.6,24.5,26,25.3,26.4z M42.7,20.1c1.2-2.6,5.1-12,8.2-19.6h-3.8c-3.1,7.3-6.7,15.7-7.7,18.2\n\tc-0.9,2.1-2,3.4-4.4,3.5c-1.5,0.1-3.1,0.5-3,2.5c1.9,0.9,3.7,1.8,5.6,2.6c1.9,0.8,3.7,1.6,5.7,2.4c1.6-1.2,0.8-2.8-0.2-3.9\n\tC41.5,23.8,41.7,22.1,42.7,20.1z"/>\n</svg>\n	\N
21	поддерживающая уборка	1_0R4WU7Q.svg	Для сохранения постоянной чистоты в вашем доме	10	2	t	\N	что входит в уборку?	f	2018-11-09 21:47:41+03	t	2019-01-25 20:41:40.509732+03	0	1	4	1	1	36.00 * a + 1000 * b	<?xml version="1.0" encoding="utf-8"?>\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\n<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n\t viewBox="0 0 70.9 70.9" style="enable-background:new 0 0 70.9 70.9;" xml:space="preserve">\n<style type="text/css">\n\t.st0{fill:#65bcf1;}\n</style>\n<g>\n\t<path class="st0" d="M51.1,23.6c-0.6,3.2-2.1,4.8-5.2,5.4c3.2,0.6,4.7,2.2,5.2,4.9c0.5-2.7,2.1-4.3,5.3-4.9\n\t\tC53.1,28.4,51.7,26.8,51.1,23.6z"/>\n\t<path class="st0" d="M35.4,0.7C16.2,0.7,0.6,16.3,0.6,35.5s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.7,0.7,35.4,0.7z M29.5,8.6\n\t\tc0.7,3.8,2.4,5.8,6.3,6.4c-3.9,0.8-5.7,2.7-6.3,5.9c-0.6-3.2-2.4-5.1-6.2-5.8C27,14.3,28.8,12.4,29.5,8.6z M40.1,51.4\n\t\tc-3.1,2.3-6.2,4.6-9.4,6.7c-2,1.4-4.3,2.3-6.8,2c-2.5-0.2-4.9-1.2-7-2.5c-2.2-1.3-3.7-3.3-4.2-6c-0.4-2.2,0-4.4,0.9-6.4\n\t\tc1.2-2.7,2.4-5.3,3.6-7.9c1.2-2.6,2.4-5.3,3.6-7.9c0.2-0.4,0.4-0.7,0.7-1.1c0.5-0.7,1.3-0.9,2-0.5c0.7,0.4,1.1,1.1,0.8,2\n\t\tc-0.2,0.6-0.4,1.2-0.7,1.8c-0.9,2.1-1.9,4.2-2.9,6.2c-0.2,0.5-0.1,0.9,0.4,1.2c0.5,0.3,0.9,0,1.2-0.5c0.1-0.2,0.2-0.4,0.2-0.5\n\t\tc1.5-3.3,3.1-6.7,4.6-10c0.3-0.6,0.6-1.1,0.9-1.6c0.5-0.6,1.1-0.8,1.8-0.4c1,0.5,1.3,1.2,0.8,2.3c-1.6,3.5-3.2,6.9-4.8,10.4\n\t\tc-0.4,0.9-0.3,1.5,0.3,1.7c0.8,0.3,1.1-0.3,1.4-0.9c0.4-1,0.9-1.9,1.3-2.9c1.4-3,2.8-6,4.1-9c0.5-1.1,1.4-1.3,2.4-0.8\n\t\tc0.8,0.4,1,1.3,0.5,2.4c-1.8,3.9-3.6,7.9-5.4,11.8c-0.3,0.6-0.4,1.2,0.2,1.5c0.7,0.4,1-0.2,1.3-0.8c1.4-3,2.8-6,4.2-9.1\n\t\tc0.2-0.4,0.4-0.8,0.7-1.1c0.7-0.8,1.9-0.7,2.5,0.1c0.5,0.7,0.4,1.4,0.1,2.1c-2.4,5.2-4.8,10.4-7.2,15.6c-0.2,0.5-0.5,1.1,0.1,1.4\n\t\tc0.3,0.2,1,0.1,1.4-0.1c1.5-0.7,2.9-1.5,4.4-2.2c1-0.5,1.9-0.2,2.4,0.5C41.2,49.7,41,50.7,40.1,51.4z M50.9,50.3\n\t\tc-0.7,1.5-2.4,2.1-3.9,1.5l-3.7-1.7c0-0.8-0.1-1.6-0.6-2.3c-1.1-1.7-3.6-2.3-5.5-1.3c-0.3,0.2-0.6,0.3-0.9,0.4l0,0l1.5-3.4\n\t\tc1.3-2.9,2.7-5.9,4-8.8c1.2-2.7,0.2-4.2-0.8-5c-0.5-0.4-1.1-0.6-1.8-0.9c-0.2-0.1-0.3-0.1-0.5-0.2c0.2-1.5-0.4-2.8-1.7-3.7l3.9-8.5\n\t\tc0.7-1.5,2.4-2.1,3.9-1.5l15.7,7.2c1.5,0.7,2.1,2.4,1.5,3.9L50.9,50.3z"/>\n</g>\n</svg>\n	36₽ / м²
\.


--
-- Name: goods_goods_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.goods_goods_id_seq', 66, true);


--
-- Data for Name: goods_goodsimage; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.goods_goodsimage (id, image, "order", goods_id) FROM stdin;
1	матрас1-1-1024x768.jpg	0	21
2	Кресло1-1.jpg	0	21
3	матрас1-1-1024x768_RnFY6Lk.jpg	0	21
4	Кресло1-1_JiWQgE4.jpg	0	21
\.


--
-- Name: goods_goodsimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.goods_goodsimage_id_seq', 4, true);


--
-- Data for Name: goods_goodsparam; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.goods_goodsparam (id, name, unit, min, max, step, "default", goods_id, formula_letter, "order") FROM stdin;
2	количество	шт.	0	100	1	0	30	a	0
3	количество	шт.	0	100	1	0	36	a	0
4	количество	шт.	0	100	1	0	37	a	0
5	количество	шт.	0	100	1	0	44	a	0
6	количество	шт.	0	100	1	0	53	a	0
7	количество	шт.	0	100	1	0	61	a	0
9	количество	шт.	0	100	1	0	31	a	0
10	количество	шт.	0	100	1	0	45	a	0
11	количество	шт.	0	100	1	0	54	a	0
12	количество	шт.	0	100	1	0	62	a	0
14	количество	шт.	0	100	1	0	32	a	0
15	количество	шт.	0	100	1	0	38	a	0
16	количество	шт.	0	100	1	0	46	a	0
17	количество	шт.	0	100	1	0	55	a	0
18	количество	шт.	0	100	1	0	63	a	0
19	количество	шт.	0	100	1	0	33	a	0
20	количество	шт.	0	100	1	0	39	a	0
21	количество	шт.	0	100	1	0	47	a	0
22	количество	шт.	0	100	1	0	56	a	0
23	количество	шт.	0	100	1	0	64	a	0
24	количество	шт.	0	100	1	0	51	a	0
25	количество	шт.	0	100	1	0	34	a	0
26	количество	шт.	0	100	1	0	40	a	0
27	количество	шт.	0	100	1	0	48	a	0
28	количество	шт.	0	100	1	0	57	a	0
29	количество	час	0	100	1	0	65	a	0
30	количество	шт.	0	100	1	0	35	a	0
31	количество	шт.	0	100	1	0	41	a	0
32	количество	шт.	0	100	1	0	49	a	0
33	количество	шт.	0	100	1	0	50	a	0
34	количество	шт.	0	100	1	0	58	a	0
35	количество	шт.	0	100	1	0	59	a	0
36	количество	шт.	0	100	1	0	60	a	0
37	количество	шт.	0	100	1	0	52	a	0
38	количество сан. узлов	шт.	0	100	1	1	21	b	20
39	количество сан. узлов	шт.	0	100	1	1	23	b	20
40	количество сан. узлов	шт.	0	100	1	1	24	b	20
1	площадь помещения	м²	35	10000	5	35	21	a	10
8	площадь помещения	м²	35	10000	5	35	23	a	10
13	площадь помещения	м²	35	10000	5	35	24	a	10
41	суммарная площадь ковров	м²	5	1000	1	5	66	a	10
\.


--
-- Name: goods_goodsparam_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.goods_goodsparam_id_seq', 41, true);


--
-- Data for Name: goods_order; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.goods_order (id, created_at, updated_at, is_alive, instance_id, address_id, user_id, tmp_id, datetime, confirmation_status, ind) FROM stdin;
2042	2019-01-22 01:56:32.352913+03	2019-01-22 02:01:08.266467+03	t	2	\N	19	jyfoepeigbubeek5f42l1e8o28fnqyju	\N	0	1023
2049	2019-01-22 02:10:27.876989+03	2019-01-22 02:12:17.559832+03	t	2	\N	19	p9krxe11qqle3rxe050rpl2saq9eq0dv	\N	0	1028
2043	2019-01-22 01:57:03.990366+03	2019-01-22 01:57:03.990366+03	t	2	\N	19	gnwxhlkaook5mwdv6g6cp5fqchqx7rdy	\N	0	1019
2050	2019-01-22 02:13:33.414763+03	2019-01-22 02:36:59.793933+03	t	2	671	19	9stvsxx4w9epsinuwp6xuabh4ftvho07	\N	0	1029
2052	2019-01-22 15:59:16.095604+03	2019-01-22 15:59:16.095604+03	t	2	\N	19	aed6m7ccscceofwc6ramlase3yx9la66	\N	0	1031
2062	2019-01-23 21:15:37.577897+03	2019-01-23 21:15:53.569311+03	t	2	675	19	rsno1magu1needef3os2k80tzuxsw7hs	\N	0	1041
2071	2019-01-24 15:05:33.477049+03	2019-01-24 16:06:44.998183+03	t	2	683	19	au458zhdx22mbpwfwp29ohr1ss1xxj5b	\N	0	1050
2074	2019-01-25 22:31:14.230988+03	2019-01-25 22:31:14.230988+03	t	2	\N	19	y4eqmc01wkiacuhttiz4eklkjc695ers	\N	0	1053
2076	2019-01-29 22:48:08.835308+03	2019-01-29 22:48:08.835308+03	t	2	\N	19	wcihdkh4ilkj0an11s5kohjqqc2thrst	\N	0	1055
2081	2019-02-06 21:09:26.150591+03	2019-02-06 21:15:26.478327+03	t	2	685	19	wwic2l20nocoh58abhwrhnd596hfdryr	\N	0	1060
2044	2019-01-22 01:59:48.752297+03	2019-01-22 02:06:13.587471+03	t	2	\N	19	juwfsjjxbj8x0qrkrdz1kxv4g2hcr4rx	\N	0	1021
2051	2019-01-22 02:36:59.306566+03	2019-01-22 15:59:40.63185+03	t	2	672	19	dt8xci7kwrou9ywy708rdnxb3ui6ks4t	\N	0	1030
2053	2019-01-22 15:59:44.579712+03	2019-01-22 15:59:44.579712+03	t	2	\N	19	hh97q4d6ar0cwnl8mcg8b78tycmuw5gq	\N	0	1032
2063	2019-01-23 21:36:28.875985+03	2019-01-23 21:36:39.26898+03	t	2	676	19	j43nt4j37qfsjlmau05ghogixwz38g6f	\N	0	1042
2072	2019-01-24 16:06:44.929659+03	2019-01-24 16:09:33.185231+03	t	2	\N	19	g0sc1dorzw2ejm4tu8cudcxg67suem0n	\N	0	1051
2075	2019-01-25 22:42:26.769126+03	2019-01-25 23:07:53.46349+03	t	2	684	19	dfjz6oxysqaz56cgdmpsjed7jncjjd2e	\N	0	1054
2077	2019-01-30 20:17:49.294065+03	2019-01-30 20:17:49.294065+03	t	2	\N	19	l99jhzpg6x1g1j7kmxya2h31xwuw7bo9	\N	0	1056
2082	2019-02-06 22:57:42.679022+03	2019-02-06 22:57:42.679022+03	t	2	\N	19	9pnxk2hz3hc951qusvl4zqdjesagr6h8	\N	0	1061
2113	2019-02-09 17:01:05.030653+03	2019-02-09 17:01:05.08689+03	t	2	686	19	oqwylupc6obhernmjdk4kw8y8x8jsyw6	\N	0	1063
2045	2019-01-22 02:06:40.218284+03	2019-01-22 02:06:40.218284+03	t	2	\N	19	7xb6em03a0qrc36s60y9zog4a01jbf0u	\N	0	1024
2054	2019-01-22 16:01:03.027121+03	2019-01-22 16:01:03.027121+03	t	2	\N	19	bjn3ufp2b2m5bubfp81910t8y5cixkyk	\N	0	1033
2064	2019-01-23 21:44:00.976188+03	2019-01-23 21:44:09.316941+03	t	2	677	19	say74fvo5z9ebn8t3butl97imhygp6rg	\N	0	1043
2073	2019-01-24 17:03:47.365421+03	2019-01-24 17:05:46.431412+03	t	2	\N	19	lawwnjupywiigf22dx9h05g1ht6cuz0n	\N	0	1052
2078	2019-01-31 19:39:33.703512+03	2019-01-31 19:39:33.703512+03	t	2	\N	19	je4ebwcq5cssg9wjqfnd3wp9cskybb3m	\N	0	1057
2055	2019-01-22 16:02:41.344875+03	2019-01-22 16:02:41.344875+03	t	2	\N	19	3oamqqeawmbukdkzl5nukp7ayne2n27z	\N	0	1034
2046	2019-01-22 02:07:13.62974+03	2019-01-22 02:07:20.470147+03	t	2	\N	19	s413ygpej244b9cuuve8fvt708djkoe0	\N	0	1025
2065	2019-01-23 21:45:23.73215+03	2019-01-23 21:45:31.138132+03	t	2	678	19	2lrnf1ih0dc12682bg94kf12ohftv66y	\N	0	1044
2079	2019-01-31 19:46:56.692552+03	2019-01-31 19:46:56.692552+03	t	2	\N	19	5b5qst0277ux31fg2f4i18m84dumw0eo	\N	0	1058
2047	2019-01-22 02:07:30.119292+03	2019-01-22 02:11:37.208517+03	t	2	\N	19	ef2uftrplvjmkjobuih9rkgqfgocpdxx	\N	0	1026
2056	2019-01-22 16:03:10.090685+03	2019-01-22 16:03:10.090685+03	t	2	\N	19	nxb0roh8js5qcee9cjw9pu5noevxeafj	\N	0	1035
2066	2019-01-23 21:51:27.001725+03	2019-01-23 21:51:31.136291+03	t	2	679	19	3ypsk6wyl2ohvagse68lek1rhc3r2mpp	\N	0	1045
2080	2019-01-31 20:04:15.775318+03	2019-01-31 20:04:15.775318+03	t	2	\N	19	8vnzkec96e6c4vn5i0enj840o0eet7q3	\N	0	1059
2057	2019-01-22 16:04:19.038085+03	2019-01-22 16:04:19.038085+03	t	2	\N	19	uyyevv0u3r2m0kucuhore4t131hm81di	\N	0	1036
2048	2019-01-22 02:08:42.864332+03	2019-01-22 02:09:19.186273+03	t	2	\N	19	elu54ni0qq5dzk5e8e4630img27ilmqp	\N	0	1027
2067	2019-01-23 21:52:57.470168+03	2019-01-24 01:10:13.349941+03	t	2	680	19	hko5sx8ht4dawtv1lzbyv9ic1xqt7apc	\N	0	1046
2058	2019-01-22 16:04:47.540334+03	2019-01-22 16:04:47.540334+03	t	2	\N	19	tqpsdcg2upw0uahw2z2h6hvvfe3i552h	\N	0	1037
2068	2019-01-24 01:10:13.280397+03	2019-01-24 01:10:13.280397+03	t	2	\N	19	dktox6u7e3k4thz7kwafavj2st1fdlte	\N	0	1047
2059	2019-01-22 16:36:44.407164+03	2019-01-22 16:37:05.127157+03	t	2	673	19	q7m1f3d05npwq8gs7w81qw6orwwa1lsz	\N	0	1038
2069	2019-01-24 02:33:10.365287+03	2019-01-24 02:43:34.21136+03	t	2	681	19	x8f0u3cedpn5lg0pf7573purph4pq9gw	\N	0	1048
2060	2019-01-22 16:37:51.564887+03	2019-01-22 16:39:27.719151+03	t	2	674	19	pjf3wzig745szsz5vwnq5qg4obyxi9hk	\N	0	1039
2070	2019-01-24 03:08:11.290721+03	2019-01-24 16:06:44.969745+03	t	2	682	19	0ixcgnovhezdxa5jitfk5uboqo3bnwhq	\N	0	1049
2061	2019-01-22 16:41:40.640293+03	2019-01-22 16:43:28.656718+03	t	2	\N	19	mo1g2ctasyo6jmgpjyo9nprk939p7cqv	\N	0	1040
2039	2019-01-22 01:44:04.064026+03	2019-01-22 01:48:01.779707+03	t	2	5	19	\N	\N	0	1004
2110	2019-02-08 21:31:01.506179+03	2019-02-08 21:31:01.506179+03	t	2	\N	19	6uqomdve3srzoo9hiswhx0rrr20vwwdi	\N	0	1062
2040	2019-01-22 01:46:49.002291+03	2019-01-22 01:48:14.662953+03	t	2	\N	19	\N	\N	0	1005
2041	2019-01-22 01:50:02.229344+03	2019-01-22 02:10:27.770648+03	t	2	\N	19	ra8fjbvorl90byhtyzv7ok6395t4tu1z	\N	0	1022
\.


--
-- Name: goods_order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.goods_order_id_seq', 9683, true);


--
-- Data for Name: goods_orderitem; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.goods_orderitem (id, order_id, goods_id, order_field, goods_dump, is_added_by_user) FROM stdin;
10962	2057	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/1_0R4WU7Q.svg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2019-01-22T13:02:31.843205", "description": "Для сохранения постоянной чистоты в вашем доме", "price_formula": "36.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10995	2065	23	0	{"id": 23, "name": "генеральная уборка", "type": 1, "image": "http://192.168.0.15:8000/media/2.svg", "order": 20, "params": [{"id": 8, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 39, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "updated_at": "2019-01-23T14:46:30.188459", "description": "До блеска и неузнаваемости отмоем вашу квартиру", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#65bcf1;}\\n</style>\\n<path class=\\"st0\\" d=\\"M59.7,57.2L59.7,57.2l-0.3,0.1c-1.3,0.5-3.5,0.8-5.3,0.3c-1.7-0.5-2.9-1.7-4.3-2.9c-0.5-0.5-1.1-1-1.5-1.4\\n\\tL43,49l8.4,2.2c0.5,0.1,1.3,0.3,2.1,0.5c3.6,0.8,5.9,1.3,6.8,2.3C61.6,55.5,60.9,56.6,59.7,57.2z M59.1,46.9c0.8-0.4,1-1.2,0-2.3\\n\\tc-1-1.1-8.3-2.3-11.1-3c2.4,1.9,4.9,5,7.2,5.7C56.5,47.7,58.5,47.2,59.1,46.9z M36.9,57.1c1.4,0.4,3.4,0,4.1-0.4\\n\\tc0.9-0.4,1-1.3,0-2.4c-1-1.1-8.7-2.4-11.6-3.2C31.9,53.1,34.5,56.3,36.9,57.1z M35.4,0.6c-6.1,0-11.8,1.5-16.7,4.3\\n\\tc0.9,2.1,1.7,4.1,2.6,6.2c1.5,3.5,2.8,7.2,4.3,10.7c1.1,2.4,1.7,5.2,3.4,5.8c2.2,0.8,5.7,0.8,8,0.7c3.7-0.3,4.1-0.1,8.9-0.2\\n\\tc1.6,0,2.8,1.1,3.4,2.5c1.6,3.6-1,4.3-2,4.7c-5.7,2.5-11.5,5-17.2,7.4c-5.7,2.7-11.3,5.3-17,7.9c-1,0.4-3.2,2-4.8-1.6\\n\\tc-0.6-1.4-0.7-3,0.4-4.2c3.3-3.5,3.3-3.9,6-6.5c1.7-1.6,4-4.2,4.8-6.4c0.6-1.7-1-4-2.1-6.4c-1.6-3.5-3.4-6.9-5.1-10.4\\n\\tc-0.6-1.3-1.2-2.6-1.8-3.9C4.3,17.4,0.6,26,0.6,35.4c0,19.2,15.6,34.8,34.8,34.8c5.9,0,11.5-1.5,16.4-4.1L12.6,52.9l37.1-17l19.9,6\\n\\tc0.4-2.1,0.6-4.3,0.6-6.5C70.3,16.2,54.7,0.6,35.4,0.6z M51.9,66.2C51.8,66.2,51.8,66.2,51.9,66.2L51.9,66.2\\n\\tC51.8,66.2,51.8,66.2,51.9,66.2z M17.8,17c0.2,0,0.5-0.1,0.7-0.2c0.2-0.1,0.4-0.3,0.6-0.4c1-0.7,1.1-1.5,0.5-2.9c-1-2.4-2-4.8-3-7.2\\n\\tc0,0,0-0.1,0-0.1c0.7-0.5,1.4-0.9,2.2-1.3c-2.3,1.3-4.4,2.8-6.4,4.5c1,2,1.9,4.1,2.9,6.1C15.9,16.8,16.6,17.2,17.8,17z M12.4,9.3\\n\\tC12.4,9.3,12.3,9.3,12.4,9.3C12.3,9.3,12.4,9.3,12.4,9.3C12.4,9.3,12.4,9.3,12.4,9.3z M10.4,11.2c0.6-0.6,1.2-1.2,1.9-1.8\\n\\tC11.7,9.9,11.1,10.5,10.4,11.2z M29.5,40.2L29.5,40.2c5.8-2.4,11.5-4.9,17.3-7.3c0.9-0.4,0.7-1.1,0.4-1.7c-0.5-1.1-2.6-0.1-4,0.5\\n\\tc-5,2.2-10.1,4.3-15.2,6.5c-5,2.3-9.9,4.7-14.9,7c-1.3,0.6-3.5,1.6-3,2.7c0.2,0.5,0.6,1.2,1.5,0.8c5.6-2.7,11.3-5.3,17-8l0,0\\n\\tc0.2-0.1,0.3-0.1,0.5-0.2C29.2,40.3,29.4,40.3,29.5,40.2z M43.2,29.7c-1,0-9,0.3-11.4,0.3c-0.6,0-1.2,0.1-1.9,0.1\\n\\tC27.8,30,26.5,29,25.7,27c-1-2.4-2-4.7-3.1-7c-0.6-1.2-1.4-1.6-2.8-1.2c-0.1,0-0.2,0.1-0.3,0.1c-0.1,0.1-0.2,0.1-0.3,0.2\\n\\tc-1.2,0.8-1.5,1.6-0.9,2.9c1,2.3,2,4.7,3.1,7c0.9,2,0.9,3.6-0.5,5.2c-0.4,0.5-0.9,0.9-1.3,1.3c-1.6,1.8-7.2,7.5-7.8,8.3\\n\\tc-0.7,0.8,1.3-0.5,4.3-1.8c2.7-1.2,7-3.3,11-5.2l0,0c0.2-0.1,0.3-0.1,0.5-0.2c0.2-0.1,0.3-0.1,0.5-0.2l0,0c4.1-1.7,8.5-3.5,11.2-4.8\\n\\tC42,30.4,44.2,29.7,43.2,29.7z M69.7,42\\"/>\\n</svg>\\n", "price_formula": "90.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
11097	2070	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/1_0R4WU7Q.svg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "м²", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "parent_id": 0, "updated_at": "2019-01-24T00:02:28.271328", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#65bcf1;}\\n</style>\\n<g>\\n\\t<path class=\\"st0\\" d=\\"M51.1,23.6c-0.6,3.2-2.1,4.8-5.2,5.4c3.2,0.6,4.7,2.2,5.2,4.9c0.5-2.7,2.1-4.3,5.3-4.9\\n\\t\\tC53.1,28.4,51.7,26.8,51.1,23.6z\\"/>\\n\\t<path class=\\"st0\\" d=\\"M35.4,0.7C16.2,0.7,0.6,16.3,0.6,35.5s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.7,0.7,35.4,0.7z M29.5,8.6\\n\\t\\tc0.7,3.8,2.4,5.8,6.3,6.4c-3.9,0.8-5.7,2.7-6.3,5.9c-0.6-3.2-2.4-5.1-6.2-5.8C27,14.3,28.8,12.4,29.5,8.6z M40.1,51.4\\n\\t\\tc-3.1,2.3-6.2,4.6-9.4,6.7c-2,1.4-4.3,2.3-6.8,2c-2.5-0.2-4.9-1.2-7-2.5c-2.2-1.3-3.7-3.3-4.2-6c-0.4-2.2,0-4.4,0.9-6.4\\n\\t\\tc1.2-2.7,2.4-5.3,3.6-7.9c1.2-2.6,2.4-5.3,3.6-7.9c0.2-0.4,0.4-0.7,0.7-1.1c0.5-0.7,1.3-0.9,2-0.5c0.7,0.4,1.1,1.1,0.8,2\\n\\t\\tc-0.2,0.6-0.4,1.2-0.7,1.8c-0.9,2.1-1.9,4.2-2.9,6.2c-0.2,0.5-0.1,0.9,0.4,1.2c0.5,0.3,0.9,0,1.2-0.5c0.1-0.2,0.2-0.4,0.2-0.5\\n\\t\\tc1.5-3.3,3.1-6.7,4.6-10c0.3-0.6,0.6-1.1,0.9-1.6c0.5-0.6,1.1-0.8,1.8-0.4c1,0.5,1.3,1.2,0.8,2.3c-1.6,3.5-3.2,6.9-4.8,10.4\\n\\t\\tc-0.4,0.9-0.3,1.5,0.3,1.7c0.8,0.3,1.1-0.3,1.4-0.9c0.4-1,0.9-1.9,1.3-2.9c1.4-3,2.8-6,4.1-9c0.5-1.1,1.4-1.3,2.4-0.8\\n\\t\\tc0.8,0.4,1,1.3,0.5,2.4c-1.8,3.9-3.6,7.9-5.4,11.8c-0.3,0.6-0.4,1.2,0.2,1.5c0.7,0.4,1-0.2,1.3-0.8c1.4-3,2.8-6,4.2-9.1\\n\\t\\tc0.2-0.4,0.4-0.8,0.7-1.1c0.7-0.8,1.9-0.7,2.5,0.1c0.5,0.7,0.4,1.4,0.1,2.1c-2.4,5.2-4.8,10.4-7.2,15.6c-0.2,0.5-0.5,1.1,0.1,1.4\\n\\t\\tc0.3,0.2,1,0.1,1.4-0.1c1.5-0.7,2.9-1.5,4.4-2.2c1-0.5,1.9-0.2,2.4,0.5C41.2,49.7,41,50.7,40.1,51.4z M50.9,50.3\\n\\t\\tc-0.7,1.5-2.4,2.1-3.9,1.5l-3.7-1.7c0-0.8-0.1-1.6-0.6-2.3c-1.1-1.7-3.6-2.3-5.5-1.3c-0.3,0.2-0.6,0.3-0.9,0.4l0,0l1.5-3.4\\n\\t\\tc1.3-2.9,2.7-5.9,4-8.8c1.2-2.7,0.2-4.2-0.8-5c-0.5-0.4-1.1-0.6-1.8-0.9c-0.2-0.1-0.3-0.1-0.5-0.2c0.2-1.5-0.4-2.8-1.7-3.7l3.9-8.5\\n\\t\\tc0.7-1.5,2.4-2.1,3.9-1.5l15.7,7.2c1.5,0.7,2.1,2.4,1.5,3.9L50.9,50.3z\\"/>\\n</g>\\n</svg>\\n", "price_formula": "36.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "description_line_1": "Для сохранения постоянной чистоты в вашем доме", "description_line_2": "36₽ / м²", "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
11099	2070	24	0	{"id": 24, "name": "уборка после ремонта", "type": 1, "image": "http://192.168.0.15:8000/media/3.svg", "order": 30, "params": [{"id": 13, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "м²", "default": 35, "formula_letter": "a"}, {"id": 40, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "parent_id": 0, "updated_at": "2019-01-24T00:02:46.814855", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#66BCEE;}\\n</style>\\n<g>\\n\\t<polygon class=\\"st0\\" points=\\"55.9,45.2 51.4,45.2 53.9,48.7 58.4,48.7 \\t\\"/>\\n\\t<path class=\\"st0\\" d=\\"M35.4,0.6C16.2,0.6,0.6,16.2,0.6,35.4s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.7,0.6,35.4,0.6z M56.5,23.5\\n\\t\\tc0.6,3.2,2,4.8,5.3,5.3c-3.2,0.6-4.8,2.3-5.3,4.9c-0.5-2.7-2-4.3-5.2-4.9C54.5,28.3,56,26.7,56.5,23.5z M39.5,13.6\\n\\t\\tc1.9-1.9,2.6-5,3.8-7.3c0.6,6.2,4.1,10.1,11,10.9c-6.7,0.9-10.5,4.6-11.3,11.5c-0.8-6.7-4.3-10.6-10.7-11.1\\n\\t\\tC34.7,16.3,37.7,15.5,39.5,13.6z M24.2,64.4c-0.5-2.7-2-4.3-5.2-4.9c3.1-0.6,4.6-2.2,5.2-5.4c0.6,3.2,2,4.8,5.3,5.3\\n\\t\\tC26.3,60.1,24.7,61.7,24.2,64.4z M48.8,50.8L48.8,50.8l-4.2-1.6l-13.5-1.2c-1,0.2-1.9,0.3-2.7,0.3c-0.1,0-0.2,0-0.3,0\\n\\t\\tc-0.4,0-0.9,0-1.3,0c-3.3,0-6.8-0.1-10.2-4.8c-1.8-2.5-2.4-4.2-2.9-5.4c-0.1-0.2-0.2-0.5-0.2-0.6c-0.2,0.1-0.3,0.1-0.5,0.1\\n\\t\\tc-0.5,0-0.9-0.2-1.2-0.5l-5-5c-0.3-0.3-0.5-0.7-0.5-1.2c0-0.5,0.2-0.9,0.5-1.2l9.2-9.2c0.7-0.7,1.7-0.7,2.4,0l5,5\\n\\t\\tc0.6,0.6,0.7,1.6,0.1,2.3c1.5,1.4,2.9,2.5,3.4,2.6c0.4,0.1,1.5,0.2,2.7,0.3c3.6,0.4,9.1,0.9,11.5,1.6c2.1,0.7,2.5,0.9,3.3,1.9\\n\\t\\tc0.4,0.4,0.8,1,2,2c3.6,3.1,5.3,5.7,6,6.9l5.1,0.1l5.3,7.5L48.8,50.8z\\"/>\\n\\t<path class=\\"st0\\" d=\\"M44.3,37.8c-1.3-1.1-2.6-2.4-4.2-2.9c-2.1-0.7-7.6-1.2-10.9-1.5c-1.4-0.1-2.4-0.2-2.9-0.3\\n\\t\\tc-1.4-0.2-3.4-1.9-5-3.4l-5.7,5.7c0.3,0.5,0.5,1,0.7,1.6c0.4,1.2,1,2.6,2.5,4.8c2.5,3.6,4.8,3.6,7.9,3.6c0.4,0,0.9,0,1.4,0\\n\\t\\tc2.7,0.1,7.9-1.7,10.4-2.6c-0.1-0.1-0.3-0.2-0.5-0.3c-0.4-0.2-0.9-0.2-1.3-0.3c-1-0.1-1.7-0.3-2.6-0.4c-1,0-1.7,0.1-2.6,0.4\\n\\t\\tc-1.9,0.6-2.7-2.4-0.8-3c1.2-0.3,2.4-0.6,3.6-0.5c1.2,0.1,2.4,0.3,3.7,0.5c1.2,0.2,2.1,0.8,3.1,1.5c0.5,0.4,1,0.7,1.4,1.2\\n\\t\\tc3.4,4.3,7.7,2.9,7,2C48.8,42.9,47.2,40.4,44.3,37.8z\\"/>\\n</g>\\n</svg>\\n", "price_formula": "90.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "description_line_1": "Уберем и очистим вашу квартиру от строительной пыли и мусора", "description_line_2": "90₽ / м²", "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
11101	2070	61	0	{"id": 61, "name": "чистка духовки внутри", "type": 1, "image": null, "order": 10, "params": [{"id": 7, "max": 100, "min": 0, "name": "количество", "step": 1, "unit": "шт.", "default": 0, "formula_letter": "a"}], "is_alive": true, "min_price": null, "parent_id": 29, "updated_at": "2019-01-23T14:46:30.412603", "image_content": null, "price_formula": "450.00 * a", "is_slave_goods": true, "force_to_basket": false, "additional_goods": [], "description_lines": [], "description_line_1": "", "description_line_2": null, "details_button_text": null, "not_display_in_catalog": false}	f
11102	2070	62	0	{"id": 62, "name": "чистка холодильника внутри", "type": 1, "image": null, "order": 20, "params": [{"id": 12, "max": 100, "min": 0, "name": "количество", "step": 1, "unit": "шт.", "default": 0, "formula_letter": "a"}], "is_alive": true, "min_price": null, "parent_id": 29, "updated_at": "2019-01-23T14:46:30.417780", "image_content": null, "price_formula": "450.00 * a", "is_slave_goods": true, "force_to_basket": false, "additional_goods": [], "description_lines": [], "description_line_1": "", "description_line_2": null, "details_button_text": null, "not_display_in_catalog": false}	f
11115	2074	23	0	{"id": 23, "name": "генеральная уборка", "type": 1, "image": "http://192.168.0.15:8000/media/2.svg", "order": 20, "params": [{"id": 8, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "м²", "default": 35, "formula_letter": "a"}, {"id": 39, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "parent_id": 0, "updated_at": "2019-01-24T00:02:40.510788", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#65bcf1;}\\n</style>\\n<path class=\\"st0\\" d=\\"M59.7,57.2L59.7,57.2l-0.3,0.1c-1.3,0.5-3.5,0.8-5.3,0.3c-1.7-0.5-2.9-1.7-4.3-2.9c-0.5-0.5-1.1-1-1.5-1.4\\n\\tL43,49l8.4,2.2c0.5,0.1,1.3,0.3,2.1,0.5c3.6,0.8,5.9,1.3,6.8,2.3C61.6,55.5,60.9,56.6,59.7,57.2z M59.1,46.9c0.8-0.4,1-1.2,0-2.3\\n\\tc-1-1.1-8.3-2.3-11.1-3c2.4,1.9,4.9,5,7.2,5.7C56.5,47.7,58.5,47.2,59.1,46.9z M36.9,57.1c1.4,0.4,3.4,0,4.1-0.4\\n\\tc0.9-0.4,1-1.3,0-2.4c-1-1.1-8.7-2.4-11.6-3.2C31.9,53.1,34.5,56.3,36.9,57.1z M35.4,0.6c-6.1,0-11.8,1.5-16.7,4.3\\n\\tc0.9,2.1,1.7,4.1,2.6,6.2c1.5,3.5,2.8,7.2,4.3,10.7c1.1,2.4,1.7,5.2,3.4,5.8c2.2,0.8,5.7,0.8,8,0.7c3.7-0.3,4.1-0.1,8.9-0.2\\n\\tc1.6,0,2.8,1.1,3.4,2.5c1.6,3.6-1,4.3-2,4.7c-5.7,2.5-11.5,5-17.2,7.4c-5.7,2.7-11.3,5.3-17,7.9c-1,0.4-3.2,2-4.8-1.6\\n\\tc-0.6-1.4-0.7-3,0.4-4.2c3.3-3.5,3.3-3.9,6-6.5c1.7-1.6,4-4.2,4.8-6.4c0.6-1.7-1-4-2.1-6.4c-1.6-3.5-3.4-6.9-5.1-10.4\\n\\tc-0.6-1.3-1.2-2.6-1.8-3.9C4.3,17.4,0.6,26,0.6,35.4c0,19.2,15.6,34.8,34.8,34.8c5.9,0,11.5-1.5,16.4-4.1L12.6,52.9l37.1-17l19.9,6\\n\\tc0.4-2.1,0.6-4.3,0.6-6.5C70.3,16.2,54.7,0.6,35.4,0.6z M51.9,66.2C51.8,66.2,51.8,66.2,51.9,66.2L51.9,66.2\\n\\tC51.8,66.2,51.8,66.2,51.9,66.2z M17.8,17c0.2,0,0.5-0.1,0.7-0.2c0.2-0.1,0.4-0.3,0.6-0.4c1-0.7,1.1-1.5,0.5-2.9c-1-2.4-2-4.8-3-7.2\\n\\tc0,0,0-0.1,0-0.1c0.7-0.5,1.4-0.9,2.2-1.3c-2.3,1.3-4.4,2.8-6.4,4.5c1,2,1.9,4.1,2.9,6.1C15.9,16.8,16.6,17.2,17.8,17z M12.4,9.3\\n\\tC12.4,9.3,12.3,9.3,12.4,9.3C12.3,9.3,12.4,9.3,12.4,9.3C12.4,9.3,12.4,9.3,12.4,9.3z M10.4,11.2c0.6-0.6,1.2-1.2,1.9-1.8\\n\\tC11.7,9.9,11.1,10.5,10.4,11.2z M29.5,40.2L29.5,40.2c5.8-2.4,11.5-4.9,17.3-7.3c0.9-0.4,0.7-1.1,0.4-1.7c-0.5-1.1-2.6-0.1-4,0.5\\n\\tc-5,2.2-10.1,4.3-15.2,6.5c-5,2.3-9.9,4.7-14.9,7c-1.3,0.6-3.5,1.6-3,2.7c0.2,0.5,0.6,1.2,1.5,0.8c5.6-2.7,11.3-5.3,17-8l0,0\\n\\tc0.2-0.1,0.3-0.1,0.5-0.2C29.2,40.3,29.4,40.3,29.5,40.2z M43.2,29.7c-1,0-9,0.3-11.4,0.3c-0.6,0-1.2,0.1-1.9,0.1\\n\\tC27.8,30,26.5,29,25.7,27c-1-2.4-2-4.7-3.1-7c-0.6-1.2-1.4-1.6-2.8-1.2c-0.1,0-0.2,0.1-0.3,0.1c-0.1,0.1-0.2,0.1-0.3,0.2\\n\\tc-1.2,0.8-1.5,1.6-0.9,2.9c1,2.3,2,4.7,3.1,7c0.9,2,0.9,3.6-0.5,5.2c-0.4,0.5-0.9,0.9-1.3,1.3c-1.6,1.8-7.2,7.5-7.8,8.3\\n\\tc-0.7,0.8,1.3-0.5,4.3-1.8c2.7-1.2,7-3.3,11-5.2l0,0c0.2-0.1,0.3-0.1,0.5-0.2c0.2-0.1,0.3-0.1,0.5-0.2l0,0c4.1-1.7,8.5-3.5,11.2-4.8\\n\\tC42,30.4,44.2,29.7,43.2,29.7z M69.7,42\\"/>\\n</svg>\\n", "price_formula": "90.00 * a + 1000 * b", "displace_goods": [21, 24], "gallery_images": [], "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "description_line_1": "До блеска и неузнаваемости отмоем вашу квартиру", "description_line_2": "90₽ / м²", "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
11116	2074	66	0	{"id": 66, "name": "ковер", "type": 1, "image": "http://192.168.0.15:8000/media/9_xFBdCYj.svg", "order": 30, "params": [{"id": 41, "max": 1000, "min": 5, "name": "суммарная площадь ковров", "step": 1, "unit": "м²", "default": 5, "formula_letter": "a"}], "is_alive": true, "parent_id": 28, "updated_at": "2019-01-24T00:02:55.239519", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#6CBBE8;}\\n</style>\\n<path class=\\"st0\\" d=\\"M35.4,3.7c17.6,0,31.8,14.3,31.8,31.8S53,67.3,35.4,67.3S3.6,53.1,3.6,35.5S17.9,3.7,35.4,3.7 M35.4,0.7\\n\\tC16.2,0.7,0.6,16.3,0.6,35.5s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.7,0.7,35.4,0.7L35.4,0.7z M44.4,45.9\\n\\tc0.4-0.4,0.7-0.8,1.1-1.1c0.6-0.7,0.7-1.6,0.1-2.3c-0.2-0.3-0.4-0.5-0.6-0.8l-1.2-1.5l0.4-0.5c0.5-0.6,0.9-1.1,1.4-1.7\\n\\tc0.6-0.8,0.6-1.6,0-2.4l-1.8-2.2l0.5-0.6c0.4-0.5,0.9-1.1,1.3-1.6c0.7-0.8,0.6-1.8-0.2-2.5c-1.1-0.9-2.2-1.8-3.3-2.7\\n\\tc-0.7-0.6-1.6-0.6-2.3,0c-0.4,0.4-0.9,0.7-1.3,1.1l-0.8,0.7c0,0-0.1,0-0.1,0.1l-0.5-0.5c-0.4-0.4-0.9-0.8-1.3-1.2\\n\\tc-0.7-0.7-1.6-0.7-2.4-0.1c-0.4,0.3-0.8,0.7-1.2,1l-0.7,0.6c-0.1,0.1-0.2,0.2-0.4,0.3l-0.6-0.5c-0.6-0.5-1.1-0.9-1.6-1.4\\n\\tc-0.5-0.4-1-0.5-1.5-0.4c-0.4,0.1-0.7,0.3-0.9,0.5c-0.6,0.5-1.1,0.9-1.7,1.4l-0.5,0.4L24,27.4c-0.6-0.5-1.2-1-1.8-1.5\\n\\tc-0.8-0.7-1.6-0.7-2.4,0L19,26.6c-0.6,0.5-1.3,1.1-2,1.6c-0.5,0.4-0.9,0.8-1.1,1.4l0,0.6l0,0.1c0.2,0.7,0.7,1.1,1.1,1.6\\n\\tc0.2,0.2,0.4,0.5,0.6,0.7c0.2,0.2,0.3,0.4,0.5,0.6l0.1,0.1l-0.3,0.4c-0.4,0.5-0.8,0.9-1.1,1.4c-0.2,0.3-0.4,0.6-0.6,0.9\\n\\tc-0.1,0.1-0.2,0.3-0.2,0.4l0,0.1l0,0.4c0.1,0.6,0.5,1.1,0.9,1.5c0.1,0.2,0.3,0.3,0.4,0.5c0.2,0.3,0.5,0.6,0.8,0.9l0.2,0.3l-1.1,1.3\\n\\tc-0.2,0.3-0.5,0.6-0.7,0.9c-0.1,0.2-0.3,0.4-0.4,0.6c0,0.1-0.1,0.2-0.1,0.3l0,0.1v0.4l0,0.1c0.2,0.6,0.6,1,1,1.4l0.2,0.2\\n\\tc0.3,0.3,0.6,0.6,0.9,0.9l0.2,0.2L17.8,47c-0.4,0.5-0.8,0.9-1.1,1.4c-0.2,0.2-0.3,0.5-0.5,0.7c-0.1,0.1-0.1,0.2-0.2,0.3l0,0.1v0.7\\n\\tl0,0.1c0.1,0.1,0.1,0.2,0.2,0.3c0.2,0.3,0.3,0.5,0.6,0.7c0.4,0.4,0.9,0.7,1.3,1.1c0.5,0.4,0.9,0.7,1.3,1.1c1.2,1.1,2,1.1,3.3,0\\n\\tc0.4-0.4,0.8-0.7,1.3-1c0.1-0.1,0.3-0.2,0.4-0.3l0.7,0.6c0.4,0.3,0.7,0.7,1.1,1c0.8,0.7,1.6,0.7,2.4,0.1c0.2-0.2,0.4-0.3,0.6-0.5\\n\\tl1.7-1.4l2.1,1.7c0.9,0.8,1.7,0.8,2.6,0l2.1-1.7l0.9,0.8c0.4,0.4,0.9,0.7,1.3,1.1c0.3,0.3,0.7,0.4,1.1,0.4c0.4,0,0.8-0.1,1.1-0.4\\n\\tc1.1-0.9,2.2-1.8,3.4-2.8c0.5-0.4,0.7-1.1,0.6-1.7c-0.1-0.3-0.3-0.6-0.5-0.8c-0.4-0.5-0.9-1.1-1.3-1.6l-0.4-0.5\\n\\tc0-0.1,0.1-0.1,0.1-0.2L44.4,45.9z M39.8,39.9c-0.1,0.5,0,0.9,0.4,1.4c0.4,0.5,0.9,1,1.3,1.6l0.4,0.5c0,0,0.1,0.1,0.1,0.1l-1.6,1.7\\n\\tc-0.8,0.8-0.8,1.7-0.1,2.6l1.6,1.9l-0.1,0.1c-0.3,0.2-0.6,0.5-0.9,0.7c0,0-0.1,0-0.1-0.1c-0.5-0.4-1.1-0.9-1.6-1.3l-0.5-0.4\\n\\tc-0.8-0.7-1.6-0.7-2.5,0l-0.2,0.2c-0.6,0.5-1.3,1-1.9,1.6c-0.1,0-0.1,0.1-0.1,0.1c0,0,0,0-0.1-0.1c-0.6-0.5-1.1-0.9-1.7-1.4\\n\\tl-0.4-0.3c-0.4-0.3-0.8-0.5-1.2-0.5c-0.4,0-0.8,0.2-1.2,0.5c-0.5,0.4-1.1,0.9-1.6,1.3l-0.5,0.4l-1.7-1.7c-0.8-0.7-1.6-0.8-2.5-0.1\\n\\tL21,50.4l-1-0.8c0,0,0.1-0.1,0.1-0.1l1.5-1.8c0.7-0.9,0.7-1.7-0.1-2.6c-0.4-0.4-0.8-0.8-1.2-1.3l-0.4-0.4l1.7-2\\n\\tc0.8-0.9,0.8-1.7,0-2.6L21,38.2c-0.4-0.5-0.7-0.9-1.1-1.3c-0.1-0.1-0.1-0.1-0.1-0.1c0,0,0,0,0.1-0.1c0.5-0.6,1.1-1.3,1.6-1.9\\n\\tl0.1-0.1c0.7-0.8,0.7-1.6,0-2.4c-0.4-0.5-0.8-1-1.2-1.4L20,30.2l1-0.8l0.5,0.4c0.5,0.4,1.1,0.9,1.6,1.3c0.9,0.7,1.6,0.7,2.5,0\\n\\tl2.1-1.8l2.1,1.7c0.9,0.8,1.7,0.8,2.6,0l2-1.7l1.6,1.6c0.9,0.8,1.7,0.9,2.6,0.1c0.7-0.5,1.3-1.1,2-1.6l0.1-0.1l1,0.8l-1.6,1.9\\n\\tc-0.7,0.9-0.7,1.6,0,2.5l1.8,2.1l-0.4,0.5c-0.5,0.6-1,1.2-1.4,1.7C40,39.2,39.9,39.6,39.8,39.9z M50,16.6c-0.9,0.7-1.7,1.4-2.6,2.1\\n\\tc-0.2,0.1-0.3,0.3-0.5,0.4c-0.7-0.6-1.3-1.3-2-1.9c-0.2-0.2-0.5-0.4-0.8-0.6c-0.1,0-0.3,0-0.4,0c-0.8,0.7-1.7,1.3-2.5,2\\n\\tc-0.2,0.2-0.4,0.3-0.6,0.5c-1.1-0.8-2.1-1.7-3.2-2.5c-0.1,0-0.3,0-0.4,0c-0.4,0.3-0.9,0.6-1.3,0.9C35,18,34.5,18.5,33.9,19\\n\\tc-0.2,0.1-0.3,0.2-0.5,0c-0.6-0.5-1.2-1-1.8-1.5c-0.4-0.3-0.7-0.6-1.1-0.8c-0.2,0-0.3,0-0.5,0c-0.1,0.1-0.2,0.1-0.3,0.2\\n\\tc-1.2,1-2.3,1.9-3.5,2.9c-0.4,0.3-0.4,0.9-0.1,1.3c0.3,0.4,0.6,0.8,1,1.1c0.1,0.1,0.2,0.1,0.4,0.1c1.2-0.2,2.3,0.1,3.2,0.9\\n\\tc0.3,0.2,0.6,0.5,0.9,0.7c2.1-2.1,4.3-2.3,6.5-0.2c0.2-0.2,0.5-0.4,0.7-0.6c1.5-1.2,3.5-1.2,4.9,0c1.1,0.9,2.7,2.4,3.8,3.3\\n\\tc1.8,1.5,2,3.8,0.5,5.6c-0.2,0.3-0.4,0.5-0.7,0.8c0.2,0.3,0.4,0.5,0.6,0.7c1.4,1.7,1.4,3.7,0,5.4c-0.6,0.7-0.6,0.7,0,1.4\\n\\tc0.6,0.7,1,1.4,1,2.3c0,0.7,0.2,1.1,0.8,1.4c0,0,0.1,0.1,0.1,0.1c0.6,0.5,1,0.5,1.6,0c1-0.8,2-1.7,3-2.5c0.7-0.6,0.8-1,0.2-1.8\\n\\tc-0.7-0.8-1.4-1.7-2.1-2.5c0.1-0.1,0.2-0.2,0.3-0.3c0.6-0.6,1.2-1.2,1.8-1.9c0.5-0.6,0.6-1,0.1-1.6c-0.2-0.2-0.3-0.4-0.5-0.6\\n\\tc-0.5-0.7-1.1-1.3-1.6-2c0.7-0.9,1.4-1.7,2.1-2.6c0.5-0.7,0.5-1,0-1.7c-0.7-0.8-1.4-1.7-2.1-2.5c0-0.1,0-0.1,0.1-0.1\\n\\tc0.7-0.9,1.4-1.7,2.1-2.6c0.5-0.6,0.4-1.1-0.1-1.5c-0.4-0.4-0.9-0.7-1.3-1.1c-0.8-0.6-2-1.8-2.8-2.4C50.3,16.6,50.1,16.6,50,16.6z\\"\\n\\t/>\\n</svg>\\n", "price_formula": "a * 300", "displace_goods": [], "gallery_images": [], "is_slave_goods": false, "force_to_basket": true, "additional_goods": [], "description_lines": [], "description_line_1": null, "description_line_2": null, "details_button_text": null, "not_display_in_catalog": false}	t
11124	2077	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/1_0R4WU7Q.svg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "м²", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "parent_id": 0, "updated_at": "2019-01-25T17:41:40.509732", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#65bcf1;}\\n</style>\\n<g>\\n\\t<path class=\\"st0\\" d=\\"M51.1,23.6c-0.6,3.2-2.1,4.8-5.2,5.4c3.2,0.6,4.7,2.2,5.2,4.9c0.5-2.7,2.1-4.3,5.3-4.9\\n\\t\\tC53.1,28.4,51.7,26.8,51.1,23.6z\\"/>\\n\\t<path class=\\"st0\\" d=\\"M35.4,0.7C16.2,0.7,0.6,16.3,0.6,35.5s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.7,0.7,35.4,0.7z M29.5,8.6\\n\\t\\tc0.7,3.8,2.4,5.8,6.3,6.4c-3.9,0.8-5.7,2.7-6.3,5.9c-0.6-3.2-2.4-5.1-6.2-5.8C27,14.3,28.8,12.4,29.5,8.6z M40.1,51.4\\n\\t\\tc-3.1,2.3-6.2,4.6-9.4,6.7c-2,1.4-4.3,2.3-6.8,2c-2.5-0.2-4.9-1.2-7-2.5c-2.2-1.3-3.7-3.3-4.2-6c-0.4-2.2,0-4.4,0.9-6.4\\n\\t\\tc1.2-2.7,2.4-5.3,3.6-7.9c1.2-2.6,2.4-5.3,3.6-7.9c0.2-0.4,0.4-0.7,0.7-1.1c0.5-0.7,1.3-0.9,2-0.5c0.7,0.4,1.1,1.1,0.8,2\\n\\t\\tc-0.2,0.6-0.4,1.2-0.7,1.8c-0.9,2.1-1.9,4.2-2.9,6.2c-0.2,0.5-0.1,0.9,0.4,1.2c0.5,0.3,0.9,0,1.2-0.5c0.1-0.2,0.2-0.4,0.2-0.5\\n\\t\\tc1.5-3.3,3.1-6.7,4.6-10c0.3-0.6,0.6-1.1,0.9-1.6c0.5-0.6,1.1-0.8,1.8-0.4c1,0.5,1.3,1.2,0.8,2.3c-1.6,3.5-3.2,6.9-4.8,10.4\\n\\t\\tc-0.4,0.9-0.3,1.5,0.3,1.7c0.8,0.3,1.1-0.3,1.4-0.9c0.4-1,0.9-1.9,1.3-2.9c1.4-3,2.8-6,4.1-9c0.5-1.1,1.4-1.3,2.4-0.8\\n\\t\\tc0.8,0.4,1,1.3,0.5,2.4c-1.8,3.9-3.6,7.9-5.4,11.8c-0.3,0.6-0.4,1.2,0.2,1.5c0.7,0.4,1-0.2,1.3-0.8c1.4-3,2.8-6,4.2-9.1\\n\\t\\tc0.2-0.4,0.4-0.8,0.7-1.1c0.7-0.8,1.9-0.7,2.5,0.1c0.5,0.7,0.4,1.4,0.1,2.1c-2.4,5.2-4.8,10.4-7.2,15.6c-0.2,0.5-0.5,1.1,0.1,1.4\\n\\t\\tc0.3,0.2,1,0.1,1.4-0.1c1.5-0.7,2.9-1.5,4.4-2.2c1-0.5,1.9-0.2,2.4,0.5C41.2,49.7,41,50.7,40.1,51.4z M50.9,50.3\\n\\t\\tc-0.7,1.5-2.4,2.1-3.9,1.5l-3.7-1.7c0-0.8-0.1-1.6-0.6-2.3c-1.1-1.7-3.6-2.3-5.5-1.3c-0.3,0.2-0.6,0.3-0.9,0.4l0,0l1.5-3.4\\n\\t\\tc1.3-2.9,2.7-5.9,4-8.8c1.2-2.7,0.2-4.2-0.8-5c-0.5-0.4-1.1-0.6-1.8-0.9c-0.2-0.1-0.3-0.1-0.5-0.2c0.2-1.5-0.4-2.8-1.7-3.7l3.9-8.5\\n\\t\\tc0.7-1.5,2.4-2.1,3.9-1.5l15.7,7.2c1.5,0.7,2.1,2.4,1.5,3.9L50.9,50.3z\\"/>\\n</g>\\n</svg>\\n", "price_formula": "36.00 * a + 1000 * b", "displace_goods": [23, 24], "gallery_images": [{"image_url": "http://192.168.0.15:8000/media/матрас1-1-1024x768.jpg"}, {"image_url": "http://192.168.0.15:8000/media/Кресло1-1.jpg"}, {"image_url": "http://192.168.0.15:8000/media/матрас1-1-1024x768_RnFY6Lk.jpg"}, {"image_url": "http://192.168.0.15:8000/media/Кресло1-1_JiWQgE4.jpg"}], "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [{"id": 4, "text": "Что входит в уборку?", "line_type": 1}, {"id": 5, "text": "раз", "line_type": 0}, {"id": 6, "text": "два", "line_type": 0}, {"id": 7, "text": "три", "line_type": 0}, {"id": 1, "text": "Как мы делаем уборку?", "line_type": 1}, {"id": 2, "text": "вот так", "line_type": 0}, {"id": 3, "text": "и так", "line_type": 0}], "description_line_1": "Для сохранения постоянной чистоты в вашем доме", "description_line_2": "36₽ / м²", "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10963	2058	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/1_0R4WU7Q.svg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2019-01-22T13:02:31.843205", "description": "Для сохранения постоянной чистоты в вашем доме", "price_formula": "36.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10983	2061	24	0	{"id": 24, "name": "уборка после ремонта", "type": 1, "image": "http://192.168.0.15:8000/media/3.svg", "order": 30, "params": [{"id": 13, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 40, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2019-01-22T13:03:33.106713", "description": "Уберем и очистим вашу квартиру от строительной пыли и мусора", "price_formula": "90.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10984	2061	23	0	{"id": 23, "name": "генеральная уборка", "type": 1, "image": "http://192.168.0.15:8000/media/2.svg", "order": 20, "params": [{"id": 8, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 39, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2019-01-22T13:02:56.738768", "description": "До блеска и неузнаваемости отмоем вашу квартиру", "price_formula": "90.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
11100	2071	24	0	{"id": 24, "name": "уборка после ремонта", "type": 1, "image": "http://192.168.0.15:8000/media/3.svg", "order": 30, "params": [{"id": 13, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "м²", "default": 35, "formula_letter": "a"}, {"id": 40, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "parent_id": 0, "updated_at": "2019-01-24T00:02:46.814855", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#66BCEE;}\\n</style>\\n<g>\\n\\t<polygon class=\\"st0\\" points=\\"55.9,45.2 51.4,45.2 53.9,48.7 58.4,48.7 \\t\\"/>\\n\\t<path class=\\"st0\\" d=\\"M35.4,0.6C16.2,0.6,0.6,16.2,0.6,35.4s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.7,0.6,35.4,0.6z M56.5,23.5\\n\\t\\tc0.6,3.2,2,4.8,5.3,5.3c-3.2,0.6-4.8,2.3-5.3,4.9c-0.5-2.7-2-4.3-5.2-4.9C54.5,28.3,56,26.7,56.5,23.5z M39.5,13.6\\n\\t\\tc1.9-1.9,2.6-5,3.8-7.3c0.6,6.2,4.1,10.1,11,10.9c-6.7,0.9-10.5,4.6-11.3,11.5c-0.8-6.7-4.3-10.6-10.7-11.1\\n\\t\\tC34.7,16.3,37.7,15.5,39.5,13.6z M24.2,64.4c-0.5-2.7-2-4.3-5.2-4.9c3.1-0.6,4.6-2.2,5.2-5.4c0.6,3.2,2,4.8,5.3,5.3\\n\\t\\tC26.3,60.1,24.7,61.7,24.2,64.4z M48.8,50.8L48.8,50.8l-4.2-1.6l-13.5-1.2c-1,0.2-1.9,0.3-2.7,0.3c-0.1,0-0.2,0-0.3,0\\n\\t\\tc-0.4,0-0.9,0-1.3,0c-3.3,0-6.8-0.1-10.2-4.8c-1.8-2.5-2.4-4.2-2.9-5.4c-0.1-0.2-0.2-0.5-0.2-0.6c-0.2,0.1-0.3,0.1-0.5,0.1\\n\\t\\tc-0.5,0-0.9-0.2-1.2-0.5l-5-5c-0.3-0.3-0.5-0.7-0.5-1.2c0-0.5,0.2-0.9,0.5-1.2l9.2-9.2c0.7-0.7,1.7-0.7,2.4,0l5,5\\n\\t\\tc0.6,0.6,0.7,1.6,0.1,2.3c1.5,1.4,2.9,2.5,3.4,2.6c0.4,0.1,1.5,0.2,2.7,0.3c3.6,0.4,9.1,0.9,11.5,1.6c2.1,0.7,2.5,0.9,3.3,1.9\\n\\t\\tc0.4,0.4,0.8,1,2,2c3.6,3.1,5.3,5.7,6,6.9l5.1,0.1l5.3,7.5L48.8,50.8z\\"/>\\n\\t<path class=\\"st0\\" d=\\"M44.3,37.8c-1.3-1.1-2.6-2.4-4.2-2.9c-2.1-0.7-7.6-1.2-10.9-1.5c-1.4-0.1-2.4-0.2-2.9-0.3\\n\\t\\tc-1.4-0.2-3.4-1.9-5-3.4l-5.7,5.7c0.3,0.5,0.5,1,0.7,1.6c0.4,1.2,1,2.6,2.5,4.8c2.5,3.6,4.8,3.6,7.9,3.6c0.4,0,0.9,0,1.4,0\\n\\t\\tc2.7,0.1,7.9-1.7,10.4-2.6c-0.1-0.1-0.3-0.2-0.5-0.3c-0.4-0.2-0.9-0.2-1.3-0.3c-1-0.1-1.7-0.3-2.6-0.4c-1,0-1.7,0.1-2.6,0.4\\n\\t\\tc-1.9,0.6-2.7-2.4-0.8-3c1.2-0.3,2.4-0.6,3.6-0.5c1.2,0.1,2.4,0.3,3.7,0.5c1.2,0.2,2.1,0.8,3.1,1.5c0.5,0.4,1,0.7,1.4,1.2\\n\\t\\tc3.4,4.3,7.7,2.9,7,2C48.8,42.9,47.2,40.4,44.3,37.8z\\"/>\\n</g>\\n</svg>\\n", "price_formula": "90.00 * a + 1000 * b", "displace_goods": [21, 23], "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "description_line_1": "Уберем и очистим вашу квартиру от строительной пыли и мусора", "description_line_2": "90₽ / м²", "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10953	2051	23	0	{"id": 23, "name": "генеральная уборка", "type": 1, "image": "http://192.168.0.15:8000/media/___general_9eeqHT3.jpg", "order": 20, "params": [{"id": 8, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 39, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2018-12-03T21:48:55.349000", "description": "До блеска и неузнаваемости отмоем вашу квартиру", "price_formula": "90.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10954	2051	46	0	{"id": 46, "name": "кресло", "type": 1, "image": null, "order": 30, "params": [{"id": 16, "max": 100, "min": 0, "name": "количество", "step": 1, "unit": "шт.", "default": 0, "formula_letter": "a"}], "is_alive": true, "min_price": null, "parent_id": 42, "unique_set": null, "updated_at": "2018-11-10T21:09:14.173000", "description": "", "price_formula": "450.00 * a", "is_slave_goods": true, "force_to_basket": false, "additional_goods": [], "description_lines": [], "image_need_border": false, "details_button_text": null, "not_display_in_catalog": false}	t
10955	2051	47	0	{"id": 47, "name": "матрас", "type": 1, "image": null, "order": 40, "params": [{"id": 21, "max": 100, "min": 0, "name": "количество", "step": 1, "unit": "шт.", "default": 0, "formula_letter": "a"}], "is_alive": true, "min_price": null, "parent_id": 42, "unique_set": null, "updated_at": "2018-11-10T21:09:46.603000", "description": "", "price_formula": "500.00 * a", "is_slave_goods": true, "force_to_basket": false, "additional_goods": [], "description_lines": [], "image_need_border": false, "details_button_text": null, "not_display_in_catalog": false}	t
10956	2051	51	0	{"id": 51, "name": "кухонный уголок", "type": 1, "image": null, "order": 45, "params": [{"id": 24, "max": 100, "min": 0, "name": "количество", "step": 1, "unit": "шт.", "default": 0, "formula_letter": "a"}], "is_alive": true, "min_price": null, "parent_id": 42, "unique_set": null, "updated_at": "2018-11-10T21:12:36.686000", "description": "", "price_formula": "900.00 * a", "is_slave_goods": true, "force_to_basket": false, "additional_goods": [], "description_lines": [], "image_need_border": false, "details_button_text": null, "not_display_in_catalog": false}	t
10957	2051	49	0	{"id": 49, "name": "3-х местный диван", "type": 1, "image": null, "order": 60, "params": [{"id": 32, "max": 100, "min": 0, "name": "количество", "step": 1, "unit": "шт.", "default": 0, "formula_letter": "a"}], "is_alive": true, "min_price": null, "parent_id": 42, "unique_set": null, "updated_at": "2018-11-10T21:11:08.999000", "description": "", "price_formula": "1500.00 * a", "is_slave_goods": true, "force_to_basket": false, "additional_goods": [], "description_lines": [], "image_need_border": false, "details_button_text": null, "not_display_in_catalog": false}	t
11125	2078	24	0	{"id": 24, "name": "уборка после ремонта", "type": 1, "image": "http://192.168.0.15:8000/media/3.svg", "order": 30, "params": [{"id": 13, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "м²", "default": 35, "formula_letter": "a"}, {"id": 40, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "parent_id": 0, "updated_at": "2019-01-24T00:02:46.814855", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#66BCEE;}\\n</style>\\n<g>\\n\\t<polygon class=\\"st0\\" points=\\"55.9,45.2 51.4,45.2 53.9,48.7 58.4,48.7 \\t\\"/>\\n\\t<path class=\\"st0\\" d=\\"M35.4,0.6C16.2,0.6,0.6,16.2,0.6,35.4s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.7,0.6,35.4,0.6z M56.5,23.5\\n\\t\\tc0.6,3.2,2,4.8,5.3,5.3c-3.2,0.6-4.8,2.3-5.3,4.9c-0.5-2.7-2-4.3-5.2-4.9C54.5,28.3,56,26.7,56.5,23.5z M39.5,13.6\\n\\t\\tc1.9-1.9,2.6-5,3.8-7.3c0.6,6.2,4.1,10.1,11,10.9c-6.7,0.9-10.5,4.6-11.3,11.5c-0.8-6.7-4.3-10.6-10.7-11.1\\n\\t\\tC34.7,16.3,37.7,15.5,39.5,13.6z M24.2,64.4c-0.5-2.7-2-4.3-5.2-4.9c3.1-0.6,4.6-2.2,5.2-5.4c0.6,3.2,2,4.8,5.3,5.3\\n\\t\\tC26.3,60.1,24.7,61.7,24.2,64.4z M48.8,50.8L48.8,50.8l-4.2-1.6l-13.5-1.2c-1,0.2-1.9,0.3-2.7,0.3c-0.1,0-0.2,0-0.3,0\\n\\t\\tc-0.4,0-0.9,0-1.3,0c-3.3,0-6.8-0.1-10.2-4.8c-1.8-2.5-2.4-4.2-2.9-5.4c-0.1-0.2-0.2-0.5-0.2-0.6c-0.2,0.1-0.3,0.1-0.5,0.1\\n\\t\\tc-0.5,0-0.9-0.2-1.2-0.5l-5-5c-0.3-0.3-0.5-0.7-0.5-1.2c0-0.5,0.2-0.9,0.5-1.2l9.2-9.2c0.7-0.7,1.7-0.7,2.4,0l5,5\\n\\t\\tc0.6,0.6,0.7,1.6,0.1,2.3c1.5,1.4,2.9,2.5,3.4,2.6c0.4,0.1,1.5,0.2,2.7,0.3c3.6,0.4,9.1,0.9,11.5,1.6c2.1,0.7,2.5,0.9,3.3,1.9\\n\\t\\tc0.4,0.4,0.8,1,2,2c3.6,3.1,5.3,5.7,6,6.9l5.1,0.1l5.3,7.5L48.8,50.8z\\"/>\\n\\t<path class=\\"st0\\" d=\\"M44.3,37.8c-1.3-1.1-2.6-2.4-4.2-2.9c-2.1-0.7-7.6-1.2-10.9-1.5c-1.4-0.1-2.4-0.2-2.9-0.3\\n\\t\\tc-1.4-0.2-3.4-1.9-5-3.4l-5.7,5.7c0.3,0.5,0.5,1,0.7,1.6c0.4,1.2,1,2.6,2.5,4.8c2.5,3.6,4.8,3.6,7.9,3.6c0.4,0,0.9,0,1.4,0\\n\\t\\tc2.7,0.1,7.9-1.7,10.4-2.6c-0.1-0.1-0.3-0.2-0.5-0.3c-0.4-0.2-0.9-0.2-1.3-0.3c-1-0.1-1.7-0.3-2.6-0.4c-1,0-1.7,0.1-2.6,0.4\\n\\t\\tc-1.9,0.6-2.7-2.4-0.8-3c1.2-0.3,2.4-0.6,3.6-0.5c1.2,0.1,2.4,0.3,3.7,0.5c1.2,0.2,2.1,0.8,3.1,1.5c0.5,0.4,1,0.7,1.4,1.2\\n\\t\\tc3.4,4.3,7.7,2.9,7,2C48.8,42.9,47.2,40.4,44.3,37.8z\\"/>\\n</g>\\n</svg>\\n", "price_formula": "90.00 * a + 1000 * b", "displace_goods": [21, 23], "gallery_images": [], "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "description_line_1": "Уберем и очистим вашу квартиру от строительной пыли и мусора", "description_line_2": "90₽ / м²", "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
11103	2072	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/1_0R4WU7Q.svg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "м²", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "parent_id": 0, "updated_at": "2019-01-24T00:02:28.271328", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#65bcf1;}\\n</style>\\n<g>\\n\\t<path class=\\"st0\\" d=\\"M51.1,23.6c-0.6,3.2-2.1,4.8-5.2,5.4c3.2,0.6,4.7,2.2,5.2,4.9c0.5-2.7,2.1-4.3,5.3-4.9\\n\\t\\tC53.1,28.4,51.7,26.8,51.1,23.6z\\"/>\\n\\t<path class=\\"st0\\" d=\\"M35.4,0.7C16.2,0.7,0.6,16.3,0.6,35.5s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.7,0.7,35.4,0.7z M29.5,8.6\\n\\t\\tc0.7,3.8,2.4,5.8,6.3,6.4c-3.9,0.8-5.7,2.7-6.3,5.9c-0.6-3.2-2.4-5.1-6.2-5.8C27,14.3,28.8,12.4,29.5,8.6z M40.1,51.4\\n\\t\\tc-3.1,2.3-6.2,4.6-9.4,6.7c-2,1.4-4.3,2.3-6.8,2c-2.5-0.2-4.9-1.2-7-2.5c-2.2-1.3-3.7-3.3-4.2-6c-0.4-2.2,0-4.4,0.9-6.4\\n\\t\\tc1.2-2.7,2.4-5.3,3.6-7.9c1.2-2.6,2.4-5.3,3.6-7.9c0.2-0.4,0.4-0.7,0.7-1.1c0.5-0.7,1.3-0.9,2-0.5c0.7,0.4,1.1,1.1,0.8,2\\n\\t\\tc-0.2,0.6-0.4,1.2-0.7,1.8c-0.9,2.1-1.9,4.2-2.9,6.2c-0.2,0.5-0.1,0.9,0.4,1.2c0.5,0.3,0.9,0,1.2-0.5c0.1-0.2,0.2-0.4,0.2-0.5\\n\\t\\tc1.5-3.3,3.1-6.7,4.6-10c0.3-0.6,0.6-1.1,0.9-1.6c0.5-0.6,1.1-0.8,1.8-0.4c1,0.5,1.3,1.2,0.8,2.3c-1.6,3.5-3.2,6.9-4.8,10.4\\n\\t\\tc-0.4,0.9-0.3,1.5,0.3,1.7c0.8,0.3,1.1-0.3,1.4-0.9c0.4-1,0.9-1.9,1.3-2.9c1.4-3,2.8-6,4.1-9c0.5-1.1,1.4-1.3,2.4-0.8\\n\\t\\tc0.8,0.4,1,1.3,0.5,2.4c-1.8,3.9-3.6,7.9-5.4,11.8c-0.3,0.6-0.4,1.2,0.2,1.5c0.7,0.4,1-0.2,1.3-0.8c1.4-3,2.8-6,4.2-9.1\\n\\t\\tc0.2-0.4,0.4-0.8,0.7-1.1c0.7-0.8,1.9-0.7,2.5,0.1c0.5,0.7,0.4,1.4,0.1,2.1c-2.4,5.2-4.8,10.4-7.2,15.6c-0.2,0.5-0.5,1.1,0.1,1.4\\n\\t\\tc0.3,0.2,1,0.1,1.4-0.1c1.5-0.7,2.9-1.5,4.4-2.2c1-0.5,1.9-0.2,2.4,0.5C41.2,49.7,41,50.7,40.1,51.4z M50.9,50.3\\n\\t\\tc-0.7,1.5-2.4,2.1-3.9,1.5l-3.7-1.7c0-0.8-0.1-1.6-0.6-2.3c-1.1-1.7-3.6-2.3-5.5-1.3c-0.3,0.2-0.6,0.3-0.9,0.4l0,0l1.5-3.4\\n\\t\\tc1.3-2.9,2.7-5.9,4-8.8c1.2-2.7,0.2-4.2-0.8-5c-0.5-0.4-1.1-0.6-1.8-0.9c-0.2-0.1-0.3-0.1-0.5-0.2c0.2-1.5-0.4-2.8-1.7-3.7l3.9-8.5\\n\\t\\tc0.7-1.5,2.4-2.1,3.9-1.5l15.7,7.2c1.5,0.7,2.1,2.4,1.5,3.9L50.9,50.3z\\"/>\\n</g>\\n</svg>\\n", "price_formula": "36.00 * a + 1000 * b", "displace_goods": [23, 24], "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "description_line_1": "Для сохранения постоянной чистоты в вашем доме", "description_line_2": "36₽ / м²", "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10997	2066	24	0	{"id": 24, "name": "уборка после ремонта", "type": 1, "image": "http://192.168.0.15:8000/media/3.svg", "order": 30, "params": [{"id": 13, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 40, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "updated_at": "2019-01-23T14:46:30.192602", "description": "Уберем и очистим вашу квартиру от строительной пыли и мусора", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#66BCEE;}\\n</style>\\n<g>\\n\\t<polygon class=\\"st0\\" points=\\"55.9,45.2 51.4,45.2 53.9,48.7 58.4,48.7 \\t\\"/>\\n\\t<path class=\\"st0\\" d=\\"M35.4,0.6C16.2,0.6,0.6,16.2,0.6,35.4s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.7,0.6,35.4,0.6z M56.5,23.5\\n\\t\\tc0.6,3.2,2,4.8,5.3,5.3c-3.2,0.6-4.8,2.3-5.3,4.9c-0.5-2.7-2-4.3-5.2-4.9C54.5,28.3,56,26.7,56.5,23.5z M39.5,13.6\\n\\t\\tc1.9-1.9,2.6-5,3.8-7.3c0.6,6.2,4.1,10.1,11,10.9c-6.7,0.9-10.5,4.6-11.3,11.5c-0.8-6.7-4.3-10.6-10.7-11.1\\n\\t\\tC34.7,16.3,37.7,15.5,39.5,13.6z M24.2,64.4c-0.5-2.7-2-4.3-5.2-4.9c3.1-0.6,4.6-2.2,5.2-5.4c0.6,3.2,2,4.8,5.3,5.3\\n\\t\\tC26.3,60.1,24.7,61.7,24.2,64.4z M48.8,50.8L48.8,50.8l-4.2-1.6l-13.5-1.2c-1,0.2-1.9,0.3-2.7,0.3c-0.1,0-0.2,0-0.3,0\\n\\t\\tc-0.4,0-0.9,0-1.3,0c-3.3,0-6.8-0.1-10.2-4.8c-1.8-2.5-2.4-4.2-2.9-5.4c-0.1-0.2-0.2-0.5-0.2-0.6c-0.2,0.1-0.3,0.1-0.5,0.1\\n\\t\\tc-0.5,0-0.9-0.2-1.2-0.5l-5-5c-0.3-0.3-0.5-0.7-0.5-1.2c0-0.5,0.2-0.9,0.5-1.2l9.2-9.2c0.7-0.7,1.7-0.7,2.4,0l5,5\\n\\t\\tc0.6,0.6,0.7,1.6,0.1,2.3c1.5,1.4,2.9,2.5,3.4,2.6c0.4,0.1,1.5,0.2,2.7,0.3c3.6,0.4,9.1,0.9,11.5,1.6c2.1,0.7,2.5,0.9,3.3,1.9\\n\\t\\tc0.4,0.4,0.8,1,2,2c3.6,3.1,5.3,5.7,6,6.9l5.1,0.1l5.3,7.5L48.8,50.8z\\"/>\\n\\t<path class=\\"st0\\" d=\\"M44.3,37.8c-1.3-1.1-2.6-2.4-4.2-2.9c-2.1-0.7-7.6-1.2-10.9-1.5c-1.4-0.1-2.4-0.2-2.9-0.3\\n\\t\\tc-1.4-0.2-3.4-1.9-5-3.4l-5.7,5.7c0.3,0.5,0.5,1,0.7,1.6c0.4,1.2,1,2.6,2.5,4.8c2.5,3.6,4.8,3.6,7.9,3.6c0.4,0,0.9,0,1.4,0\\n\\t\\tc2.7,0.1,7.9-1.7,10.4-2.6c-0.1-0.1-0.3-0.2-0.5-0.3c-0.4-0.2-0.9-0.2-1.3-0.3c-1-0.1-1.7-0.3-2.6-0.4c-1,0-1.7,0.1-2.6,0.4\\n\\t\\tc-1.9,0.6-2.7-2.4-0.8-3c1.2-0.3,2.4-0.6,3.6-0.5c1.2,0.1,2.4,0.3,3.7,0.5c1.2,0.2,2.1,0.8,3.1,1.5c0.5,0.4,1,0.7,1.4,1.2\\n\\t\\tc3.4,4.3,7.7,2.9,7,2C48.8,42.9,47.2,40.4,44.3,37.8z\\"/>\\n</g>\\n</svg>\\n", "price_formula": "90.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
11104	2072	36	0	{"id": 36, "name": "одна створка", "type": 1, "image": null, "order": 10, "params": [{"id": 3, "max": 100, "min": 0, "name": "количество", "step": 1, "unit": "шт.", "default": 0, "formula_letter": "a"}], "is_alive": true, "parent_id": 27, "updated_at": "2019-01-23T14:46:30.246875", "image_content": null, "price_formula": "300.00 * a", "displace_goods": [30], "is_slave_goods": true, "force_to_basket": false, "additional_goods": [], "description_lines": [], "description_line_1": "", "description_line_2": null, "details_button_text": null, "not_display_in_catalog": false}	t
11105	2072	37	0	{"id": 37, "name": "две створки", "type": 1, "image": null, "order": 10, "params": [{"id": 4, "max": 100, "min": 0, "name": "количество", "step": 1, "unit": "шт.", "default": 0, "formula_letter": "a"}], "is_alive": true, "parent_id": 27, "updated_at": "2019-01-23T14:46:30.251752", "image_content": null, "price_formula": "480.00 * a", "displace_goods": [31], "is_slave_goods": true, "force_to_basket": false, "additional_goods": [], "description_lines": [], "description_line_1": "", "description_line_2": null, "details_button_text": null, "not_display_in_catalog": false}	t
11106	2072	40	0	{"id": 40, "name": "балконная дверь", "type": 1, "image": null, "order": 50, "params": [{"id": 26, "max": 100, "min": 0, "name": "количество", "step": 1, "unit": "шт.", "default": 0, "formula_letter": "a"}], "is_alive": true, "parent_id": 27, "updated_at": "2019-01-23T14:46:30.266852", "image_content": null, "price_formula": "400.00 * a", "displace_goods": [34], "is_slave_goods": true, "force_to_basket": false, "additional_goods": [], "description_lines": [], "description_line_1": "", "description_line_2": null, "details_button_text": null, "not_display_in_catalog": false}	t
11126	2079	24	0	{"id": 24, "name": "уборка после ремонта", "type": 1, "image": "http://192.168.0.15:8000/media/3.svg", "order": 30, "params": [{"id": 13, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "м²", "default": 35, "formula_letter": "a"}, {"id": 40, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "parent_id": 0, "updated_at": "2019-01-24T00:02:46.814855", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#66BCEE;}\\n</style>\\n<g>\\n\\t<polygon class=\\"st0\\" points=\\"55.9,45.2 51.4,45.2 53.9,48.7 58.4,48.7 \\t\\"/>\\n\\t<path class=\\"st0\\" d=\\"M35.4,0.6C16.2,0.6,0.6,16.2,0.6,35.4s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.7,0.6,35.4,0.6z M56.5,23.5\\n\\t\\tc0.6,3.2,2,4.8,5.3,5.3c-3.2,0.6-4.8,2.3-5.3,4.9c-0.5-2.7-2-4.3-5.2-4.9C54.5,28.3,56,26.7,56.5,23.5z M39.5,13.6\\n\\t\\tc1.9-1.9,2.6-5,3.8-7.3c0.6,6.2,4.1,10.1,11,10.9c-6.7,0.9-10.5,4.6-11.3,11.5c-0.8-6.7-4.3-10.6-10.7-11.1\\n\\t\\tC34.7,16.3,37.7,15.5,39.5,13.6z M24.2,64.4c-0.5-2.7-2-4.3-5.2-4.9c3.1-0.6,4.6-2.2,5.2-5.4c0.6,3.2,2,4.8,5.3,5.3\\n\\t\\tC26.3,60.1,24.7,61.7,24.2,64.4z M48.8,50.8L48.8,50.8l-4.2-1.6l-13.5-1.2c-1,0.2-1.9,0.3-2.7,0.3c-0.1,0-0.2,0-0.3,0\\n\\t\\tc-0.4,0-0.9,0-1.3,0c-3.3,0-6.8-0.1-10.2-4.8c-1.8-2.5-2.4-4.2-2.9-5.4c-0.1-0.2-0.2-0.5-0.2-0.6c-0.2,0.1-0.3,0.1-0.5,0.1\\n\\t\\tc-0.5,0-0.9-0.2-1.2-0.5l-5-5c-0.3-0.3-0.5-0.7-0.5-1.2c0-0.5,0.2-0.9,0.5-1.2l9.2-9.2c0.7-0.7,1.7-0.7,2.4,0l5,5\\n\\t\\tc0.6,0.6,0.7,1.6,0.1,2.3c1.5,1.4,2.9,2.5,3.4,2.6c0.4,0.1,1.5,0.2,2.7,0.3c3.6,0.4,9.1,0.9,11.5,1.6c2.1,0.7,2.5,0.9,3.3,1.9\\n\\t\\tc0.4,0.4,0.8,1,2,2c3.6,3.1,5.3,5.7,6,6.9l5.1,0.1l5.3,7.5L48.8,50.8z\\"/>\\n\\t<path class=\\"st0\\" d=\\"M44.3,37.8c-1.3-1.1-2.6-2.4-4.2-2.9c-2.1-0.7-7.6-1.2-10.9-1.5c-1.4-0.1-2.4-0.2-2.9-0.3\\n\\t\\tc-1.4-0.2-3.4-1.9-5-3.4l-5.7,5.7c0.3,0.5,0.5,1,0.7,1.6c0.4,1.2,1,2.6,2.5,4.8c2.5,3.6,4.8,3.6,7.9,3.6c0.4,0,0.9,0,1.4,0\\n\\t\\tc2.7,0.1,7.9-1.7,10.4-2.6c-0.1-0.1-0.3-0.2-0.5-0.3c-0.4-0.2-0.9-0.2-1.3-0.3c-1-0.1-1.7-0.3-2.6-0.4c-1,0-1.7,0.1-2.6,0.4\\n\\t\\tc-1.9,0.6-2.7-2.4-0.8-3c1.2-0.3,2.4-0.6,3.6-0.5c1.2,0.1,2.4,0.3,3.7,0.5c1.2,0.2,2.1,0.8,3.1,1.5c0.5,0.4,1,0.7,1.4,1.2\\n\\t\\tc3.4,4.3,7.7,2.9,7,2C48.8,42.9,47.2,40.4,44.3,37.8z\\"/>\\n</g>\\n</svg>\\n", "price_formula": "90.00 * a + 1000 * b", "displace_goods": [21, 23], "gallery_images": [], "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "description_line_1": "Уберем и очистим вашу квартиру от строительной пыли и мусора", "description_line_2": "90₽ / м²", "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10958	2053	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/HCu_NVM4hhM_NM4AWAP.jpg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2018-12-03T21:44:48.119000", "description": "Для сохранения постоянной чистоты в вашем доме", "price_formula": "36.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
11127	2080	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/1_0R4WU7Q.svg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "м²", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "parent_id": 0, "updated_at": "2019-01-25T17:41:40.509732", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#65bcf1;}\\n</style>\\n<g>\\n\\t<path class=\\"st0\\" d=\\"M51.1,23.6c-0.6,3.2-2.1,4.8-5.2,5.4c3.2,0.6,4.7,2.2,5.2,4.9c0.5-2.7,2.1-4.3,5.3-4.9\\n\\t\\tC53.1,28.4,51.7,26.8,51.1,23.6z\\"/>\\n\\t<path class=\\"st0\\" d=\\"M35.4,0.7C16.2,0.7,0.6,16.3,0.6,35.5s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.7,0.7,35.4,0.7z M29.5,8.6\\n\\t\\tc0.7,3.8,2.4,5.8,6.3,6.4c-3.9,0.8-5.7,2.7-6.3,5.9c-0.6-3.2-2.4-5.1-6.2-5.8C27,14.3,28.8,12.4,29.5,8.6z M40.1,51.4\\n\\t\\tc-3.1,2.3-6.2,4.6-9.4,6.7c-2,1.4-4.3,2.3-6.8,2c-2.5-0.2-4.9-1.2-7-2.5c-2.2-1.3-3.7-3.3-4.2-6c-0.4-2.2,0-4.4,0.9-6.4\\n\\t\\tc1.2-2.7,2.4-5.3,3.6-7.9c1.2-2.6,2.4-5.3,3.6-7.9c0.2-0.4,0.4-0.7,0.7-1.1c0.5-0.7,1.3-0.9,2-0.5c0.7,0.4,1.1,1.1,0.8,2\\n\\t\\tc-0.2,0.6-0.4,1.2-0.7,1.8c-0.9,2.1-1.9,4.2-2.9,6.2c-0.2,0.5-0.1,0.9,0.4,1.2c0.5,0.3,0.9,0,1.2-0.5c0.1-0.2,0.2-0.4,0.2-0.5\\n\\t\\tc1.5-3.3,3.1-6.7,4.6-10c0.3-0.6,0.6-1.1,0.9-1.6c0.5-0.6,1.1-0.8,1.8-0.4c1,0.5,1.3,1.2,0.8,2.3c-1.6,3.5-3.2,6.9-4.8,10.4\\n\\t\\tc-0.4,0.9-0.3,1.5,0.3,1.7c0.8,0.3,1.1-0.3,1.4-0.9c0.4-1,0.9-1.9,1.3-2.9c1.4-3,2.8-6,4.1-9c0.5-1.1,1.4-1.3,2.4-0.8\\n\\t\\tc0.8,0.4,1,1.3,0.5,2.4c-1.8,3.9-3.6,7.9-5.4,11.8c-0.3,0.6-0.4,1.2,0.2,1.5c0.7,0.4,1-0.2,1.3-0.8c1.4-3,2.8-6,4.2-9.1\\n\\t\\tc0.2-0.4,0.4-0.8,0.7-1.1c0.7-0.8,1.9-0.7,2.5,0.1c0.5,0.7,0.4,1.4,0.1,2.1c-2.4,5.2-4.8,10.4-7.2,15.6c-0.2,0.5-0.5,1.1,0.1,1.4\\n\\t\\tc0.3,0.2,1,0.1,1.4-0.1c1.5-0.7,2.9-1.5,4.4-2.2c1-0.5,1.9-0.2,2.4,0.5C41.2,49.7,41,50.7,40.1,51.4z M50.9,50.3\\n\\t\\tc-0.7,1.5-2.4,2.1-3.9,1.5l-3.7-1.7c0-0.8-0.1-1.6-0.6-2.3c-1.1-1.7-3.6-2.3-5.5-1.3c-0.3,0.2-0.6,0.3-0.9,0.4l0,0l1.5-3.4\\n\\t\\tc1.3-2.9,2.7-5.9,4-8.8c1.2-2.7,0.2-4.2-0.8-5c-0.5-0.4-1.1-0.6-1.8-0.9c-0.2-0.1-0.3-0.1-0.5-0.2c0.2-1.5-0.4-2.8-1.7-3.7l3.9-8.5\\n\\t\\tc0.7-1.5,2.4-2.1,3.9-1.5l15.7,7.2c1.5,0.7,2.1,2.4,1.5,3.9L50.9,50.3z\\"/>\\n</g>\\n</svg>\\n", "price_formula": "36.00 * a + 1000 * b", "displace_goods": [23, 24], "gallery_images": [{"image_url": "http://192.168.0.15:8000/media/матрас1-1-1024x768.jpg"}, {"image_url": "http://192.168.0.15:8000/media/Кресло1-1.jpg"}, {"image_url": "http://192.168.0.15:8000/media/матрас1-1-1024x768_RnFY6Lk.jpg"}, {"image_url": "http://192.168.0.15:8000/media/Кресло1-1_JiWQgE4.jpg"}], "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [{"id": 4, "text": "Что входит в уборку?", "line_type": 1}, {"id": 5, "text": "раз", "line_type": 0}, {"id": 6, "text": "два", "line_type": 0}, {"id": 7, "text": "три", "line_type": 0}, {"id": 1, "text": "Как мы делаем уборку?", "line_type": 1}, {"id": 2, "text": "вот так", "line_type": 0}, {"id": 3, "text": "и так", "line_type": 0}], "description_line_1": "Для сохранения постоянной чистоты в вашем доме", "description_line_2": "36₽ / м²", "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10863	2041	23	0	{"id": 23, "name": "генеральная уборка", "type": 1, "image": "http://192.168.0.15:8000/media/___general_9eeqHT3.jpg", "order": 20, "params": [{"id": 8, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 39, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2018-12-03T21:48:55.349000", "description": "До блеска и неузнаваемости отмоем вашу квартиру", "price_formula": "90.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10864	2041	24	0	{"id": 24, "name": "уборка после ремонта", "type": 1, "image": "http://192.168.0.15:8000/media/___posle_vQFVppS.jpg", "order": 30, "params": [{"id": 13, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 40, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2018-12-03T21:47:24.824000", "description": "Уберем и очистим вашу квартиру от строительной пыли и мусора", "price_formula": "90.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10865	2041	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/HCu_NVM4hhM_NM4AWAP.jpg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2018-12-03T21:44:48.119000", "description": "Для сохранения постоянной чистоты в вашем доме", "price_formula": "36.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
11015	2069	48	0	{"id": 48, "name": "2-х местный диван", "type": 1, "image": null, "order": 50, "params": [{"id": 27, "max": 100, "min": 0, "name": "количество", "step": 1, "unit": "шт.", "default": 0, "formula_letter": "a"}], "is_alive": true, "min_price": null, "parent_id": 42, "updated_at": "2019-01-23T14:46:30.329269", "image_content": null, "price_formula": "1200.00 * a", "is_slave_goods": true, "force_to_basket": false, "additional_goods": [], "description_lines": [], "description_line_1": "", "description_line_2": null, "details_button_text": null, "not_display_in_catalog": false}	t
11016	2069	49	0	{"id": 49, "name": "3-х местный диван", "type": 1, "image": null, "order": 60, "params": [{"id": 32, "max": 100, "min": 0, "name": "количество", "step": 1, "unit": "шт.", "default": 0, "formula_letter": "a"}], "is_alive": true, "min_price": null, "parent_id": 42, "updated_at": "2019-01-23T14:46:30.335407", "image_content": null, "price_formula": "1500.00 * a", "is_slave_goods": true, "force_to_basket": false, "additional_goods": [], "description_lines": [], "description_line_1": "", "description_line_2": null, "details_button_text": null, "not_display_in_catalog": false}	t
10970	2059	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/1_0R4WU7Q.svg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2019-01-22T13:16:03.538209", "description": "Для сохранения постоянной чистоты в вашем доме", "price_formula": "36.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10971	2059	23	0	{"id": 23, "name": "генеральная уборка", "type": 1, "image": "http://192.168.0.15:8000/media/2.svg", "order": 20, "params": [{"id": 8, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 39, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2019-01-22T13:02:56.738768", "description": "До блеска и неузнаваемости отмоем вашу квартиру", "price_formula": "90.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10778	2042	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/HCu_NVM4hhM_NM4AWAP.jpg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2018-12-03T21:44:48.119000", "description": "Для сохранения постоянной чистоты в вашем доме", "price_formula": "36.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
11111	2073	37	0	{"id": 37, "name": "две створки", "type": 1, "image": null, "order": 10, "params": [{"id": 4, "max": 100, "min": 0, "name": "количество", "step": 1, "unit": "шт.", "default": 0, "formula_letter": "a"}], "is_alive": true, "parent_id": 27, "updated_at": "2019-01-23T14:46:30.251752", "image_content": null, "price_formula": "480.00 * a", "displace_goods": [31], "is_slave_goods": true, "force_to_basket": false, "additional_goods": [], "description_lines": [], "description_line_1": "", "description_line_2": null, "details_button_text": null, "not_display_in_catalog": false}	t
11112	2073	38	0	{"id": 38, "name": "три створки", "type": 1, "image": null, "order": 30, "params": [{"id": 15, "max": 100, "min": 0, "name": "количество", "step": 1, "unit": "шт.", "default": 0, "formula_letter": "a"}], "is_alive": true, "parent_id": 27, "updated_at": "2019-01-23T14:46:30.256827", "image_content": null, "price_formula": "600.00 * a", "displace_goods": [32], "is_slave_goods": true, "force_to_basket": false, "additional_goods": [], "description_lines": [], "description_line_1": "", "description_line_2": null, "details_button_text": null, "not_display_in_catalog": false}	t
10959	2054	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/1.svg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2019-01-22T12:59:29.837525", "description": "Для сохранения постоянной чистоты в вашем доме", "price_formula": "36.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10782	2043	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/HCu_NVM4hhM_NM4AWAP.jpg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2018-12-03T21:44:48.119000", "description": "Для сохранения постоянной чистоты в вашем доме", "price_formula": "36.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10989	2062	24	0	{"id": 24, "name": "уборка после ремонта", "type": 1, "image": "http://192.168.0.15:8000/media/3.svg", "order": 30, "params": [{"id": 13, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 40, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "updated_at": "2019-01-23T14:46:30.192602", "description": "Уберем и очистим вашу квартиру от строительной пыли и мусора", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#66BCEE;}\\n</style>\\n<g>\\n\\t<polygon class=\\"st0\\" points=\\"55.9,45.2 51.4,45.2 53.9,48.7 58.4,48.7 \\t\\"/>\\n\\t<path class=\\"st0\\" d=\\"M35.4,0.6C16.2,0.6,0.6,16.2,0.6,35.4s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.7,0.6,35.4,0.6z M56.5,23.5\\n\\t\\tc0.6,3.2,2,4.8,5.3,5.3c-3.2,0.6-4.8,2.3-5.3,4.9c-0.5-2.7-2-4.3-5.2-4.9C54.5,28.3,56,26.7,56.5,23.5z M39.5,13.6\\n\\t\\tc1.9-1.9,2.6-5,3.8-7.3c0.6,6.2,4.1,10.1,11,10.9c-6.7,0.9-10.5,4.6-11.3,11.5c-0.8-6.7-4.3-10.6-10.7-11.1\\n\\t\\tC34.7,16.3,37.7,15.5,39.5,13.6z M24.2,64.4c-0.5-2.7-2-4.3-5.2-4.9c3.1-0.6,4.6-2.2,5.2-5.4c0.6,3.2,2,4.8,5.3,5.3\\n\\t\\tC26.3,60.1,24.7,61.7,24.2,64.4z M48.8,50.8L48.8,50.8l-4.2-1.6l-13.5-1.2c-1,0.2-1.9,0.3-2.7,0.3c-0.1,0-0.2,0-0.3,0\\n\\t\\tc-0.4,0-0.9,0-1.3,0c-3.3,0-6.8-0.1-10.2-4.8c-1.8-2.5-2.4-4.2-2.9-5.4c-0.1-0.2-0.2-0.5-0.2-0.6c-0.2,0.1-0.3,0.1-0.5,0.1\\n\\t\\tc-0.5,0-0.9-0.2-1.2-0.5l-5-5c-0.3-0.3-0.5-0.7-0.5-1.2c0-0.5,0.2-0.9,0.5-1.2l9.2-9.2c0.7-0.7,1.7-0.7,2.4,0l5,5\\n\\t\\tc0.6,0.6,0.7,1.6,0.1,2.3c1.5,1.4,2.9,2.5,3.4,2.6c0.4,0.1,1.5,0.2,2.7,0.3c3.6,0.4,9.1,0.9,11.5,1.6c2.1,0.7,2.5,0.9,3.3,1.9\\n\\t\\tc0.4,0.4,0.8,1,2,2c3.6,3.1,5.3,5.7,6,6.9l5.1,0.1l5.3,7.5L48.8,50.8z\\"/>\\n\\t<path class=\\"st0\\" d=\\"M44.3,37.8c-1.3-1.1-2.6-2.4-4.2-2.9c-2.1-0.7-7.6-1.2-10.9-1.5c-1.4-0.1-2.4-0.2-2.9-0.3\\n\\t\\tc-1.4-0.2-3.4-1.9-5-3.4l-5.7,5.7c0.3,0.5,0.5,1,0.7,1.6c0.4,1.2,1,2.6,2.5,4.8c2.5,3.6,4.8,3.6,7.9,3.6c0.4,0,0.9,0,1.4,0\\n\\t\\tc2.7,0.1,7.9-1.7,10.4-2.6c-0.1-0.1-0.3-0.2-0.5-0.3c-0.4-0.2-0.9-0.2-1.3-0.3c-1-0.1-1.7-0.3-2.6-0.4c-1,0-1.7,0.1-2.6,0.4\\n\\t\\tc-1.9,0.6-2.7-2.4-0.8-3c1.2-0.3,2.4-0.6,3.6-0.5c1.2,0.1,2.4,0.3,3.7,0.5c1.2,0.2,2.1,0.8,3.1,1.5c0.5,0.4,1,0.7,1.4,1.2\\n\\t\\tc3.4,4.3,7.7,2.9,7,2C48.8,42.9,47.2,40.4,44.3,37.8z\\"/>\\n</g>\\n</svg>\\n", "price_formula": "90.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
11122	2075	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/1_0R4WU7Q.svg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "м²", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "parent_id": 0, "updated_at": "2019-01-25T17:41:40.509732", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#65bcf1;}\\n</style>\\n<g>\\n\\t<path class=\\"st0\\" d=\\"M51.1,23.6c-0.6,3.2-2.1,4.8-5.2,5.4c3.2,0.6,4.7,2.2,5.2,4.9c0.5-2.7,2.1-4.3,5.3-4.9\\n\\t\\tC53.1,28.4,51.7,26.8,51.1,23.6z\\"/>\\n\\t<path class=\\"st0\\" d=\\"M35.4,0.7C16.2,0.7,0.6,16.3,0.6,35.5s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.7,0.7,35.4,0.7z M29.5,8.6\\n\\t\\tc0.7,3.8,2.4,5.8,6.3,6.4c-3.9,0.8-5.7,2.7-6.3,5.9c-0.6-3.2-2.4-5.1-6.2-5.8C27,14.3,28.8,12.4,29.5,8.6z M40.1,51.4\\n\\t\\tc-3.1,2.3-6.2,4.6-9.4,6.7c-2,1.4-4.3,2.3-6.8,2c-2.5-0.2-4.9-1.2-7-2.5c-2.2-1.3-3.7-3.3-4.2-6c-0.4-2.2,0-4.4,0.9-6.4\\n\\t\\tc1.2-2.7,2.4-5.3,3.6-7.9c1.2-2.6,2.4-5.3,3.6-7.9c0.2-0.4,0.4-0.7,0.7-1.1c0.5-0.7,1.3-0.9,2-0.5c0.7,0.4,1.1,1.1,0.8,2\\n\\t\\tc-0.2,0.6-0.4,1.2-0.7,1.8c-0.9,2.1-1.9,4.2-2.9,6.2c-0.2,0.5-0.1,0.9,0.4,1.2c0.5,0.3,0.9,0,1.2-0.5c0.1-0.2,0.2-0.4,0.2-0.5\\n\\t\\tc1.5-3.3,3.1-6.7,4.6-10c0.3-0.6,0.6-1.1,0.9-1.6c0.5-0.6,1.1-0.8,1.8-0.4c1,0.5,1.3,1.2,0.8,2.3c-1.6,3.5-3.2,6.9-4.8,10.4\\n\\t\\tc-0.4,0.9-0.3,1.5,0.3,1.7c0.8,0.3,1.1-0.3,1.4-0.9c0.4-1,0.9-1.9,1.3-2.9c1.4-3,2.8-6,4.1-9c0.5-1.1,1.4-1.3,2.4-0.8\\n\\t\\tc0.8,0.4,1,1.3,0.5,2.4c-1.8,3.9-3.6,7.9-5.4,11.8c-0.3,0.6-0.4,1.2,0.2,1.5c0.7,0.4,1-0.2,1.3-0.8c1.4-3,2.8-6,4.2-9.1\\n\\t\\tc0.2-0.4,0.4-0.8,0.7-1.1c0.7-0.8,1.9-0.7,2.5,0.1c0.5,0.7,0.4,1.4,0.1,2.1c-2.4,5.2-4.8,10.4-7.2,15.6c-0.2,0.5-0.5,1.1,0.1,1.4\\n\\t\\tc0.3,0.2,1,0.1,1.4-0.1c1.5-0.7,2.9-1.5,4.4-2.2c1-0.5,1.9-0.2,2.4,0.5C41.2,49.7,41,50.7,40.1,51.4z M50.9,50.3\\n\\t\\tc-0.7,1.5-2.4,2.1-3.9,1.5l-3.7-1.7c0-0.8-0.1-1.6-0.6-2.3c-1.1-1.7-3.6-2.3-5.5-1.3c-0.3,0.2-0.6,0.3-0.9,0.4l0,0l1.5-3.4\\n\\t\\tc1.3-2.9,2.7-5.9,4-8.8c1.2-2.7,0.2-4.2-0.8-5c-0.5-0.4-1.1-0.6-1.8-0.9c-0.2-0.1-0.3-0.1-0.5-0.2c0.2-1.5-0.4-2.8-1.7-3.7l3.9-8.5\\n\\t\\tc0.7-1.5,2.4-2.1,3.9-1.5l15.7,7.2c1.5,0.7,2.1,2.4,1.5,3.9L50.9,50.3z\\"/>\\n</g>\\n</svg>\\n", "price_formula": "36.00 * a + 1000 * b", "displace_goods": [23, 24], "gallery_images": [{"image_url": "http://192.168.0.15:8000/media/матрас1-1-1024x768.jpg"}, {"image_url": "http://192.168.0.15:8000/media/Кресло1-1.jpg"}, {"image_url": "http://192.168.0.15:8000/media/матрас1-1-1024x768_RnFY6Lk.jpg"}, {"image_url": "http://192.168.0.15:8000/media/Кресло1-1_JiWQgE4.jpg"}], "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [{"id": 4, "text": "Что входит в уборку?", "line_type": 1}, {"id": 5, "text": "раз", "line_type": 0}, {"id": 6, "text": "два", "line_type": 0}, {"id": 7, "text": "три", "line_type": 0}, {"id": 1, "text": "Как мы делаем уборку?", "line_type": 1}, {"id": 2, "text": "вот так", "line_type": 0}, {"id": 3, "text": "и так", "line_type": 0}], "description_line_1": "Для сохранения постоянной чистоты в вашем доме", "description_line_2": "36₽ / м²", "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10881	2047	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/HCu_NVM4hhM_NM4AWAP.jpg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2018-12-03T21:44:48.119000", "description": "Для сохранения постоянной чистоты в вашем доме", "price_formula": "36.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10905	2050	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/HCu_NVM4hhM_NM4AWAP.jpg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2018-12-03T21:44:48.119000", "description": "Для сохранения постоянной чистоты в вашем доме", "price_formula": "36.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10790	2044	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/HCu_NVM4hhM_NM4AWAP.jpg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2018-12-03T21:44:48.119000", "description": "Для сохранения постоянной чистоты в вашем доме", "price_formula": "36.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10791	2045	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/HCu_NVM4hhM_NM4AWAP.jpg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2018-12-03T21:44:48.119000", "description": "Для сохранения постоянной чистоты в вашем доме", "price_formula": "36.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
11138	2081	24	0	{"id": 24, "name": "уборка после ремонта", "type": 1, "image": "http://192.168.0.15:8000/media/3.svg", "order": 30, "params": [{"id": 13, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "м²", "default": 35, "formula_letter": "a"}, {"id": 40, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "parent_id": 0, "updated_at": "2019-01-24T00:02:46.814855", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#66BCEE;}\\n</style>\\n<g>\\n\\t<polygon class=\\"st0\\" points=\\"55.9,45.2 51.4,45.2 53.9,48.7 58.4,48.7 \\t\\"/>\\n\\t<path class=\\"st0\\" d=\\"M35.4,0.6C16.2,0.6,0.6,16.2,0.6,35.4s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.7,0.6,35.4,0.6z M56.5,23.5\\n\\t\\tc0.6,3.2,2,4.8,5.3,5.3c-3.2,0.6-4.8,2.3-5.3,4.9c-0.5-2.7-2-4.3-5.2-4.9C54.5,28.3,56,26.7,56.5,23.5z M39.5,13.6\\n\\t\\tc1.9-1.9,2.6-5,3.8-7.3c0.6,6.2,4.1,10.1,11,10.9c-6.7,0.9-10.5,4.6-11.3,11.5c-0.8-6.7-4.3-10.6-10.7-11.1\\n\\t\\tC34.7,16.3,37.7,15.5,39.5,13.6z M24.2,64.4c-0.5-2.7-2-4.3-5.2-4.9c3.1-0.6,4.6-2.2,5.2-5.4c0.6,3.2,2,4.8,5.3,5.3\\n\\t\\tC26.3,60.1,24.7,61.7,24.2,64.4z M48.8,50.8L48.8,50.8l-4.2-1.6l-13.5-1.2c-1,0.2-1.9,0.3-2.7,0.3c-0.1,0-0.2,0-0.3,0\\n\\t\\tc-0.4,0-0.9,0-1.3,0c-3.3,0-6.8-0.1-10.2-4.8c-1.8-2.5-2.4-4.2-2.9-5.4c-0.1-0.2-0.2-0.5-0.2-0.6c-0.2,0.1-0.3,0.1-0.5,0.1\\n\\t\\tc-0.5,0-0.9-0.2-1.2-0.5l-5-5c-0.3-0.3-0.5-0.7-0.5-1.2c0-0.5,0.2-0.9,0.5-1.2l9.2-9.2c0.7-0.7,1.7-0.7,2.4,0l5,5\\n\\t\\tc0.6,0.6,0.7,1.6,0.1,2.3c1.5,1.4,2.9,2.5,3.4,2.6c0.4,0.1,1.5,0.2,2.7,0.3c3.6,0.4,9.1,0.9,11.5,1.6c2.1,0.7,2.5,0.9,3.3,1.9\\n\\t\\tc0.4,0.4,0.8,1,2,2c3.6,3.1,5.3,5.7,6,6.9l5.1,0.1l5.3,7.5L48.8,50.8z\\"/>\\n\\t<path class=\\"st0\\" d=\\"M44.3,37.8c-1.3-1.1-2.6-2.4-4.2-2.9c-2.1-0.7-7.6-1.2-10.9-1.5c-1.4-0.1-2.4-0.2-2.9-0.3\\n\\t\\tc-1.4-0.2-3.4-1.9-5-3.4l-5.7,5.7c0.3,0.5,0.5,1,0.7,1.6c0.4,1.2,1,2.6,2.5,4.8c2.5,3.6,4.8,3.6,7.9,3.6c0.4,0,0.9,0,1.4,0\\n\\t\\tc2.7,0.1,7.9-1.7,10.4-2.6c-0.1-0.1-0.3-0.2-0.5-0.3c-0.4-0.2-0.9-0.2-1.3-0.3c-1-0.1-1.7-0.3-2.6-0.4c-1,0-1.7,0.1-2.6,0.4\\n\\t\\tc-1.9,0.6-2.7-2.4-0.8-3c1.2-0.3,2.4-0.6,3.6-0.5c1.2,0.1,2.4,0.3,3.7,0.5c1.2,0.2,2.1,0.8,3.1,1.5c0.5,0.4,1,0.7,1.4,1.2\\n\\t\\tc3.4,4.3,7.7,2.9,7,2C48.8,42.9,47.2,40.4,44.3,37.8z\\"/>\\n</g>\\n</svg>\\n", "price_formula": "90.00 * a + 1000 * b", "displace_goods": [21, 23], "gallery_images": [], "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "description_line_1": "Уберем и очистим вашу квартиру от строительной пыли и мусора", "description_line_2": "90₽ / м²", "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10960	2055	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/1.svg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2019-01-22T12:59:29.837525", "description": "Для сохранения постоянной чистоты в вашем доме", "price_formula": "36.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
11139	2081	38	0	{"id": 38, "name": "три створки", "type": 1, "image": null, "order": 30, "params": [{"id": 15, "max": 100, "min": 0, "name": "количество", "step": 1, "unit": "шт.", "default": 0, "formula_letter": "a"}], "is_alive": true, "parent_id": 27, "updated_at": "2019-01-23T14:46:30.256827", "image_content": null, "price_formula": "600.00 * a", "displace_goods": [32], "gallery_images": [], "is_slave_goods": true, "force_to_basket": false, "additional_goods": [], "description_lines": [], "description_line_1": "", "description_line_2": null, "details_button_text": null, "not_display_in_catalog": false}	t
11140	2082	24	0	{"id": 24, "name": "уборка после ремонта", "type": 1, "image": "http://192.168.0.15:8000/media/3.svg", "order": 30, "params": [{"id": 13, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "м²", "default": 35, "formula_letter": "a"}, {"id": 40, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "parent_id": 0, "updated_at": "2019-01-24T00:02:46.814855", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#66BCEE;}\\n</style>\\n<g>\\n\\t<polygon class=\\"st0\\" points=\\"55.9,45.2 51.4,45.2 53.9,48.7 58.4,48.7 \\t\\"/>\\n\\t<path class=\\"st0\\" d=\\"M35.4,0.6C16.2,0.6,0.6,16.2,0.6,35.4s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.7,0.6,35.4,0.6z M56.5,23.5\\n\\t\\tc0.6,3.2,2,4.8,5.3,5.3c-3.2,0.6-4.8,2.3-5.3,4.9c-0.5-2.7-2-4.3-5.2-4.9C54.5,28.3,56,26.7,56.5,23.5z M39.5,13.6\\n\\t\\tc1.9-1.9,2.6-5,3.8-7.3c0.6,6.2,4.1,10.1,11,10.9c-6.7,0.9-10.5,4.6-11.3,11.5c-0.8-6.7-4.3-10.6-10.7-11.1\\n\\t\\tC34.7,16.3,37.7,15.5,39.5,13.6z M24.2,64.4c-0.5-2.7-2-4.3-5.2-4.9c3.1-0.6,4.6-2.2,5.2-5.4c0.6,3.2,2,4.8,5.3,5.3\\n\\t\\tC26.3,60.1,24.7,61.7,24.2,64.4z M48.8,50.8L48.8,50.8l-4.2-1.6l-13.5-1.2c-1,0.2-1.9,0.3-2.7,0.3c-0.1,0-0.2,0-0.3,0\\n\\t\\tc-0.4,0-0.9,0-1.3,0c-3.3,0-6.8-0.1-10.2-4.8c-1.8-2.5-2.4-4.2-2.9-5.4c-0.1-0.2-0.2-0.5-0.2-0.6c-0.2,0.1-0.3,0.1-0.5,0.1\\n\\t\\tc-0.5,0-0.9-0.2-1.2-0.5l-5-5c-0.3-0.3-0.5-0.7-0.5-1.2c0-0.5,0.2-0.9,0.5-1.2l9.2-9.2c0.7-0.7,1.7-0.7,2.4,0l5,5\\n\\t\\tc0.6,0.6,0.7,1.6,0.1,2.3c1.5,1.4,2.9,2.5,3.4,2.6c0.4,0.1,1.5,0.2,2.7,0.3c3.6,0.4,9.1,0.9,11.5,1.6c2.1,0.7,2.5,0.9,3.3,1.9\\n\\t\\tc0.4,0.4,0.8,1,2,2c3.6,3.1,5.3,5.7,6,6.9l5.1,0.1l5.3,7.5L48.8,50.8z\\"/>\\n\\t<path class=\\"st0\\" d=\\"M44.3,37.8c-1.3-1.1-2.6-2.4-4.2-2.9c-2.1-0.7-7.6-1.2-10.9-1.5c-1.4-0.1-2.4-0.2-2.9-0.3\\n\\t\\tc-1.4-0.2-3.4-1.9-5-3.4l-5.7,5.7c0.3,0.5,0.5,1,0.7,1.6c0.4,1.2,1,2.6,2.5,4.8c2.5,3.6,4.8,3.6,7.9,3.6c0.4,0,0.9,0,1.4,0\\n\\t\\tc2.7,0.1,7.9-1.7,10.4-2.6c-0.1-0.1-0.3-0.2-0.5-0.3c-0.4-0.2-0.9-0.2-1.3-0.3c-1-0.1-1.7-0.3-2.6-0.4c-1,0-1.7,0.1-2.6,0.4\\n\\t\\tc-1.9,0.6-2.7-2.4-0.8-3c1.2-0.3,2.4-0.6,3.6-0.5c1.2,0.1,2.4,0.3,3.7,0.5c1.2,0.2,2.1,0.8,3.1,1.5c0.5,0.4,1,0.7,1.4,1.2\\n\\t\\tc3.4,4.3,7.7,2.9,7,2C48.8,42.9,47.2,40.4,44.3,37.8z\\"/>\\n</g>\\n</svg>\\n", "price_formula": "90.00 * a + 1000 * b", "displace_goods": [21, 23], "gallery_images": [], "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "description_line_1": "Уберем и очистим вашу квартиру от строительной пыли и мусора", "description_line_2": "90₽ / м²", "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10991	2063	23	0	{"id": 23, "name": "генеральная уборка", "type": 1, "image": "http://192.168.0.15:8000/media/2.svg", "order": 20, "params": [{"id": 8, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 39, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "updated_at": "2019-01-23T14:46:30.188459", "description": "До блеска и неузнаваемости отмоем вашу квартиру", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#65bcf1;}\\n</style>\\n<path class=\\"st0\\" d=\\"M59.7,57.2L59.7,57.2l-0.3,0.1c-1.3,0.5-3.5,0.8-5.3,0.3c-1.7-0.5-2.9-1.7-4.3-2.9c-0.5-0.5-1.1-1-1.5-1.4\\n\\tL43,49l8.4,2.2c0.5,0.1,1.3,0.3,2.1,0.5c3.6,0.8,5.9,1.3,6.8,2.3C61.6,55.5,60.9,56.6,59.7,57.2z M59.1,46.9c0.8-0.4,1-1.2,0-2.3\\n\\tc-1-1.1-8.3-2.3-11.1-3c2.4,1.9,4.9,5,7.2,5.7C56.5,47.7,58.5,47.2,59.1,46.9z M36.9,57.1c1.4,0.4,3.4,0,4.1-0.4\\n\\tc0.9-0.4,1-1.3,0-2.4c-1-1.1-8.7-2.4-11.6-3.2C31.9,53.1,34.5,56.3,36.9,57.1z M35.4,0.6c-6.1,0-11.8,1.5-16.7,4.3\\n\\tc0.9,2.1,1.7,4.1,2.6,6.2c1.5,3.5,2.8,7.2,4.3,10.7c1.1,2.4,1.7,5.2,3.4,5.8c2.2,0.8,5.7,0.8,8,0.7c3.7-0.3,4.1-0.1,8.9-0.2\\n\\tc1.6,0,2.8,1.1,3.4,2.5c1.6,3.6-1,4.3-2,4.7c-5.7,2.5-11.5,5-17.2,7.4c-5.7,2.7-11.3,5.3-17,7.9c-1,0.4-3.2,2-4.8-1.6\\n\\tc-0.6-1.4-0.7-3,0.4-4.2c3.3-3.5,3.3-3.9,6-6.5c1.7-1.6,4-4.2,4.8-6.4c0.6-1.7-1-4-2.1-6.4c-1.6-3.5-3.4-6.9-5.1-10.4\\n\\tc-0.6-1.3-1.2-2.6-1.8-3.9C4.3,17.4,0.6,26,0.6,35.4c0,19.2,15.6,34.8,34.8,34.8c5.9,0,11.5-1.5,16.4-4.1L12.6,52.9l37.1-17l19.9,6\\n\\tc0.4-2.1,0.6-4.3,0.6-6.5C70.3,16.2,54.7,0.6,35.4,0.6z M51.9,66.2C51.8,66.2,51.8,66.2,51.9,66.2L51.9,66.2\\n\\tC51.8,66.2,51.8,66.2,51.9,66.2z M17.8,17c0.2,0,0.5-0.1,0.7-0.2c0.2-0.1,0.4-0.3,0.6-0.4c1-0.7,1.1-1.5,0.5-2.9c-1-2.4-2-4.8-3-7.2\\n\\tc0,0,0-0.1,0-0.1c0.7-0.5,1.4-0.9,2.2-1.3c-2.3,1.3-4.4,2.8-6.4,4.5c1,2,1.9,4.1,2.9,6.1C15.9,16.8,16.6,17.2,17.8,17z M12.4,9.3\\n\\tC12.4,9.3,12.3,9.3,12.4,9.3C12.3,9.3,12.4,9.3,12.4,9.3C12.4,9.3,12.4,9.3,12.4,9.3z M10.4,11.2c0.6-0.6,1.2-1.2,1.9-1.8\\n\\tC11.7,9.9,11.1,10.5,10.4,11.2z M29.5,40.2L29.5,40.2c5.8-2.4,11.5-4.9,17.3-7.3c0.9-0.4,0.7-1.1,0.4-1.7c-0.5-1.1-2.6-0.1-4,0.5\\n\\tc-5,2.2-10.1,4.3-15.2,6.5c-5,2.3-9.9,4.7-14.9,7c-1.3,0.6-3.5,1.6-3,2.7c0.2,0.5,0.6,1.2,1.5,0.8c5.6-2.7,11.3-5.3,17-8l0,0\\n\\tc0.2-0.1,0.3-0.1,0.5-0.2C29.2,40.3,29.4,40.3,29.5,40.2z M43.2,29.7c-1,0-9,0.3-11.4,0.3c-0.6,0-1.2,0.1-1.9,0.1\\n\\tC27.8,30,26.5,29,25.7,27c-1-2.4-2-4.7-3.1-7c-0.6-1.2-1.4-1.6-2.8-1.2c-0.1,0-0.2,0.1-0.3,0.1c-0.1,0.1-0.2,0.1-0.3,0.2\\n\\tc-1.2,0.8-1.5,1.6-0.9,2.9c1,2.3,2,4.7,3.1,7c0.9,2,0.9,3.6-0.5,5.2c-0.4,0.5-0.9,0.9-1.3,1.3c-1.6,1.8-7.2,7.5-7.8,8.3\\n\\tc-0.7,0.8,1.3-0.5,4.3-1.8c2.7-1.2,7-3.3,11-5.2l0,0c0.2-0.1,0.3-0.1,0.5-0.2c0.2-0.1,0.3-0.1,0.5-0.2l0,0c4.1-1.7,8.5-3.5,11.2-4.8\\n\\tC42,30.4,44.2,29.7,43.2,29.7z M69.7,42\\"/>\\n</svg>\\n", "price_formula": "90.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
11003	2068	23	0	{"id": 23, "name": "генеральная уборка", "type": 1, "image": "http://192.168.0.15:8000/media/2.svg", "order": 20, "params": [{"id": 8, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 39, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "updated_at": "2019-01-23T21:35:22.903237", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#65bcf1;}\\n</style>\\n<path class=\\"st0\\" d=\\"M59.7,57.2L59.7,57.2l-0.3,0.1c-1.3,0.5-3.5,0.8-5.3,0.3c-1.7-0.5-2.9-1.7-4.3-2.9c-0.5-0.5-1.1-1-1.5-1.4\\n\\tL43,49l8.4,2.2c0.5,0.1,1.3,0.3,2.1,0.5c3.6,0.8,5.9,1.3,6.8,2.3C61.6,55.5,60.9,56.6,59.7,57.2z M59.1,46.9c0.8-0.4,1-1.2,0-2.3\\n\\tc-1-1.1-8.3-2.3-11.1-3c2.4,1.9,4.9,5,7.2,5.7C56.5,47.7,58.5,47.2,59.1,46.9z M36.9,57.1c1.4,0.4,3.4,0,4.1-0.4\\n\\tc0.9-0.4,1-1.3,0-2.4c-1-1.1-8.7-2.4-11.6-3.2C31.9,53.1,34.5,56.3,36.9,57.1z M35.4,0.6c-6.1,0-11.8,1.5-16.7,4.3\\n\\tc0.9,2.1,1.7,4.1,2.6,6.2c1.5,3.5,2.8,7.2,4.3,10.7c1.1,2.4,1.7,5.2,3.4,5.8c2.2,0.8,5.7,0.8,8,0.7c3.7-0.3,4.1-0.1,8.9-0.2\\n\\tc1.6,0,2.8,1.1,3.4,2.5c1.6,3.6-1,4.3-2,4.7c-5.7,2.5-11.5,5-17.2,7.4c-5.7,2.7-11.3,5.3-17,7.9c-1,0.4-3.2,2-4.8-1.6\\n\\tc-0.6-1.4-0.7-3,0.4-4.2c3.3-3.5,3.3-3.9,6-6.5c1.7-1.6,4-4.2,4.8-6.4c0.6-1.7-1-4-2.1-6.4c-1.6-3.5-3.4-6.9-5.1-10.4\\n\\tc-0.6-1.3-1.2-2.6-1.8-3.9C4.3,17.4,0.6,26,0.6,35.4c0,19.2,15.6,34.8,34.8,34.8c5.9,0,11.5-1.5,16.4-4.1L12.6,52.9l37.1-17l19.9,6\\n\\tc0.4-2.1,0.6-4.3,0.6-6.5C70.3,16.2,54.7,0.6,35.4,0.6z M51.9,66.2C51.8,66.2,51.8,66.2,51.9,66.2L51.9,66.2\\n\\tC51.8,66.2,51.8,66.2,51.9,66.2z M17.8,17c0.2,0,0.5-0.1,0.7-0.2c0.2-0.1,0.4-0.3,0.6-0.4c1-0.7,1.1-1.5,0.5-2.9c-1-2.4-2-4.8-3-7.2\\n\\tc0,0,0-0.1,0-0.1c0.7-0.5,1.4-0.9,2.2-1.3c-2.3,1.3-4.4,2.8-6.4,4.5c1,2,1.9,4.1,2.9,6.1C15.9,16.8,16.6,17.2,17.8,17z M12.4,9.3\\n\\tC12.4,9.3,12.3,9.3,12.4,9.3C12.3,9.3,12.4,9.3,12.4,9.3C12.4,9.3,12.4,9.3,12.4,9.3z M10.4,11.2c0.6-0.6,1.2-1.2,1.9-1.8\\n\\tC11.7,9.9,11.1,10.5,10.4,11.2z M29.5,40.2L29.5,40.2c5.8-2.4,11.5-4.9,17.3-7.3c0.9-0.4,0.7-1.1,0.4-1.7c-0.5-1.1-2.6-0.1-4,0.5\\n\\tc-5,2.2-10.1,4.3-15.2,6.5c-5,2.3-9.9,4.7-14.9,7c-1.3,0.6-3.5,1.6-3,2.7c0.2,0.5,0.6,1.2,1.5,0.8c5.6-2.7,11.3-5.3,17-8l0,0\\n\\tc0.2-0.1,0.3-0.1,0.5-0.2C29.2,40.3,29.4,40.3,29.5,40.2z M43.2,29.7c-1,0-9,0.3-11.4,0.3c-0.6,0-1.2,0.1-1.9,0.1\\n\\tC27.8,30,26.5,29,25.7,27c-1-2.4-2-4.7-3.1-7c-0.6-1.2-1.4-1.6-2.8-1.2c-0.1,0-0.2,0.1-0.3,0.1c-0.1,0.1-0.2,0.1-0.3,0.2\\n\\tc-1.2,0.8-1.5,1.6-0.9,2.9c1,2.3,2,4.7,3.1,7c0.9,2,0.9,3.6-0.5,5.2c-0.4,0.5-0.9,0.9-1.3,1.3c-1.6,1.8-7.2,7.5-7.8,8.3\\n\\tc-0.7,0.8,1.3-0.5,4.3-1.8c2.7-1.2,7-3.3,11-5.2l0,0c0.2-0.1,0.3-0.1,0.5-0.2c0.2-0.1,0.3-0.1,0.5-0.2l0,0c4.1-1.7,8.5-3.5,11.2-4.8\\n\\tC42,30.4,44.2,29.7,43.2,29.7z M69.7,42\\"/>\\n</svg>\\n", "price_formula": "90.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "description_line_1": "До блеска и неузнаваемости отмоем вашу квартиру", "description_line_2": "90₽ / м²", "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10961	2056	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/1_0R4WU7Q.svg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2019-01-22T13:02:31.843205", "description": "Для сохранения постоянной чистоты в вашем доме", "price_formula": "36.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
11004	2067	23	0	{"id": 23, "name": "генеральная уборка", "type": 1, "image": "http://192.168.0.15:8000/media/2.svg", "order": 20, "params": [{"id": 8, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 39, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "updated_at": "2019-01-23T14:46:30.188459", "description": "До блеска и неузнаваемости отмоем вашу квартиру", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#65bcf1;}\\n</style>\\n<path class=\\"st0\\" d=\\"M59.7,57.2L59.7,57.2l-0.3,0.1c-1.3,0.5-3.5,0.8-5.3,0.3c-1.7-0.5-2.9-1.7-4.3-2.9c-0.5-0.5-1.1-1-1.5-1.4\\n\\tL43,49l8.4,2.2c0.5,0.1,1.3,0.3,2.1,0.5c3.6,0.8,5.9,1.3,6.8,2.3C61.6,55.5,60.9,56.6,59.7,57.2z M59.1,46.9c0.8-0.4,1-1.2,0-2.3\\n\\tc-1-1.1-8.3-2.3-11.1-3c2.4,1.9,4.9,5,7.2,5.7C56.5,47.7,58.5,47.2,59.1,46.9z M36.9,57.1c1.4,0.4,3.4,0,4.1-0.4\\n\\tc0.9-0.4,1-1.3,0-2.4c-1-1.1-8.7-2.4-11.6-3.2C31.9,53.1,34.5,56.3,36.9,57.1z M35.4,0.6c-6.1,0-11.8,1.5-16.7,4.3\\n\\tc0.9,2.1,1.7,4.1,2.6,6.2c1.5,3.5,2.8,7.2,4.3,10.7c1.1,2.4,1.7,5.2,3.4,5.8c2.2,0.8,5.7,0.8,8,0.7c3.7-0.3,4.1-0.1,8.9-0.2\\n\\tc1.6,0,2.8,1.1,3.4,2.5c1.6,3.6-1,4.3-2,4.7c-5.7,2.5-11.5,5-17.2,7.4c-5.7,2.7-11.3,5.3-17,7.9c-1,0.4-3.2,2-4.8-1.6\\n\\tc-0.6-1.4-0.7-3,0.4-4.2c3.3-3.5,3.3-3.9,6-6.5c1.7-1.6,4-4.2,4.8-6.4c0.6-1.7-1-4-2.1-6.4c-1.6-3.5-3.4-6.9-5.1-10.4\\n\\tc-0.6-1.3-1.2-2.6-1.8-3.9C4.3,17.4,0.6,26,0.6,35.4c0,19.2,15.6,34.8,34.8,34.8c5.9,0,11.5-1.5,16.4-4.1L12.6,52.9l37.1-17l19.9,6\\n\\tc0.4-2.1,0.6-4.3,0.6-6.5C70.3,16.2,54.7,0.6,35.4,0.6z M51.9,66.2C51.8,66.2,51.8,66.2,51.9,66.2L51.9,66.2\\n\\tC51.8,66.2,51.8,66.2,51.9,66.2z M17.8,17c0.2,0,0.5-0.1,0.7-0.2c0.2-0.1,0.4-0.3,0.6-0.4c1-0.7,1.1-1.5,0.5-2.9c-1-2.4-2-4.8-3-7.2\\n\\tc0,0,0-0.1,0-0.1c0.7-0.5,1.4-0.9,2.2-1.3c-2.3,1.3-4.4,2.8-6.4,4.5c1,2,1.9,4.1,2.9,6.1C15.9,16.8,16.6,17.2,17.8,17z M12.4,9.3\\n\\tC12.4,9.3,12.3,9.3,12.4,9.3C12.3,9.3,12.4,9.3,12.4,9.3C12.4,9.3,12.4,9.3,12.4,9.3z M10.4,11.2c0.6-0.6,1.2-1.2,1.9-1.8\\n\\tC11.7,9.9,11.1,10.5,10.4,11.2z M29.5,40.2L29.5,40.2c5.8-2.4,11.5-4.9,17.3-7.3c0.9-0.4,0.7-1.1,0.4-1.7c-0.5-1.1-2.6-0.1-4,0.5\\n\\tc-5,2.2-10.1,4.3-15.2,6.5c-5,2.3-9.9,4.7-14.9,7c-1.3,0.6-3.5,1.6-3,2.7c0.2,0.5,0.6,1.2,1.5,0.8c5.6-2.7,11.3-5.3,17-8l0,0\\n\\tc0.2-0.1,0.3-0.1,0.5-0.2C29.2,40.3,29.4,40.3,29.5,40.2z M43.2,29.7c-1,0-9,0.3-11.4,0.3c-0.6,0-1.2,0.1-1.9,0.1\\n\\tC27.8,30,26.5,29,25.7,27c-1-2.4-2-4.7-3.1-7c-0.6-1.2-1.4-1.6-2.8-1.2c-0.1,0-0.2,0.1-0.3,0.1c-0.1,0.1-0.2,0.1-0.3,0.2\\n\\tc-1.2,0.8-1.5,1.6-0.9,2.9c1,2.3,2,4.7,3.1,7c0.9,2,0.9,3.6-0.5,5.2c-0.4,0.5-0.9,0.9-1.3,1.3c-1.6,1.8-7.2,7.5-7.8,8.3\\n\\tc-0.7,0.8,1.3-0.5,4.3-1.8c2.7-1.2,7-3.3,11-5.2l0,0c0.2-0.1,0.3-0.1,0.5-0.2c0.2-0.1,0.3-0.1,0.5-0.2l0,0c4.1-1.7,8.5-3.5,11.2-4.8\\n\\tC42,30.4,44.2,29.7,43.2,29.7z M69.7,42\\"/>\\n</svg>\\n", "price_formula": "90.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10884	2049	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/HCu_NVM4hhM_NM4AWAP.jpg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2018-12-03T21:44:48.119000", "description": "Для сохранения постоянной чистоты в вашем доме", "price_formula": "36.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10806	2046	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/HCu_NVM4hhM_NM4AWAP.jpg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2018-12-03T21:44:48.119000", "description": "Для сохранения постоянной чистоты в вашем доме", "price_formula": "36.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10993	2064	23	0	{"id": 23, "name": "генеральная уборка", "type": 1, "image": "http://192.168.0.15:8000/media/2.svg", "order": 20, "params": [{"id": 8, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 39, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "updated_at": "2019-01-23T14:46:30.188459", "description": "До блеска и неузнаваемости отмоем вашу квартиру", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#65bcf1;}\\n</style>\\n<path class=\\"st0\\" d=\\"M59.7,57.2L59.7,57.2l-0.3,0.1c-1.3,0.5-3.5,0.8-5.3,0.3c-1.7-0.5-2.9-1.7-4.3-2.9c-0.5-0.5-1.1-1-1.5-1.4\\n\\tL43,49l8.4,2.2c0.5,0.1,1.3,0.3,2.1,0.5c3.6,0.8,5.9,1.3,6.8,2.3C61.6,55.5,60.9,56.6,59.7,57.2z M59.1,46.9c0.8-0.4,1-1.2,0-2.3\\n\\tc-1-1.1-8.3-2.3-11.1-3c2.4,1.9,4.9,5,7.2,5.7C56.5,47.7,58.5,47.2,59.1,46.9z M36.9,57.1c1.4,0.4,3.4,0,4.1-0.4\\n\\tc0.9-0.4,1-1.3,0-2.4c-1-1.1-8.7-2.4-11.6-3.2C31.9,53.1,34.5,56.3,36.9,57.1z M35.4,0.6c-6.1,0-11.8,1.5-16.7,4.3\\n\\tc0.9,2.1,1.7,4.1,2.6,6.2c1.5,3.5,2.8,7.2,4.3,10.7c1.1,2.4,1.7,5.2,3.4,5.8c2.2,0.8,5.7,0.8,8,0.7c3.7-0.3,4.1-0.1,8.9-0.2\\n\\tc1.6,0,2.8,1.1,3.4,2.5c1.6,3.6-1,4.3-2,4.7c-5.7,2.5-11.5,5-17.2,7.4c-5.7,2.7-11.3,5.3-17,7.9c-1,0.4-3.2,2-4.8-1.6\\n\\tc-0.6-1.4-0.7-3,0.4-4.2c3.3-3.5,3.3-3.9,6-6.5c1.7-1.6,4-4.2,4.8-6.4c0.6-1.7-1-4-2.1-6.4c-1.6-3.5-3.4-6.9-5.1-10.4\\n\\tc-0.6-1.3-1.2-2.6-1.8-3.9C4.3,17.4,0.6,26,0.6,35.4c0,19.2,15.6,34.8,34.8,34.8c5.9,0,11.5-1.5,16.4-4.1L12.6,52.9l37.1-17l19.9,6\\n\\tc0.4-2.1,0.6-4.3,0.6-6.5C70.3,16.2,54.7,0.6,35.4,0.6z M51.9,66.2C51.8,66.2,51.8,66.2,51.9,66.2L51.9,66.2\\n\\tC51.8,66.2,51.8,66.2,51.9,66.2z M17.8,17c0.2,0,0.5-0.1,0.7-0.2c0.2-0.1,0.4-0.3,0.6-0.4c1-0.7,1.1-1.5,0.5-2.9c-1-2.4-2-4.8-3-7.2\\n\\tc0,0,0-0.1,0-0.1c0.7-0.5,1.4-0.9,2.2-1.3c-2.3,1.3-4.4,2.8-6.4,4.5c1,2,1.9,4.1,2.9,6.1C15.9,16.8,16.6,17.2,17.8,17z M12.4,9.3\\n\\tC12.4,9.3,12.3,9.3,12.4,9.3C12.3,9.3,12.4,9.3,12.4,9.3C12.4,9.3,12.4,9.3,12.4,9.3z M10.4,11.2c0.6-0.6,1.2-1.2,1.9-1.8\\n\\tC11.7,9.9,11.1,10.5,10.4,11.2z M29.5,40.2L29.5,40.2c5.8-2.4,11.5-4.9,17.3-7.3c0.9-0.4,0.7-1.1,0.4-1.7c-0.5-1.1-2.6-0.1-4,0.5\\n\\tc-5,2.2-10.1,4.3-15.2,6.5c-5,2.3-9.9,4.7-14.9,7c-1.3,0.6-3.5,1.6-3,2.7c0.2,0.5,0.6,1.2,1.5,0.8c5.6-2.7,11.3-5.3,17-8l0,0\\n\\tc0.2-0.1,0.3-0.1,0.5-0.2C29.2,40.3,29.4,40.3,29.5,40.2z M43.2,29.7c-1,0-9,0.3-11.4,0.3c-0.6,0-1.2,0.1-1.9,0.1\\n\\tC27.8,30,26.5,29,25.7,27c-1-2.4-2-4.7-3.1-7c-0.6-1.2-1.4-1.6-2.8-1.2c-0.1,0-0.2,0.1-0.3,0.1c-0.1,0.1-0.2,0.1-0.3,0.2\\n\\tc-1.2,0.8-1.5,1.6-0.9,2.9c1,2.3,2,4.7,3.1,7c0.9,2,0.9,3.6-0.5,5.2c-0.4,0.5-0.9,0.9-1.3,1.3c-1.6,1.8-7.2,7.5-7.8,8.3\\n\\tc-0.7,0.8,1.3-0.5,4.3-1.8c2.7-1.2,7-3.3,11-5.2l0,0c0.2-0.1,0.3-0.1,0.5-0.2c0.2-0.1,0.3-0.1,0.5-0.2l0,0c4.1-1.7,8.5-3.5,11.2-4.8\\n\\tC42,30.4,44.2,29.7,43.2,29.7z M69.7,42\\"/>\\n</svg>\\n", "price_formula": "90.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10947	2052	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/HCu_NVM4hhM_NM4AWAP.jpg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2018-12-03T21:44:48.119000", "description": "Для сохранения постоянной чистоты в вашем доме", "price_formula": "36.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10980	2060	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/1_0R4WU7Q.svg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2019-01-22T13:16:03.538209", "description": "Для сохранения постоянной чистоты в вашем доме", "price_formula": "36.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
10981	2060	23	0	{"id": 23, "name": "генеральная уборка", "type": 1, "image": "http://192.168.0.15:8000/media/2.svg", "order": 20, "params": [{"id": 8, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 39, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2019-01-22T13:02:56.738768", "description": "До блеска и неузнаваемости отмоем вашу квартиру", "price_formula": "90.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
11335	2110	53	0	{"id": 53, "name": "стул без спинки", "type": 1, "image": null, "order": 10, "params": [{"id": 6, "max": 100, "min": 0, "name": "количество", "step": 1, "unit": "шт.", "default": 0, "formula_letter": "a"}], "is_alive": true, "parent_id": 43, "updated_at": "2019-01-23T14:46:30.360381", "image_content": null, "price_formula": "250.00 * a", "displace_goods": [], "gallery_images": [], "is_slave_goods": true, "force_to_basket": false, "additional_goods": [], "description_lines": [], "description_line_1": "", "description_line_2": null, "details_button_text": null, "not_display_in_catalog": false}	t
11336	2110	54	0	{"id": 54, "name": "стул со спинкой", "type": 1, "image": null, "order": 20, "params": [{"id": 11, "max": 100, "min": 0, "name": "количество", "step": 1, "unit": "шт.", "default": 0, "formula_letter": "a"}], "is_alive": true, "parent_id": 43, "updated_at": "2019-01-23T14:46:30.366885", "image_content": null, "price_formula": "300.00 * a", "displace_goods": [], "gallery_images": [], "is_slave_goods": true, "force_to_basket": false, "additional_goods": [], "description_lines": [], "description_line_1": "", "description_line_2": null, "details_button_text": null, "not_display_in_catalog": false}	t
10847	2048	21	0	{"id": 21, "name": "поддерживающая уборка", "type": 1, "image": "http://192.168.0.15:8000/media/HCu_NVM4hhM_NM4AWAP.jpg", "order": 10, "params": [{"id": 1, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "кв.м.", "default": 35, "formula_letter": "a"}, {"id": 38, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "min_price": null, "parent_id": 0, "unique_set": 1, "updated_at": "2018-12-03T21:44:48.119000", "description": "Для сохранения постоянной чистоты в вашем доме", "price_formula": "36.00 * a + 1000 * b", "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "image_need_border": false, "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
11337	2110	60	0	{"id": 60, "name": "«П» образный диван", "type": 1, "image": null, "order": 80, "params": [{"id": 36, "max": 100, "min": 0, "name": "количество", "step": 1, "unit": "шт.", "default": 0, "formula_letter": "a"}], "is_alive": true, "parent_id": 43, "updated_at": "2019-01-23T14:46:30.404947", "image_content": null, "price_formula": "3200.00 * a", "displace_goods": [], "gallery_images": [], "is_slave_goods": true, "force_to_basket": false, "additional_goods": [], "description_lines": [], "description_line_1": "", "description_line_2": null, "details_button_text": null, "not_display_in_catalog": false}	t
11377	2113	24	0	{"id": 24, "name": "уборка после ремонта", "type": 1, "image": "http://192.168.0.15:8000/media/3.svg", "order": 30, "params": [{"id": 13, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "м²", "default": 35, "formula_letter": "a"}, {"id": 40, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "parent_id": 0, "updated_at": "2019-01-24T00:02:46.814855", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#66BCEE;}\\n</style>\\n<g>\\n\\t<polygon class=\\"st0\\" points=\\"55.9,45.2 51.4,45.2 53.9,48.7 58.4,48.7 \\t\\"/>\\n\\t<path class=\\"st0\\" d=\\"M35.4,0.6C16.2,0.6,0.6,16.2,0.6,35.4s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.7,0.6,35.4,0.6z M56.5,23.5\\n\\t\\tc0.6,3.2,2,4.8,5.3,5.3c-3.2,0.6-4.8,2.3-5.3,4.9c-0.5-2.7-2-4.3-5.2-4.9C54.5,28.3,56,26.7,56.5,23.5z M39.5,13.6\\n\\t\\tc1.9-1.9,2.6-5,3.8-7.3c0.6,6.2,4.1,10.1,11,10.9c-6.7,0.9-10.5,4.6-11.3,11.5c-0.8-6.7-4.3-10.6-10.7-11.1\\n\\t\\tC34.7,16.3,37.7,15.5,39.5,13.6z M24.2,64.4c-0.5-2.7-2-4.3-5.2-4.9c3.1-0.6,4.6-2.2,5.2-5.4c0.6,3.2,2,4.8,5.3,5.3\\n\\t\\tC26.3,60.1,24.7,61.7,24.2,64.4z M48.8,50.8L48.8,50.8l-4.2-1.6l-13.5-1.2c-1,0.2-1.9,0.3-2.7,0.3c-0.1,0-0.2,0-0.3,0\\n\\t\\tc-0.4,0-0.9,0-1.3,0c-3.3,0-6.8-0.1-10.2-4.8c-1.8-2.5-2.4-4.2-2.9-5.4c-0.1-0.2-0.2-0.5-0.2-0.6c-0.2,0.1-0.3,0.1-0.5,0.1\\n\\t\\tc-0.5,0-0.9-0.2-1.2-0.5l-5-5c-0.3-0.3-0.5-0.7-0.5-1.2c0-0.5,0.2-0.9,0.5-1.2l9.2-9.2c0.7-0.7,1.7-0.7,2.4,0l5,5\\n\\t\\tc0.6,0.6,0.7,1.6,0.1,2.3c1.5,1.4,2.9,2.5,3.4,2.6c0.4,0.1,1.5,0.2,2.7,0.3c3.6,0.4,9.1,0.9,11.5,1.6c2.1,0.7,2.5,0.9,3.3,1.9\\n\\t\\tc0.4,0.4,0.8,1,2,2c3.6,3.1,5.3,5.7,6,6.9l5.1,0.1l5.3,7.5L48.8,50.8z\\"/>\\n\\t<path class=\\"st0\\" d=\\"M44.3,37.8c-1.3-1.1-2.6-2.4-4.2-2.9c-2.1-0.7-7.6-1.2-10.9-1.5c-1.4-0.1-2.4-0.2-2.9-0.3\\n\\t\\tc-1.4-0.2-3.4-1.9-5-3.4l-5.7,5.7c0.3,0.5,0.5,1,0.7,1.6c0.4,1.2,1,2.6,2.5,4.8c2.5,3.6,4.8,3.6,7.9,3.6c0.4,0,0.9,0,1.4,0\\n\\t\\tc2.7,0.1,7.9-1.7,10.4-2.6c-0.1-0.1-0.3-0.2-0.5-0.3c-0.4-0.2-0.9-0.2-1.3-0.3c-1-0.1-1.7-0.3-2.6-0.4c-1,0-1.7,0.1-2.6,0.4\\n\\t\\tc-1.9,0.6-2.7-2.4-0.8-3c1.2-0.3,2.4-0.6,3.6-0.5c1.2,0.1,2.4,0.3,3.7,0.5c1.2,0.2,2.1,0.8,3.1,1.5c0.5,0.4,1,0.7,1.4,1.2\\n\\t\\tc3.4,4.3,7.7,2.9,7,2C48.8,42.9,47.2,40.4,44.3,37.8z\\"/>\\n</g>\\n</svg>\\n", "price_formula": "90.00 * a + 1000 * b", "displace_goods": [21, 23], "gallery_images": [], "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "description_line_1": "Уберем и очистим вашу квартиру от строительной пыли и мусора", "description_line_2": "90₽ / м²", "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
11378	2113	61	0	{"id": 61, "name": "чистка духовки внутри", "type": 1, "image": null, "order": 10, "params": [{"id": 7, "max": 100, "min": 0, "name": "количество", "step": 1, "unit": "шт.", "default": 0, "formula_letter": "a"}], "is_alive": true, "parent_id": 29, "updated_at": "2019-01-23T14:46:30.412603", "image_content": null, "price_formula": "450.00 * a", "displace_goods": [], "gallery_images": [], "is_slave_goods": true, "force_to_basket": false, "additional_goods": [], "description_lines": [], "description_line_1": "", "description_line_2": null, "details_button_text": null, "not_display_in_catalog": false}	f
11379	2113	62	0	{"id": 62, "name": "чистка холодильника внутри", "type": 1, "image": null, "order": 20, "params": [{"id": 12, "max": 100, "min": 0, "name": "количество", "step": 1, "unit": "шт.", "default": 0, "formula_letter": "a"}], "is_alive": true, "parent_id": 29, "updated_at": "2019-01-23T14:46:30.417780", "image_content": null, "price_formula": "450.00 * a", "displace_goods": [], "gallery_images": [], "is_slave_goods": true, "force_to_basket": false, "additional_goods": [], "description_lines": [], "description_line_1": "", "description_line_2": null, "details_button_text": null, "not_display_in_catalog": false}	f
11123	2076	24	0	{"id": 24, "name": "уборка после ремонта", "type": 1, "image": "http://192.168.0.15:8000/media/3.svg", "order": 30, "params": [{"id": 13, "max": 10000, "min": 35, "name": "площадь помещения", "step": 5, "unit": "м²", "default": 35, "formula_letter": "a"}, {"id": 40, "max": 100, "min": 0, "name": "количество сан. узлов", "step": 1, "unit": "шт.", "default": 1, "formula_letter": "b"}], "is_alive": true, "parent_id": 0, "updated_at": "2019-01-24T00:02:46.814855", "image_content": "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>\\n<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->\\n<svg version=\\"1.1\\" id=\\"Layer_1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" x=\\"0px\\" y=\\"0px\\"\\n\\t viewBox=\\"0 0 70.9 70.9\\" style=\\"enable-background:new 0 0 70.9 70.9;\\" xml:space=\\"preserve\\">\\n<style type=\\"text/css\\">\\n\\t.st0{fill:#66BCEE;}\\n</style>\\n<g>\\n\\t<polygon class=\\"st0\\" points=\\"55.9,45.2 51.4,45.2 53.9,48.7 58.4,48.7 \\t\\"/>\\n\\t<path class=\\"st0\\" d=\\"M35.4,0.6C16.2,0.6,0.6,16.2,0.6,35.4s15.6,34.8,34.8,34.8s34.8-15.6,34.8-34.8S54.7,0.6,35.4,0.6z M56.5,23.5\\n\\t\\tc0.6,3.2,2,4.8,5.3,5.3c-3.2,0.6-4.8,2.3-5.3,4.9c-0.5-2.7-2-4.3-5.2-4.9C54.5,28.3,56,26.7,56.5,23.5z M39.5,13.6\\n\\t\\tc1.9-1.9,2.6-5,3.8-7.3c0.6,6.2,4.1,10.1,11,10.9c-6.7,0.9-10.5,4.6-11.3,11.5c-0.8-6.7-4.3-10.6-10.7-11.1\\n\\t\\tC34.7,16.3,37.7,15.5,39.5,13.6z M24.2,64.4c-0.5-2.7-2-4.3-5.2-4.9c3.1-0.6,4.6-2.2,5.2-5.4c0.6,3.2,2,4.8,5.3,5.3\\n\\t\\tC26.3,60.1,24.7,61.7,24.2,64.4z M48.8,50.8L48.8,50.8l-4.2-1.6l-13.5-1.2c-1,0.2-1.9,0.3-2.7,0.3c-0.1,0-0.2,0-0.3,0\\n\\t\\tc-0.4,0-0.9,0-1.3,0c-3.3,0-6.8-0.1-10.2-4.8c-1.8-2.5-2.4-4.2-2.9-5.4c-0.1-0.2-0.2-0.5-0.2-0.6c-0.2,0.1-0.3,0.1-0.5,0.1\\n\\t\\tc-0.5,0-0.9-0.2-1.2-0.5l-5-5c-0.3-0.3-0.5-0.7-0.5-1.2c0-0.5,0.2-0.9,0.5-1.2l9.2-9.2c0.7-0.7,1.7-0.7,2.4,0l5,5\\n\\t\\tc0.6,0.6,0.7,1.6,0.1,2.3c1.5,1.4,2.9,2.5,3.4,2.6c0.4,0.1,1.5,0.2,2.7,0.3c3.6,0.4,9.1,0.9,11.5,1.6c2.1,0.7,2.5,0.9,3.3,1.9\\n\\t\\tc0.4,0.4,0.8,1,2,2c3.6,3.1,5.3,5.7,6,6.9l5.1,0.1l5.3,7.5L48.8,50.8z\\"/>\\n\\t<path class=\\"st0\\" d=\\"M44.3,37.8c-1.3-1.1-2.6-2.4-4.2-2.9c-2.1-0.7-7.6-1.2-10.9-1.5c-1.4-0.1-2.4-0.2-2.9-0.3\\n\\t\\tc-1.4-0.2-3.4-1.9-5-3.4l-5.7,5.7c0.3,0.5,0.5,1,0.7,1.6c0.4,1.2,1,2.6,2.5,4.8c2.5,3.6,4.8,3.6,7.9,3.6c0.4,0,0.9,0,1.4,0\\n\\t\\tc2.7,0.1,7.9-1.7,10.4-2.6c-0.1-0.1-0.3-0.2-0.5-0.3c-0.4-0.2-0.9-0.2-1.3-0.3c-1-0.1-1.7-0.3-2.6-0.4c-1,0-1.7,0.1-2.6,0.4\\n\\t\\tc-1.9,0.6-2.7-2.4-0.8-3c1.2-0.3,2.4-0.6,3.6-0.5c1.2,0.1,2.4,0.3,3.7,0.5c1.2,0.2,2.1,0.8,3.1,1.5c0.5,0.4,1,0.7,1.4,1.2\\n\\t\\tc3.4,4.3,7.7,2.9,7,2C48.8,42.9,47.2,40.4,44.3,37.8z\\"/>\\n</g>\\n</svg>\\n", "price_formula": "90.00 * a + 1000 * b", "displace_goods": [21, 23], "gallery_images": [], "is_slave_goods": null, "force_to_basket": true, "additional_goods": [29], "description_lines": [], "description_line_1": "Уберем и очистим вашу квартиру от строительной пыли и мусора", "description_line_2": "90₽ / м²", "details_button_text": "что входит в уборку?", "not_display_in_catalog": false}	t
\.


--
-- Name: goods_orderitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.goods_orderitem_id_seq', 30682, true);


--
-- Data for Name: goods_orderitemparamvalue; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.goods_orderitemparamvalue (id, float_value, order_item_id, param_id) FROM stdin;
14608	35	10778	1
14609	1	10778	38
15023	60	11003	8
15024	1	11003	39
15025	35	11004	8
15026	1	11004	39
14616	35	10782	1
14617	1	10782	38
15144	1	11097	38
15145	35	11097	1
14778	35	10863	8
14779	1	10863	39
14780	1	10864	40
14781	35	10864	13
14782	1	10865	38
14783	35	10865	1
15147	35	11099	13
15148	1	11099	40
15149	35	11100	13
14632	1	10790	38
14633	35	10790	1
14634	1	10791	38
15037	1	11015	27
15038	2	11016	32
14637	35	10791	1
15150	3	11101	7
15151	1	11100	40
14919	1	10947	38
14920	35	10947	1
15152	2	11102	12
15153	3	11103	38
15154	40	11103	1
15155	1	11104	3
15156	1	11105	4
15157	1	11106	26
14927	1	10953	39
14928	35	10953	8
14862	35	10905	1
14863	1	10905	38
14929	1	10954	16
14930	1	10955	21
14931	1	10956	24
14932	1	10957	32
14933	1	10958	38
14934	35	10958	1
14935	1	10959	38
14936	35	10959	1
14937	1	10960	38
14938	35	10960	1
14939	1	10961	38
14940	35	10961	1
14664	1	10806	38
14665	35	10806	1
14941	1	10962	38
14942	35	10962	1
14943	1	10963	38
14944	35	10963	1
14814	1	10881	38
14815	35	10881	1
14746	1	10847	38
14747	35	10847	1
15162	1	11111	4
15163	3	11112	15
14820	35	10884	1
14821	1	10884	38
15167	35	11115	8
15168	1	11115	39
15169	19	11116	41
14957	35	10970	1
14958	1	10970	38
14959	1	10971	39
14960	35	10971	8
15438	1	11335	6
15439	1	11336	11
15440	1	11337	36
15180	1	11122	38
15181	35	11122	1
15182	35	11123	13
15183	1	11123	40
15184	35	11124	1
15185	1	11124	38
15186	35	11125	13
15187	1	11125	40
15188	35	11126	13
15189	1	11126	40
14977	35	10980	1
14978	1	10980	38
14979	1	10981	39
14980	35	10981	8
15190	35	11127	1
15191	1	11127	38
14983	1	10983	40
14984	35	10983	13
14985	1	10984	39
14986	65	10984	8
14995	35	10989	13
14996	1	10989	40
14999	35	10991	8
15000	1	10991	39
15003	35	10993	8
15004	1	10993	39
15007	35	10995	8
15008	1	10995	39
15011	1	10997	40
15012	35	10997	13
15493	35	11377	13
15494	1	11377	40
15495	1	11378	7
15496	1	11379	12
15210	35	11138	13
15211	1	11138	40
15212	1	11139	15
15213	35	11140	13
15214	1	11140	40
\.


--
-- Name: goods_orderitemparamvalue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.goods_orderitemparamvalue_id_seq', 39944, true);


--
-- Data for Name: instances_colorscheme; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.instances_colorscheme (id, system_name, verbose_name, created_at, is_alive, updated_at, base_accent, base_additional, button_bg, button_color, base_color, base_bg, menu_bg, menu_accent, menu_color, base_border, menu_border, button_border, button_disabled_bg, button_disabled_border, button_disabled_color, button_error_bg, button_error_border, button_error_color, button_error_border_color) FROM stdin;
1	CLEANSIDE	CLEANSIDE	2018-10-22 15:27:28.223+03	t	2018-11-12 17:38:16.471+03	338ec7	777	65bcf1	fff	444444	fff	f9f9f9	0095e6	8f8f8f	ddd	ddd	65bcf1	f9f9f9	ddd	8f8f8f	fef1f1	f16565	f16565	fff
\.


--
-- Name: instances_colorscheme_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.instances_colorscheme_id_seq', 1, true);


--
-- Data for Name: instances_instance; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.instances_instance (id, name, created_at, is_alive, updated_at, admin_id) FROM stdin;
1	PRESENTATION	2018-10-22 15:21:23.475+03	t	2018-10-22 15:21:23.475+03	1
2	CLEANSIDE	2018-10-22 15:24:38.547+03	t	2018-11-12 17:38:16.476+03	2
\.


--
-- Name: instances_instance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.instances_instance_id_seq', 2, true);


--
-- Data for Name: instances_instancepage; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.instances_instancepage (id, created_at, updated_at, is_alive, "order", instance_id, page_class_id) FROM stdin;
1	2018-10-22 16:03:30.172+03	2018-10-22 16:05:39.251+03	t	10	2	1
2	2018-10-22 16:05:39.253+03	2018-10-22 16:05:39.253+03	t	20	2	3
3	2018-10-22 16:05:39.254+03	2018-10-22 16:05:39.254+03	t	30	2	4
\.


--
-- Data for Name: instances_instancesettings; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.instances_instancesettings (id, created_at, updated_at, is_alive, logo, color_scheme_id, instance_id, company_name, open_logo_id) FROM stdin;
1	2018-10-22 15:21:23.48+03	2018-10-22 15:21:23.48+03	t		\N	1	\N	\N
2	2018-10-22 15:24:38.551+03	2018-10-22 15:49:59.347+03	t		1	2	Чистая сторона	\N
\.


--
-- Name: instances_instancesettings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.instances_instancesettings_id_seq', 2, true);


--
-- Data for Name: instances_openlogo; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.instances_openlogo (id, created_at, updated_at, is_alive, name, logo) FROM stdin;
\.


--
-- Name: instances_openlogo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.instances_openlogo_id_seq', 1, false);


--
-- Name: instances_page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.instances_page_id_seq', 3, true);


--
-- Data for Name: instances_pageclass; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.instances_pageclass (id, created_at, updated_at, is_alive, system_name, verbose_name, "order") FROM stdin;
1	2018-10-22 16:01:22.289+03	2018-10-22 16:02:21.869+03	t	goods	услуги/товары	10
2	2018-10-22 16:01:46.332+03	2018-10-22 16:02:26.828+03	t	faq	вопосы/ответы	20
3	2018-10-22 16:02:16.229+03	2018-10-22 16:02:30.86+03	t	chat	чат	30
4	2018-10-22 16:02:46.506+03	2018-10-22 16:02:53.591+03	t	profile	личный кабинет	40
\.


--
-- Name: instances_pageclass_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.instances_pageclass_id_seq', 4, true);


--
-- Name: reset_reset_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.reset_reset_id_seq', 106, true);


--
-- Data for Name: reset_resetrequest; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.reset_resetrequest (id, created_at, updated_at, is_alive, instance_id) FROM stdin;
1	2018-12-09 23:00:55.956167+03	2018-12-09 23:00:55.956167+03	t	\N
2	2018-12-09 23:01:22.601389+03	2018-12-09 23:01:22.601389+03	t	\N
3	2018-12-28 19:04:09.668554+03	2018-12-28 19:04:09.668554+03	t	\N
4	2018-12-28 19:06:09.620135+03	2018-12-28 19:06:09.620135+03	t	\N
5	2018-12-28 19:06:33.606945+03	2018-12-28 19:06:33.606945+03	t	\N
6	2018-12-30 19:55:07.194019+03	2018-12-30 19:55:07.194019+03	t	\N
7	2018-12-30 19:59:32.783965+03	2018-12-30 19:59:32.783965+03	t	\N
8	2019-01-03 22:04:56.061715+03	2019-01-03 22:04:56.061715+03	t	\N
9	2019-01-03 22:07:05.799541+03	2019-01-03 22:07:05.799541+03	t	\N
10	2019-01-03 22:24:36.369021+03	2019-01-03 22:24:36.369021+03	t	2
11	2019-01-03 22:24:43.651195+03	2019-01-03 22:24:43.651195+03	t	2
12	2019-01-03 22:26:02.116939+03	2019-01-03 22:26:02.116939+03	t	2
13	2019-01-03 22:26:37.939868+03	2019-01-03 22:26:37.939868+03	t	2
14	2019-01-03 22:26:55.476134+03	2019-01-03 22:26:55.476134+03	t	2
15	2019-01-03 22:27:21.39679+03	2019-01-03 22:27:21.39679+03	t	2
16	2019-01-03 22:27:31.300305+03	2019-01-03 22:27:31.300305+03	t	2
17	2019-01-03 22:27:42.783236+03	2019-01-03 22:27:42.783236+03	t	2
18	2019-01-03 22:27:58.370296+03	2019-01-03 22:27:58.370296+03	t	2
19	2019-01-03 22:29:05.625715+03	2019-01-03 22:29:05.625715+03	t	2
20	2019-01-03 22:29:11.691281+03	2019-01-03 22:29:11.691281+03	t	2
21	2019-01-03 22:32:06.217006+03	2019-01-03 22:32:06.217006+03	t	2
22	2019-01-03 22:32:45.002928+03	2019-01-03 22:32:45.002928+03	t	2
23	2019-01-03 22:34:45.753369+03	2019-01-03 22:34:45.753369+03	t	2
24	2019-01-03 22:34:49.786579+03	2019-01-03 22:34:49.786579+03	t	2
25	2019-01-03 22:37:11.230119+03	2019-01-03 22:37:11.230119+03	t	2
26	2019-01-03 22:37:14.951047+03	2019-01-03 22:37:14.951047+03	t	2
27	2019-01-03 22:43:19.811636+03	2019-01-03 22:43:19.811636+03	t	2
28	2019-01-03 22:43:46.54088+03	2019-01-03 22:43:46.54088+03	t	2
29	2019-01-03 22:49:48.097245+03	2019-01-03 22:49:48.097245+03	t	2
30	2019-01-03 22:49:53.148933+03	2019-01-03 22:49:53.148933+03	t	2
31	2019-01-03 22:51:32.827259+03	2019-01-03 22:51:32.827259+03	t	2
32	2019-01-03 22:51:37.030924+03	2019-01-03 22:51:37.030924+03	t	2
33	2019-01-03 22:52:54.845311+03	2019-01-03 22:52:54.845311+03	t	2
34	2019-01-03 22:52:59.17409+03	2019-01-03 22:52:59.17409+03	t	2
35	2019-01-03 22:53:06.527484+03	2019-01-03 22:53:06.527484+03	t	2
36	2019-01-03 22:53:11.70428+03	2019-01-03 22:53:11.70428+03	t	2
37	2019-01-03 22:54:38.05325+03	2019-01-03 22:54:38.05325+03	t	2
38	2019-01-03 22:54:56.381486+03	2019-01-03 22:54:56.381486+03	t	2
39	2019-01-03 23:07:21.999529+03	2019-01-03 23:07:21.999529+03	t	2
40	2019-01-03 23:25:35.3218+03	2019-01-03 23:25:35.3218+03	t	2
41	2019-01-03 23:25:39.532576+03	2019-01-03 23:25:39.532576+03	t	2
42	2019-01-03 23:32:27.200622+03	2019-01-03 23:32:27.200622+03	t	2
43	2019-01-03 23:32:32.370065+03	2019-01-03 23:32:32.370065+03	t	2
44	2019-01-03 23:33:13.21312+03	2019-01-03 23:33:13.21312+03	t	2
45	2019-01-03 23:33:17.243892+03	2019-01-03 23:33:17.243892+03	t	2
46	2019-01-03 23:33:36.482063+03	2019-01-03 23:33:36.482063+03	t	2
47	2019-01-03 23:33:38.518415+03	2019-01-03 23:33:38.518415+03	t	2
48	2019-01-03 23:33:43.994698+03	2019-01-03 23:33:43.994698+03	t	2
49	2019-01-03 23:34:45.68405+03	2019-01-03 23:34:45.68405+03	t	2
50	2019-01-03 23:34:50.47595+03	2019-01-03 23:34:50.47595+03	t	2
51	2019-01-03 23:39:25.504445+03	2019-01-03 23:39:25.504445+03	t	2
52	2019-01-03 23:40:46.213931+03	2019-01-03 23:40:46.213931+03	t	2
53	2019-01-03 23:41:25.22456+03	2019-01-03 23:41:25.22456+03	t	2
54	2019-01-03 23:41:29.851786+03	2019-01-03 23:41:29.851786+03	t	2
55	2019-01-03 23:42:09.142141+03	2019-01-03 23:42:09.142141+03	t	2
56	2019-01-03 23:42:11.892008+03	2019-01-03 23:42:11.892008+03	t	2
57	2019-01-03 23:42:53.987328+03	2019-01-03 23:42:53.987328+03	t	2
58	2019-01-03 23:42:57.784687+03	2019-01-03 23:42:57.784687+03	t	2
59	2019-01-03 23:44:31.118139+03	2019-01-03 23:44:31.118139+03	t	2
60	2019-01-03 23:44:55.815576+03	2019-01-03 23:44:55.815576+03	t	2
61	2019-01-03 23:44:57.917755+03	2019-01-03 23:44:57.917755+03	t	2
62	2019-01-03 23:45:08.866484+03	2019-01-03 23:45:08.866484+03	t	2
63	2019-01-03 23:45:40.110326+03	2019-01-03 23:45:40.110326+03	t	2
64	2019-01-03 23:45:44.280584+03	2019-01-03 23:45:44.280584+03	t	2
65	2019-01-03 23:46:04.184379+03	2019-01-03 23:46:04.184379+03	t	2
66	2019-01-03 23:46:08.52984+03	2019-01-03 23:46:08.52984+03	t	2
67	2019-01-03 23:47:20.475697+03	2019-01-03 23:47:20.475697+03	t	2
68	2019-01-03 23:47:24.637919+03	2019-01-03 23:47:24.637919+03	t	2
69	2019-01-05 10:13:44.109812+03	2019-01-05 10:13:44.109812+03	t	\N
70	2019-01-05 10:29:30.885358+03	2019-01-05 10:29:30.885358+03	t	2
71	2019-01-05 10:30:01.534889+03	2019-01-05 10:30:01.534889+03	t	2
72	2019-01-05 10:41:12.746715+03	2019-01-05 10:41:12.746715+03	t	2
73	2019-01-05 10:41:18.521311+03	2019-01-05 10:41:18.521311+03	t	2
74	2019-01-05 10:41:45.988272+03	2019-01-05 10:41:45.988272+03	t	2
75	2019-01-05 10:41:50.832125+03	2019-01-05 10:41:50.832125+03	t	2
76	2019-01-05 11:25:33.472445+03	2019-01-05 11:25:33.472445+03	t	2
77	2019-01-05 11:25:38.29088+03	2019-01-05 11:25:38.29088+03	t	2
78	2019-01-05 11:50:59.751342+03	2019-01-05 11:50:59.751342+03	t	2
79	2019-01-05 11:51:04.27938+03	2019-01-05 11:51:04.27938+03	t	2
80	2019-01-07 12:41:04.508252+03	2019-01-07 12:41:04.508252+03	t	\N
81	2019-01-07 13:12:59.805701+03	2019-01-07 13:12:59.805701+03	t	\N
82	2019-01-07 13:13:16.54734+03	2019-01-07 13:13:16.54734+03	t	\N
83	2019-01-07 13:13:32.855479+03	2019-01-07 13:13:32.855479+03	t	\N
84	2019-01-07 15:03:54.378311+03	2019-01-07 15:03:54.378311+03	t	\N
85	2019-01-07 15:41:22.121342+03	2019-01-07 15:41:22.121342+03	t	2
86	2019-01-07 15:41:28.626996+03	2019-01-07 15:41:28.626996+03	t	2
87	2019-01-07 18:50:38.44946+03	2019-01-07 18:50:38.44946+03	t	2
88	2019-01-07 18:50:45.337657+03	2019-01-07 18:50:45.337657+03	t	2
89	2019-01-07 18:51:47.403473+03	2019-01-07 18:51:47.403473+03	t	2
90	2019-01-07 18:52:04.52578+03	2019-01-07 18:52:04.52578+03	t	2
91	2019-01-07 18:52:21.631379+03	2019-01-07 18:52:21.631379+03	t	2
92	2019-01-18 18:35:04.946024+03	2019-01-18 18:35:04.946024+03	t	2
93	2019-01-18 18:35:11.750085+03	2019-01-18 18:35:11.750085+03	t	2
94	2019-01-18 18:35:29.197368+03	2019-01-18 18:35:29.197368+03	t	2
95	2019-01-18 18:35:34.928847+03	2019-01-18 18:35:34.928847+03	t	2
96	2019-01-22 02:10:51.800962+03	2019-01-22 02:10:51.800962+03	t	2
97	2019-01-23 19:55:04.405721+03	2019-01-23 19:55:04.405721+03	t	\N
98	2019-01-23 21:53:31.334449+03	2019-01-23 21:53:31.334449+03	t	\N
99	2019-01-23 21:53:55.504929+03	2019-01-23 21:53:55.504929+03	t	\N
100	2019-01-23 21:54:14.458696+03	2019-01-23 21:54:14.458696+03	t	\N
101	2019-01-24 00:28:26.960055+03	2019-01-24 00:28:26.960055+03	t	\N
102	2019-01-24 00:29:52.983814+03	2019-01-24 00:29:52.983814+03	t	\N
103	2019-01-24 00:30:03.89786+03	2019-01-24 00:30:03.89786+03	t	\N
104	2019-01-24 14:50:38.589042+03	2019-01-24 14:50:38.589042+03	t	\N
105	2019-01-25 21:18:28.518369+03	2019-01-25 21:18:28.518369+03	t	\N
106	2019-01-25 21:20:26.017985+03	2019-01-25 21:20:26.017985+03	t	\N
\.


--
-- Data for Name: sms_sms; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.sms_sms (id, text, is_delivered, is_error, user_id, created_at, is_alive, status_code, updated_at, message_type, provider_sms_id, attempt_number, status_attempt_number) FROM stdin;
1	Hello World!\nnew line	f	f	20	2019-02-13 19:49:20.846583+03	t	\N	2019-02-13 19:49:20.846583+03	TEST	\N	0	0
2	Hello World!\nnew line	f	f	20	2019-02-13 19:50:12.61137+03	t	\N	2019-02-13 19:50:12.61137+03	TEST	\N	0	0
3	Hello World!\nnew line	f	f	20	2019-02-13 19:50:33.207592+03	t	\N	2019-02-13 19:50:33.207592+03	TEST	\N	0	0
4	Hello World!\nnew line	f	f	20	2019-02-13 19:52:46.703403+03	t	\N	2019-02-13 19:52:46.703403+03	TEST	\N	0	0
5	Hello World!\nnew line	f	f	20	2019-02-13 20:24:45.037309+03	t	\N	2019-02-13 20:24:45.037309+03	TEST	\N	0	0
6	Hello World!\nnew line	f	f	20	2019-02-13 20:24:55.807249+03	t	\N	2019-02-13 20:24:55.807249+03	TEST	\N	0	0
7	Hello World!\nnew line	f	f	20	2019-02-13 20:25:07.674515+03	t	\N	2019-02-13 20:25:07.674515+03	TEST	\N	0	0
8	Hello World!\nnew line	f	f	20	2019-02-13 20:25:39.575483+03	t	\N	2019-02-13 20:25:39.575483+03	TEST	\N	0	0
9	Hello World!\nnew line	f	f	20	2019-02-13 20:36:29.249495+03	t	\N	2019-02-13 20:36:29.249495+03	TEST	\N	0	0
10	Hello World!\nnew line	f	f	20	2019-02-13 20:36:53.641893+03	t	100	2019-02-13 20:36:53.948807+03	TEST	201907-1000006	0	0
12	Hello World!\nnew line	f	f	20	2019-02-13 21:55:52.003905+03	t	100	2019-02-13 21:55:52.34558+03	TEST	201907-1000008	0	0
11	Hello World!\nnew line	t	f	20	2019-02-13 21:50:53.086346+03	t	103	2019-02-13 22:06:39.485554+03	TEST	201907-1000007	0	0
14	Hello World!\nnew line	f	f	20	2019-02-13 22:13:34.61459+03	t	102	2019-02-13 22:13:45.484898+03	TEST	201907-1000009	0	0
15	Hello World!\nnew line	f	f	20	2019-02-14 00:30:41.444223+03	t	102	2019-02-14 00:30:53.094964+03	TEST	201907-1000010	1	0
25	Hello World!\nnew line	f	f	20	2019-02-14 00:53:09.003049+03	t	100	2019-02-14 00:53:54.975593+03	TEST	201907-1000026	2	0
21	Hello World!\nnew line	t	f	20	2019-02-14 00:43:37.006994+03	t	103	2019-02-14 00:44:35.652209+03	TEST	201907-1000018	1	0
30	Hello World!\nnew line	f	t	20	2019-02-14 01:26:02.883006+03	t	202	2019-02-14 01:26:03.192685+03	TEST	\N	1	0
16	Hello World!\nnew line	t	f	20	2019-02-14 00:32:14.787018+03	t	103	2019-02-14 00:33:23.996964+03	TEST	201907-1000011	1	0
22	Hello World!\nnew line	f	f	20	2019-02-14 00:45:08.209036+03	t	100	2019-02-14 00:45:32.104142+03	TEST	201907-1000020	2	0
17	Hello World!\nnew line	f	f	20	2019-02-14 00:35:02.331961+03	t	100	2019-02-14 00:35:26.52932+03	TEST	201907-1000013	2	0
34	Hello World!\nnew line	f	t	20	2019-02-14 02:06:28.713688+03	t	202	2019-02-14 02:13:08.743956+03	TEST	\N	5	0
18	Hello World!\nnew line	t	f	20	2019-02-14 00:37:39.405056+03	t	103	2019-02-14 00:38:25.001316+03	TEST	201907-1000014	1	0
23	Hello World!\nnew line	f	f	20	2019-02-14 00:47:08.905074+03	t	100	2019-02-14 00:47:44.379797+03	TEST	201907-1000022	2	0
32	Hello World!\nnew line	f	t	20	2019-02-14 01:44:04.708787+03	t	202	2019-02-14 01:44:36.226734+03	TEST	\N	5	0
19	Hello World!\nnew line	f	f	20	2019-02-14 00:40:55.234727+03	t	100	2019-02-14 00:41:29.652997+03	TEST	201907-1000016	2	0
24	Hello World!\nnew line	f	f	20	2019-02-14 00:48:37.351688+03	t	100	2019-02-14 00:49:47.534698+03	TEST	201907-1000024	2	0
20	Hello World!\nnew line	t	f	20	2019-02-14 00:42:44.612515+03	t	103	2019-02-14 00:43:29.521877+03	TEST	201907-1000017	1	0
26	Hello World!\nnew line	f	t	20	2019-02-14 00:55:58.24382+03	t	150	2019-02-14 01:01:12.25684+03	TEST	201907-1000027	2	0
27	Hello World!\nnew line	f	f	20	2019-02-14 01:03:03.619938+03	t	\N	2019-02-14 01:03:03.669443+03	TEST	\N	1	0
28	Hello World!\nnew line	f	f	20	2019-02-14 01:03:30.449713+03	t	\N	2019-02-14 01:03:30.522607+03	TEST	\N	1	0
29	Hello World!\nnew line	f	f	20	2019-02-14 01:25:48.476038+03	t	\N	2019-02-14 01:25:48.533506+03	TEST	\N	1	0
31	Hello World!\nnew line	f	t	20	2019-02-14 01:26:33.801204+03	t	202	2019-02-14 01:37:03.643647+03	TEST	\N	135	0
33	Hello World!\nnew line	f	t	20	2019-02-14 01:57:20.711776+03	t	202	2019-02-14 01:58:06.98057+03	TEST	\N	5	0
53	Ваш код: 55555	f	t	20	2019-02-14 19:57:41.979404+03	t	232	2019-02-14 19:58:39.885556+03	PHONE	\N	5	5
37	Hello World!\nnew line	f	t	20	2019-02-14 02:47:44.61886+03	t	232	2019-02-14 03:03:49.45936+03	TEST	201907-1000034	5	83
36	Hello World!\nnew line	f	f	20	2019-02-14 02:44:51.843423+03	t	102	2019-02-14 02:47:00.59982+03	TEST	201907-1000029	1	11
35	Hello World!\nnew line	f	t	20	2019-02-14 02:28:52.016724+03	t	202	2019-02-14 02:29:37.982047+03	TEST	\N	5	0
59	Ваш код: 55555	f	t	20	2019-02-14 20:30:43.09333+03	t	232	2019-02-14 20:31:41.063124+03	PHONE	\N	5	5
56	Ваш код: 55555	f	t	20	2019-02-14 20:17:58.970227+03	t	232	2019-02-14 20:18:54.432795+03	PHONE	\N	5	5
42	Ваш код: 55555	t	f	20	2019-02-14 17:16:02.101066+03	t	103	2019-02-14 17:16:13.689774+03	PHONE	201907-1000039	1	1
38	Hello World!\nnew line	t	f	20	2019-02-14 03:12:17.750472+03	t	103	2019-02-14 03:12:29.558525+03	TEST	201907-1000035	1	1
52	Ваш код: 55555	f	t	20	2019-02-14 19:56:43.380346+03	t	232	2019-02-14 19:57:39.780539+03	PHONE	\N	5	5
39	Ваш код: 55555	t	f	20	2019-02-14 17:03:45.752299+03	t	103	2019-02-14 17:03:58.515349+03	PHONE	201907-1000036	1	1
45	Ваш код: 55555	f	t	20	2019-02-14 17:28:10.61476+03	t	232	2019-02-14 17:29:08.664729+03	PHONE	\N	5	5
43	Ваш код: 55555	t	f	20	2019-02-14 17:23:08.770959+03	t	103	2019-02-14 17:23:20.355037+03	PHONE	201907-1000040	1	1
44	Ваш код: 55555	f	t	20	2019-02-14 17:25:45.747154+03	t	232	2019-02-14 17:26:42.419131+03	PHONE	\N	5	5
40	Ваш код: 55555	t	f	20	2019-02-14 17:14:47.014475+03	t	103	2019-02-14 17:14:59.940662+03	PHONE	201907-1000037	1	1
46	Ваш код: 55555	f	t	20	2019-02-14 19:08:52.140826+03	t	232	2019-02-14 19:09:48.967013+03	PHONE	\N	5	5
41	Ваш код: 55555	t	f	20	2019-02-14 17:15:49.897584+03	t	103	2019-02-14 17:16:00.666104+03	PHONE	201907-1000038	1	1
47	Ваш код: 55555	f	t	20	2019-02-14 19:20:54.460128+03	t	232	2019-02-14 19:21:53.143563+03	PHONE	\N	5	5
50	Ваш код: 55555	f	t	20	2019-02-14 19:53:56.924837+03	t	232	2019-02-14 19:54:52.146651+03	PHONE	\N	5	5
60	Ваш код: 55555	f	t	20	2019-02-14 20:31:42.515654+03	t	232	2019-02-14 20:32:39.142508+03	PHONE	\N	5	5
48	Ваш код: 55555	f	t	20	2019-02-14 19:21:59.142214+03	t	232	2019-02-14 19:22:56.238922+03	PHONE	\N	5	5
51	Ваш код: 55555	f	t	20	2019-02-14 19:54:49.744937+03	t	232	2019-02-14 19:55:45.574904+03	PHONE	\N	5	5
58	Ваш код: 55555	f	t	20	2019-02-14 20:28:38.907885+03	t	232	2019-02-14 20:29:34.665255+03	PHONE	\N	5	5
57	Ваш код: 55555	f	t	20	2019-02-14 20:25:03.300417+03	t	232	2019-02-14 20:26:00.524431+03	PHONE	\N	5	5
49	Ваш код: 55555	f	t	20	2019-02-14 19:52:53.778649+03	t	232	2019-02-14 19:53:51.387851+03	PHONE	\N	5	5
54	Ваш код: 55555	f	t	20	2019-02-14 19:59:33.741512+03	t	232	2019-02-14 20:00:32.089082+03	PHONE	\N	5	5
55	Ваш код: 55555	f	t	20	2019-02-14 20:01:33.147547+03	t	232	2019-02-14 20:02:30.259838+03	PHONE	\N	5	5
61	Ваш код: 55555	f	t	20	2019-02-14 20:33:05.575443+03	t	232	2019-02-14 20:34:03.264763+03	PHONE	\N	5	5
64	Ваш код: 55555	f	t	20	2019-02-14 20:45:54.434089+03	t	232	2019-02-14 20:46:50.491207+03	PHONE	\N	5	5
62	Ваш код: 55555	f	t	20	2019-02-14 20:33:58.399251+03	t	232	2019-02-14 20:34:54.793068+03	PHONE	\N	5	5
63	Ваш код: 55555	f	t	20	2019-02-14 20:35:00.143861+03	t	232	2019-02-14 20:35:57.440436+03	PHONE	\N	5	5
65	Ваш код: 55555	f	t	20	2019-02-14 20:51:47.414511+03	t	232	2019-02-14 20:52:43.080846+03	PHONE	\N	5	5
66	Ваш код: 55555	f	t	20	2019-02-14 21:00:04.67613+03	t	232	2019-02-14 21:01:00.367391+03	PHONE	\N	5	5
67	Ваш код: 55555	f	t	20	2019-02-14 21:01:47.871011+03	t	232	2019-02-14 21:02:44.049574+03	PHONE	\N	5	5
68	Ваш код: 55555	f	t	20	2019-02-14 21:03:00.377875+03	t	232	2019-02-14 21:03:58.171285+03	PHONE	\N	5	5
70	Ваш код: 55555	f	t	20	2019-02-14 21:13:39.876527+03	t	232	2019-02-14 21:14:32.33436+03	PHONE	\N	5	5
69	Ваш код: 55555	f	f	23	2019-02-14 21:13:03.086328+03	t	102	2019-02-14 21:32:23.207426+03	PHONE	201907-1000041	1	101
73	Ваш код: 55555	f	t	20	2019-02-14 21:24:34.915732+03	t	232	2019-02-14 21:25:30.565207+03	PHONE	\N	5	5
78	Ваш код: 55555	f	t	20	2019-02-14 21:40:22.534766+03	t	232	2019-02-14 21:41:19.783862+03	PHONE	\N	5	5
72	Ваш код: 55555	f	t	20	2019-02-14 21:16:40.232077+03	t	232	2019-02-14 21:17:35.477081+03	PHONE	\N	5	5
86	Ваш код: 55555	f	t	20	2019-02-14 22:53:33.875042+03	t	232	2019-02-14 22:54:31.211122+03	PHONE	\N	5	5
81	Ваш код: 55555	f	t	20	2019-02-14 22:48:05.901649+03	t	232	2019-02-14 22:49:02.6579+03	PHONE	\N	5	5
93	Ваш код: 55555	f	t	20	2019-02-14 23:10:00.383217+03	t	232	2019-02-14 23:10:56.828428+03	PHONE	\N	5	5
95	Ваш код: 55555	f	t	20	2019-02-14 23:45:17.055745+03	t	232	2019-02-14 23:46:13.381148+03	PHONE	\N	5	5
76	Ваш код: 55555	f	t	20	2019-02-14 21:37:57.972045+03	t	232	2019-02-14 21:38:55.542855+03	PHONE	\N	5	5
84	Ваш код: 55555	f	t	20	2019-02-14 22:51:39.634704+03	t	232	2019-02-14 22:52:37.850248+03	PHONE	\N	5	5
91	Ваш код: 55555	f	t	20	2019-02-14 23:07:01.893723+03	t	232	2019-02-14 23:07:58.194362+03	PHONE	\N	5	5
79	Ваш код: 55555	f	t	20	2019-02-14 22:45:07.204523+03	t	232	2019-02-14 22:46:04.369374+03	PHONE	\N	5	5
75	Ваш код: 55555	f	t	19	2019-02-14 21:28:52.216768+03	t	209	2019-02-14 21:29:45.829097+03	PHONE	\N	5	5
71	Ваш код: 55555	f	t	20	2019-02-14 21:15:02.385428+03	t	232	2019-02-14 21:15:59.317829+03	PHONE	\N	5	5
74	Ваш код: 55555	f	t	20	2019-02-14 21:26:15.179626+03	t	232	2019-02-14 21:27:07.472581+03	PHONE	\N	5	5
96	Ваш код: 55555	t	f	21	2019-02-14 23:51:34.843372+03	t	103	2019-02-14 23:51:47.242909+03	PHONE	201907-1000042	1	1
82	Ваш код: 55555	f	t	20	2019-02-14 22:48:58.873486+03	t	232	2019-02-14 22:49:56.761889+03	PHONE	\N	5	5
89	Ваш код: 55555	f	t	20	2019-02-14 23:04:44.514304+03	t	232	2019-02-14 23:05:43.139736+03	PHONE	\N	5	5
87	Ваш код: 55555	f	t	20	2019-02-14 22:56:46.736718+03	t	232	2019-02-14 22:57:42.97049+03	PHONE	\N	5	5
77	Ваш код: 55555	f	t	20	2019-02-14 21:39:25.364566+03	t	232	2019-02-14 21:40:21.681317+03	PHONE	\N	5	5
97	Ваш код: 55555	t	f	21	2019-02-14 23:54:58.736604+03	t	103	2019-02-14 23:55:11.606115+03	PHONE	201907-1000043	1	1
80	Ваш код: 55555	f	t	20	2019-02-14 22:47:06.730434+03	t	232	2019-02-14 22:48:04.558753+03	PHONE	\N	5	5
85	Ваш код: 55555	f	t	20	2019-02-14 22:52:38.936036+03	t	232	2019-02-14 22:53:37.875734+03	PHONE	\N	5	5
94	Ваш код: 55555	f	t	20	2019-02-14 23:26:57.81639+03	t	232	2019-02-14 23:27:54.458913+03	PHONE	\N	5	5
92	Ваш код: 55555	f	t	20	2019-02-14 23:08:28.71963+03	t	232	2019-02-14 23:09:26.6722+03	PHONE	\N	5	5
83	Ваш код: 55555	f	t	20	2019-02-14 22:50:26.534128+03	t	232	2019-02-14 22:51:24.9146+03	PHONE	\N	5	5
90	Ваш код: 55555	f	t	20	2019-02-14 23:05:38.74061+03	t	232	2019-02-14 23:06:36.410115+03	PHONE	\N	5	5
88	Ваш код: 55555	f	t	20	2019-02-14 23:03:14.451621+03	t	232	2019-02-14 23:04:13.103712+03	PHONE	\N	5	5
\.


--
-- Name: sms_sms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.sms_sms_id_seq', 97, true);


--
-- Data for Name: users_profile; Type: TABLE DATA; Schema: public; Owner: impulse
--

COPY public.users_profile (id, phone_number, user_id, websocket_session, instance_id, is_phone_verified) FROM stdin;
180	79219765863	24	0202723e-880b-4c8d-902f-8fb5c0a12b99	2	f
181	79219765864	25	123ec5eb-aab6-41c6-960d-c5f6b059b06d	2	t
182	79219765811	26	53983e4e-2d72-4e37-a4ae-f68088bad3d8	2	f
183	79999999999	19	8d203c67-b473-4320-863b-ea6d793e0e1e	2	t
184	79219765853	22	24b16bb1-125c-4391-ac20-7bde5bc2b9a3	2	f
178	79219765851	23	5a59c8c3-ba21-4d51-9f68-d7fe9a40fb04	2	f
179	79219765859	20	7e063578-8f7c-4bf4-b8a4-4af249a914ff	2	t
185	79219765856	21	f1fde83b-fbdf-4ada-8f27-72fa93c6f4d8	2	t
\.


--
-- Name: users_profile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: impulse
--

SELECT pg_catalog.setval('public.users_profile_id_seq', 7755, true);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: chat_message_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.chat_message
    ADD CONSTRAINT chat_message_pkey PRIMARY KEY (id);


--
-- Name: chat_room_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.chat_room
    ADD CONSTRAINT chat_room_pkey PRIMARY KEY (id);


--
-- Name: chat_roomparticipant_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.chat_roomparticipant
    ADD CONSTRAINT chat_roomparticipant_pkey PRIMARY KEY (id);


--
-- Name: chat_roomparticipant_room_id_user_id_89e0d2bf_uniq; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.chat_roomparticipant
    ADD CONSTRAINT chat_roomparticipant_room_id_user_id_89e0d2bf_uniq UNIQUE (room_id, user_id);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: faq_faqgroup_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.faq_faqgroup
    ADD CONSTRAINT faq_faqgroup_pkey PRIMARY KEY (id);


--
-- Name: faq_question_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.faq_question
    ADD CONSTRAINT faq_question_pkey PRIMARY KEY (id);


--
-- Name: feed_item_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.feed_item
    ADD CONSTRAINT feed_item_pkey PRIMARY KEY (id);


--
-- Name: feed_itemimage_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.feed_itemimage
    ADD CONSTRAINT feed_itemimage_pkey PRIMARY KEY (id);


--
-- Name: goods_additionalgoods_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_additionalgoods
    ADD CONSTRAINT goods_additionalgoods_pkey PRIMARY KEY (id);


--
-- Name: goods_address_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_address
    ADD CONSTRAINT goods_address_pkey PRIMARY KEY (id);


--
-- Name: goods_descriptionline_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_descriptionline
    ADD CONSTRAINT goods_descriptionline_pkey PRIMARY KEY (id);


--
-- Name: goods_displacegoods_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_displacegoods
    ADD CONSTRAINT goods_displacegoods_pkey PRIMARY KEY (id);


--
-- Name: goods_goods_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_goods
    ADD CONSTRAINT goods_goods_pkey PRIMARY KEY (id);


--
-- Name: goods_goodsimage_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_goodsimage
    ADD CONSTRAINT goods_goodsimage_pkey PRIMARY KEY (id);


--
-- Name: goods_goodsparam_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_goodsparam
    ADD CONSTRAINT goods_goodsparam_pkey PRIMARY KEY (id);


--
-- Name: goods_order_instance_id_ind_588b3b82_uniq; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_order
    ADD CONSTRAINT goods_order_instance_id_ind_588b3b82_uniq UNIQUE (instance_id, ind);


--
-- Name: goods_order_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_order
    ADD CONSTRAINT goods_order_pkey PRIMARY KEY (id);


--
-- Name: goods_orderitem_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_orderitem
    ADD CONSTRAINT goods_orderitem_pkey PRIMARY KEY (id);


--
-- Name: goods_orderitemparamvalue_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_orderitemparamvalue
    ADD CONSTRAINT goods_orderitemparamvalue_pkey PRIMARY KEY (id);


--
-- Name: instances_colorscheme_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.instances_colorscheme
    ADD CONSTRAINT instances_colorscheme_pkey PRIMARY KEY (id);


--
-- Name: instances_instance_admin_id_455c864a_uniq; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.instances_instance
    ADD CONSTRAINT instances_instance_admin_id_455c864a_uniq UNIQUE (admin_id);


--
-- Name: instances_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.instances_instance
    ADD CONSTRAINT instances_instance_pkey PRIMARY KEY (id);


--
-- Name: instances_instancesettings_instance_id_key; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.instances_instancesettings
    ADD CONSTRAINT instances_instancesettings_instance_id_key UNIQUE (instance_id);


--
-- Name: instances_instancesettings_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.instances_instancesettings
    ADD CONSTRAINT instances_instancesettings_pkey PRIMARY KEY (id);


--
-- Name: instances_openlogo_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.instances_openlogo
    ADD CONSTRAINT instances_openlogo_pkey PRIMARY KEY (id);


--
-- Name: instances_page_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.instances_instancepage
    ADD CONSTRAINT instances_page_pkey PRIMARY KEY (id);


--
-- Name: instances_pageclass_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.instances_pageclass
    ADD CONSTRAINT instances_pageclass_pkey PRIMARY KEY (id);


--
-- Name: reset_reset_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.reset_resetrequest
    ADD CONSTRAINT reset_reset_pkey PRIMARY KEY (id);


--
-- Name: sms_sms_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.sms_sms
    ADD CONSTRAINT sms_sms_pkey PRIMARY KEY (id);


--
-- Name: users_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.users_profile
    ADD CONSTRAINT users_profile_pkey PRIMARY KEY (id);


--
-- Name: users_profile_user_id_key; Type: CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.users_profile
    ADD CONSTRAINT users_profile_user_id_key UNIQUE (user_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: chat_message_participant_id_b5259eac; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX chat_message_participant_id_b5259eac ON public.chat_message USING btree (participant_id);


--
-- Name: chat_roomparticipant_room_id_35b31a9f; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX chat_roomparticipant_room_id_35b31a9f ON public.chat_roomparticipant USING btree (room_id);


--
-- Name: chat_roomparticipant_user_id_8964d6f5; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX chat_roomparticipant_user_id_8964d6f5 ON public.chat_roomparticipant USING btree (user_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: faq_faqgroup_instance_id_2225fe98; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX faq_faqgroup_instance_id_2225fe98 ON public.faq_faqgroup USING btree (instance_id);


--
-- Name: faq_question_group_id_233549b9; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX faq_question_group_id_233549b9 ON public.faq_question USING btree (group_id);


--
-- Name: feed_item_page_id_06d8b8ce; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX feed_item_page_id_06d8b8ce ON public.feed_item USING btree (page_id);


--
-- Name: feed_itemimage_item_id_e77cd057; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX feed_itemimage_item_id_e77cd057 ON public.feed_itemimage USING btree (item_id);


--
-- Name: goods_additionalgoods_goods_add_id_0c3f667f; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX goods_additionalgoods_goods_add_id_0c3f667f ON public.goods_additionalgoods USING btree (goods_add_id);


--
-- Name: goods_additionalgoods_goods_to_id_92f33faa; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX goods_additionalgoods_goods_to_id_92f33faa ON public.goods_additionalgoods USING btree (goods_to_id);


--
-- Name: goods_address_user_id_f98a2118; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX goods_address_user_id_f98a2118 ON public.goods_address USING btree (user_id);


--
-- Name: goods_descriptionline_goods_id_efc1d45a; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX goods_descriptionline_goods_id_efc1d45a ON public.goods_descriptionline USING btree (goods_id);


--
-- Name: goods_displacegoods_displace_by_id_0cdb49de; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX goods_displacegoods_displace_by_id_0cdb49de ON public.goods_displacegoods USING btree (displace_by_id);


--
-- Name: goods_displacegoods_goods_id_9e5a6f32; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX goods_displacegoods_goods_id_9e5a6f32 ON public.goods_displacegoods USING btree (goods_id);


--
-- Name: goods_goods_instance_id_496ce158; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX goods_goods_instance_id_496ce158 ON public.goods_goods USING btree (instance_id);


--
-- Name: goods_goods_level_93d2fe17; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX goods_goods_level_93d2fe17 ON public.goods_goods USING btree (level);


--
-- Name: goods_goods_lft_c49bab55; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX goods_goods_lft_c49bab55 ON public.goods_goods USING btree (lft);


--
-- Name: goods_goods_parent_id_f9a247fb; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX goods_goods_parent_id_f9a247fb ON public.goods_goods USING btree (parent_id);


--
-- Name: goods_goods_rght_1edb3b83; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX goods_goods_rght_1edb3b83 ON public.goods_goods USING btree (rght);


--
-- Name: goods_goods_tree_id_03a9f39a; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX goods_goods_tree_id_03a9f39a ON public.goods_goods USING btree (tree_id);


--
-- Name: goods_goodsimage_goods_id_08cb23b1; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX goods_goodsimage_goods_id_08cb23b1 ON public.goods_goodsimage USING btree (goods_id);


--
-- Name: goods_goodsparam_goods_id_959911fe; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX goods_goodsparam_goods_id_959911fe ON public.goods_goodsparam USING btree (goods_id);


--
-- Name: goods_order_address_id_7adace68; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX goods_order_address_id_7adace68 ON public.goods_order USING btree (address_id);


--
-- Name: goods_order_instance_id_ba192724; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX goods_order_instance_id_ba192724 ON public.goods_order USING btree (instance_id);


--
-- Name: goods_order_user_id_bd5a6274; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX goods_order_user_id_bd5a6274 ON public.goods_order USING btree (user_id);


--
-- Name: goods_orderitem_goods_id_fdac0967; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX goods_orderitem_goods_id_fdac0967 ON public.goods_orderitem USING btree (goods_id);


--
-- Name: goods_orderitem_order_id_0c207842; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX goods_orderitem_order_id_0c207842 ON public.goods_orderitem USING btree (order_id);


--
-- Name: goods_orderitemparamvalue_order_item_id_1f051de0; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX goods_orderitemparamvalue_order_item_id_1f051de0 ON public.goods_orderitemparamvalue USING btree (order_item_id);


--
-- Name: goods_orderitemparamvalue_param_id_13e5ef75; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX goods_orderitemparamvalue_param_id_13e5ef75 ON public.goods_orderitemparamvalue USING btree (param_id);


--
-- Name: instances_instancepage_instance_id_5c1cd4b9; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX instances_instancepage_instance_id_5c1cd4b9 ON public.instances_instancepage USING btree (instance_id);


--
-- Name: instances_instancepage_page_class_id_76983287; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX instances_instancepage_page_class_id_76983287 ON public.instances_instancepage USING btree (page_class_id);


--
-- Name: instances_instancesettings_color_scheme_id_d21921a6; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX instances_instancesettings_color_scheme_id_d21921a6 ON public.instances_instancesettings USING btree (color_scheme_id);


--
-- Name: instances_instancesettings_open_logo_id_7626bcdc; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX instances_instancesettings_open_logo_id_7626bcdc ON public.instances_instancesettings USING btree (open_logo_id);


--
-- Name: reset_reset_instance_id_6e6433a8; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX reset_reset_instance_id_6e6433a8 ON public.reset_resetrequest USING btree (instance_id);


--
-- Name: sms_sms_user_id_45fac640; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX sms_sms_user_id_45fac640 ON public.sms_sms USING btree (user_id);


--
-- Name: users_profile_instance_id_4c06dedd; Type: INDEX; Schema: public; Owner: impulse
--

CREATE INDEX users_profile_instance_id_4c06dedd ON public.users_profile USING btree (instance_id);


--
-- Name: auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: chat_message_participant_id_b5259eac_fk_chat_roomparticipant_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.chat_message
    ADD CONSTRAINT chat_message_participant_id_b5259eac_fk_chat_roomparticipant_id FOREIGN KEY (participant_id) REFERENCES public.chat_roomparticipant(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: chat_roomparticipant_room_id_35b31a9f_fk_chat_room_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.chat_roomparticipant
    ADD CONSTRAINT chat_roomparticipant_room_id_35b31a9f_fk_chat_room_id FOREIGN KEY (room_id) REFERENCES public.chat_room(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: chat_roomparticipant_user_id_8964d6f5_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.chat_roomparticipant
    ADD CONSTRAINT chat_roomparticipant_user_id_8964d6f5_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: faq_faqgroup_instance_id_2225fe98_fk_instances_instance_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.faq_faqgroup
    ADD CONSTRAINT faq_faqgroup_instance_id_2225fe98_fk_instances_instance_id FOREIGN KEY (instance_id) REFERENCES public.instances_instance(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: faq_question_group_id_233549b9_fk_faq_faqgroup_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.faq_question
    ADD CONSTRAINT faq_question_group_id_233549b9_fk_faq_faqgroup_id FOREIGN KEY (group_id) REFERENCES public.faq_faqgroup(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: feed_item_page_id_06d8b8ce_fk_instances_instancepage_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.feed_item
    ADD CONSTRAINT feed_item_page_id_06d8b8ce_fk_instances_instancepage_id FOREIGN KEY (page_id) REFERENCES public.instances_instancepage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: feed_itemimage_item_id_e77cd057_fk_feed_item_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.feed_itemimage
    ADD CONSTRAINT feed_itemimage_item_id_e77cd057_fk_feed_item_id FOREIGN KEY (item_id) REFERENCES public.feed_item(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: goods_additionalgoods_goods_add_id_0c3f667f_fk_goods_goods_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_additionalgoods
    ADD CONSTRAINT goods_additionalgoods_goods_add_id_0c3f667f_fk_goods_goods_id FOREIGN KEY (goods_add_id) REFERENCES public.goods_goods(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: goods_additionalgoods_goods_to_id_92f33faa_fk_goods_goods_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_additionalgoods
    ADD CONSTRAINT goods_additionalgoods_goods_to_id_92f33faa_fk_goods_goods_id FOREIGN KEY (goods_to_id) REFERENCES public.goods_goods(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: goods_address_user_id_f98a2118_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_address
    ADD CONSTRAINT goods_address_user_id_f98a2118_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: goods_descriptionline_goods_id_efc1d45a_fk_goods_goods_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_descriptionline
    ADD CONSTRAINT goods_descriptionline_goods_id_efc1d45a_fk_goods_goods_id FOREIGN KEY (goods_id) REFERENCES public.goods_goods(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: goods_displacegoods_displace_by_id_0cdb49de_fk_goods_goods_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_displacegoods
    ADD CONSTRAINT goods_displacegoods_displace_by_id_0cdb49de_fk_goods_goods_id FOREIGN KEY (displace_by_id) REFERENCES public.goods_goods(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: goods_displacegoods_goods_id_9e5a6f32_fk_goods_goods_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_displacegoods
    ADD CONSTRAINT goods_displacegoods_goods_id_9e5a6f32_fk_goods_goods_id FOREIGN KEY (goods_id) REFERENCES public.goods_goods(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: goods_goods_instance_id_496ce158_fk_instances_instance_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_goods
    ADD CONSTRAINT goods_goods_instance_id_496ce158_fk_instances_instance_id FOREIGN KEY (instance_id) REFERENCES public.instances_instance(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: goods_goods_parent_id_f9a247fb_fk_goods_goods_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_goods
    ADD CONSTRAINT goods_goods_parent_id_f9a247fb_fk_goods_goods_id FOREIGN KEY (parent_id) REFERENCES public.goods_goods(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: goods_goodsimage_goods_id_08cb23b1_fk_goods_goods_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_goodsimage
    ADD CONSTRAINT goods_goodsimage_goods_id_08cb23b1_fk_goods_goods_id FOREIGN KEY (goods_id) REFERENCES public.goods_goods(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: goods_goodsparam_goods_id_959911fe_fk_goods_goods_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_goodsparam
    ADD CONSTRAINT goods_goodsparam_goods_id_959911fe_fk_goods_goods_id FOREIGN KEY (goods_id) REFERENCES public.goods_goods(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: goods_order_address_id_7adace68_fk_goods_address_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_order
    ADD CONSTRAINT goods_order_address_id_7adace68_fk_goods_address_id FOREIGN KEY (address_id) REFERENCES public.goods_address(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: goods_order_instance_id_ba192724_fk_instances_instance_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_order
    ADD CONSTRAINT goods_order_instance_id_ba192724_fk_instances_instance_id FOREIGN KEY (instance_id) REFERENCES public.instances_instance(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: goods_order_user_id_bd5a6274_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_order
    ADD CONSTRAINT goods_order_user_id_bd5a6274_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: goods_orderitem_goods_id_fdac0967_fk_goods_goods_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_orderitem
    ADD CONSTRAINT goods_orderitem_goods_id_fdac0967_fk_goods_goods_id FOREIGN KEY (goods_id) REFERENCES public.goods_goods(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: goods_orderitem_order_id_0c207842_fk_goods_order_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_orderitem
    ADD CONSTRAINT goods_orderitem_order_id_0c207842_fk_goods_order_id FOREIGN KEY (order_id) REFERENCES public.goods_order(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: goods_orderitemparam_order_item_id_1f051de0_fk_goods_ord; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_orderitemparamvalue
    ADD CONSTRAINT goods_orderitemparam_order_item_id_1f051de0_fk_goods_ord FOREIGN KEY (order_item_id) REFERENCES public.goods_orderitem(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: goods_orderitemparam_param_id_13e5ef75_fk_goods_goo; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.goods_orderitemparamvalue
    ADD CONSTRAINT goods_orderitemparam_param_id_13e5ef75_fk_goods_goo FOREIGN KEY (param_id) REFERENCES public.goods_goodsparam(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: instances_instance_admin_id_455c864a_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.instances_instance
    ADD CONSTRAINT instances_instance_admin_id_455c864a_fk_auth_user_id FOREIGN KEY (admin_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: instances_instancepa_instance_id_5c1cd4b9_fk_instances; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.instances_instancepage
    ADD CONSTRAINT instances_instancepa_instance_id_5c1cd4b9_fk_instances FOREIGN KEY (instance_id) REFERENCES public.instances_instance(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: instances_instancepa_page_class_id_76983287_fk_instances; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.instances_instancepage
    ADD CONSTRAINT instances_instancepa_page_class_id_76983287_fk_instances FOREIGN KEY (page_class_id) REFERENCES public.instances_pageclass(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: instances_instancese_color_scheme_id_d21921a6_fk_instances; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.instances_instancesettings
    ADD CONSTRAINT instances_instancese_color_scheme_id_d21921a6_fk_instances FOREIGN KEY (color_scheme_id) REFERENCES public.instances_colorscheme(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: instances_instancese_instance_id_8b171e18_fk_instances; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.instances_instancesettings
    ADD CONSTRAINT instances_instancese_instance_id_8b171e18_fk_instances FOREIGN KEY (instance_id) REFERENCES public.instances_instance(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: instances_instancese_open_logo_id_7626bcdc_fk_instances; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.instances_instancesettings
    ADD CONSTRAINT instances_instancese_open_logo_id_7626bcdc_fk_instances FOREIGN KEY (open_logo_id) REFERENCES public.instances_openlogo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: reset_resetrequest_instance_id_0a547728_fk_instances; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.reset_resetrequest
    ADD CONSTRAINT reset_resetrequest_instance_id_0a547728_fk_instances FOREIGN KEY (instance_id) REFERENCES public.instances_instance(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sms_sms_user_id_45fac640_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.sms_sms
    ADD CONSTRAINT sms_sms_user_id_45fac640_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_profile_instance_id_4c06dedd_fk_instances_instance_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.users_profile
    ADD CONSTRAINT users_profile_instance_id_4c06dedd_fk_instances_instance_id FOREIGN KEY (instance_id) REFERENCES public.instances_instance(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_profile_user_id_2112e78d_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: impulse
--

ALTER TABLE ONLY public.users_profile
    ADD CONSTRAINT users_profile_user_id_2112e78d_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

