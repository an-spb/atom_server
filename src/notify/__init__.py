import json

import pika


def notify_users(users_data):
    """users_data is a list of tuples (user, data)"""
    try:
        connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
        channel = connection.channel()
        for user, data in users_data:
            channel.basic_publish(exchange='tornado_input',
                                routing_key='#',
                                body=json.dumps({'session': str(user.profile.get_websocket_session()),
                                                'data': data}))
        connection.close()
    except pika.exceptions.AMQPConnectionError:
        pass


def notify_instances(instances_data):
    """instances_data is a list of tuples (instance, data)"""
    try:
        connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
        channel = connection.channel()
        for instance, data in instances_data:
            channel.basic_publish(exchange='tornado_input',
                                routing_key='#',
                                body=json.dumps({'instance_id': instance.id,
                                                'data': data}))
        connection.close()
    except pika.exceptions.AMQPConnectionError:
        pass
    

