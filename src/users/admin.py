from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.contrib import admin

from users.models import Profile


class Admin(UserAdmin):
    class ProfileInline(admin.TabularInline):
        model = Profile
    inlines = (ProfileInline,)    


admin.site.unregister(User)
admin.site.register(User, Admin)