import random
import string
import json
import uuid

from django.db import models
from django.contrib.auth.models import User
from annoying.fields import AutoOneToOneField
from django.db import IntegrityError

from instances.models import Instance


User.__str__ = lambda self: '%s%s' % (self.username, ' (%s)' % self.first_name if self.first_name else '')


class Profile(models.Model):
    user = AutoOneToOneField(User, related_name='profile', on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=20)
    is_phone_verified = models.BooleanField(default=False)
    websocket_session = models.UUIDField(default=uuid.uuid4, editable=False)
    instance = models.ForeignKey(Instance, related_name='profiles',
                                 null=True, # REMOVE THIS
                                 on_delete=models.CASCADE)
    def get_websocket_session(self, *args, **kwargs):
        return self.websocket_session

