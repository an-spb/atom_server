from random import randint
import time

from django.contrib.auth.models import User
from django.contrib.auth import login, logout

from utils import rpc_method
from sms.tasks import send_sms


@rpc_method('user.get_auth_info')
def user_get_auth_info(request):
    user = request.user
    is_authorized = not user.is_anonymous
    return {'is_authorized': is_authorized,
            'id': user.id if is_authorized else None,
            'name': user.first_name if is_authorized else None,
            'websocket_session': user.profile.get_websocket_session() if is_authorized else None}


@rpc_method('user.get_profile_info')
def user_get_profile_info(request):
    user = request.user
    is_authorized = not user.is_anonymous
    return [{'id': 0,
             'name': user.first_name if is_authorized else None}]


@rpc_method('phone.start_verification')
def phone_start_verification(request, instance_id, phone_number):
    username = 'app%s__%s' % (instance_id, phone_number)
    user, is_new = User.objects.get_or_create(username=username)
    user.profile.instance_id = instance_id
    user.profile.phone_number = phone_number
    user.profile.save()
    if request.session.get('pv_user_id', None) != user.id:
        code = str(randint(10000, 99999))
        request.session['pv_user_id'] = user.id    
        request.session['pv_code'] = '55555' #code
    res = phone_send_verification_sms(request)    
    return dict(res, phone_number=phone_number, **user_get_auth_info(request))


@rpc_method('phone.send_verification_sms')
def phone_send_verification_sms(request):
    time.sleep(2)
    ts = time.time()
    last_ts = request.session.get('pv_sms_timeout', 0)
    if ts - last_ts > 50 and 'pv_user_id' in request.session:
        # send_sms.delay(request.session['pv_user_id'],
        #                'PHONE',
        #                'Ваш код: %s' % request.session['pv_code'])
        request.session['pv_sms_timeout'] = ts
        return {'sms_sent': True}
    else:
        return {'sms_sent': False}


@rpc_method('phone.verify')
def phone_verify(request, code):
    is_verified = code == request.session.get('pv_code')
    if is_verified:
        user = User.objects.get(id=request.session['pv_user_id'])
        user.profile.is_phone_verified = True
        user.profile.save()
        login(request, user)
        del request.session['pv_code']
        del request.session['pv_user_id']
        del request.session['pv_sms_timeout']
        request.session.modified = True
    return dict(is_verified=is_verified, **user_get_auth_info(request))


@rpc_method('user.save_name')
def user_save_profile_info(request, name):
    request.user.first_name = name
    request.user.save()
    return user_get_auth_info(request)



@rpc_method('user.logout')
def user_logout(request):
    logout(request)
    return user_get_auth_info(request)
