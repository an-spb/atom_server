from django.contrib import admin
from django import forms

from utils import NotDeletableAdmin, NotDeletableInline
from reset.models import ResetRequest
from reset.notifiers import notify_about_reset_request_change


class ResetRequestAdmin(NotDeletableAdmin):
    list_display = ('instance', 'created_at')
    list_filter = ('instance',)
    def save_related(self, request, form, formsets, change):
        super(ResetRequestAdmin, self).save_related(request, form, formsets, change)
        notify_about_reset_request_change(form.instance)

admin.site.register(ResetRequest, ResetRequestAdmin)