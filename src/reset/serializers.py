def serialize_reset_request(reset_request):
    return {
        'id': reset_request.id,
        'instance_id': reset_request.instance.id if reset_request.instance else None,
        'updated_at': reset_request.updated_at.isoformat(),
    }
