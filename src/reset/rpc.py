from dateutil import parser

from utils import rpc_method
from reset.models import ResetRequest
from reset.serializers import serialize_reset_request
from django.db.models import Q

from instances.models import Instance


@rpc_method('reset.get_reset_requests')
def reset_get_reset_requests(request, instance_id, updated_at_gte=None):
    instance = Instance.objects.get(id=instance_id)
    reset_requests = ResetRequest.objects.filter(Q(instance__isnull=True) | Q(instance=instance))
    if updated_at_gte:
        updated_at_gte = parser.parse(updated_at_gte)
        reset_requests = reset_requests.filter(updated_at__gte=updated_at_gte)
    return {'data': [serialize_reset_request(r) for r in reset_requests]}
