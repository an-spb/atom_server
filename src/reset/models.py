from django.db import models

from instances.models import Instance
from utils import NotDeletableModel


class ResetRequest(NotDeletableModel):
    instance = models.ForeignKey(Instance, related_name='resets',
                                 on_delete=models.CASCADE,
                                 null=True, blank=True)
