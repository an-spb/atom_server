from django.contrib.auth.models import User

from notify import notify_instances
from instances.models import Instance
from reset.serializers import serialize_reset_request


def notify_about_reset_request_change(reset_request):
    if reset_request.instance:
        instances = [reset_request.instance]
    else:
        instances = Instance.objects.all()
    instances_data = []
    for instance in instances:
        instances_data.append((instance,
                               {'key': 'reset.get_reset_requests',
                                'params': [instance.id],
                                'data': [serialize_reset_request(reset_request)]}))
    notify_instances(instances_data)