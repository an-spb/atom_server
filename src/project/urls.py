"""fitplan_server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.views.static import serve as static_serve
from django.conf import settings
from jsonrpc import jsonrpc_site

from instances.rpc import *
from users.rpc import *
from chat.rpc import *
from faq.rpc import *
from feed.rpc import *
from goods.rpc import *
from reset.rpc import *
from react_log.rpc import *


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^rpc/', jsonrpc_site.dispatch, name='jsonrpc_mountpoint'),
]

# urlpatterns += [url(r'^static/(?P<path>.*)$', static_serve,
#                     {'document_root': settings.STATIC_ROOT})]

urlpatterns += [url(r'^media/(?P<path>.*)$', static_serve,
                    {'document_root': settings.MEDIA_ROOT})]