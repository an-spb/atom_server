from dateutil import parser


from utils import rpc_method
from feed.models import Item
from feed.serializers import serialize_item


@rpc_method('feed.get_items')
def feed_get_items(request, updated_at_gte=None):
    items = Item.objects.all().order_by('-published_at')
    if updated_at_gte:
        updated_at_gte = parser.parse(updated_at_gte)
        items = items.filter(updated_at__gte=updated_at_gte)
    return {'data': [serialize_item(item) for item in items]}
