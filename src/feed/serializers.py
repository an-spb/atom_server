def serialize_item(item):
    return {
        'id': item.id,
        'title': item.title,
        'text': item.text,
        'have_order_button': item.have_order_button,
        'published_at': item.published_at.isoformat(),
        'updated_at': item.published_at.isoformat(),
        'is_alive': item.is_alive,
        'starts_from_space': item.starts_from_space, # Kill me, please check out a comment in models.py
        # 'video_file': item.video_file,
        # 'images': [{img.image_file for img in item.images.all()}],
    }
