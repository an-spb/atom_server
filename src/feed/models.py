from django.db import models

from instances.models import InstancePage
from utils import NotDeletableModel


class Item(NotDeletableModel):
    page = models.ForeignKey(InstancePage, related_name='feed_items',
                             on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    text = models.TextField()
    published_at = models.DateTimeField()
    video_file = models.FileField(null=True, blank=True)
    have_order_button = models.BooleanField(default=False)
    # Please kill me, HTML textarea (or Django) drops leading space
    # and I can't find a way to fix that
    starts_from_space = models.BooleanField(default=False)
    __str__ = lambda self: self.title
    class Meta:
        ordering = ('-published_at',)


class FeedItem(Item):
    class Meta:
        proxy = True


class DiscountItem(Item):
    class Meta:
        proxy = True
        

class ItemImage(NotDeletableModel):
    item = models.ForeignKey(Item, related_name='images',
                             on_delete=models.CASCADE)
    image_file = models.ImageField()
    
