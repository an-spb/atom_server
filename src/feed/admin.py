from django.contrib import admin
from django import forms

from utils import NotDeletableAdmin, NotDeletableInline
from feed.models import Item, FeedItem, DiscountItem, ItemImage
from feed.notifiers import notify_about_item_change


class ItemImageInline(NotDeletableInline):
    model = ItemImage
    fields = ('image_file',)

class ItemForm(forms.ModelForm):
    class Meta:
        model = Item
        exclude = ('created_at', 'updated_at')
    def clean(self):
        pass

class ItemAdmin(NotDeletableAdmin):
    form = ItemForm
    list_display = ('title', 'published_at', 'is_alive')
    search_fields = ('title', 'text')
    list_filter = ('is_alive',)
    inlines = (ItemImageInline,)
    def save_related(self, request, form, formsets, change):
        super(ItemAdmin, self).save_related(request, form, formsets, change)
        notify_about_item_change(form.instance)
    def changelist_view(self, request, extra_context=None):    
        return super(ItemAdmin, self).changelist_view(request, extra_context)
    def get_readonly_fields(self, request, obj=None):
        return ()

admin.site.register(FeedItem, ItemAdmin)
admin.site.register(DiscountItem, ItemAdmin)
