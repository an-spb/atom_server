# Generated by Django 2.0.6 on 2018-09-14 19:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feed', '0011_item_have_order_button'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='start_from_space',
            field=models.BooleanField(default=False),
        ),
    ]
