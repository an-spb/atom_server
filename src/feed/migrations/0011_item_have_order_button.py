# Generated by Django 2.0.6 on 2018-09-14 19:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feed', '0010_auto_20180914_0941'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='have_order_button',
            field=models.BooleanField(default=False),
        ),
    ]
