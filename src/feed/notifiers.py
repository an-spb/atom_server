from django.contrib.auth.models import User

from notify import notify_users
from feed.serializers import serialize_item


def notify_about_item_change(item):
    users_data = []
    for user in User.objects.all():
        users_data.append((user,
                           {'key': 'feed.get_items',
                            'params': [],
                            'data': [serialize_item(item)]}))
    notify_users(users_data)

    