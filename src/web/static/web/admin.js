'use strict';

class Parser {
  constructor() {
    this.records = $('#result_list tbody tr').map((i, r) => {
      const item = {
        id: parseInt($(r).find('.field-id').html()),
        level: parseInt($(r).find('.field-level').html()),
        type: $(r).find('.field-item_type').html(),
        $name: $(r).find('.field-name'),
      };
      item.$name.css('padding-left', item.level * 20);
      return item;
    });
  };
};

$(document).ready(() => {
  $('#result_list').find('.field-id, .field-level, .column-id, .column-level').hide();
  const parser = new Parser();
  console.log(parser.records)
});