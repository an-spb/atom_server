from django.db import models
from django.contrib.auth.models import User

from utils import NotDeletableModel


class Sms(NotDeletableModel):
    user = models.ForeignKey(User, related_name='sms',
                             on_delete=models.CASCADE)
    message_type = models.CharField(max_length=30)
    provider_sms_id = models.CharField(max_length=20, null=True)
    text = models.TextField()
    status_code = models.IntegerField(null=True)
    is_error = models.BooleanField(default=False)
    is_delivered = models.BooleanField(default=False)
    attempt_number = models.IntegerField(default=0)
    status_attempt_number = models.IntegerField(default=0)


