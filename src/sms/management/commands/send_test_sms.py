from django.core.management.base import BaseCommand, CommandError

from sms.tasks import send_sms, update_sms_status


class Command(BaseCommand):
    def handle(self, *args, **options):
        send_sms.delay(20, 'TEST', 'Hello World!\nnew line')
        # update_sms_status(11)

