import urllib.request
import urllib.parse
import json

from celery import shared_task
from django.contrib.auth.models import User

from sms.models import Sms


UPDATE_STATUS_TIMEOUT = 10
MAX_ATTEMPTS = 5
MAX_STATUS_ATTEMPTS = 100


@shared_task
def send_sms(user_id, message_type, text):
    sms = Sms.objects.create(user_id=user_id, message_type=message_type, text=text)
    smsru_send(sms.id)
    update_sms_status.apply_async((sms.id,), countdown=UPDATE_STATUS_TIMEOUT)


@shared_task
def update_sms_status(sms_id):
    smsru_request_status(sms_id)
    sms = Sms.objects.get(id=sms_id)
    sms.status_attempt_number += 1
    sms.save()
    if sms.status_attempt_number > MAX_STATUS_ATTEMPTS:
        return
    if sms.provider_sms_id and sms.status_code < 103:
        update_sms_status.apply_async((sms_id,), countdown=UPDATE_STATUS_TIMEOUT)
    elif sms.is_error and sms.attempt_number < MAX_ATTEMPTS:
        smsru_send(sms_id)
        update_sms_status.apply_async((sms_id,), countdown=UPDATE_STATUS_TIMEOUT)


@shared_task
def smsru_send(sms_id):
    sms = Sms.objects.get(id=sms_id)
    if sms.attempt_number >= MAX_ATTEMPTS:
        return
    sms.attempt_number += 1
    sms.save()
    phone_number = sms.user.profile.phone_number
    params = {'api_id': '354B8B97-17C8-A412-0C0C-6C6B595C2DEF',
              'to': phone_number,
              'msg': sms.text,
              'test': 1,
              'json': 1}
    url = 'https://sms.ru/sms/send?' + urllib.parse.urlencode(params)
    content = urllib.request.urlopen(url).read()
    response = json.loads(content.decode('utf-8'))
    print('SMSRU SEND RESPONSE', response)
    if 'sms' in response and phone_number in response['sms']:
        sms_data = response['sms'][phone_number]
        sms.provider_sms_id = sms_data.get('sms_id', None)
        update_sms_status_code(sms, int(sms_data['status_code']))
        sms.save()
    else:
        code = response.get('status_code', None)
        if code:
            update_sms_status_code(sms, code)
            sms.save()
    sms.save()


@shared_task
def smsru_request_status(sms_id):
    sms = Sms.objects.get(id=sms_id)
    if not sms.provider_sms_id:
        return
    phone_number = sms.user.profile.phone_number
    params = {'api_id': '354B8B97-17C8-A412-0C0C-6C6B595C2DEF',
              'sms_id': sms.provider_sms_id,
              'json': 1}
    url = 'https://sms.ru/sms/status?' + urllib.parse.urlencode(params)
    content = urllib.request.urlopen(url).read()
    response = json.loads(content.decode('utf-8'))
    print('SMSRU STATUS RESPONSE', response)
    if 'sms' in response and sms.provider_sms_id in response['sms']:
        sms_data = response['sms'][sms.provider_sms_id]
        update_sms_status_code(sms, int(sms_data['status_code']))
        sms.save()


def update_sms_status_code(sms, code):
    sms.status_code = code
    sms.is_delivered = code in [103, 110]
    sms.is_error = code > 103 and code != 110

#354B8B97-17C8-A412-0C0C-6C6B595C2DEF