from django.contrib import admin
from django import forms

from utils import NotDeletableAdmin, NotDeletableInline
from sms.models import Sms


class SmsAdmin(NotDeletableAdmin):
    list_display = ('id', 'user', 'provider_sms_id', 'status_code', 'is_delivered', 'created_at', 'updated_at')
    search_fields = ('id', 'user__first_name', 'user__username')
    list_filter = ('message_type', 'is_error', 'is_delivered', 'status_code')

admin.site.register(Sms, SmsAdmin)


