# Generated by Django 2.0.6 on 2018-10-23 13:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('instances', '0022_auto_20181022_2325'),
    ]

    operations = [
        migrations.AddField(
            model_name='colorscheme',
            name='superlight',
            field=models.CharField(max_length=6, null=True),
        ),
    ]
