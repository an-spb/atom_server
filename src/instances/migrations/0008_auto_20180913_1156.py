# Generated by Django 2.0.6 on 2018-09-13 11:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('instances', '0007_auto_20180913_1151'),
    ]

    operations = [
        migrations.AlterField(
            model_name='instancesettings',
            name='feed_button_text',
            field=models.CharField(blank=True, max_length=15, null=True),
        ),
        migrations.AlterField(
            model_name='instancesettings',
            name='logo',
            field=models.ImageField(blank=True, null=True, upload_to=''),
        ),
    ]
