# Generated by Django 2.0.6 on 2018-11-08 12:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('instances', '0028_auto_20181108_1221'),
    ]

    operations = [
        migrations.AddField(
            model_name='colorscheme',
            name='base_border',
            field=models.CharField(max_length=6, null=True),
        ),
    ]
