# Generated by Django 2.0.6 on 2018-09-14 08:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('instances', '0011_instancesettings_company_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='OpenLogo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_alive', models.BooleanField(default=True)),
                ('name', models.CharField(blank=True, max_length=255, null=True)),
                ('logo', models.ImageField(blank=True, null=True, upload_to='')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='instancesettings',
            name='order_page_text',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='instancesettings',
            name='open_logo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='instances.OpenLogo'),
        ),
    ]
