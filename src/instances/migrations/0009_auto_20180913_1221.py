# Generated by Django 2.0.6 on 2018-09-13 12:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('instances', '0008_auto_20180913_1156'),
    ]

    operations = [
        migrations.AlterField(
            model_name='colorscheme',
            name='name',
            field=models.CharField(max_length=255),
        ),
    ]
