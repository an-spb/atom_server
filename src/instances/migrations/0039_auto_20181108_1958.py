# Generated by Django 2.0.6 on 2018-11-08 19:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('instances', '0038_auto_20181108_1227'),
    ]

    operations = [
        migrations.AddField(
            model_name='colorscheme',
            name='button_error_border_color',
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
        migrations.AlterField(
            model_name='colorscheme',
            name='base_accent',
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
        migrations.AlterField(
            model_name='colorscheme',
            name='base_additional',
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
        migrations.AlterField(
            model_name='colorscheme',
            name='base_bg',
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
        migrations.AlterField(
            model_name='colorscheme',
            name='base_border',
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
        migrations.AlterField(
            model_name='colorscheme',
            name='base_color',
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
        migrations.AlterField(
            model_name='colorscheme',
            name='button_bg',
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
        migrations.AlterField(
            model_name='colorscheme',
            name='button_border',
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
        migrations.AlterField(
            model_name='colorscheme',
            name='button_color',
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
        migrations.AlterField(
            model_name='colorscheme',
            name='button_disabled_bg',
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
        migrations.AlterField(
            model_name='colorscheme',
            name='button_disabled_border',
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
        migrations.AlterField(
            model_name='colorscheme',
            name='button_disabled_color',
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
        migrations.AlterField(
            model_name='colorscheme',
            name='button_error_bg',
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
        migrations.AlterField(
            model_name='colorscheme',
            name='button_error_border',
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
        migrations.AlterField(
            model_name='colorscheme',
            name='button_error_color',
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
        migrations.AlterField(
            model_name='colorscheme',
            name='menu_accent',
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
        migrations.AlterField(
            model_name='colorscheme',
            name='menu_bg',
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
        migrations.AlterField(
            model_name='colorscheme',
            name='menu_border',
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
        migrations.AlterField(
            model_name='colorscheme',
            name='menu_color',
            field=models.CharField(blank=True, max_length=6, null=True),
        ),
    ]
