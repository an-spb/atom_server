from django.conf import settings


def serialize_instance(instance):
    return {
        'id': instance.id,
        'color_scheme': serialize_color_scheme(instance.settings.color_scheme),
        'pages': serialize_instance_pages(instance),
        'company_name': instance.settings.company_name,
        'min_price': instance.settings.min_price,
        'updated_at': instance.updated_at.isoformat(),
        'is_alive': instance.is_alive,
    }


def serialize_color_scheme(color_scheme):
    return {
        'base.bg': color_scheme.base_bg,
        'base.color': color_scheme.base_color,
        'base.additional': color_scheme.base_additional,
        'base.accent': color_scheme.base_accent,
        'base.border': color_scheme.base_border,
        'menu.bg': color_scheme.menu_bg,
        'menu.color': color_scheme.menu_color,
        'menu.accent': color_scheme.menu_accent,
        'menu.border': color_scheme.menu_border,
        'button.bg': color_scheme.button_bg,
        'button.color': color_scheme.button_color,
        'button.border': color_scheme.button_border,
        'button.disabled.bg': color_scheme.button_disabled_bg,
        'button.disabled.color': color_scheme.button_disabled_color,
        'button.disabled.border': color_scheme.button_disabled_border,
        'button.error.bg': color_scheme.button_error_bg,
        'button.error.color': color_scheme.button_error_color,
        'button.error.border': color_scheme.button_error_border,
        'button.error.border.color': color_scheme.button_error_border_color,
    }


def serialize_instance_pages(instance):
    return [{'system_name': p.page_class.system_name} for p in instance.pages.all()]


