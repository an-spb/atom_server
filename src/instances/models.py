from django.db import models
from django.contrib.auth.models import User

from utils import NotDeletableModel


class Instance(NotDeletableModel):
    name = models.CharField(max_length=255, null=True, blank=True)
    admin = models.OneToOneField(User, related_name='instance',
                                 null=True, blank=True,
                                 on_delete=models.CASCADE)
    __str__ = lambda self: '#%s%s' % (self.id, (' %s' % self.name if self.name else ''))
    def save(self, *args, **kwargs):
        super(Instance, self).save(*args, **kwargs)
        if not hasattr(self, 'settings'):
            InstanceSettings.objects.create(instance=self)
        if not self.name:
            self.name = 'APP#%s' % self.id
            super(Instance, self).save(*args, **kwargs)


class ColorScheme(NotDeletableModel):
    system_name = models.CharField(max_length=255)
    verbose_name = models.CharField(max_length=255, null=True, blank=True)
    base_bg = models.CharField(max_length=6, null=True, blank=True)
    base_color = models.CharField(max_length=6, null=True, blank=True)
    base_additional = models.CharField(max_length=6, null=True, blank=True)
    base_accent = models.CharField(max_length=6, null=True, blank=True)
    base_border = models.CharField(max_length=6, null=True, blank=True)
    menu_bg = models.CharField(max_length=6, null=True, blank=True)
    menu_color = models.CharField(max_length=6, null=True, blank=True)
    menu_accent = models.CharField(max_length=6, null=True, blank=True)
    menu_border = models.CharField(max_length=6, null=True, blank=True)
    button_bg = models.CharField(max_length=6, null=True, blank=True)
    button_color = models.CharField(max_length=6, null=True, blank=True)
    button_border = models.CharField(max_length=6, null=True, blank=True)
    button_disabled_bg = models.CharField(max_length=6, null=True, blank=True)
    button_disabled_color = models.CharField(max_length=6, null=True, blank=True)
    button_disabled_border = models.CharField(max_length=6, null=True, blank=True)
    button_error_bg = models.CharField(max_length=6, null=True, blank=True)
    button_error_color = models.CharField(max_length=6, null=True, blank=True)
    button_error_border = models.CharField(max_length=6, null=True, blank=True)
    button_error_border_color = models.CharField(max_length=6, null=True, blank=True)
    __str__ = lambda self: '%s (%s)' % (self.verbose_name, self.system_name)
    def save(self, *args, **kwargs):
        super(ColorScheme, self).save(*args, **kwargs)
        instances = Instance.objects.filter(settings__color_scheme=self)
        for instance in instances:
            instance.save()


class OpenLogo(NotDeletableModel):
    name = models.CharField(max_length=255, null=True, blank=True)
    logo = models.ImageField(null=True, blank=True)    
    __str__ = lambda self: '#%s%s' % (self.id, (' %s' % self.name if self.name else ''))


class InstanceSettings(NotDeletableModel):
    instance = models.OneToOneField(Instance, related_name='settings',
                                    null=True, blank=True,
                                    on_delete=models.CASCADE)
    company_name = models.CharField(max_length=255, null=True, blank=True)                                    
    logo = models.ImageField(null=True, blank=True)
    open_logo = models.ForeignKey(OpenLogo,
                                  null=True, blank=True,
                                  on_delete=models.CASCADE)
    color_scheme = models.ForeignKey(ColorScheme,
                                     null=True, blank=True,
                                     on_delete=models.CASCADE)
    min_price = models.IntegerField(default=1000)
    __str__ = lambda self: '#%s %s (#%s)' % (self.id, self.instance.name, self.instance.id)


class PageClass(NotDeletableModel):
    system_name = models.CharField(max_length=255)
    verbose_name = models.CharField(max_length=255)
    order = models.IntegerField(default=0)
    __str__ = lambda self: self.verbose_name
    class Meta:    
        ordering = ('order',)


class InstancePage(NotDeletableModel):
    instance = models.ForeignKey(Instance, related_name='pages',
                                 on_delete=models.CASCADE)
    page_class = models.ForeignKey(PageClass, related_name='pages',
                                   on_delete=models.CASCADE)
    order = models.IntegerField(default=0)
    __str__ = lambda self: '#%s %s/%s' % (self.id, self.instance.name, self.page_class.system_name)
    class Meta:    
        ordering = ('order',)


