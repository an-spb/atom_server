from django.contrib import admin
from django.http import HttpResponseRedirect
from django.shortcuts import reverse
from django import forms

from utils import NotDeletableAdmin, NotDeletableInline
from instances.models import Instance, InstanceSettings, ColorScheme,\
        OpenLogo, PageClass, InstancePage
from instances.notifiers import notify_about_instance_change,\
        notify_about_color_scheme_change


class InstanceAdmin(NotDeletableAdmin):
    list_display = ('name', 'id', 'created_at', 'updated_at')
    search_fields = ('name', 'id')
    class SettingsInline(admin.StackedInline):
        model = InstanceSettings
        fields = ('company_name', 'logo', 'open_logo', 'color_scheme',)
    class PagesInline(NotDeletableInline):
        model = InstancePage
        fields = ('page_class', 'order')
    inlines = (SettingsInline, PagesInline,)
    def save_related(self, request, form, formsets, change):
        super(InstanceAdmin, self).save_related(request, form, formsets, change)
        notify_about_instance_change(form.instance)


class InstanceSettingsAdmin(NotDeletableAdmin):
    class Form(forms.ModelForm):
        class Meta:
            model = InstanceSettings
            fields = ('company_name', 'logo', 'open_logo', 'color_scheme',)
    form = Form
    def get_queryset(self, request):
        qs = super(InstanceSettingsAdmin, self).get_queryset(request)
        return qs.filter(instance__admin=request.user)
    def changelist_view(self, request, extra_context=None):
        obj = self.get_queryset(request)[0]
        return HttpResponseRedirect(reverse("admin:%s_%s_change" %(self.model._meta.app_label, self.model._meta.model_name), args=(obj.id,)))
    def get_readonly_fields(self, request, obj=None):
        return ()


class ColorSchemeAdmin(NotDeletableAdmin):
    list_display = ('system_name', 'verbose_name',)
    search_fields = ('system_name', 'verbose_name',)
    def save_related(self, request, form, formsets, change):
        super(ColorSchemeAdmin, self).save_related(request, form, formsets, change)
        notify_about_color_scheme_change(form.instance)
    

class OpenLogoAdmin(NotDeletableAdmin):
    pass


class PageClassAdmin(NotDeletableAdmin):
    list_display = ('system_name', 'verbose_name', 'order')
        
    
admin.site.register(Instance, InstanceAdmin)
admin.site.register(InstanceSettings, InstanceSettingsAdmin)
admin.site.register(ColorScheme, ColorSchemeAdmin)
admin.site.register(OpenLogo, OpenLogoAdmin)
admin.site.register(PageClass, PageClassAdmin)
