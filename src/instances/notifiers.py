from django.contrib.auth.models import User

from notify import notify_instances
from instances.serializers import serialize_instance
from instances.models import Instance


def notify_about_instance_change(instance):
    notify_instances([(instance,
                       {'key': 'instances.get_instance',
                        'params': [instance.id],
                        'data': [serialize_instance(instance)]})])


def notify_about_color_scheme_change(color_scheme):
    instances = Instance.objects.filter(settings__color_scheme=color_scheme)
    for instance in instances:
        notify_about_instance_change(instance)

