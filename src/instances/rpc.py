from dateutil import parser

from utils import rpc_method
from instances.models import Instance
from instances.serializers import serialize_instance


@rpc_method('instances.get_instance')
def instances_get_instance(request, instance_id, updated_at_gte=None):
    instances = Instance.objects.filter(id=instance_id)
    if updated_at_gte:
        updated_at_gte = parser.parse(updated_at_gte)
        instances = instances.filter(updated_at__gte=updated_at_gte)
    return {'data': [serialize_instance(i) for i in instances]}

