from jsonrpc import jsonrpc_method
from termcolor import cprint
import pprint


@jsonrpc_method('react_log.log')
def log(request, color, is_print, *args):
    if is_print:
        pprint.pprint(args)
    else:
        cprint(' '.join([str(a) for a in args]), color)
