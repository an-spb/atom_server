# Generated by Django 2.0.6 on 2018-10-16 20:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('goods', '0013_auto_20181015_1835'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='is_goods_group',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='descriptionline',
            name='category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='description_lines', to='goods.Category'),
        ),
        migrations.AlterField(
            model_name='descriptionline',
            name='goods',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='description_lines', to='goods.Goods'),
        ),
    ]
