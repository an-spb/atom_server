# Generated by Django 2.0.6 on 2018-11-09 18:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('goods', '0032_auto_20181109_1838'),
    ]

    operations = [
        migrations.AddField(
            model_name='goods',
            name='level',
            field=models.PositiveIntegerField(db_index=True, default=0, editable=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='goods',
            name='lft',
            field=models.PositiveIntegerField(db_index=True, default=0, editable=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='goods',
            name='rght',
            field=models.PositiveIntegerField(db_index=True, default=0, editable=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='goods',
            name='tree_id',
            field=models.PositiveIntegerField(db_index=True, default=0, editable=False),
            preserve_default=False,
        ),
    ]
