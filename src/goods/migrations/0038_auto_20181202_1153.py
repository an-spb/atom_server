from django.db import migrations


def migrate_params(apps, schema_editor):
    Goods = apps.get_model('goods', 'Goods')
    GoodsParam = apps.get_model('goods', 'GoodsParam')
    for goods in Goods.objects.filter(param_name__isnull=False):
        GoodsParam.objects.create(goods=goods,
                                  param_name=goods.param_name,
                                  param_unit=goods.param_unit,
                                  param_min=goods.param_min,
                                  param_max=goods.param_max,
                                  param_step=goods.param_step,
                                  param_default=goods.param_default)


class Migration(migrations.Migration):

    dependencies = [
        ('goods', '0037_orderitem_price_formula'),
    ]

    operations = [
        migrations.RunPython(migrate_params)
    ]
