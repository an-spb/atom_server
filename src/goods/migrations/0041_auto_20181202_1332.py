from django.db import migrations


def migrate_price_to_formula(apps, schema_editor):
    Goods = apps.get_model('goods', 'Goods')
    for goods in Goods.objects.filter(param_name__isnull=False):
        goods.price_formula = '%s * a' % goods.price
        goods.save()

class Migration(migrations.Migration):

    dependencies = [
        ('goods', '0040_auto_20181202_1238'),
    ]

    operations = [
        migrations.RunPython(migrate_price_to_formula)
    ]
