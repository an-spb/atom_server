# Generated by Django 2.0.6 on 2019-01-23 23:57

from django.db import migrations, models
import django.db.models.deletion
import goods.validators


class Migration(migrations.Migration):

    dependencies = [
        ('goods', '0067_auto_20190123_2127'),
    ]

    operations = [
        migrations.CreateModel(
            name='GoodsImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.FileField(blank=True, null=True, upload_to='', validators=[goods.validators.validate_svg])),
                ('order', models.IntegerField(default=0)),
            ],
            options={
                'ordering': ('order',),
            },
        ),
        migrations.RemoveField(
            model_name='goods',
            name='min_price',
        ),
        migrations.AddField(
            model_name='goodsimage',
            name='goods',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='images', to='goods.Goods'),
        ),
    ]
