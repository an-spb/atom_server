# Generated by Django 2.0.6 on 2018-11-10 20:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('goods', '0033_auto_20181109_1840'),
    ]

    operations = [
        migrations.AddField(
            model_name='goods',
            name='item_type',
            field=models.IntegerField(choices=[(0, 'category'), (1, 'goods')], default=1),
        ),
    ]
