# Generated by Django 2.0.6 on 2018-10-22 23:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('instances', '0021_auto_20181022_2315'),
        ('goods', '0017_auto_20181022_1330'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='category',
            name='page',
        ),
        migrations.RemoveField(
            model_name='goods',
            name='page',
        ),
        migrations.RemoveField(
            model_name='order',
            name='page',
        ),
        migrations.AddField(
            model_name='category',
            name='instance',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='goods_categories', to='instances.Instance'),
        ),
        migrations.AddField(
            model_name='goods',
            name='instance',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='goods', to='instances.Instance'),
        ),
        migrations.AddField(
            model_name='order',
            name='instance',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='orders', to='instances.Instance'),
        ),
    ]
