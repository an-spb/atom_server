# Generated by Django 2.0.6 on 2018-12-08 05:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('goods', '0053_auto_20181208_0345'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='tmp_id',
            field=models.CharField(editable=False, max_length=32, null=True),
        ),
    ]
