# Generated by Django 2.0.6 on 2018-10-15 15:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('goods', '0009_goods_param_unit'),
    ]

    operations = [
        migrations.CreateModel(
            name='DescriptionLine',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('line_type', models.IntegerField(choices=[(0, 'simple'), (1, 'header')])),
                ('text', models.TextField()),
                ('category', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='goods.Category')),
                ('goods', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='goods.Goods')),
            ],
        ),
    ]
