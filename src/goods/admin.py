from django.contrib import admin
from django import forms
from mptt.admin import MPTTModelAdmin

from utils import NotDeletableAdmin, NotDeletableInline
from goods.models import Goods, DescriptionLine, Order, OrderItem,\
         OrderItemParamValue, AdditionalGoods, GoodsParam, DisplaceGoods, \
         GoodsImage
from goods.notifiers import notify_about_goods_change, notify_about_order_change


class GoodsAdmin(admin.ModelAdmin):
    list_display = ('name', 'id', 'order', 'phantom_order', 'level', 'item_type')
    def get_queryset(self, *args, **kwargs):
        return Goods.objects.filter(is_alive=True)
    def get_ordering(self, request):
        mptt_opts = self.model._mptt_meta
        return mptt_opts.tree_id_attr, mptt_opts.left_attr
    def get_actions(self, request):
        actions = super(GoodsAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions
    def save_related(self, request, form, formsets, change):
        super(GoodsAdmin, self).save_related(request, form, formsets, change)
        notify_about_goods_change(form.instance)
    class ParamInline(admin.TabularInline):
        model = GoodsParam
    class GoodsImageInline(admin.TabularInline):
        model = GoodsImage
    class AdditionalGoodsInline(admin.TabularInline):
        model = AdditionalGoods
        fk_name = 'goods_to'
        fields = ('goods_add', 'order')
    class DisplaceGoodsInline(admin.TabularInline):
        model = DisplaceGoods
        fk_name = 'displace_by'
    class DescriptionLineInline(admin.TabularInline):
        model = DescriptionLine
        fields = ('line_type', 'text', 'order')
        class DescriptionLineForm(forms.ModelForm):
            text = forms.CharField()
            class Meta:
                model = DescriptionLine
                exclude = ()
        form = DescriptionLineForm
    inlines = (ParamInline, GoodsImageInline, DescriptionLineInline, \
               AdditionalGoodsInline, DisplaceGoodsInline,)
    class Media:
        js = (
            'web/admin.js',
        )


class OrderAdmin(NotDeletableAdmin):
    list_display = ('ind', 'address', 'datetime', 'user')
    class OrderItemInline(admin.TabularInline):
        model = OrderItem
    inlines = (OrderItemInline,)
    def save_related(self, request, form, formsets, change):
        super(OrderAdmin, self).save_related(request, form, formsets, change)
        notify_about_order_change(form.instance)


admin.site.register(Goods, GoodsAdmin)
admin.site.register(Order, OrderAdmin)

