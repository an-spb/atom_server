from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from instances.models import Instance
from goods.models import Goods, Order
from reset.models import ResetRequest
from reset.notifiers import notify_about_reset_request_change


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '-i', 
            '--instance_id',
            type=int
        )
        parser.add_argument(
            '-u', 
            '--user_id',
            type=int
        )
    def handle(self, *args, **options):
        instance = Instance.objects.get(id=options['instance_id'])
        user = User.objects.get(id=options['user_id'])
        Order.objects.filter(instance=instance, user=user).delete(True)
        rr = ResetRequest.objects.create(instance=instance)
        notify_about_reset_request_change(rr)
                
        

    