import datetime
import random

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from instances.models import Instance
from goods.models import Goods, Order, OrderItem, OrderItemParamValue, Address
from goods.serializers import serialize_goods
from reset.models import ResetRequest
from reset.notifiers import notify_about_reset_request_change


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '-i', 
            '--instance_id',
            type=int
        )
        parser.add_argument(
            '-u', 
            '--user_id',
            type=int
        )
    def handle(self, *args, **options):
        instance = Instance.objects.get(id=options['instance_id'])
        user = User.objects.get(id=options['user_id'])
        ### REMOVE THIS
        Order.objects.filter(instance=instance, user=user).delete(True)
        ### REMOVE THIS
        dt = datetime.datetime.now()
        MIN_DAYS = -2
        MAX_DAYS = 2
        for day_shift in range(MIN_DAYS, MAX_DAYS):
            if day_shift != MAX_DAYS:
                for i in range(0, 3):
                    self.generate_order(instance, user,
                                        dt + datetime.timedelta(days=day_shift, minutes=i * 10),
                                        random.choice(['clean', 'clean', 'windows']),
                                        random.choice([True, False, False, False]))
        self.generate_order(instance, user,
                            dt + datetime.timedelta(days=MAX_DAYS, minutes=20),
                            'windows', False)
        self.generate_order(instance, user,
                            dt + datetime.timedelta(days=MAX_DAYS, minutes=30),
                            'clean', True)
        self.generate_order(instance, user,
                            dt + datetime.timedelta(days=MAX_DAYS, minutes=50),
                            'clean', False)
        rr = ResetRequest.objects.create(instance=instance)
        notify_about_reset_request_change(rr)
    def generate_order(self, instance, user, dt, order_type, add_furniture):
        address = Address(user=user,
                          city='Санкт-Петербург',
                          street=random.choice(['Большая Пушкарская', 'Ленская', 'Добролюбова', 'Аэродромная']),
                          house=random.choice(['12', '33', '24', '5', '7', '11', '8', '3', '21', '27', '28', '37', '39']),
                          housing='',
                          flat=random.choice(['121', '3', '57', '30', '1', '15', '17', '21', '67', '81', '95', '99', '110']))
        address.save()
        order = Order(instance=instance,
                      user=user,
                      address=address,
                      datetime=dt)
        order.save()
        def add_order_items(order_field, count, goods_ids, param1=None, param2=None):
            for i in range(count):
                goods_id = random.choice(goods_ids)
                goods_ids.remove(goods_id)
                goods = Goods.objects.get(id=goods_id)
                oi = OrderItem(order=order,
                                goods=goods,
                                goods_dump=serialize_goods(goods),
                                is_added_by_user=True,
                                order_field=order_field + i)
                oi.save()
                if param1:
                    pv = OrderItemParamValue(order_item=oi,
                                                param=goods.params.all()[0],
                                                float_value=random.choice(param1))
                    pv.save()
                if param2:
                    pv = OrderItemParamValue(order_item=oi,
                                                param=goods.params.all()[1],
                                                float_value=random.choice(param2))
                    pv.save()
        if order_type == 'clean':
            add_order_items(10, 1, [21] if add_furniture else [21, 23, 24], [55, 60, 70, 75], [1])
            if not add_furniture:
                add_order_items(20, random.choice([1, 2, 2]), [61, 62, 63], [1])
        elif order_type == 'windows':
            add_order_items(10, random.choice([2, 3, 4]), [30, 31, 32, 33], [1, 2, 3])                    
            add_order_items(20, random.choice([0, 1]), [35], [1])                    
        if add_furniture:
            add_order_items(100, random.choice([2, 3]), [46, 47, 48, 49, 50], [1, 2])                    
        


        

    