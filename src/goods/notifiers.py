from django.contrib.auth.models import User

from notify import notify_users, notify_instances
from goods.serializers import serialize_goods, serialize_order


def notify_about_goods_change(goods):
    notify_instances([(goods.instance,
                       {'key': 'goods.get_goods',
                        'params': [goods.instance.id],
                        'data': [serialize_goods(goods)]})])


def notify_about_order_change(order):
    instance = order.instance
    users = [instance.admin, order.user]
    users_data = []
    for user in users:
        print(user.profile.phone_number, user.profile.id, user.profile.instance)
        users_data.append((user,
                           {'key': 'goods.get_orders',
                            'params': [instance.id],
                            'data': [serialize_order(order)]}))
    notify_users(users_data)    

