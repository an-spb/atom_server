from django.db import models
from django.contrib.postgres.fields import JSONField
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from mptt.models import MPTTModel
from datetime import datetime

from instances.models import Instance
from utils import NotDeletableModel

from goods.validators import validate_svg


class GoodsQuerySet(models.query.QuerySet):
    def delete(self, force_delete=False):
        if not force_delete:
            self.update(is_alive=False)
        else:
            super(GoodsQuerySet, self).delete()


class GoodsManager(models.Manager):
    def get_queryset(self):
        return GoodsQuerySet(self.model)


class Goods(MPTTModel):
    instance = models.ForeignKey(Instance, related_name='goods',
                                 on_delete=models.CASCADE)
    parent = models.ForeignKey('self', null=True, blank=True,
                               related_name='children',
                               on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    image = models.FileField(null=True, blank=True, validators=[validate_svg])
    image_content = models.TextField(null=True, blank=True, editable=False)
    description_line_1 = models.CharField(null=True, blank=True, max_length=255)
    description_line_2 = models.CharField(null=True, blank=True, max_length=255)
    
    TYPE_CATEGORY = 0
    TYPE_GOODS = 1
    TYPE_MASTER_GOODS = 2
    TYPE_CHOICES = ((TYPE_CATEGORY, 'category'),
                    (TYPE_GOODS, 'goods'),
                    (TYPE_MASTER_GOODS, 'master goods'))
    item_type = models.IntegerField(choices=TYPE_CHOICES, default=1)

    price_formula = models.CharField(max_length=255, null=True, blank=True)

    details_button_text = models.CharField(max_length=255, null=True, blank=True)
    force_to_basket = models.BooleanField(default=False)
    not_display_in_catalog = models.BooleanField(default=False)
    
    order = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    is_alive = models.BooleanField(default=True)    
    
    is_phantom = models.BooleanField(default=False)
    phantom_order = models.IntegerField(default=0)
    phantom_name = models.CharField(max_length=255, null=True, blank=True)
    
    __str__ = lambda self: '%s  %s' % ('|--' * self.level, self.name)
    
    class Meta:    
        ordering = ('order',)
    class MPTTMeta:
        order_insertion_by = ['order']
    def save(self, *args, **kwargs):
        if self.parent and self.parent.instance_id != self.parent.instance_id:
            raise ValidationError('instance id of parent and child must be the same')
        super(Goods, self).save(*args, **kwargs)
        self.preload_image_content()
    def delete(self):
        self.is_alive = False
        self.save()
    def preload_image_content(self):
        if not self.image:
            self.image_content = None
            return
        self.image.open(mode='rt') 
        self.image_content = self.image.read()
        self.image.close()
        super(Goods, self).save()


class GoodsParam(models.Model):
    goods = models.ForeignKey(Goods, related_name='params', on_delete=models.CASCADE)
    formula_letter = models.CharField(max_length=1, null=True, blank=True)
    name = models.CharField(max_length=255, null=True, blank=True)
    unit = models.CharField(max_length=255, null=True, blank=True)
    min = models.IntegerField(null=True, blank=True)
    max = models.IntegerField(null=True, blank=True)
    step = models.IntegerField(null=True, blank=True)
    default = models.IntegerField(null=True, blank=True)
    order = models.IntegerField(default=0)
    class Meta:    
        ordering = ('order',)
    

class AdditionalGoods(models.Model):
    goods_to = models.ForeignKey(Goods, related_name='additional_goods',
                                 on_delete=models.CASCADE)
    goods_add = models.ForeignKey(Goods, related_name='additional_to',
                                  on_delete=models.CASCADE)
    order = models.IntegerField(default=0)
    class Meta:    
        ordering = ('order',)


class DisplaceGoods(models.Model):
    displace_by = models.ForeignKey(Goods, related_name='displace_goods',
                                    on_delete=models.CASCADE)
    goods = models.ForeignKey(Goods, related_name='displaced_by',
                              on_delete=models.CASCADE)


class GoodsImage(models.Model):
    goods = models.ForeignKey(Goods, related_name='images',
                              on_delete=models.CASCADE)
    image = models.ImageField(null=True, blank=True)                            
    order = models.IntegerField(default=0)
    class Meta:    
        ordering = ('order',)
    

class DescriptionLine(models.Model):
    goods = models.ForeignKey(Goods, on_delete=models.CASCADE,
                              related_name='description_lines')
    line_type = models.IntegerField(choices=((0, 'simple'), (1, 'header')), default=0)
    text = models.TextField()
    order = models.IntegerField(default=0)
    class Meta:    
        ordering = ('order',)


class Order(NotDeletableModel):
    tmp_id = models.CharField(max_length=32, null=True, editable=False)
    instance = models.ForeignKey(Instance, related_name='orders',
                                 on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='orders',
                             on_delete=models.CASCADE)
    ind = models.IntegerField(editable=False)
    address = models.ForeignKey('Address', related_name='orders',
                                null=True, blank=True,
                                on_delete=models.CASCADE)
    datetime = models.DateTimeField(null=True, blank=True)
    NEW = 0
    CONFIRMED = 1
    CANCELED = 2
    STATUS_CHOICES = ((NEW, 'new'), (CONFIRMED, 'confirmed'), (CANCELED, 'canceled'))
    confirmation_status = models.IntegerField(choices=STATUS_CHOICES, default=0)
    __str__ = lambda self: str(self.ind)
    class Meta:
        unique_together = ('instance', 'ind')    
    def save(self, *args, **kwargs):
        Order.objects.all().select_for_update()
        if not self.ind:
            max_ind = Order.objects.order_by('-ind')
            self.ind = max_ind[0].ind + 1 if max_ind.count() else 1000
        return super(Order, self).save(*args, **kwargs)


class OrderItem(models.Model):
    order = models.ForeignKey(Order, related_name='items',
                              on_delete=models.CASCADE)
    goods = models.ForeignKey(Goods, related_name='order_items',
                              on_delete=models.CASCADE)
    goods_dump = JSONField()
    is_added_by_user = models.BooleanField(default=True)
    order_field = models.IntegerField(default=0)
    class Meta:    
        ordering = ('order_field',)


class OrderItemParamValue(models.Model):
    order_item = models.ForeignKey(OrderItem, related_name='params', on_delete=models.CASCADE)
    param = models.ForeignKey(GoodsParam, on_delete=models.CASCADE)
    float_value = models.FloatField()    


class Address(NotDeletableModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    city = models.CharField(max_length=255)
    street = models.CharField(max_length=255)
    house = models.CharField(max_length=255)
    housing = models.CharField(max_length=255)
    flat = models.CharField(max_length=255)
    intercome_code = models.CharField(max_length=255)
    floor = models.CharField(max_length=255)
    def __str__(self):
        parts = [
            self.city,
            self.street,
            'дом %s' % self.house + (' к %s' % self.housing if self.housing else ''),
            'кв %s' % self.flat,
            'домофон %s' % self.intercome_code if self.intercome_code else None,
            'этаж %s' % self.floor if self.floor else None,
        ]
        return ', '.join([p for p in parts if p is not None])
        
