from django.conf import settings

from goods.models import Goods


def serialize_goods(goods):
    return {
        'id': goods.id,
        'parent_id': goods.parent_id or 0,
        'type': goods.item_type,
        'is_slave_goods': goods.parent and goods.parent.item_type == Goods.TYPE_MASTER_GOODS,
        'name': goods.name,
        'image': '%s/media/%s' % (settings.HOST_URL, str(goods.image)) if goods.image else None,
        'image_content': goods.image_content,
        'description_line_1': goods.description_line_1,
        'description_line_2': goods.description_line_2,
        'params': [serialize_param(param) for param in goods.params.all()],
        'price_formula': goods.price_formula,
        'order': goods.order,
        'force_to_basket': goods.force_to_basket,
        'details_button_text': goods.details_button_text,
        'not_display_in_catalog': goods.not_display_in_catalog,
        'gallery_images': [serialize_gallery_image(gi) for gi in goods.images.all()],
        'description_lines': serialize_description_lines(goods),
        'additional_goods': [g.goods_add_id for g in goods.additional_goods.all()],
        'displace_goods': [g.goods_id for g in goods.displace_goods.all()],
        'is_phantom': goods.is_phantom,
        'phantom_order': goods.phantom_order,
        'phantom_name': goods.phantom_name,
        'updated_at': goods.updated_at.isoformat(),
        'is_alive': goods.is_alive,
    }


def serialize_param(goods_param):
    return {
        'id': goods_param.id,
        'formula_letter': goods_param.formula_letter,
        'name': goods_param.name,
        'unit': goods_param.unit,
        'min': goods_param.min,
        'max': goods_param.max,
        'step': goods_param.step,
        'default': goods_param.default,
    }


def serialize_description_lines(goods):
    return [{'id': l.id,
             'line_type': l.line_type,
             'text': l.text} for l in goods.description_lines.all()]


def serialize_gallery_image(image):
    return {'image_url': '%s/media/%s' % (settings.HOST_URL, str(image.image))}


def serialize_order(order):
    return {
        'id': order.id,
        'ind': order.ind,
        'tmp_id': order.tmp_id,
        'items': [serialize_order_item(item) for item in order.items.all()],
        'address': serialize_address(order.address) if order.address else None,
        'datetime': order.datetime.isoformat() if order.datetime else None,
        'confirmation_status': order.confirmation_status,
        'updated_at': order.updated_at.isoformat(),
        'is_alive': order.is_alive,
    }


def serialize_order_item(order_item):
    return {
        'id': order_item.id,
        'goods': order_item.goods_dump,
        'params': {p.param.id: p.float_value for p in order_item.params.all()},
        'is_added_by_user': order_item.is_added_by_user,
        'order_field': order_item.order_field,
    }


def serialize_address(address):
    return {
        'city': address.city,
        'street': address.street,
        'house': address.house,
        'housing': address.housing,
        'flat': address.flat,
        'intercome_code': address.intercome_code,
    }


