import os

from django.core.exceptions import ValidationError


def validate_svg(value):
    ext = os.path.splitext(value.name)[1]
    if ext.lower() != '.svg':
        raise ValidationError(u'You must upload .svg file')