from dateutil import parser

from django.db import transaction

from utils import rpc_method
from goods.models import Goods, Order, OrderItem, OrderItemParamValue, Address
from goods.serializers import serialize_goods, serialize_order
from goods.notifiers import notify_about_order_change
from instances.models import Instance


@rpc_method('goods.get_goods')
def goods_get_goods(request, instance_id, updated_at_gte=None):
    instance = Instance.objects.get(id=instance_id)
    goods = Goods.objects.filter(instance=instance)
    if updated_at_gte:
        updated_at_gte = parser.parse(updated_at_gte)
        goods = goods.filter(updated_at__gte=updated_at_gte)
    return {'data': [serialize_goods(g) for g in goods]}


@rpc_method('goods.get_orders')
def goods_get_orders(request, instance_id, updated_at_gte=None):
    if request.user.is_anonymous:
        return {'data': []}
    instance = Instance.objects.get(id=instance_id)
    if instance.admin == request.user:
        orders = Order.objects.filter(instance=instance)
    else:
        orders = Order.objects.filter(instance=instance,
                                      user=request.user)
    if updated_at_gte:
        updated_at_gte = parser.parse(updated_at_gte)
        orders = orders.filter(updated_at__gte=updated_at_gte)
    return {'data': [serialize_order(order) for order in orders]}


@rpc_method('goods.get_orders_update')
@transaction.atomic
def goods_get_orders_update(request, data):
    if 'id' in data and data['id']:
        order = Order.objects.get(id=data['id'])
    else:
        order_params = {'user': request.user,
                        'instance': request.user.profile.instance,
                        'tmp_id': data['tmp_id']}
        orders = Order.objects.filter(**order_params)
        if len(orders):
            order = orders[0]
        else:
            order = Order.objects.create(**order_params)
    if 'confirmation_status' in data:
        order.confirmation_status = data['confirmation_status']
        order.save()
    if 'items' in data:
        order.items.all().delete()
        for item_data in data['items']:
            item = OrderItem(order=order)
            item.goods_id = item_data['goods']['id']
            item.goods_dump = item_data['goods']
            item.is_added_by_user = item_data.get('is_added_by_user', False)
            item.order_field = item_data.get('order', 0)
            item.save()
            for param_id, value in item_data['params'].items():
                param = OrderItemParamValue(order_item=item, param_id=param_id)
                param.float_value = value
                param.save()
    if 'address' in data and data['address']:
        if not order.address:
            order.address = Address.objects.create(user=request.user)
            order.save()
        Address.objects.filter(id=order.address.id).update(**data['address'])
    if 'datetime' in data and data['datetime']:
        print(data['datetime'])
        order.datetime = data['datetime']
        order.save()
    order = Order.objects.get(id=order.id)
    notify_about_order_change(order)



