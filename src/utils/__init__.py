from datetime import datetime

from django.db import models
from django.contrib import admin
from jsonrpc import jsonrpc_method
from termcolor import cprint


def rpc_method(method_name):
    def decorator(fn):
        def decorated(request, *args, **kwargs):
            cprint('RPC> %s' % method_name, attrs=['bold'])
            # cprint('RPC> %s %s %s' % (method_name, args, kwargs), attrs=['bold'])
            return fn(request, *args, **kwargs)
        return jsonrpc_method(method_name)(decorated)
    return decorator


class NotDeletableQuerySet(models.query.QuerySet):
    def delete(self, force_delete=False):
        if not force_delete:
            self.update(is_alive=False)
        else:
            super(NotDeletableQuerySet, self).delete()


class NotDeletableManager(models.Manager):
    def get_queryset(self):
        return NotDeletableQuerySet(self.model)


class NotDeletableModel(models.Model):
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    is_alive = models.BooleanField(default=True)    
    objects = NotDeletableManager()
    class Meta:
        abstract = True
    def save(self, *args, **kwargs):
        # Not using auto_now and auto_now_add to be able to use this fields
        # in Django Admin forms as read only fields
        self.updated_at = datetime.now()
        if not self.id:
            self.created_at = self.updated_at
        return super(NotDeletableModel, self).save(*args, **kwargs)
    def delete(self):
        self.is_alive = False
        self.save()


class NotDeletableAdmin(admin.ModelAdmin):
    can_delete = False
    def get_readonly_fields(self, request, obj=None):
        return ('created_at', 'updated_at',)
    def has_delete_permission(self, request, obj=None):
        return False
    def get_actions(self, request):
        actions = super(NotDeletableAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions


class NotDeletableInline(admin.TabularInline):
    can_delete = False
    def get_readonly_fields(self, request, obj=None):
        return ('created_at', 'updated_at',)
