def serialize_faq_group(faq_group):
    return {
        'id': faq_group.id,
        'header': faq_group.header,
        'not_display_name': faq_group.not_display_name,
        'questions': [serialize_question(q) for q in faq_group.questions.all()],
    }


def serialize_question(question):
    return {
        'id': question.id,
        'question': question.question,
        'answer': question.answer,
        'is_opened': question.is_opened,
    }


