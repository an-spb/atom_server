from dateutil import parser

from utils import rpc_method
from instances.models import Instance
from faq.models import FAQGroup
from faq.serializers import serialize_faq_group


@rpc_method('faq.get_faq')
def faq_get_faq(request, instance_id, updated_at_gte=None):
    instance = Instance.objects.get(id=instance_id)
    faq_groups = FAQGroup.objects.filter(instance=instance)
    if updated_at_gte:
        updated_at_gte = parser.parse(updated_at_gte)
        faq_groups = faq_groups.filter(updated_at__gte=updated_at_gte)
    return {'data': [serialize_faq_group(g) for g in faq_groups]}
