from notify import notify_instances
from faq.serializers import serialize_faq_group


def notify_about_faq_group_change(faq_group):
    notify_instances([(faq_group.instance,
                       {'key': 'faq.get_faq',
                        'params': [faq_group.instance.id],
                        'data': [serialize_faq_group(faq_group)]})])
