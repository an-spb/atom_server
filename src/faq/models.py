from django.db import models

from instances.models import Instance
from utils import NotDeletableModel


class FAQGroup(NotDeletableModel):
    instance = models.ForeignKey(Instance, related_name='faq_groups',
                                 on_delete=models.CASCADE)
    header = models.CharField(max_length=255)
    not_display_name = models.BooleanField(default=False)
    order = models.IntegerField(default=0)
    __str__ = lambda self: str(self.header)
    class Meta:    
        ordering = ('order',)


class Question(models.Model):
    group = models.ForeignKey(FAQGroup, related_name='questions',
                              on_delete=models.CASCADE)
    question = models.CharField(max_length=255)
    answer = models.TextField()
    is_opened = models.BooleanField(default=False)
    order = models.IntegerField(default=0)
    __str__ = lambda self: str(self.question)
    class Meta:    
        ordering = ('order',)

