from django.contrib import admin
from django import forms

from utils import NotDeletableAdmin, NotDeletableInline
from faq.models import FAQGroup, Question
from faq.notifiers import notify_about_faq_group_change


class FAQGroupAdmin(NotDeletableAdmin):
    list_display = ('header', 'order')
    def get_queryset(self, *args, **kwargs):
        return FAQGroup.objects.filter(is_alive=True)
    def save_related(self, request, form, formsets, change):
        super(FAQGroupAdmin, self).save_related(request, form, formsets, change)
        notify_about_faq_group_change(form.instance)
    class QuestionInline(admin.TabularInline):
        model = Question
    inlines = (QuestionInline,)


admin.site.register(FAQGroup, FAQGroupAdmin)

