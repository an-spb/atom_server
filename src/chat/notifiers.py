from notify import notify_users
from chat.serializers import serialize_room, serialize_message


def notify_about_room_change(room):
    users_data = []
    for p in room.participants.all():
        users_data.append((p.user,
                           {'key': 'chat.get_rooms',
                            'params': [],
                            'data': [serialize_room(room, p.is_alive)]}))
    notify_users(users_data)


def notify_about_message_change(message):
    users_data = []
    for p in message.participant.room.participants.all():
        users_data.append((p.user,
                           {'key': 'chat.get_room_messages',
                            'params': [message.participant.room.id],
                            'data': [serialize_message(message)]}))
    notify_users(users_data)
    