from dateutil import parser

from utils import rpc_method
from chat.models import RoomParticipant, Message
from chat.serializers import serialize_room, serialize_message
from chat.notifiers import notify_about_message_change, notify_about_room_change


@rpc_method('chat.get_rooms')
def chat_get_rooms(request, updated_at_gte=None):
    participants = RoomParticipant.objects \
                        .filter(user=request.user) \
                        .order_by('room__updated_at')
    if updated_at_gte:
        updated_at_gte = parser.parse(updated_at_gte)
        participants = participants.filter(room__updated_at__gte=updated_at_gte)
    return {'data': [serialize_room(p.room, p.is_alive) for p in participants]}


@rpc_method('chat.get_room_messages')
def chat_get_room_messages(request, room_id, updated_at_gte=None):
    messages = Message.objects \
                        .filter(participant__room_id=room_id) \
                        .order_by('created_at')
    if updated_at_gte:
        updated_at_gte = parser.parse(updated_at_gte)
        messages = messages.filter(updated_at__gte=updated_at_gte)
    return {'data': [serialize_message(m) for m in messages]}


@rpc_method('chat.get_room_messages_create')
def chat_get_room_messages_create(request, values):
    message = Message.objects.create(participant_id=values['participant_id'],
                                     text=values['text'],
                                     tmp_id=values['tmp_id'])
    message.participant.room.save()
    notify_about_message_change(message)
    notify_about_room_change(message.participant.room)


@rpc_method('chat.get_room_messages_update')
def chat_get_room_messages_create(request, id, values):
    try:
        message = Message.objects.get(id=id)
        message.is_received = values.get('is_received', message.is_received)
        message.is_read = values.get('is_read', message.is_read)
        message.is_received = message.is_read or message.is_received
        message.save()
        notify_about_message_change(message)
    except Message.DoesNotExist:
        pass
