# Generated by Django 2.0.6 on 2018-08-10 16:18

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('chat', '0002_auto_20180810_1427'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='created_at',
            field=models.DateTimeField(blank=True),
        ),
        migrations.AlterField(
            model_name='message',
            name='updated_at',
            field=models.DateTimeField(blank=True),
        ),
        migrations.AlterField(
            model_name='room',
            name='created_at',
            field=models.DateTimeField(blank=True),
        ),
        migrations.AlterField(
            model_name='room',
            name='updated_at',
            field=models.DateTimeField(blank=True),
        ),
        migrations.AlterField(
            model_name='roomparticipant',
            name='created_at',
            field=models.DateTimeField(blank=True),
        ),
        migrations.AlterField(
            model_name='roomparticipant',
            name='updated_at',
            field=models.DateTimeField(blank=True),
        ),
        migrations.AlterUniqueTogether(
            name='roomparticipant',
            unique_together={('room', 'user')},
        ),
    ]
