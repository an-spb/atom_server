# Generated by Django 2.0.6 on 2018-08-17 15:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0007_message_is_auto'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='message',
            name='is_auto',
        ),
        migrations.AddField(
            model_name='message',
            name='is_human_made',
            field=models.BooleanField(default=True),
        ),
    ]
