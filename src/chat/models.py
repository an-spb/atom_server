from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from utils import NotDeletableModel


class Room(NotDeletableModel):
    def get_users_str(self):
        return ', '.join([str(p.user) for p in self.participants.all()])
    get_users_str.short_description = 'users'


class RoomParticipant(NotDeletableModel):
    room = models.ForeignKey(Room, on_delete=models.CASCADE, related_name='participants')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    __str__ = lambda self: '%s ===> %s' % (self.room, self.user)
    class Meta:
        unique_together = ('room', 'user')    
    def __init__(self, *args, **kwargs):
        super(RoomParticipant, self).__init__(*args, **kwargs)
        self.initial_room = getattr(self, 'room', None)
        self.initial_user = getattr(self, 'user', None)
    def clean(self):
        if self.id:
            if self.initial_room != self.room or self.initial_user != self.user:
                raise ValidationError('Changing user is not allowed')
        

class Message(NotDeletableModel):
    participant = models.ForeignKey(RoomParticipant, on_delete=models.CASCADE)
    text = models.TextField()
    is_received = models.BooleanField(default=False)
    is_read = models.BooleanField(default=False)
    is_human_made = models.BooleanField(default=True)
    tmp_id = models.CharField(max_length=32, null=True, editable=False)
    def __init__(self, *args, **kwargs):
        super(Message, self).__init__(*args, **kwargs)
        self.initial_participant = getattr(self, 'participant', None)
    def clean(self):
        if self.id:
            if self.initial_participant != self.participant:
                raise ValidationError('Changing participant is not allowed')

