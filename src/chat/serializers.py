def serialize_room(room, is_alive):
    return {
        'id': room.id,
        'updated_at': room.updated_at.isoformat(),
        'is_alive': room.is_alive and is_alive,
        'users': [{'id': p.user.id,
                   'participant_id': p.id,
                   'name': p.user.first_name}
                  for p in room.participants.filter(is_alive=True)],
    }

def serialize_message(message):
    return {
        'id': message.id,
        'user_id': message.participant.user.id,
        'text': message.text,
        'is_received': message.is_received,
        'is_read': message.is_read,
        'tmp_id': message.tmp_id,        
        'created_at': message.created_at.isoformat(),
        'updated_at': message.updated_at.isoformat(),
    }
