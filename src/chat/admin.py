from django.contrib import admin
from django import forms

from utils import NotDeletableAdmin, NotDeletableInline
from chat.models import Room, RoomParticipant, Message
from chat.notifiers import notify_about_room_change, notify_about_message_change


class RoomParticipantInline(NotDeletableInline):
    model = RoomParticipant
    fields = ('user', 'is_alive', 'created_at', 'updated_at')

class RoomForm(forms.ModelForm):
    class Meta:
        model = Room
        exclude = ()
    def clean(self):
        participants_num = 0
        for i in range(int(self.data['participants-TOTAL_FORMS'])):
            participants_num += 1 if self.data.get('participants-%s-user' % i) else 0
        if participants_num != 2:
            raise forms.ValidationError('Number of participants should be exactly 2')

class RoomAdmin(NotDeletableAdmin):
    form = RoomForm
    list_display = ('id', 'get_users_str', 'is_alive', 'created_at', 'updated_at')
    search_fields = ('id', 'participants__user__first_name', 'participants__user__username')
    list_filter = ('is_alive',)
    inlines = (RoomParticipantInline,)
    def save_related(self, request, form, formsets, change):
        super(RoomAdmin, self).save_related(request, form, formsets, change)
        notify_about_room_change(form.instance)

admin.site.register(Room, RoomAdmin)


class MessageAdmin(NotDeletableAdmin):
    list_display = ('id', 'participant', 'text', 'is_alive', 'is_received', 'is_read', 'is_human_made', 'created_at', 'updated_at')
    search_fields = ('id', 'participant__room__id', 'participant__author__first_name', 'participant__author__username')
    list_filter = ('is_alive', 'is_received', 'is_read', 'is_human_made')
    def save_related(self, request, form, formsets, change):
        super(MessageAdmin, self).save_related(request, form, formsets, change)
        notify_about_message_change(form.instance)

admin.site.register(Message, MessageAdmin)


